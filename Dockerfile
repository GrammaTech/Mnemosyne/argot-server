FROM grammatech/sel

# TODO Remove ones that SEL has already installed.
RUN apt-get --fix-missing -eany update \
    && apt-get -y --fix-missing \
        install autoconf build-essential \
        texinfo graphviz git curl wget expect time \
        clang clang-format clang-tidy bear astyle \
        emacs-nox elpa-paredit jq \
        pkg-config libboost-iostreams-dev libboost-system-dev libboost-serialization-dev \
        python3-pip python3-venv \
        python3-dev gnupg2 libffi-dev \
        rlwrap tmux locales vim \
        libmpfr-dev libssl-dev \
        && rm -rf /var/lib/apt/lists/*
ENV LC_ALL=C.UTF-8 LANG=C.UTF-8
RUN npm install --global javascript-typescript-langserver@2.11.3 \
        && npm cache clean --force
RUN mkdir -p ~/.local/bin && python3 -m pip install pipx
# NB Change this if we ever run as not-root.
ENV HOME=/root
ENV PATH="$PATH:$HOME/.local/bin"

# Install minimal Racket (no DrRacket). Cf.
# https://github.com/jackfirth/racket-docker/blob/master/racket.Dockerfile.
ARG RACKET_VERSION=8.2
ARG RACKET_INSTALLER_URL=https://mirror.racket-lang.org/installers/${RACKET_VERSION}/racket-minimal-${RACKET_VERSION}-x86_64-linux.sh
RUN wget -O racket-install.sh -q ${RACKET_INSTALLER_URL} && \
        echo "yes\n1\n" | sh racket-install.sh --create-dir --unix-style --dest /usr/local/ && \
        rm racket-install.sh && \
        raco setup && \
        raco pkg config --set catalogs \
        "https://download.racket-lang.org/releases/${RACKET_VERSION}/catalog/" \
        "https://pkg-build.racket-lang.org/server/built/catalog/" \
        "https://pkgs.racket-lang.org" \
        "https://planet-compats.racket-lang.org"
# Install Herbie for the current user (see uwplse/herbie#403).
RUN raco pkg install --auto herbie
ENV PATH=$HOME/.local/share/racket/${RACKET_VERSION}/bin:$PATH
# Assert that Herbie is on path.
RUN which herbie

# DIG dynamic invariant generation
RUN apt-get update -eany && apt-get install -y build-essential sagemath z3 git ant
# z3-solver needs to be installed globally for sage to import it.
RUN python3 -m pip install z3-solver
RUN git clone --recurse-submodules https://gitlab.com/GrammaTech/Mnemosyne/muses/dig.git /dig

# CVC4 for Sygus
RUN wget --no-check-certificate -q \
    https://github.com/CVC4/CVC4/releases/download/1.8/cvc4-1.8-x86_64-linux-opt
RUN mv cvc4-1.8-x86_64-linux-opt /usr/local/bin/cvc4
RUN chmod 755 /usr/local/bin/cvc4

# Setup Argot-Server environment
# RUN git clone https://gitlab.com/GrammaTech/Mnemosyne/argot-server /root/quicklisp/local-projects/argot-server
WORKDIR /root/quicklisp/local-projects/argot-server

# fast-check test case generation
RUN npm install --also=dev
ENV PATH=/root/quicklisp/local-projects/argot-server/node_modules/.bin/:$PATH
ENV NODE_PATH=/root/quicklisp/local-projects/argot-server/node_modules/

# RUN git submodule update --init --recursive
# #       Use local version of the repository
COPY lens /root/quicklisp/local-projects/argot-server/lens
COPY utility /root/quicklisp/local-projects/argot-server/utility
COPY muses /root/quicklisp/local-projects/argot-server/muses
COPY test /root/quicklisp/local-projects/argot-server/test
COPY .cl-make/ /root/quicklisp/local-projects/argot-server/.cl-make
COPY .dir-locals.el \
     .ignore \
     .qlfile \
     .lisp-format \
     .pre-commit-config.yaml \
     .gitlab-ci.yml \
     Makefile \
     *.lisp \
     *.asd \
     *.py \
     /root/quicklisp/local-projects/argot-server/

# For busting cache.
ARG LISP_DEPS_VERSION

# Download binary-only libraries
ARG DOWNLOAD_USER
ARG DOWNLOAD_PASS
ARG DOWNLOAD_URL
RUN curl -u $DOWNLOAD_USER:$DOWNLOAD_PASS "$DOWNLOAD_URL/refactoring-mutations.tar.gz"|tar xzf - -C /root/quicklisp/local-projects
RUN curl -u $DOWNLOAD_USER:$DOWNLOAD_PASS "$DOWNLOAD_URL/resolve.tar.gz"|tar xzf - -C /root/quicklisp/local-projects
RUN curl -u $DOWNLOAD_USER:$DOWNLOAD_PASS "$DOWNLOAD_URL/ssr.tar.gz"|tar xzf - -C /root/quicklisp/local-projects
RUN curl -u $DOWNLOAD_USER:$DOWNLOAD_PASS "$DOWNLOAD_URL/genpatcher.tar.gz"|tar xzf - -C /root/quicklisp/local-projects

# Install the Lisp dependencies in a separate layer for easier
# debug/rebuild. (That is, if there's a bug in the Argot Server build
# it's nice not to have to rebuild all the dependencies before trying
# again.)
RUN make -j$(nproc) dependencies
RUN make -j$(nproc)
# At this point one could delete the Common Lisp cache
# (~/.cache/common-lisp) for a gain of ~40Mb, but that would make
# debugging harder.

WORKDIR /root/quicklisp/local-projects/argot-server
COPY bin /root/quicklisp/local-projects/argot-server/bin
RUN ln -s /root/quicklisp/local-projects/argot-server/argot-server /usr/bin/
ENV MNEMOSYNE_DOCKER=1

COPY bin/fork-server /usr/local/bin
EXPOSE 10003
CMD fork-server 10003 argot-server --mode stdio --log-file 2 2>&1 --allow-local=false
