(defpackage :argot-server/passthrough
  (:use :gt/full
        :lsp-server/lsp-server
        :lsp-server/protocol
        :lsp-server/logger
        :lsp-server/utilities/lsp-utilities
        :command-line-arguments
        :argot-server/readtable
        :argot-server/utility
        :argot-server/merge-results
        :argot-server/muses/muse
        :argot-server/muses/cli-muses
        :argot-server/muses/passthrough
        :argot-server/annotation-host
        :software-evolution-library/software/lisp
        :software-evolution-library/software/python
        :software-evolution-library/software/javascript
        :software-evolution-library/software/typescript
        :software-evolution-library/software/c
        :software-evolution-library/software/cpp
        :software-evolution-library/software/tree-sitter
        :jsonrpc)
  (:import-from :software-evolution-library/utility/debug
                :note)
  (:import-from :bordeaux-threads)
  (:import-from :usocket
                :unknown-error)
  (:import-from :jsonrpc/connection
                :*connection*)
  (:local-nicknames (:lp :lparallel)
                    (:tg :trivial-garbage)
                    (:it :iterate))
  (:shadow :add-hook :remove-hook :run-hooks)
  (:export :set-passthrough
           :argot-server
           :muse
           :argot-server-listen
           :pass-through
           :muse-name
           :*argot-kernel*
           :passthrough-server
           :muse-named
           :muse-named*
           :document-lang
           :document-connection
           :ensure-kernel
           :server-timeout
           :stop-server-kernel
           :muse-timeout
           :server-muses))
(in-package :argot-server/passthrough)
(in-readtable argot-readtable)

(defvar *document-meta* (dict)
  "Table of document metadata.")

(define-reset-hook clear-document-meta ()
  (synchronized (*document-meta*)
    (clrhash *document-meta*)))

(defgeneric argot-server-listen (server &rest args)
  (:method ((server server) &rest args)
    (let ((bt:*default-special-bindings*
           (append `((*server* . ,server))
                   bt:*default-special-bindings*)))
      ;; Start muses eagerly in a background thread. Randomize the
      ;; startup order to avoid letting implicit dependencies creep in.
      (bt:make-thread (lambda ()
                        (let ((lp:*kernel* (ensure-kernel server)))
                          (lp:pmap nil
                                   #'muse-start
                                   (reshuffle (server-muses server)))))
                      :name (fmt "Muse startup for ~a" server))
      ;; Silence errors from jsonrpc trying to kill threads that have
      ;; already exited.
      (ignore-some-conditions (unknown-error)
        (apply #'server-listen server args))))
  (:method :around ((server t) &rest args)
    (declare (ignore args))
    (unwind-protect
         (call-next-method)
      (mapc #'muse-stop (server-muses server)))))

(defun document-meta (uri key)
  (synchronized (*document-meta*)
    (getf (gethash uri *document-meta*) key)))

(defun (setf document-meta) (value uri key)
  (synchronized (*document-meta*)
    (setf (getf (gethash uri *document-meta*) key)
          value)))

(defplace document-lang (uri)
  (document-meta uri :lang))

(defplace document-connection (uri)
  (document-meta uri :conn))

(defun id-language (id)
  ;; NB The LSP spec has a list of IDs.
  (string-case (string-downcase id)
    ("c" 'c)
    ("cpp" 'cpp)
    ("go" 'go)
    ("java" 'java)
    ("json" 'json)
    ;; Do any clients use py? Might as well check it.
    (("python" "py") 'python)
    ;; ts/tsx in addition to the official ID for extra safety.
    (("typescript" "ts") 'typescript-ts)
    (("typescript-react" "typescriptreact" "tsx") 'typescript-tsx)
    ;; The official ID is javascript, but Eglot uses js.
    (("js" "javascript") 'javascript)
    ;; CL doesn't have an official ID, so check them all.
    (("lisp" "cl" "commonlisp" "common-lisp" "clisp") 'lisp)))

(defun disabled-muses (&optional (params (initialize-params-safe)))
  (when params
    (match (slot-value-safe params 'initializationOptions)
      ((dict "argot" (dict "disable" '("")))
       '())
      ((dict "argot" (dict "disable" (and disable (type list))))
       (mapcar #'trim-whitespace disable)))))

(defmethod muse-disabled? or ((muse muse) (params InitializeParams))
  (member (muse-name muse)
          (disabled-muses params)
          :test #'string-equal))

(defclass passthrough-server (lsp-server)
  ((muses
    :initarg :muses
    :type list
    :accessor server-muses)
   (timeout
    :initarg :timeout
    :type (or null (integer 0 *))
    :accessor muse-timeout))
  (:documentation "A server that merges its results with the results of its muse servers.")
  (:default-initargs
   :muses nil
   :timeout nil))

(defgeneric muse-named (server name)
  (:documentation "Find the muse named NAME.")
  (:method ((server passthrough-server) name)
    (find name (server-muses server)
          :key #'muse-name
          :test #'equal))
  (:method ((muse muse) name)
    (muse-named (muse-server muse) name)))

(defun muse-named* (name)
  "Get the muse named NAME from the current server."
  (muse-named *server* name))

(defmethod initialize-instance :after ((self passthrough-server) &key)
  (assert (equal (server-muses self)
                 (nub (server-muses self) :key #'muse-name))))

(defclass parallel-server (passthrough-server)
  ()
  (:documentation "A server that calls its muses in parallel."))

(defvar *servers* (tg:make-weak-hash-table :weakness :key))

(defun all-servers ()
  (synchronized (*servers*)
    (hash-table-values *servers*)))

(defclass argot-server (annotation-host parallel-server)
  ((kernel :accessor server-kernel))
  (:documentation "The class of the Argot server."))

(defmethod muse-server ((server argot-server))
  server)

(defmethod initialize-instance :after ((self argot-server) &key
                                       &aux (id (server-id self)))
  ;; Add to the global table.
  (synchronized (*servers*)
    (setf (gethash id *servers*) self))
  (tg:finalize self (lambda ()
                      (synchronized (*servers*)
                        (remhash id *servers*)))))

(defmethod slot-unbound ((self argot-server) (class t)
                         (slot-name (eql 'kernel)))
  "Lazily initialize the server's kernel."
  (ensure-kernel self))

(defun ensure-kernel (&optional (server *server*))
  (synchronized (server)
    (if (slot-boundp server 'kernel)
        (slot-value server 'kernel)
        (setf (slot-value server 'kernel)
              (make-server-kernel server)))))

(defun make-server-kernel (server)
  "Make a kernel for SERVER."
  (lp:make-kernel (max (length (server-muses server))
                       (count-cpus))
                  :name (fmt "Argot kernel ~a" (server-id server))
                  :bindings
                  (append
                   `((*standard-output* . ,*standard-output*)
                     (*error-output* . ,*error-output*)
                     (*trace-output* . ,*trace-output*)
                     (*server* . ,*server*))
                   bt:*default-special-bindings*)))

(defun stop-server-kernel (server)
  "Stop the kernel for SERVER.
Afterwards SERVER's kernel slot will be unbound."
  (let ((kernel
         (synchronized (server)
           (when (slot-boundp server 'kernel)
             (let ((kernel (slot-value server 'kernel)))
               (slot-makunbound server 'kernel)
               kernel)))))
    (stop-kernel kernel)
    server))

(defun stop-kernel (kernel)
  (let ((lp:*kernel* kernel))
    (lp:end-kernel)))

(defun stop-argot-kernels ()
  "Stop all server kernels."
  (mapc #'stop-server-kernel (all-servers)))

(uiop:register-image-dump-hook 'stop-argot-kernels)

(define-class-method "initialize" argot-server
    (params InitializeParams)
  ;; Note that the initialization options for Argot Server per se will
  ;; be merged with the initialization options from the muses.
  (let ((default-caps
         (make 'ServerCapabilities
               :textDocumentSync
               (make 'TextDocumentSyncOptions
                     :openClose t
                     :save t
                     :change |TextDocumentSyncKind.Incremental|)
               :hoverProvider t
               :completion-provider
               (make 'CompletionOptions
                     :trigger-characters
                     ;; For the benefit of argument-predictor.
                     (list "(" "\"" "'"))
               :referencesProvider
               (make 'ReferenceOptions
                     :workDoneProgress t))))
    (merge-server-capabilities
     (cons default-caps
           (mappend #'contribute-server-capabilities
                    (server-muses *server*))))))

(define-class-method "initialized" argot-server
    (params)
  (show-message (welcome-message *server*)))

(defconst +argot-server-image-tag+
  #.(assure string
      (match (read-file-into-string
              (asdf:system-relative-pathname
               :argot-server
               ".gitlab-ci.yml"))
        ((ppcre "image: mnemo/argot-server:(.*)" version)
         version))))

(defun welcome-message (&optional (server *server*))
  (let* ((disabled (disabled-muses server))
         (muses (remove-if (op (member _ disabled :test #'string-equal))
                           (server-muses server)
                           :key #'muse-name)))
    (fmt "Mnemosyne ~a with ~a muse~:p~@[:~%~{~a~^, ~}~]"
         +argot-server-image-tag+
         (length muses)
         (sort (mapcar #'muse-name muses)
               #'string-lessp))))

(defmethod jsonrpc:call-async ((muse muse) method
                               &optional params callback error-callback)
  (jsonrpc:call-async (muse-server muse) method params callback error-callback))

(defmethod jsonrpc:notify-async ((muse muse) method &optional params)
  (jsonrpc:notify-async (muse-server muse) method params))

(defmethod jsonrpc:notify ((muse muse) method &optional params)
  (jsonrpc:notify (muse-server muse) method params))

(defmethod muse-supports? ((muse t) (uri string))
  (values
   (subtypep (document-lang uri)
             (muse-lang muse))))

(defun argot-server! (server)
  (if (typep server 'argot-server) server
      (change-class server 'argot-server)))

(defun set-passthrough (server muses)
  (setf (server-muses (argot-server! server))
        (mapc (lambda (muse)
                (setf (muse-server muse) server))
              (filter #'muse-available? muses))))

(defgeneric pass-through (server muse request)
  (:documentation "Pass through REQUEST to MUSE.")
  (:method-combination standard/context)
  (:method :context (server muse request)
    (let ((*server* muse))
      (with-thread-name (:name (muse-name muse))
        (call-next-method))))
  (:method :before (server muse request)
    (declare (ignore server request))
    (muse-start muse))
  (:method :context (server (muse external-muse) request)
    (let* ((method (request-method request)))
      (note 4 "# Passing through ~a to ~a"
            (request-method request) muse)
      (and (or (equal method "initialize")
               (server-support muse method))
           (call-next-method))))
  (:method (server
            (muse muse)
            request)
    (dispatch muse request)))

(defmethod dispatch :around ((server parallel-server)
                             (request request))
  (let ((lp:*kernel* (ensure-kernel server)))
    (call-next-method)))

(def +timeout+ "timeout")

(defun handle-muse-timeout (muse connection
                            &key method
                            &aux (*connection* connection))
  "Handle MUSE's having timed out.
This is done by marking MUSE as hung and invoking `muse-timed-out' on
MUSE in a separate thread."
  (setf (muse-hung? muse) t)
  (show-message (fmt "Muse ~a deactivated due to timeout~@[ in method ~a~]"
                     (muse-name muse)
                     method)
                :type :log)
  (let ((inits
         (let ((*server* (muse-server muse)))
           ;; We may not be initialized yet!
           (ignore-errors
            (initialize-params*)))))
    (bt:make-thread
     (lambda ()
       (synchronized (muse)
         (muse-timed-out
          muse
          (lambda ()
            (handler-case
                (progn
                  (setf (muse-hung? muse)
                        nil)
                  (let ((*connection* connection))
                    (show-message
                     (fmt "Muse ~a reinitialized"
                          (muse-name muse))
                     :type :log)))
              (error ()
                (show-message (fmt "Could not restart ~a"
                                   (muse-name muse))))))
          :initialize-params inits)))
     :name (fmt "Timeout handler for ~a"
                muse))))

(defun map-parallel (fn seq &key (timeout 60)
                              default
                              timeout-default
                              (handler (constantly nil)))

  "Map FN across SEQ, a sequence, with a timeout, possibly in parallel.

The mapping is done in parallel only if `lparallel:*kernel*` is bound,
otherwise sequentially.

TIMEOUT is per-invocation, not overall. (If it were overall slow
inputs could use up all the available threads.)

Return a list of the same length as SEQ, with DEFAULT in place of an
results that timed out.

As a second value, return a list of the inputs that timed out (with
TIMEOUT-DEFAULT in place of arguments that succeeded)."
  (let* ((fn (ensure-function fn))
         (fn (lambda (item)
               (handler-case
                   (if timeout
                       (with-deadline (timeout)
                         (funcall fn item))
                       (funcall fn item))
                 (timeout ()
                   (prog1 +timeout+
                     (funcall handler item))))))
         (raw-results
          (if (not (boundp 'lp:*kernel*))
              (cl:map 'list fn seq)
              (lp:task-handler-bind ((error #'lp:invoke-transfer-error))
                (lp:pmap 'list fn :parts (max 1 (length seq)) seq)))))
    (iter (for result in raw-results)
          (for muse in (coerce seq 'list))
          (receive (result failed-input)
              (if (eq result +timeout+)
                  (values default muse)
                  (values result timeout-default))
              (collect result into results)
              (collect failed-input into failed-inputs))
          (finally (return (values results failed-inputs))))))

(defmethod dispatch :around ((server passthrough-server)
                             (request request))
  "Merge the results from SERVER with its muse servers."
  (nest
   (with-thread-name (:name "Passthru"))
   (with-slots (muses) server)
   (let* ((result1
            ;; Don't log the internal response.
            (let (*response-log*)
              (call-next-method)))
          (method (request-method request))
          (muses
            ;; Remove hung muses, disabled muses, muses that don't
            ;; support the document, and muses that don't want Argot
            ;; requests.
            (iter (it:with params = (initialize-params-safe))
                  (it:with request-params = (request-params request))
                  (it:with document =
                           (ignore-some-conditions (type-error)
                             (href request-params "textDocument" "uri")))
                  (for muse in muses)
                  (unless (or (muse-disabled? muse params)
                              (not (muse-supports? muse document))
                              (muse-hung? muse)
                              (and (string^= "argot/" method)
                                   (not (muse-speaks-argot? muse)))
                              (not (muse-accepts-command-p muse
                                                           request-params)))
                    (collect muse))))
          (more-results
           (with-thread-name (:name "Gather responses")
             (map-parallel
              ;; Propagate the connection for muses
              ;; that need to perform asynchronous
              ;; replies.
              (dynamic-closure
               '(*connection* *trace-output*)
               (lambda (muse)
                 (ignore-some-conditions (jsonrpc-error)
                   (pass-through server muse request))))
              muses
              :handler (dynamic-closure
                        '(*server*)
                        (rcurry #'handle-muse-timeout
                                (bound-value '*connection*)
                                :method method))
              :timeout (muse-timeout server))))
          (final
           (with-thread-name (:name "Combine responses")
             (reduce #'merge-results
                     more-results
                     :initial-value result1))))
     ;; Do log the final response.
     (response-log final)
     (when (request-id request)
       final))))

(defmethod dispatch :before ((server passthrough-server) (request request))
  "Save the language ID of the document."
  (when (equal "textDocument/didOpen" (request-method request))
    (let* ((params (request-params request))
           (uri (href params "textDocument" "uri"))
           (id (href params "textDocument" "languageId")))
      (synchronized ('*document-meta*)
        (setf (document-lang uri)
              (or (and id (id-language id))
                  (and (stringp uri)
                       (guess-language uri))))
        (when (boundp 'jsonrpc/connection:*connection*)
          (setf (document-connection uri)
                jsonrpc/connection:*connection*))))))
