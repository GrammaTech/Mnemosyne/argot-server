(defsystem "argot-server"
    :name "argot-server"
    :author "GrammaTech"
    :licence "MIT"
    :description "LSP Server for the Argot program language extension language"
    :long-description "Yada"
    :depends-on (:argot-server/server)
    :class :package-inferred-system
    :build-operation "asdf:program-op"
    :build-pathname "argot-server"
    :entry-point "argot-server/server::run-server"
    :in-order-to ((test-op (load-op "argot-server/test")))
    :perform (test-op (o c) (symbol-call :argot-server/test '#:run-batch)))

(register-system-packages "cl-lsp" '(:cl-lsp/protocol :cl-lsp/main :cl-lsp/slime))
(register-system-packages "fxml" '(:fxml-dom :fxml.dom))
(register-system-packages "lparallel" '(:lparallel.queue))
(register-system-packages "lsp-server" '(:lsp-server/logger))
(register-system-packages "lsp-server" '(:lsp-server/lsp-server))
(register-system-packages "lsp-server" '(:lsp-server/protocol))
(register-system-packages "lsp-server" '(:lsp-server/protocol-util))
(register-system-packages "lsp-server/lem-base" '(:lsp-server.lem-base))
(register-system-packages "lsp-server/utilities" '(:lsp-server/utilities/lsp-utilities))
(register-system-packages "lsp-server/utilities" '(:lsp-server/utilities/worker-thread))
(register-system-packages "trivia" '(:trivia.fail))

(when (uiop:getenv "INHIBIT_REFACTORING_MUTATIONS")
  (pushnew :inhibit-refactoring-mutations *features*))
