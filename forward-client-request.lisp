(defpackage :argot-server/forward-client-request
  (:use :gt/full
        :argot-server/passthrough
        :argot-server/muses/muse
        :lsp-server
        :lsp-server/protocol
        :argot
        :argot-server/readtable
        :argot-server/utility
        :jsonrpc)
  (:import-from :jsonrpc/connection
                :*connection*
                :connection)
  (:import-from :jsonrpc/errors
                :jsonrpc-callback-error)
  (:import-from :jsonrpc/class
                :server-client-connections
                :notify-to)
  (:local-nicknames (:lp :lparallel))
  (:export :make-forwarded-client-request
           :forward-client-request))
(in-package :argot-server/forward-client-request)
(in-readtable argot-readtable)

(defun make-forwarded-client-request* (method params-keyword params-class
                                       &key uri line column)
  (make '|ForwardedClientRequest|
        :method-request method
        :client-request-params
        (make '|ClientRequestParams|
              params-keyword
              (apply #'make-instance
                     params-class
                     `(:text-document ,(make '|TextDocumentIdentifier|
                                            :uri (or uri ""))
                       :position ,(make '|Position|
                                        :line (or line 0)
                                        :character (or column 0))
                       ,@(when (eql params-class '|ReferenceParams|)
                           (list :context (make '|ReferenceContext|
                                                :include-declaration t))))))))

(defgeneric make-forwarded-client-request (method
                                           &rest rest
                                           &key uri line column)
  (:method ((method (eql :textDocument/definition))
            &rest rest &key uri line column)
    (declare (ignore uri line column))
    (apply #'make-forwarded-client-request* "textDocument/definition"
           :definition-params '|DefinitionParams| rest))
  (:method ((method (eql :textDocument/references))
            &rest rest &key uri line column)
    (declare (ignore uri line column))
    (apply #'make-forwarded-client-request* "textDocument/references"
           :reference-params '|ReferenceParams| rest))
  (:method ((method (eql :textDocument/documentHighlight))
            &rest rest &key uri line column)
    (declare (ignore uri line column))
    (apply #'make-forwarded-client-request* "textDocument/documentHighlight"
           :document-highlight-params '|DocumentHighlightParams| rest)))

(defgeneric forward-client-request (uri request)
  (:method (uri (request |ForwardedClientRequest|))
    (jsonrpc:call *server*
                  "argot/forwardClientRequest"
                  (convert 'hash-table request))))
