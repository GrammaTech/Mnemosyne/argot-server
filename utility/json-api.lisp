(defpackage :argot-server/utility/json-api
  (:use :gt/full :drakma :quri)
  (:shadowing-import-from :quri :url-encode)
  (:import-from :yason)
  (:export :api :api-endpoint
           :endpoint-path
           :api-error
           :api-call)
  (:documentation "High-level implementation of an HTTP API using JSON
  payloads."))
(in-package :argot-server/utility/json-api)

(deftype http-status ()
  "Type for an HTTP status, an an integer."
  '(integer 100 999))

(deftype tcp-port ()
  "Type for a possible TCP port."
  ;; Should we exclude IANA ephemeral ports?
  '(integer 0 (65536)))

(deftype http-scheme ()
  "Type for a possible HTTP scheme, as a keyword."
  '(member :http :https))

(deftype http-method ()
  "Type for the relevant subset of HTTP methods, as keywords."
  '(member :options :get :head :post :put :patch))

(defconst +http-port+ 80
  "The default HTTP port.")

(defconst +https-port+ 443
  "The default HTTPS port.")

(defclass api ()
  ((scheme :initarg :scheme :type http-scheme :reader api-scheme)
   (host :initarg :host :type string :reader api-host)
   (port :initarg :port :type tcp-port :reader api-port)
   (parser :initarg :parser :type function :reader api-parser)
   (encoder :initarg :encoder :type function :reader api-encoder))
  (:documentation "A JSON API.")
  (:default-initargs
   :parser #'yason:parse
   :encoder (lambda (x)
              (if (null x) ""
                  (with-output-to-string (s)
                    (yason:encode x s))))))

(defmethod initialize-instance :after ((api api) &key scheme port)
  (assert (or scheme port))
  (when (and scheme (not port))
    (ecase-of http-scheme scheme
      (:http +http-port+)
      (:https +https-port+)))
  (when (and port (not scheme))
    (ecase port
      (#.+http-port+ :http)
      (#.+https-port+ :https))))

(defmethod print-object ((self api) stream)
  (print-unreadable-object (self stream :type t)
    (princ (quri:make-uri :scheme (api-scheme self)
                          :host (api-host self)
                          :port (api-port self))
           stream)))

(defclass api-endpoint ()
  ((method :initarg :method
           :type http-method
           :reader endpoint-method)
   (template :initarg :path
             :type list
             :reader endpoint-template))
  (:default-initargs
   :method :get)
  (:documentation "A JSON API endpoint.

The endpoint has a path, which is a list of strings or keywords.
Keywords are named gaps in the path that can be filled in with
arguments later."))

(defmethod print-object ((self api-endpoint) stream)
  (print-unreadable-object (self stream :type t)
    (format stream "~a ~{~a~^/~}"
            (endpoint-method self)
            (endpoint-template self))))

(defgeneric endpoint-path (endpoint &rest keywords &key &allow-other-keys)
  (:documentation "Fill in the gaps in ENDPOINT's path by looking them up in KEYWORDS.")
  (:method :around ((e api-endpoint) &rest keywords)
    (declare (ignore keywords))
    (fmt "/~{~a~^/~}" (call-next-method)))
  (:method ((e api-endpoint) &rest keywords)
    (let ((no-match "no-match"))
      (iter (for component in (endpoint-template e))
            (if (keywordp component)
                (let ((match (getf keywords component no-match)))
                  (if (eq match no-match)
                      (error "Missing component: ~a" component)
                      (collect (url-encode match))))
                (collect component))))))

(defcondition api-error (error)
  ((api :initarg :api)
   (endpoint :initarg :endpoint)
   (status :initarg :status :type http-status)
   (content :initarg :content :type string))
  (:documentation "An API error.")
  (:report (lambda (c s)
             (with-slots (status content) c
               (format s "API error (code ~a): ~a"
                       status content)))))

(defgeneric api-call (api endpoint content &key headers query args)
  (:method-combination standard/context)
  (:method (api endpoint content &key headers query args)
    (mvlet* ((drakma:*text-content-types*
              (cons '("application" . "json")
                    drakma:*text-content-types*))
             (uri (quri:make-uri :scheme (api-scheme api)
                                 :host (api-host api)
                                 :port (api-port api)
                                 :path (apply #'endpoint-path endpoint args)
                                 :query query))
             (uri-string (princ-to-string uri))
             (response status
              (drakma:http-request uri-string
                                   :method (endpoint-method endpoint)
                                   :content-type "application/json"
                                   :additional-headers
                                   (append '((:accept . "application/json"))
                                           headers)
                                   :content (assure string
                                              (funcall (api-encoder api)
                                                       content)))))
      (if (<= 200 status 299)
          (funcall (api-parser api) response)
          (error 'api-error
                 :content response
                 :status status
                 :api api
                 :endpoint endpoint)))))
