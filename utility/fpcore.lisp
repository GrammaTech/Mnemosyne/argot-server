(uiop:define-package :argot-server/utility/fpcore
    (:use :gt/full
          :software-evolution-library/software/tree-sitter
          :software-evolution-library/software/parseable
          :argot-server/lens/fpcore)
  (:shadow :op)
  (:shadowing-import-from :serapeum :~> :collecting)
  (:import-from :trivia.fail :fail)
  (:local-nicknames (:l :fresnel/lens)
                    (:fpcore :argot-server/lens/fpcore-builtins)
                    (:py :argot-server/lens/python))
  (:export
    :fpcore :fpcore-expr :fpcore-symbols :fpcore-properties :copy-fpcore
    :with-fpcore-io
    :fpcore->infix-string
    :extract-fpcore-math-nodes
    :fpcore-expression-symbols
    :fpcore-improvement)
  (:documentation "Representation for the FPCore format used by
  Herbie. This is the target for FPCore lens."))
(in-package :argot-server/utility/fpcore)

(in-readtable :curry-compose-reader-macros)

(defconstructor fpcore
  (symbols list)
  (properties list)
  (expr t))

(defmethod equal? ((x fpcore) (y fpcore))
  (eql :equal
       (compare-slots x y
                      #'fpcore-symbols
                      #'fpcore-properties
                      #'fpcore-expr)))

(defmethod convert ((to (eql 'fpcore))
                    (list list)
                    &key)
  (ematch list
    ((list* (eql 'fpcore:|FPCore|) syms properties-and-expr)
     (multiple-value-bind (properties exprs)
         (parse-leading-keywords properties-and-expr)
       (fpcore syms properties (only-elt exprs))))))

(defmethod convert ((to (eql 'list))
                    (fpcore fpcore)
                    &key)
  (ematch fpcore
    ((fpcore symbols props expr)
     `(fpcore:|FPCore| ,symbols ,@props ,expr))))

(defmethod convert ((to (eql 'string))
                    (fpcore fpcore)
                    &key)
  (with-fpcore-io ()
    (fmt "~s" (convert 'list fpcore))))

(defun fpcore-expression-symbols (expr)
  "Extract symbols from EXPR, an FPCore expression."
  (nub (filter #'fpcore-variable? (flatten expr))))

(defmethod convert ((to (eql 'fpcore))
                    (ast tree-sitter-ast)
                    &key)
  nil)

(defmethod convert ((to (eql 'fpcore))
                    (ast python-ast)
                    &key)
  (when-let ((expr (l:get (l:compose (fpcore-lens)
                                     (py:translator))
                          ast)))
    (let ((syms (fpcore-expression-symbols expr)))
      (fpcore syms () expr))))

(defmethod convert ((to (eql 'fpcore))
                    (string string)
                    &key)
  (convert 'fpcore
           (with-fpcore-io ()
             (read-from-string string))))

(defmethod convert ((to (eql 'python-ast)) (fpcore fpcore) &key)
  (l:create (l:compose (fpcore-lens)
                       (py:translator))
            (fpcore-expr fpcore)))

(-> fpcore->infix-string
    (fpcore &key (:stream t))
    (values string &optional))
(defun fpcore->infix-string (fpcore &key stream)
  "Convert an FPCore expression back into an infix expression."
  (with-fpcore-io ()
    (source-text (l:create (fpcore-lens)
                           (fpcore-expr fpcore))
                 :stream stream)))

(def +symbol-package+ (find-package :argot-server/lens/fpcore-symbols))

(defgeneric extract-fpcore-math-nodes (ast)
  (:documentation "Return all nodes of AST that can be parsed into FPCore.")
  (:method ((ast tree-sitter-ast))
    (collecting
     (labels ((rec (ast)
                (when (typep ast 'ast)
                  (if-let* ((fpcore (convert 'fpcore ast))
                            (expr (fpcore-expr fpcore)))
                    (and (consp (fpcore-expr fpcore))
                         ;; Don't offer to optimize nodes that are
                         ;; just function calls on variables.
                         (notevery
                          (lambda (leaf)
                            (and (symbolp leaf)
                                 (equal (symbol-package leaf)
                                        +symbol-package+)))
                          (flatten expr))
                         (collect ast))
                    (mapc #'rec (children ast))))))
       (rec ast)))))

(defun fpcore-improvement (fpcore)
  "Return the before and after average error."
  (let* ((props (fpcore-properties fpcore))
         (inputs (getf props :|herbie-error-input|))
         (outputs (getf props :|herbie-error-output|)))
    ;; From an email exchange: "The web demo highlights the "Average"
    ;; error meaning the average pointwise bits error across the test
    ;; set (which is about 8k points by default). The points are
    ;; uniformly chosen over floating-point values (as in uniform bit
    ;; representations) that are finite and satisfy the preconditions,
    ;; and where the correct (error-free) output of the given
    ;; expression rounds to a finite floating-point value and does not
    ;; trigger any domain errors. If your expression is something like
    ;; sqrt(x)*sqrt(x), the output expression could be valid on points
    ;; the original wasn't, so x would be a valid output with no
    ;; error."
    (flet ((err (xs) (assocadr 8000 xs))
           (round-bits (bits) (/ (round (* bits 10)) 10.0)))
      ;; We want to present this the same way the Herbie web UI does
      ;; so calculate it the same way.
      (values (round-bits (err inputs))
              (round-bits (err outputs))))))
