(defpackage :argot-server/utility/gtest
  (:use :gt/full
        :software-evolution-library/software/tree-sitter
        :software-evolution-library/software/cpp-project
        :argot-server/utility/testing
        :argot-server/lens/ast
        :argot
        :lsp-server/protocol
        :argot/readtable
        :cmd)
  (:import-from :software-evolution-library
                :from-string
                :from-file
                :genome
                :software)
  (:import-from :software-evolution-library/software/parseable
                :ast-annotations
                :ast-source-ranges
                :source-text
                :scopes)
  (:import-from :software-evolution-library/software/project
                :collect-evolve-files
                :project-dir)
  (:import-from :trivia.fail :fail)
  (:import-from :software-evolution-library/components/file
                :original-path)
  (:import-from :argot-server/ast-utils
                :node-source-range)
  (:import-from :software-evolution-library/command-line
                :create-software)
  (:local-nicknames (:xunit :argot-server/utility/xunit)
                    (:l :fresnel/lens)
                    (:f :fresnel/fresnel))
  (:export :example-lens
           :gtest
           :gtest-case
           :gtest-suite
           :gtest-assert
           :gtest-expect
           :gtest-fixture
           :extract-example))
(in-package :argot-server/utility/gtest)
(in-readtable argot-readtable)

(defclass gtest (test-framework)
  ()
  (:default-initargs
   :name "googletest"
   :test-framework-case-class 'gtest-case
   :test-framework-suite-class 'gtest-suite))

(defclass gtest-case (unit-test-case parsed-test)
  ()
  (:documentation "A single test case.

NB Googletest used to call test suites test cases."))

(defclass gtest-suite (test-suite)
  ()
  (:default-initargs :nestable nil))

(defclass gtest-assert (fatal-test-condition parsed-test)
  ())

(defclass gtest-expect (non-fatal-test-condition parsed-test)
  ())

(defclass gtest-fixture (gtest-suite)
  ())

(defconst +test-definers+
  '("TEST" "TEST_F" "TEST_P" "TYPED_TEST_P"))

(defmethod tests-for ((framework gtest) (sw cpp))
  ;; XXX Hack to work around slow parsing of certain files: only
  ;; consider a sw if there is a string match to a test definer.
  (match (slot-value sw 'genome)
    ((and s (type string))
     (unless (some (op (string*= _ s)) +test-definers+)
       (return-from tests-for nil))))
  (let ((ranges (ast-source-ranges sw)))
    (iter (for node in-tree (genome sw))
          (when-let (test (node-google-test sw node :ranges ranges))
            (collect test)))))

(def +vendor-scanner+
  ;; Cf. https://github.com/github/linguist/blob/master/lib/linguist/vendor.yml
  (create-scanner
   `(:alternation
     ,@(mapcar (op `(:regex ,_))
               '("(3rd|[Tt]hird)[-_]?[Pp]arty/"
                 "(^|/)vendors?/"
                 "(^|/)[Ee]xtern(als?)?/"
                 "(^|/)[Vv]+endor/")))))

(defmethod tests-for ((framework gtest) (p cpp-project))
  ;; TODO Ideally we would put the tests in the order they would be
  ;; run in; for gtest that is nominally undefined.
  (let ((files (sort (collect-evolve-files p) #'string<
                     :key [#'namestring #'car])))
    (iter (for (path . file) in files)
          (unless (scan +vendor-scanner+ (namestring path))
            (appending (tests-for framework file))))))

(defun run-gtests (&key only verbose project)
  (with-temporary-file (:pathname tmp)
    (funcall (if verbose #'cmd #'cmd!)
             "env" (remove nil
                           (list (fmt "GTEST_OUTPUT=xml:~a" tmp)
                                 (and only
                                      (string+
                                       "GTEST_FILTER="
                                       (mapconcat #'gtest-filter only ":")))))
             "make check"
             :in (project-dir project))
    (xunit:parse (pathname tmp))))

(defgeneric gtest-filter (gtest)
  (:method ((test gtest-case))
    (fmt "~{~a~^.~}" (test-path test)))
  (:method ((test gtest-suite))
    (fmt "~a.*" (only-elt (test-path test)))))

(defmethod run-tests-for ((framework gtest) (p cpp-project)
                          &key verbose)
  (run-gtests :verbose verbose :project p))

(defmethod run-tests ((framework gtest) (test gtest-case)
                      &key project)
  (run-tests framework (list test) :project project))

(defmethod run-tests ((framework gtest) (tests list)
                      &key verbose project)
  (run-gtests :only tests
              :verbose verbose
              :project project))

(defun includes-gtest-header? (sw)
  (find-if (lambda (node)
             (match node
               ((cpp-preproc-include
                 :cpp-path
                 (or (cpp-system-lib-string
                      :text "<gtest/gtest.h>")
                     (cpp-string-literal
                      :text "\"gtest/gtest.h\""))))))
           (genome sw)))

(defun node-google-test (sw node &key (ranges (ast-source-ranges sw)))
  (match node
    ((cpp-function-definition
      :cpp-declarator
      (cpp-function-declarator
       :cpp-declarator id
       :cpp-parameters
       (cpp-parameter-list :children (list test-group test-name))))
     (let ((id (source-text id)))
       (unless (member id +test-definers+ :test #'equal)
         (trivia.fail:fail))
       (let* ((test-name (source-text test-name))
              (test-group (source-text test-group))
              (disabled?
               (some {string^= "DISABLED_"}
                     (list test-name test-group))))
         (make 'gtest-case
               :software sw
               :object node
               :target
               (make 'Location
                     :uri (string+ "file://" (original-path sw))
                     :range (convert 'Range (assocdr node ranges)))
               :path (list test-group test-name)
               :disabled disabled?))))))

(defmethod test-case-conditions ((test gtest-case))
  (nest
   (when-match (cpp-function-definition
                :cpp-body body)
               (test-object test))
   (iter (for node in-tree body))
   (when-match (cpp-call-expression :cpp-function id)
               node)
   (let* ((id (source-text id))
          (sw (test-software test))))
   (when-let (class
              (cond
                ((string^= "EXPECT_" id)
                 (find-class 'gtest-expect))
                ((string^= "ASSERT_" id)
                 (find-class 'gtest-assert)))))
   (collect)
   (make class
         :software sw
         :object node
         :path (test-path test))))

(def +assertion-alist+
  '(("EQ" . =)
    ("NE" . /=)
    ("TRUE" . true)
    ("FALSE" . not)
    ("LT" . <)
    ("GT" . >)
    ("LE" . <=)
    ("GE" . >=)
    ;; C strings, not string objects.
    ("STREQ" . string=)
    ("STRNE" . string/=)
    ("STRCASEEQ" . string-equal)
    ("STRCASENE" . string-not-equal)))

(defun example-lens ()
  (l:or (assertion-lens)
        (literal-lens)
        (nullptr-lens)
        (identifier-lens)
        (binary-lens)
        (call-lens)))

(defun binary-lens ()
  (l:match ((left (example-lens))
            (operator (terminal-symbol-lens 'cpp))
            (right (example-lens)))
    (make 'cpp-binary-expression
          :cpp-left left
          :cpp-operator operator
          :cpp-right right)
    (list operator left right)))

(defun char-literal-lens ()
  (l:match ((c (l:bij (lambda (c)
                        (and (stringp c)
                             (scan "'.'" c)
                             (aref c 1)))
                      (lambda (a)
                        (and (characterp a)
                             (fmt "'~a'" a))))))
    (make 'cpp-char-literal :text c)
    c))

(defun number-literal-lens ()
  (l:match ((n (f:read-print-lens 'number)))
    (make 'cpp-number-literal :text n)
    n))

(defun boolean-literal-lens ()
  (l:match ()
    (make 'cpp-true :text "true")
    t

    (make 'cpp-false :text "false")
    :false))

(defun string-literal-lens ()
  (l:match ((string (quoted-string-lens)))
    (make 'cpp-string-literal :text string)
    string))

(defun quoted-string-lens ()
  (l:bij (lambda (c)
           (and (stringp c)
                (string^= "\"" c)
                (string$= "\"" c)
                (slice c 1 -1)))
         (lambda (a)
           (and (stringp a)
                (concatenate 'string "\"" a "\"")))))

(defun literal-lens ()
  (l:or! (number-literal-lens)
         (boolean-literal-lens)
         (char-literal-lens)
         (string-literal-lens)))

(defun nullptr-lens ()
  (l:match ()
    (make 'cpp-nullptr :text "nullptr")
    :nullptr))

(defun identifier-lens (&key (name-lens (f:keyword-lens)))
  (l:match ((name name-lens))
    (make 'cpp-identifier :text name)
    name))

(defun field-lens ()
  (l:match ((field-id (f:keyword-lens))
            (id (identifier-lens)))
    (make 'cpp-field-expression
          :cpp-field (make 'cpp-field-identifier
                           :text field-id)
          :cpp-argument id)
    (list :dot field-id id)))

(defun aux-call-lens (fn-lens)
  (l:match ((id fn-lens)
            (args
             (l:mapcar
              (l:or (identifier-lens)
                    (literal-lens)
                    (call-lens)))))
    (make 'cpp-call-expression
          :cpp-function id
          :cpp-arguments
          ;; TODO Synthesize interleaved text.
          (make 'cpp-argument-list :children args))
    (list* id args)))

(defun call-lens ()
  (aux-call-lens (l:or! (identifier-lens) (field-lens))))

(defun assertion-name-lens ()
  (l:lens
   (lambda (c)
     (when (and (stringp c)
                (scan "^(?:ASSERT|EXPECT)_" c))
       (assocdr (drop 7 c) +assertion-alist+ :test #'equal)))
   (lambda (a c)
     (when-let (suffix (rassocar a +assertion-alist+))
       (string+ (or (and c (take 7 c)) "ASSERT_")
                suffix)))
   (lambda (a)
     (when-let ((suffix (rassocar a +assertion-alist+)))
       (string+ "ASSERT_" suffix)))))

(defun assertion-lens ()
  (aux-call-lens (identifier-lens :name-lens (assertion-name-lens))))
