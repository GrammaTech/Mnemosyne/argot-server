(defpackage :argot-server/utility/godbolt
  (:documentation "Wrapper for the Compiler Explorer API.")
  (:use :gt/full
        :argot-server/utility/json-api
        :software-evolution-library/software/tree-sitter)
  (:shadow :compile)
  (:export :*godbolt*
           :id
           :language
           :compiler
           :languages
           :compilers
           :libraries
           :compile
           :source
           :text
           :file
           :asm-lines
           :asm-source
           :asm-text
           :source-line
           :asm-source
           :output-asm
           :name
           :compilation-failed))
(in-package :argot-server/utility/godbolt)
(in-readtable :curry-compose-reader-macros)

(defvar-unbound *godbolt*
  "The current Compiler Explorer API instance.")

(defcondition compilation-failed (simple-error)
  ())

(defgeneric id (x)
  (:documentation "The ID of X, as a string.")
  (:method ((x string)) x))

(defclass thing ()
  ((id :initarg :id :reader id :type string :reader id)
   (name :initarg :name :reader name :type string :reader name))
  (:documentation "Base class for a thing with an ID and a name."))

(defmethod print-object ((thing thing) stream)
  (print-unreadable-object (thing stream :type t)
    (format stream "~a" (name thing))))

(defclass language (thing)
  ()
  (:documentation "A language supported by Compiler Explorer."))

(-> language (hash-table) (values language &optional))
(defun language (table)
  "Convert TABLE, a JSON-derived hash table, to a `language' instance."
  (ematch table
    ((dict "id" id "name" name)
     (make 'language :id id :name name))))

(defclass compiler (thing)
  ((lang :initarg :lang :reader lang))
  (:documentation "A compiler supported by Compiler Explorer."))

(-> compiler (hash-table) (values compiler &optional))
(defun compiler (table)
  "Convert TABLE, a JSON-derived hash table, to a `compiler' instance."
  (ematch table
    ((dict "id" id "name" name "lang" lang)
     (make 'compiler
           :id id
           :name name
           :lang lang))))

(defclass <source> ()
  ((file :initarg :file :type (or null string) :reader source-file)
   (line :initarg :line :type (integer 1 *) :reader source-line))
  (:documentation "Address a specific line in a file."))

(defmethod print-object ((source <source>) stream)
  (print-unreadable-object (source stream :type t)
    (with-slots (file line) source
      (format stream "~a:~a" file line))))
(deftype source ()
  "Address of a specific line in a file, or nil."
  '(or null <source>))

(-> source ((or null hash-table)) (values source &optional))
(defun source (x)
  "Convert X to a `<source>' instance, or null if X is null."
  (ematch x
    ((type null) nil)
    ((dict "file" file "line" line)
     (make '<source> :file file :line line))))

(defclass asm ()
  ((source :initarg :source :type source :reader asm-source)
   (text :initarg :text :type string :reader asm-text))
  (:documentation "A line of assembly."))

(defmethod print-object ((asm asm) stream)
  (with-slots (text) asm
    (if *print-escape*
        (print-unreadable-object (asm stream :type t)
          (format stream "~a" text))
        (format stream "~a" text))))

(-> asm (hash-table) (values asm &optional))
(defun asm (table)
  "Convert TABLE, a JSON-derived hash table, to an `asm' instance."
  (ematch table
    ((dict "text" "<Compilation failed>")
     (error 'compilation-failed
            :format-control (formatter "Compilation failed")))
    ((dict "text" text "source" source)
     (make 'asm
           :text text
           :source (source source)))))

(defclass assembly-output ()
  ((asm :initarg :asm :type list :reader output-asm)
   (asm-size :initarg :asm-size :type (integer 0 *))
   (compilation-options :initarg :compilation-options :type list))
  (:documentation "A list of lines of assembly, with metadata."))

(-> assembly-output (hash-table) (values assembly-output &optional))
(defun assembly-output (table)
  "Convert TABLE to `assembly-output'."
  (ematch table
    ((dict "asm" asm "asmSize" asm-size "compilationOptions" opts)
     (make 'assembly-output
           :asm (mapcar #'asm asm)
           :asm-size asm-size
           :compilation-options opts))))

(defmethod print-object ((obj assembly-output) stream)
  (with-slots (asm asm-size) obj
    (if *print-escape*
        (print-unreadable-object (obj stream :type t)
          (format stream "~a" asm-size))
        (iter (for (line . more) on asm)
              (write line :stream stream)
              (when more
                (terpri stream))))))

(def api-languages (make 'api-endpoint :path '("api" "languages")))

(defun languages ()
  "List available languages."
  (mapcar #'language (api-call *godbolt* api-languages nil)))

(def api-compilers (make 'api-endpoint :path '("api" "compilers")))

(defun compilers ()
  "List available compilers."
  (mapcar #'compiler (api-call *godbolt* api-compilers nil)))

(def api-language-compilers
  (make 'api-endpoint :path '("api" "compilers" :language-id)))

(def api-language-libraries
  (make 'api-endpoint :path '("api" "libraries" :language-id)))

;;; TODO
(defgeneric libraries (lang)
  (:documentation "List available libraries.")
  (:method ((lang language))
    (api-call *godbolt* api-language-libraries nil
              :args (list :language-id (id lang)))))

(def api-compile
  (make 'api-endpoint
        :method :post
        :path '("api" "compiler" :compiler-id "compile")))

(defgeneric compile (compiler source &key user-arguments)
  (:documentation
   "Compile SOURCE with COMPILER, passing USER-ARGUMENTS.")
  (:method ((compiler compiler) source &key (user-arguments ""))
    (assembly-output
     (api-call *godbolt* api-compile
               (dict "options" (dict "userArguments" user-arguments)
                     "source" source)
               :args (list :compiler-id (id compiler))))))
