(defpackage :argot-server/utility/jest
  (:use :gt/full
        :software-evolution-library/software/tree-sitter
        :software-evolution-library/software/javascript
        :software-evolution-library/software/typescript
        :software-evolution-library/software/javascript-project
        :argot-server/utility/testing
        :argot-server/lens/ast
        :argot
        :lsp-server/protocol
        :argot/readtable
        :cmd)
  ;; TODO: decide which of these import-froms are actually needed.
  (:import-from :software-evolution-library
                :from-string
                :from-file
                :genome
                :software)
  (:import-from :software-evolution-library/software/parseable
                :ast-annotations
                :ast-source-ranges
                :collect-if
                :get-parent-asts*
                :source-text
                :scopes)
  (:import-from :software-evolution-library/software/project
                :collect-evolve-files
                :project-dir
                :other-files)
  (:import-from :trivia.fail :fail)
  (:import-from :software-evolution-library/components/file
                :original-path)
  (:import-from :argot-server/ast-utils
                :node-source-range)
  (:import-from :software-evolution-library/command-line
                :create-software)
  (:local-nicknames (:xunit :argot-server/utility/xunit)
                    (:l :fresnel/lens)
                    (:f :fresnel/fresnel))
  (:export :jest
           :has-jest-dependency?))
(in-package :argot-server/utility/jest)
(in-readtable argot-readtable)
;;; TODO: jest has code coverage options. See if it makes
;;;       sense to integrate this somehow.

;;; TODO: look into jest.config.js files.
(defclass jest (test-framework)
  ()
  (:default-initargs
   :name "jest"
   :test-framework-case-class 'jest-case
   :test-framework-suite-class 'jest-suite))

(defclass jest-case (unit-test-case parsed-test)
  ()
  (:documentation "A single test case."))

(defclass jest-suite (test-suite)
  ()
  (:default-initargs :nestable t))

(defclass jest-assert (fatal-test-condition parsed-test)
  ())

(defconst +suite-definers+
  '("describe" "describe.each"))

(defconst +exclusive-suite-definers+
  '("describe.only" "describe.only.each"))

(defconst +skip-suite-definers+
  '("describe.skip" "describe.skip.each"))

(def +suite-scanner+
  (create-scanner
   ;; NOTE: Check if it matches without a period following it
   ;;       to ensure that it's not a partial match.
   (fmt "(~{~a(?!\\.)~^|~})"
        (mapcar (op (replace-all _ "." "\\."))
                (append +suite-definers+
                        +exclusive-suite-definers+
                        +skip-suite-definers+)))))

(defconst +test-definers+
  '("test" "it"
    "test.concurrent" "it.concurrent"
    "test.concurrent.each" "it.concurrent.each"
    "test.each" "it.each"))

(defconst +exclusive-test-definers+
  '("test.only" "it.only" "fit"
    "test.concurrent.only.each" "it.concurrent.only.each"
    "test.only.each" "fit.each" "it.only.each" "fit.each"))

(defconst +skip-test-definers+
  '("test.skip" "it.skip" "xit" "xtest"
    "test.skip.each" "it.skip.each" "xit.each" "xtest.each"
    "test.todo" "it.todo"))

(def +test-scanner+
  (create-scanner
   ;; NOTE: Check if it matches without a period following it
   ;;       to ensure that it's not a partial match.
   (fmt "(~{~a(?!\\.)~^|~})"
        (mapcar (op (replace-all _ "." "\\."))
                (append +test-definers+
                        +exclusive-test-definers+
                        +skip-test-definers+)))))

(-> match-jest-ast (tree-sitter-ast function)
    (values (or string null) (or tree-sitter-ast null) &optional))
(defun match-jest-ast (ast scanner)
  "Return the function AST and the function name as values if AST is a jest AST."
  (match ast
    ((ecma-call-expression
      :ecma-arguments (ecma-arguments)
      :ecma-function (and function-ast
                          (or (ecma-call-expression)
                              (identifier-ast)
                              (ecma-member-expression))))
     (if-let ((function-name
               (register-groups-bind (function-name)
                                     (scanner (source-text function-ast))
                                     function-name)))
       (values function-name function-ast)
       (values nil nil)))
    (otherwise (values nil nil))))

(-> gather-asts (tree-sitter-ast function) list)
(defun gather-asts (root scanner)
  "Gather the call expression ASTs in ROOT which that have source-text which
matches on SCANNER."
  (iter
    (for ast in-tree root)
    (when-let ((function-name (match-jest-ast ast scanner)))
      (collect (list ast function-name)))))

(-> gather-suite-asts (tree-sitter-ast) list)
(defun gather-suite-asts (root)
  "Gather the ASTs in ROOT which define suites."
  (gather-asts root +suite-scanner+))

(-> gather-test-asts (tree-sitter-ast) list)
(defun gather-test-asts (root)
  "Gather the ASTs in ROOT which define tests."
  (gather-asts root +test-scanner+))

(defmethod tests-for ((framework jest)
                      (software ecma)
                      &aux (ranges (ast-source-ranges software))
                        (genome (genome software)))
  (labels ((get-name (jest-ast)
             "Get the name/description of JEST-AST."
             (match jest-ast
               ((ecma-call-expression
                 :ecma-arguments
                 (ecma-arguments
                  :children (list* name _)))
                (source-text name))))
           (get-test-path (test-ast)
             "Get the test path associated with TEST-AST.
              This will be a list of names of the suites and the test name in
              the nested order they occur in."
             (mapcar
              #'get-name
              (append1
               (filter
                (lambda (ast)
                  (match-jest-ast ast +suite-scanner+))
                (reverse (get-parent-asts* genome test-ast)))
               test-ast)))
           (make-jest-case (test-ast)
             "Make a jtest-case object based on TEST-AST and return it."
             (make 'jest-case
                   :software software
                   :object test-ast
                   :target
                   (make 'Location
                         :uri (string+ "file://" (original-path software))
                         :range (convert 'Range (assocdr test-ast ranges)))
                   :path (get-test-path test-ast)
                   ;; TODO: this requires analysis of the whole file
                   ;;       to determine.
                   :disabled nil)))
    (mapcar
     (op (make-jest-case (car _)))
     (gather-test-asts genome))))

(defmethod tests-for ((framework jest) (project javascript-project))
  (iter
    ;; TODO: this likely won't cover every test file, but introductory documentation
    ;;       specifies that files should have .test.js. The package.json file should
    ;;       also be checked to see if jest is actually being used.
    (for file-pair in (filter (op (scan ".*[\\.-]test\\.js" (namestring (car _))))
                              (collect-evolve-files project)))
    (appending (tests-for framework (cdr file-pair)))))

(defpattern json-key-value (key-string value-form)
  `(json-pair
    :json-key
    (json-string
     :children
     (list (json-string-content :text (equal ,key-string))))
    :json-value ,value-form))

(-> destructure-json-test-result (json-ast)
  (values list (or string null) list))
(defun destructure-json-test-result (test-result)
  "Return as values the full path, status, and failure messages of test-result."
  (labels ((get-strings-from-array (value)
             "Get strings stored in a JSON array."
             (iter
               (for json-string in (direct-children value))
               (ematch json-string
                 ((json-string :children (list (json-string-content :text text)))
                  (collect text)))))
           (get-string-content (value)
             "Get the string content from a string object."
             (ematch value
               ((json-string
                 :children
                 (list (json-string-content :text text)))
                text))))
      (iter
        (iter:with parent-suites)
        (iter:with failure-messages)
        (iter:with status)
        (iter:with title)
        (for pair in (direct-children test-result))
        (match pair
          ((json-key-value "ancestorTitles" value)
           (setf parent-suites (get-strings-from-array value)))
          ((json-key-value "failureMessages" value)
           (setf failure-messages (get-strings-from-array value)))
          ((json-key-value "status" value)
           (setf status (get-string-content value)))
          ((json-key-value "title" value)
           (setf title (get-string-content value))))
        (finally (return (values (append1 parent-suites title)
                                 status
                                 failure-messages))))))

(-> process-json-jest-results ((or string pathname))
  (values list))
(defun process-json-jest-results (filename)
  (labels ((get-file-level-test-results (results-json)
             "Get the test results for each file in RESULTS-JSON."
             (iter
               (for pair in (direct-children (car (direct-children (genome results-json)))))
               (match pair
                 ((json-key-value "testResults" results)
                  (leave results)))))
           (get-test-results (file-result)
             "Get all of the test results from FILE-RESULT."
             (let ((assertion-results
                     (find-if
                      (op
                        (match _
                          ((json-pair
                            :json-key
                            (json-string
                             :children
                             (list (json-string-content
                                    :text (guard text (equal text "assertionResults"))))))
                           t)))
                      (direct-children file-result))))
               (match assertion-results
                 ((json-pair
                   :json-value
                   (json-array :children test-results))
                  test-results))))
           (create-test-status (test-path status failure-messages)
             "Create a test status object with the relevant arguments."
             (if (equal status "passed")
                 (make 'passed :path test-path)
                 (make 'failed
                       :path test-path
                       :message (or failure-messages ""))))
           (create-test-statuses (json-test-results)
             "Create the test status objects for JSON-TEST-RESULTS."
             (mapcar
              (lambda (test-result)
                (multiple-value-call #'create-test-status
                  (destructure-json-test-result test-result)))
              json-test-results)))
    (let ((file-results (get-file-level-test-results (from-file 'json filename))))
      (iter
        (for file-result in (direct-children file-results))
        (appending (create-test-statuses (get-test-results file-result)))))))

(defun run-jest-tests (&key only project runner)
  (with-temporary-file (:pathname tmp)
    (cmd!
     (case runner
       (yarn '("yarn" "test"))
       (npm '("npm" "test" "--"))
       (otherwise "jest"))
     (when only
       ;; TODO: this assumes only one currently.
       (list "-t" (apply #'string+ (test-path (car only)))))
     "--json"
     (fmt "--outputFile=~a" tmp)
     :in (project-dir project))
    (process-json-jest-results (pathname tmp))))

(defmethod run-tests-for ((framework jest) (project javascript-project) &key runner)
  (run-jest-tests :project project :runner runner))

(defmethod run-tests ((framework jest) (test jest-case)
                      &key project runner)
  (run-tests framework (list test) :project project :runner runner))

(defmethod run-tests ((framework jest) (tests list)
                      &key project runner)
  (run-jest-tests :only tests
                  :project project
                  :runner runner))

(defgeneric has-jest-dependency? (software)
  (:method (software)
    nil)
  (:method ((project javascript-project))
    (has-jest-dependency?
     (assocdr "package.json" (other-files project) :test #'equal)))
  (:method ((software json))
    (labels ((get-dev-dependencies (software)
               ;; NOTE: call genome directly to ensure it is populated.
               (match (genome software)
                 ((json-document :children (list (json-object :children children)))
                  (find-if
                   (op (match _
                         ((json-key-value "devDependencies" _)
                          t)))
                   children)))))
      (when-let ((dev-dependencies (get-dev-dependencies software)))
        (find-if
         (lambda (pair)
           (match pair
             ((json-key-value "jest" _) t)))
         (direct-children (json-value dev-dependencies)))))))

(defun create-throw-test (ast-type function-name arguments)
  (car
   (direct-children
    (convert ast-type
             (fmt "~2%test('~a does not throw', () => {
  expect(() => {
    ~a(~{~a~^, ~});
  }).not.toThrow();
});"
                  function-name function-name arguments)))))

;;; NOTE: TODO: this will likely need some modification.
(defmethod add-test ((framework jest)
                     (software ecma)
                     specification
                     &key data)
  (when-let* ((genome (genome software))
              (function-name (car data))
              (arguments (cdr data))
              (target-function (find-if (lambda (ast)
                                          (and (typep ast 'function-ast)
                                               (equal function-name (function-name ast))))
                                        genome))
              (new-function
               (create-throw-test
                (etypecase software
                  (javascript 'javascript-ast)
                  (typescript-ts 'typescript-ts-ast)
                  (typescript-tsx 'typescript-tsx-ast))
                function-name
                arguments))
              (new-software (insert software target-function new-function)))
    ;; TODO: The following is a hack to insert after the target function
    ;;       by first inserting the new test, removing the target function,
    ;;       and then reinserting the test again.
    ;;       This should instead use a one-shot #'insert-after once one is
    ;;       added to functional-trees.
    (insert (less new-software target-function)
            new-function target-function)))
