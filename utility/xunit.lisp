(defpackage :argot-server/utility/xunit
  (:use :gt/full :argot)
  (:import-from :fxml-dom :make-dom-builder)
  (:documentation "Tolerant parser for the (informal) xUnit XML output
  format.")
  (:local-nicknames (:xml :fxml) (:dom :fxml.dom))
  (:export :parse))
(in-package :argot-server/utility/xunit)
(in-readtable :curry-compose-reader-macros)

(defun att-ref (elt attr &optional default)
  (or (dom:get-attribute elt attr) default))

(defun filter-name (name node)
  (coerce (dom:get-elements-by-tag-name node name) 'list))

(defun element-text (element)
  (string-join
   (map 'vector #'dom:node-value
        (filter #'dom:text-node-p
                (dom:child-nodes element)))
   ""))

(defun parse-xml-report (source)
  (handler-bind ((xml:well-formedness-violation #'continue))
    (xml:parse source (fxml-dom:make-dom-builder))))

(defgeneric parse (source)
  (:method ((source pathname))
    (parse (parse-xml-report source)))
  (:method ((source string))
    (parse (parse-xml-report source)))
  (:method ((source list))
    (mappend #'parse source)))

(defmethod parse ((document dom:document))
  (nest
   (flet ((uniq (s) (string (gensym s)))))
   (let* ((suites (filter-name "testsuite" document))
          (suite-tests (mapcar {filter-name "testcase"} suites))
          ;; Deal with weird structures likes suites inside cases.
          (seen (fset:empty-set))))
   (iter outer
         (for suite in suites)
         (for tests in suite-tests))
   (let ((suite-name
          (or (dom:get-attribute suite "name") (uniq "suite")))))
   (iter (for test in tests))
   (unless (contains? seen test)
     (withf seen test))
   (let* ((test-name (or (dom:get-attribute test "name") (uniq "test")))
          (path (list suite-name test-name))
          (test-time
           (or (when-let (time (dom:get-attribute test "time"))
                 (ignore-errors (parse-number time)))
               0))
          (status-elements
           (append (filter-name "skipped" test)
                   (filter-name "error" test)
                   (filter-name "failure" test)))))
   (in outer)
   (nconcing)
   (if (no status-elements)
       (list (make 'passed :path path :time test-time)))
   (iter (for element in status-elements)
         (for name = (dom:tag-name element))
         (collect (make
                   (string-ecase name
                     (("flakyFailure" "rerunFailure" "failure") 'failed)
                     (("flakyError" "rerunError" "error") 'errored)
                     (("disabled" "skipped") 'skipped))
                   :path path
                   :time test-time
                   :message
                   (or (dom:get-attribute element "message") "")
                   :trace (element-text element))))))
