(defpackage :argot-server/utility/tap
  (:use :gt/full :argot)
  (:documentation "Parser for TAP (Test Anything Protocol) version 12.")
  (:export
    :tap-line
    :version
    :version-number
    :plan
    :plan-end
    :plan-directive
    :test-line
    :test-line-ok
    :test-line-test-number
    :test-line-description
    :test-line-directive
    :diagnostic
    :bailout
    :bailout-reason
    :unknown-line
    :parse
    :parse1
    :tap-test-results))
(in-package :argot-server/utility/tap)
(in-readtable :curry-compose-reader-macros)

(defunion tap-line
  (version (number (integer 12 *)))
  (plan
   (end (integer 0 *))
   (directive string))
  (test-line
   (ok boolean)
   (test-number (integer 1 *))
   (description string)
   (directive string))
  (diagnostic (string string))
  (bailout (reason string))
  (unknown-line (string string)))

(defgeneric parse (source)
  (:method ((source pathname))
    (with-input-from-file (in source)
      (parse in)))
  (:method ((source string))
    (with-input-from-string (in source)
      (parse in))))

;;; See http://testanything.org/tap-specification.html.f

(defmethod parse ((source stream))
  (assert (input-stream-p source))
  (let ((offset 1))
    (iter (for line = (read-line source nil nil))
          (while line)
          (let ((tap-line (parse1 line :offset offset)))
            (when (typep tap-line 'test-line)
              (incf offset))
            (collect tap-line)))))

(-> parse1 (string &key (:offset (integer 1 *)))
    (values tap-line &optional))
(defun parse1 (line &key (offset 1))
  (match line
    ((ppcre "^TAP version (\\d+)" (and version (type string)))
     (version (parse-integer version)))
    ((ppcre "^1..(\\d+)\\s+#(.*)$"
            (and end (type string))
            (and directive (type string)))
     (plan (parse-integer end)
           (trim-whitespace directive)))
    ((ppcre "^1..(\\d+)" (and end (type string)))
     (plan (parse-integer end) ""))
    ((ppcre "^(?:ok|not ok)")
     (parse-test-line line :offset offset))
    ((ppcre "^Bail out!(.*)" reason)
     (bailout (trim-whitespace reason)))
    ((ppcre "^#(.*)" (and text (type string)))
     (diagnostic text))
    (otherwise
     (unknown-line line))))

(-> parse-test-line (string &key (:offset (integer 1 *)))
    (values test-line &optional))
(defun parse-test-line (test-line &key (offset 1))
  (declare (string test-line))
  (let (ok test-number description directive)
    (econd
     ((string^= "ok" test-line)
      (setf ok t
            test-line (drop-prefix "ok" test-line)))
     ((string^= "not ok" test-line)
      (setf test-line (drop-prefix "not ok" test-line))))
    (setf test-line (drop-while #'whitespacep test-line))
    (let ((digits (take-while #'digit-char-p test-line)))
      (if (not (emptyp digits))
          (setf test-number (parse-integer digits)
                test-line (drop (length digits) test-line))
          (setf test-number offset)))
    (setf test-line (drop-while #'whitespacep test-line))
    (let ((desc (take-until (eqls #\#) test-line)))
      (setf test-line (drop (length desc) test-line)
            description (drop-prefix "- " (trim-whitespace desc))))
    (setf directive (trim-whitespace (drop 1 test-line)))
    (test-line ok test-number description directive)))

(defun tap-test-results (tap-source)
  (iter (for line in (parse tap-source))
        (collect
         (match line
           ((test-line _ _ description (ppcre "(?i)^(?:skip|todo)"))
            (make 'skipped :path (list description)))
           ((test-line t _ description _)
            (make 'passed :path (list description)))
           ((test-line nil _ description _)
            (make 'failed :path (list description)
                          :message description))))))
