(defpackage :argot-server/utility/testing
  (:use :gt/full
        :software-evolution-library/software/parseable
        :software-evolution-library/software/tree-sitter
        :software-evolution-library/utility/range
        :argot-server/readtable
        :lsp-server/protocol
        :argot)
  (:local-nicknames (:l :fresnel/lens))
  (:import-from :argot-server/utility :guess-language)
  (:import-from :lsp-server/utilities/lsp-utilities :file-uri-path)
  (:import-from :software-evolution-library :software :from-file)
  (:import-from :software-evolution-library/software/project
                :collect-evolve-files
                :project)
  (:import-from :abstract-classes :abstract-class)
  (:documentation "Types for modeling tests.

Type names for tests are taken from the ISTQB glossary. Types for test
statuses are based on the structure of xUnit statuses. Both of these are
extended with the idea that suites may be nestable.")
  (:export
    :test-path
    :test-statuses
    :run-tests
    :tests-for
    :run-tests-for
    :test-object
    :examples-for
    :test-software
    :test-condition-example
    :parsed-test
    :add-test))
(in-package :argot-server/utility/testing)
(in-readtable argot-readtable)

(defgeneric test-software (test)
  (:method (test)
    (ematch (test-target test)
      ((Location :uri (and uri (type string)))
       ;; TODO use create-software (too slow)
       (let* ((file (file-uri-path uri))
              (class (guess-language file))
              (software (from-file (make class) file)))
         software))))
  (:documentation "Get the software object containing the source of TEST.

If you are parsing tests from source, consider using the `parsed-test'
mixin so you can save the software object when instantiating the
test."))

(defgeneric test-object (test)
  (:method (test)
    (ematch (test-target test)
      ((Location :range range)
       (let* ((software (test-software test))
              (range (convert 'source-range range)))
         (first
          ;; Use remove-duplicates to strip off any trivial "wrappers".
          (mapcar #'car
                  (remove-duplicates
                   (remove-if-not [{contains range} #'cdr]
                                  (ast-source-ranges software))
                   :key #'cdr
                   :test #'equal?
                   :from-end nil)))))))
  (:documentation "Get the AST node corresponding to TEST.

If you are parsing tests from source, consider using the `parsed-test'
mixin so you can save the node when instantiating the test."))

(defclass parsed-test ()
  ((software :initarg :software :reader test-software)
   (object :initarg :object :reader test-object))
  (:documentation "Mixin for tests parsed from source."))

(defgeneric run-tests (framework test/s &key)
  (:documentation
   "Run a test, or a test suite, or a list thereof.
Returns a list of pass/fail statuses.")
  (:method (framework (tests list) &key)
    (mappend {run-tests framework} tests)))

(defgeneric tests-for (framework object)
  (:documentation "Extract the tests for something (project, file).")
  (:method (framework (project project))
    (mappend (op (tests-for framework (cdr _)))
             (collect-evolve-files project))))

(defgeneric run-tests-for (framework object &key)
  (:method (framework object &key)
    (run-tests framework (tests-for framework object))))

(defun bound-literals (software ast)
  "Return a hash table from variable-name -> value for variables in
AST's scope that are bound to literals."
  (let ((dict (dict)))
    (iter (for scope in (scopes software ast))
          (iter (for var-alist in scope)
                (let ((name (assocdr :name var-alist))
                      (decl (assocdr :decl var-alist)))
                  (when (typep decl 'variable-declaration-ast)
                    (unless (typep decl 'cpp-declaration)
                      (when (typep (rhs decl) 'literal-ast)
                        ;; Don't overwrite inner bindings!
                        (ensure-gethash
                         (make-keyword name)
                         dict
                         (rhs decl))))))))
    dict))

(defun test-condition-example (lens test-condition
                               &key (software (test-software test-condition))
                                 (ast (test-object test-condition)))
  (let ((example (l:get lens ast)))
    (when example
      ;; We are looking for the case where one side is a function
      ;; call and the other side is a literal (or an identifier
      ;; that can be resolved to a literal. TODO: Resolve
      ;; identifiers into literals based on `scopes'.
      (let ((literal-table (bound-literals software ast)))
        (sublis (iter (for (k v) in-hashtable literal-table)
                      (when-let (translation (l:get lens v))
                        (collect (cons k translation))))
                example
                :test #'equal)))))

(defgeneric examples-for (framework object lens function)
  (:method (framework object lens function)
    (nest
     ;; We need equal as a test since the function may be a string, a
     ;; keyword, or a list like (:dot x y).
     (mapcar {test-condition-example lens})
     (mappend #'test-case-conditions)
     (tests-for framework object))))

(defgeneric test-status-alist (framework object &key)
  (:method (framework object &key)
    (let* ((statuses (run-tests-for framework object))
           (tests (tests-for framework object))
           (status-table (empty-map)))
      (iter (for status in statuses)
            (for path = (test-path status))
            (withf status-table
                   path
                   (cons status (@ status-table path))))
      ;; TODO Flatten suites?
      (iter (for test in tests)
            (for path = (test-path test))
            (collect (cons test
                           (reverse (@ status-table path))))))))

(defgeneric add-test (framework object test-specification &key)
  (:method (framework object test-specification &key &allow-other-keys) nil)
  (:documentation "Add a FRAMEWORK test to OBJECT based on TEST-SPECIFICATION."))
