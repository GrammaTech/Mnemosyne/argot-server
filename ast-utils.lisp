(defpackage :argot-server/ast-utils
  (:documentation "Functions for handling ASTs in language-agnostic ways.")
  (:use :gt/full
        :argot-server/readtable
        :software-evolution-library/software/lisp
        :software-evolution-library/utility/range
        :software-evolution-library/software/tree-sitter)
  (:import-from :software-evolution-library/software/parseable
                :asts-containing-source-location
                :parseable
                :ast-annotations
                :ast-source-ranges
                :asts-contained-in-source-range
                :source-text)
  (:import-from :software-evolution-library
                :software
                :from-string
                :genome)
  (:export
    :function-node-in-range
    :valid-python?
    :valid-javascript?
    :valid-json?
    :valid-c?
    :valid-cpp?
    :valid-ast?
    :ast-valid?
    :nearest-function-node
    :node-source-range))
(in-package :argot-server/ast-utils)
(in-readtable argot-readtable)

(defun function-node-in-range (software start end)
  "Find a function node between START and END in SOFTWARE.
If there are multiple such nodes, only returns the first."
  (nest (find-if (of-type 'function-ast))
        (asts-contained-in-source-range software)
        (make 'source-range
              :begin (convert 'source-location start)
              :end (convert 'source-location end))))

(defun nearest-function-node (sw begin end)
  "Get the nearest function node to BEGIN and END.
If BEGIN and END are the same position, get the deepest function node
that contains that position.

If BEGIN and END are not equal, get the least deep function node
contained in that range (or the first, if there are more than one)."
  (if (equal? begin end)
      (let ((asts (asts-containing-source-location sw begin)))
        ;; TODO Is this the right order?
        (find-if (of-type 'function-ast) (reverse asts)))
      (function-node-in-range sw begin end)))

(defgeneric ast-valid? (ast)
  (:method ((sw software))
    (ast-valid? (genome sw)))
  (:method ((ast t)) t)
  (:method ((ast tree-sitter-ast))
    (not (find-if (of-type 'parse-error-ast) ast))))

(defun valid-ast? (class string)
  (assert (subtypep class 'parseable))
  (true
   (ignore-errors
    (ast-valid? (from-string (make class) string)))))

(defun valid-python? (string)
  (valid-ast? 'python string))

(defun valid-javascript? (string)
  (valid-ast? 'javascript string))

(defun valid-json? (string)
  (valid-ast? 'json string))

(defun valid-c? (string)
  (valid-ast? 'c string))

(defun valid-cpp? (string)
  (valid-ast? 'cpp string))

(defun node-source-range (ast node)
  (assocdr node (ast-source-ranges ast)))
