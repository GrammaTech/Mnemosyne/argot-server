(defpackage :argot-server/server
  (:nicknames :argot-server)
  (:use :gt/full
        :lsp-server/protocol
        :lsp-server/protocol-util
        :lsp-server/lsp-server
        :lsp-server/logger
        :lsp-server.lem-base
        :lsp-server/utilities/lsp-utilities
        :lsp-server/utilities/worker-thread
        :command-line-arguments
        :argot-server/config
        :argot-server/readtable
        :argot-server/utility
        :argot-server/passthrough
        :argot-server/muses/muse
        :argot-server/muses/cl-lsp
        #-(or inhibit-refactoring-mutations spec-map-dev) :argot-server/muses/refactoring-mutations
        :argot-server/muses/trinity
        :argot-server/muses/incrementalizer
        #+lambdanet :argot-server/muses/lambdanet
        ;; TODO Re-enable autocomplete and remove code-predictor when
        ;; we have a GPU.
        #+(or) :argot-server/muses/autocomplete
        :argot-server/muses/argument-predictor
        :argot-server/muses/hypothesis-tests
        :argot-server/muses/fast-check
        :argot-server/muses/snippet-mining
        #+regel :argot-server/muses/regel
        #-(or inhibit-refactoring-mutations spec-map-dev) :argot-server/muses/genpatcher
        :argot-server/muses/dig
        :argot-server/muses/libfuzzer
        :argot-server/muses/python-type-view
        :argot-server/muses/typescript-type-view
        :argot-server/muses/jsdoc-type-view
        :argot-server/muses/typewriter
        :argot-server/muses/type-refinement
        :argot-server/muses/test-view
        :argot-server/muses/jest-test-view
        #-(or inhibit-refactoring-mutations spec-map-dev) :argot-server/muses/ssr-muse
        :argot-server/muses/function-generator
        :argot-server/muses/pyls
        :argot-server/muses/jsls
        #+spec-map-dev :argot-server/muses/spec-map
        :argot-server/muses/sygus
        :argot-server/muses/herbie
        :argot-server/muses/godbolt-muse
        :argot-server/muses/erroir)
  (:import-from :software-evolution-library/command-line
                :exit-command
                :handle-set-verbose-argument
                :handle-swank-port-argument-and-set-interactive
                :wait-on-swank
                :with-interrupt-shutdown)
  (:import-from :swank :create-server)
  (:import-from :slynk)
  (:import-from :cl-strftime :format-time)
  (:import-from :bordeaux-threads)
  (:import-from :jsonrpc)
  (:import-from :jsonrpc/transport/stdio)
  (:import-from :yason)
  (:import-from :lsp-server)
  (:import-from :moira)
  (:import-from :software-evolution-library/utility/debug
                :*note-out*)
  (:shadow :add-hook :remove-hook :run-hooks)
  (:export :argot-server
           :*server*
           :*all-muses*
           :server
           :launch-server
           :server-handle-stop
           :all-muses
           :toggle-debug
           :server-handle-server))
(in-package :argot-server/server)
(in-readtable argot-readtable)
(in-readtable :curry-compose-reader-macros)

(defun toggle-debug (&optional (bool (not *debug-workers*)))
  "Toggle debugging errors in workers and requests."
  (setf *debug-workers* (and bool t))
  (setf jsonrpc:*on-error* (if bool :debug :print))
  bool)

;;; For general LSP specification information see:
;;; https://microsoft.github.io/language-server-protocol/specification

(unless (typep *server* 'argot-server)
  (setf *server* (make 'argot-server)))

;;; Do-nothing methods for muses to augment.

(define-class-method "workspace/executeCommand"
    argot-server
    (params)
  nil)

(define-class-method "textDocument/completion"
    argot-server
    (params)
  (vector))

(define-class-method "completionItem/resolve"
    argot-server
    (params)
  nil)

(define-class-method "textDocument/codeAction"
    argot-server
    (params)
  (vector))

(define-class-method "textDocument/rename"
    argot-server
    (params |RenameParams|)
  nil)


;;;; Start the server in the background from the REPL.

(defvar-unbound *handle*
  "The current server handle.")

(defclass server-handle ()
  ((server :initarg :server :reader server-handle-server)
   (thread :initform nil)
   (lock :initform (bt:make-recursive-lock "Server lock") :reader monitor)
   (name :initarg :name :type string))
  (:documentation "A server that runs in the background.")
  (:default-initargs
   :server *server*
   :name "Background server"))

(defun make-server-handle (server)
  (make 'server-handle :server server))

(defun dev-log-directory ()
  (nest
   (ensure-directories-exist)
   (path-join (user-homedir-pathname))
   #p".argot-server/"))

(defun new-dev-log-file ()
  (nest
   (path-join (dev-log-directory))
   (make-pathname :type "log"
                  :name)
   (fmt "argot-~a" (format-time nil "%F-%H-%M-%S"))))

(defun launch-server (&rest args
                      &key (enable (all-muses))
                        (log-file (new-dev-log-file))
                        (network *network*)
                        reset
                      &allow-other-keys)
  (when reset
    (run-hook '*reset-hook*))
  (let* ((*server* (apply #'make-instance
                          'argot-server
                          :allow-other-keys t
                          args))
         (handle (make-server-handle *server*)))
    (apply #'server-handle-start handle
           :enable enable
           :log-file log-file
           :network network
           (remove-from-plist args :reset))
    handle))

(defmethod print-object ((self server-handle) stream)
  (print-unreadable-object (self stream :type t)
    (with-slots (thread server) self
      (format stream "~a ~a" server thread))))

(defgeneric server-handle-start (server &rest args)
  (:method ((handle server-handle) &rest args)
    (with-slots (server thread name) handle
      (synchronized (handle) "Server handle"
        (assert (or (null thread)
                    (not (bt:thread-alive-p thread))))
        (nest
         (setf thread)
         (bt:make-thread
          (dynamic-closure
           '(*standard-output*
             *trace-output*)
           (lambda ()
             (let ((*server* server)
                   (*handle* handle)
                   ;; Propagate the server binding through any threads
                   ;; launched from this thread.
                   (bt:*default-special-bindings*
                    (acons '*server* server
                           bt:*default-special-bindings*)))
               (catch 'quit
                 (unwind-protect
                      (apply #'server args)
                   (stop-server-kernel server))))))
          :name (fmt "~a (~a)" name (server-id server))))))))

(defgeneric server-handle-stop (server)
  (:method ((self server-handle))
    (with-slots (thread) self
      (synchronized (self) "Server handle"
        (assert (and thread (bt:thread-alive-p thread)))
        (ignore-errors
         (bt:interrupt-thread thread (lambda () (throw 'quit nil))))))))


;;;; Start the server in the foreground at REPL or command-line with `define-command'.

(defun handle-load (path) (load path))
(defun handle-eval (string) (eval (read-from-string string)))

(defun handle-slynk-port-argument-and-set-interactive (port)
  (setf *lisp-interaction* t)
  (slynk:create-server :port port :style :spawn :dont-close t))

(defun handle-muse-list (muse-string)
  (typecase muse-string
    (string (mapcar [{intern _ #.(find-package :argot-server/server)} #'string-upcase]
                    (split-sequence #\, muse-string :remove-empty-subseqs t)))
    (t muse-string)))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defparameter *all-muses*
    '(test-view
      jest-test-view
      python-type-view
      jsdoc-type-view
      typescript-type-view
      #-(or inhibit-refactoring-mutations spec-map-dev) refactoring-mutations
      #-(or inhibit-refactoring-mutations spec-map-dev) genpatcher-muse
      ;; typewriter-muse
      ;; type-refinement-muse
      #+lambdanet lambdanet-muse
      ;; TODO Reenable autocomplete and drop code-predictor once we
      ;; have a GPU.
      #+(or) autocomplete
      argument-predictor
      hypothesis-tests
      fast-check
      snippet-mining
      #+regel regel
      trinity
      herbie-muse
      erroir
      #-(or inhibit-refactoring-mutations spec-map-dev) ssr-muse
      function-generator
      sygus
      dig
      cl-lsp
      libfuzzer
      ;; TODO Move these into separate services and add user configuration to en/disable them.
      #+(or) jsls
      #+(or) pyls
      #+spec-map-dev spec-map)
    "List of known muses.

All of these muses (assuming they satisfy `muse-available?') will be
launched by default.

Note that this variable also controls the ordering of results when
multiple muses send a response to a request.")

  (defparameter +command-line-options+
    `((("help" #\h #\?) :type boolean :optional t
       :documentation "display help output")
      (("load" #\l) :type string
       :action #'handle-load
       :documentation "load FILE as lisp code")
      (("eval" #\e) :type string
       :action #'handle-eval
       :documentation "eval STRING as lisp code")
      (("mode" #\m) :type string :initial-value "tcp"
       :documentation "set server mode to \"stdio\" or \"tcp\"")
      (("log-file" #\L) :type string
       :documentation
       "append log to file, \"STDOUT\" or \"-\" for terminal, 2 for stderr")
      (("no-log" #\n) :type boolean :optional t
       :documentation "inhibit logging")
      (("port" #\p) :type integer :initial-value 10003
       :documentation "port used in TCP mode")
      (("host" #\H) :type string :initial-value "0.0.0.0"
       :documentation "host used in TCP mode")
      (("swank" #\s) :type integer
                     :action #'handle-swank-port-argument-and-set-interactive
                     :documentation "start a swank listener on PORT")
      (("slynk" #\S) :type integer
                     :action #'handle-slynk-port-argument-and-set-interactive
                     :documentation "start a slynk listener on PORT")
      (("enable" #\m) :type string :optional t
                      :initial-value ,(fmt "~{~(~a~)~^,~}" *all-muses*)
                      :action #'handle-muse-list
                      :documentation "All muses to enable")
      (("disable" #\M) :type string :optional t
                       :action #'handle-muse-list
                       :documentation "Muses to disable")
      (("timeout" #\T) :type integer :initial-value 45
                       :documentation "Timeout (0 for no timeout)")
      (("verbose" #\V) :type integer :initial-value 2
                       :action #'sel/command-line:handle-set-verbose-argument
                       :documentation "verbosity level 0-4")
      (("network" "net") :type string
                         :initial-value "bridge"
                         :documentation "Network (host or bridge)")
      (("allow-local") :type boolean
                       :initial-value t
                       :optional t
                       :documentation "Allow loading projects from disk"))))

(defun all-muses (&key disable (enable *all-muses*))
  (flet ((make-muses (list)
           (mapcar (lambda (x)
                     (etypecase x
                       (symbol (make x))
                       (muse x)))
                   list)))
    (remove-duplicates
     (let ((disable (make-muses disable)))
       (remove-if (lambda (muse)
                    (member (muse-name muse) disable
                            :key #'muse-name
                            :test 'equal))
                  (make-muses enable))))))

;;; Arrange to unload all shared libraries before the image is dumped
;;; and load them again when the image is restored.

;;; See https://github.com/cffi/cffi/pull/163

(defparameter *dependent-lib-names*
  '(tree-sitter-wrapper))

(let (lib-names)
  (defun unload-all-libraries ()
    (mvlet* ((libs (cffi:list-foreign-libraries))
             (dependent-libs libs
              (partition (op (member (cffi:foreign-library-name _)
                                     *dependent-lib-names*
                                     :test #'string-equal))
                         libs))
             ;; Load libraries with absolute pathnames last. E.g. the
             ;; tree-sitter wrapper depends on the system tree-sitter
             ;; being loaded.
             (libs (append libs dependent-libs)))
      (setf lib-names (mapcar #'cffi:foreign-library-name libs))
      (mapc #'cffi:close-foreign-library libs)))
  (defun reload-all-libraries ()
    (mapc #'cffi:load-foreign-library lib-names)))

(uiop:register-image-dump-hook #'unload-all-libraries)
(uiop:register-image-restore-hook #'reload-all-libraries)

(define-command server (&spec +command-line-options+)
  "Start the Argot LSP server."
  #.(format nil
            "~%Built from ~a ~a on ~a.~%"
            (lisp-implementation-type) (lisp-implementation-version)
            (multiple-value-bind (second minute hour date month year)
                (get-decoded-time)
              (format nil "~4d-~2,'0d-~2,'0d ~2,'0d:~2,'0d:~2,'0d"
                      year month date hour minute second)))
  (declare (ignorable load eval))
  (setf *allow-local* allow-local)
  (moira:start-monitor)
  (setf *network*
        (assure network
          (make-keyword
           (string-upcase
            (or (getenv "ARGOT_SERVER_NETWORK")
                network)))))
  (when (plusp timeout)
    (setf (muse-timeout *server*) timeout))
  (let ((muses (all-muses :enable enable :disable disable)))
    (assert (every (distinct) muses))
    (multiple-value-bind (available unavailable)
        (partition #'muse-available? muses)
      (set-passthrough *server* available)
      (when unavailable
        (format *error-output*
                "~&Unavailable muses: ~{~a~^, ~}~%"
                (mapcar #'muse-name unavailable)))))
  (ensure-kernel)
  (when help (show-help-for-server) (quit))
  (unless mode (setf mode "stdio"))     ; Default from REPL.
  (let ((*readtable* fare-utils:*standard-readtable*)
        (mode (make-keyword (string-upcase mode)))
        (log-file (parse-log-file-options log-file no-log)))
    (when (and (eql log-file t)
               (eql mode :stdio))
      (error "Cannot use stdio transport and log to stdio."))
    (with-log-file (log-file)
      (let ((info (or *logger-stream* *error-output*)))
        ;; Redirect SEL logging to the info stream (stdio for TCP,
        ;; stderr for stdio). Otherwise it can break things when we
        ;; are running over stdio.
        (setf *note-out* info)
        (ecase mode
          (:tcp (format info "mode: tcp~%port: ~D~%" port))
          (:stdio (format info "mode: stdio~%")))
        (format info "network: ~(~a~)~%" *network*)
        (when *logger-stream*
          (format info "~@[log: ~a~%~]"
                  (etypecase-of log-file-designator log-file
                    (null nil)
                    (output-stream log-file)
                    ((or pathname string)
                     (namestring (merge-pathnames log-file)))
                    ((eql 2) "STDERR")
                    ((member t 1) "STDOUT"))))
        (with-address-in-use-handling (:interactive-p *lisp-interaction*)
          (with-interrupt-shutdown (:error-output *logger-stream*)
            (flet ((start-listening ()
                     (argot-server-listen *server*
                                          :allow-other-keys t
                                          :port port
                                          :host host
                                          :mode mode)))
              (when *logger-stream*
                (setf (logger-stream*) *logger-stream*))
              (start-listening))
            (when swank (wait-on-swank))
            (exit-command server 0 *server*)))))))
