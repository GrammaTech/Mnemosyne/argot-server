[[_TOC_]]

Contributing
============

## General

### Code of Conduct

Please read the [Mnemosyne Code of Conduct](CODE_OF_CONDUCT.md).

### General Guidelines

- Text files may not have trailing whitespace.

- Text files must end with a trailing newline.

- All tests should be able to run and pass.
  This can be checked by running `make check` on your build directory.

- All files shall be properly formatted as determined by our
  `pre-commit` configuration (see below for details).

- We ask that all contributors complete our Contributor License
  Agreement (CLA), which can be found at
  [GrammaTech-CLA-mnemosyne.pdf](./GrammaTech-CLA-mnemosyne.pdf), and
  email the completed form to `CLA@GrammaTech.com`.  Under this
  agreement contributors retain the copyright to their work but grants
  GrammaTech unlimited license to the work.

#### pre-commit

In general, code must follow a unified format. To make compliance with
this format easier, we recommend the use of
[`pre-commit`](https://pre-commit.com/) with the provided
configuration file, `.pre-commit-config.yaml`, to manage formatting.
To use `pre-commit`:

1. If `pre-commit` is not already installed on your system, install it
   now with [`pip`](https://pypi.org/project/pip/).
   ```shell
      pip3 install pre-commit
   ```
2. Install [`lisp-format`](https://github.com/eschulte/lisp-format)
   and ensure that your `~/.lisp-formatrc` file configures Emacs
   (e.g., by adding directories to your `load-path` and/or by calling
   the `package-initialize` function) so that you can load slime and
   paredit.
3. Install the formatters as a pre-commit hook. In the project root directory:
   ```shell
    pre-commit install
   ```
   If you prefer to run `pre-commit` manually instead, run this before all commits:
   ```shell
       pre-commit run
   ```

### Common Lisp Code Requirements

This project uses the
[SEL coding standards](https://grammatech.github.io/sel/SEL-Coding-Standards.html).

#### Testing Development

- All code you care about should be tested.
- Any code you don't care about should be removed.
- No unit test should take more than 0.5 seconds.

### Python Code Requirements

- Code must be [PEP8](https://www.python.org/dev/peps/pep-0008/) compliant.
  To check for PEP8 compliance, [flake8](https://pypi.org/project/flake8/) is recommended,
  and included as part of our `pre-commit` configuration.

- All code must be formatted with [Black](https://pypi.org/project/black/)
  (set to line lengths of 79, for PEP8 compliance).
  A pass through this tool is included as part of our `pre-commit` configuration.
  - Please note that `black` only works on Python version 3.6 and newer.
    This is newer than what is available on some OSes by default (for example, Ubuntu 16),
    so you may have to install Python 3.6 or newer to run `black`.
    If installing Python 3.6+ on your system is not possible, there exists
    [an online interface to Black][black] you can manually enter files into.

- The Python API should be made to run on all version of Python 3.

- Use `UpperCamelCase` for type names, `UPPER_CASE` for constant names,
  and `snake_case` for other identifier names.

#### Testing Development

- All code you care about should be tested.
- Any code you don't care about should be removed.
- Code testing is done via the built-in `unittest` framework.
- No unit test should take more than 0.5 seconds.

### Documentation

Documentation for the Mnemosyne project at large is located in the
[documentation repository](https://gitlab.com/GrammaTech/Mnemosyne/docs)
where it should be maintained.  Documentation infrastructure for the
Argot-Server has not yet been implemented.

## How to Write a Muse

Broadly speaking, there are three ways to integrate a Muse into Argot
Server.

1. Write it in Lisp.
2. Write it mostly in another language, and communicate via a Lisp
   shim using the LSP base protocol (JSON-RPC+headers).
3. Write it in another language and communicate directly via LSP.

This could be represented as simple decision tree:

- Are you writing in Lisp?
  - Yes. Use Lisp.
  - No.
    - Does your muse map well to [LSP][]?
      - Yes. Use the all-LSP approach.
      - No. Use custom JSON-RPC.

### Generalities

Every muse, no matter how it is implemented, must have a minimal
representation on the Lisp side as a class that can be instantiated by
Argot Server.

The class is needed:

- To start (or connect to) the muse and to check on its status.
- To give the muse a name. Every muse must have a unique name, to
  ensure that the code it generates can be assigned a provenance.
- To tell Argot Server what languages the muse supports.

#### Supported languages

To tell Argot Server what languages a muse supports, define a method
on `muse-lang`:

``` lisp
(defmethod muse-lang ((muse my-muse))
  '(or python javascript))
```

Or you can supply a `:lang` argument in the class definition:

``` lisp
(defclass my-muse (muse)
  ...
  (:default-initargs
   :lang '(or python javascript)))
```

### Writing an external all-LSP muse

If you are working on integrating an existing piece of software as a
muse, and the software’s functionality maps well to [LSP][], then
going all-LSP is the easiest choice.

In this case, you can use an LSP SDK in the host language and write a
trivial Lisp shim to communicate with it.
[Hypothesis](muses/hypothesis-tests.lisp) is a good example of an
all-LSP muse that communicates via stdio.

For Python, we recommend [pygls][]. For other languages, see the list
of [LSP SDKS][sdks].

If your muse will listen over a TCP port, then it should check the
`PORT` environment variable to find out what port to listen on. If
`PORT` is not set or empty it may fall back to a default port, which
should be documented.

### Writing a JSON-RPC muse

If the software you are integrating with does not map clearly to LSP,
the easiest approach is to re-use the LSP base protocol (JSON-RPC with
HTTP headers) but with custom messages. In this case you will probably
need to repurpose an HTTP server (e.g. `http.server` in Python). Both
[Trinity][] and [TypeWriter][] use this approach.

The muse should still check for the `PORT` environment variable.

### Writing a muse in Lisp

The best way to approach writing a muse in Lisp is to look at one of
the existing muses and work from there. The following notes are
intended to resolve potential ambiguities, not as a full tutorial.

A muse is an instance of a class. Ideally for a muse defined in Lisp
there should be no global state and each instance of the class will
maintain all its state internally. This helps to keep the overall
system stateless and testable.

(Every muse (and the Argot Server itself) is an LSP server in its own
right and maintains its own buffers. So when the user sends a
didChange event for a document, it is applied first to the Argot
Server's own buffer for that document, and then by each muse to its
own buffer for that document. So the overall system is mostly
stateless because each node is tracking the state independently.

It's wasteful in theory, but a negligible proportion of the resources
that the muses are consuming, and if it ever did become a problem it
could be addressed at the buffer level with COW or deduplication of
blocks, without complicating the overall system.)

Most Lisp muses use `code-actor` as their parent class. The
`code-actor` class is mostly meant for muses that are written in Lisp
and run in the same Lisp process. if you need to connect to an
external process via LSP, then that's what `tcp-muse` is for. On the
other hand, if you're not using LSP to communicate with a server, you
could define a Lisp code actor that does the communication internally.
But you'll still need methods for `muse-start`, `muse-stop`, and
`muse-started-p`.

## How to Add an Integration Test

Whenever it makes sense, demos should have corresponding integration
tests.

This is done capturing logs from LSP clients and then replaying them
against the server to make sure the responses match. (The responses
are considered to match as long as the new response is a superset of
the old response, so new features don’t break tests.)

### Capturing Logs

First, from the Lisp REPL, start the Argot Server with *just* the muse
you want to record the output for:

``` lisp
(launch-server :enable (list (make-instance 'XXX)))
```

#### VS Code

To capture logs with VS Code:

1. Configuring the Mnemosyne VS Code plugin to record logs (by
   changing the `Mnemosyne > Trace: Server` setting to `verbose` and
   the format to JSON).
2. Run through the demo by hand.
3. Copy-paste the output of Mnemosyne (under the Output tab) into a
   file in the [test/logs](test/logs) directory.
4. Edit the file so `file://` URLs point into a directory that is sure
   to exist and be writable (e.g. `/tmp/`). Some muses require files
   to exist on disk.

Configuring VS Code can be confusing because the format setting
doesn't show up and (invisibly) just defaults to text. You have to
click `Add Item` to see and set the format.

For reference, here’s what the configuration should look like:

![VS Code screenshot](etc/vscode-config.png)

#### Emacs

To capture logs with Eglot:

1. Run through the demo by hand.
2. Bring up the Eglot logs with `M-x eglot-events-buffer`.
3. Save the buffer into a file (under [test/logs](test/logs)) with
   `M-x write-file`. (It is not a good idea to try to copy and paste
   the logs.)
4. Clean `file://` URLs as described for VS Code.

### Adding Tests

In [integration.lisp](test/integration.lisp), add a new test following
the format of the others. This should look something like:

``` lisp
(define-timed-test test-vscode-XXX ()
  (rerun-baseline
   (path-join log-dir #p"XXX.json")
   :enable (list (make ‘XXX))
   :format :vscode))
```

For Eglot tests, you can omit the `:format` key.

## How to Record a Demo

Once you have written a new muse, it would be a good idea to record a
demo. The procedure is essentially the same as that for capturing logs
above (except you don’t have to worry about the logs):

1. Start an instance of Mnemosyne with the relevant muse(s) only.

    ``` lisp
    (launch-server :enable (list (make-instance 'XXX)))
    ```
2. Start a screen recorder (e.g. [OBS](https://obsproject.com)). Don’t
   record sound!

3. Fullscreen your editor and run through whatever you want to demo.

4. Stop recording and trim the output. `ffmpeg` is one way to do this;
   use `-async 1` to avoid rounding to the nearest keyframe.

    ```
    ffmpeg -i in.mkv -ss 00:00:07 -to 00:01:40 -async 1 demo.mp4
    ```

6. Set up [Git LFS](https://git-lfs.github.com) if you don’t have it
   already and make a merge request with your new demo on the
   [Mnemosyne documentation].

## How to Rebuild the Docker image locally

Although the Docker image is build in CI, it may be useful to re-build
the argot-server docker image locally.  To do so follow these steps:

1. Install `gitlab-runner` using your Linux package manager.

2. `cd` into the base of this argot-server repository.

3. Run `gitlab-runner` to populate the `external-muses/` folder with
   all of the cloned muses and binary downloads that are built into
   the Docker image.  (Note that the `[FOO_SECRET]` strings in this
   command need to be replaced with the actual protected values.)

    ```sh
    gitlab-runner exec shell --clone-url file://$(pwd) \
        --env DOWNLOAD_PASS="[DOWNLOAD_PASS_SECRET]" \
        --env DOWNLOAD_URL="[DOWNLOAD_URL_SECRET]" \
        --env DOWNLOAD_USER="[DOWNLOAD_USER_SECRET]" build-image
    ```

4. Then `C-c` once you see the Docker build portion start.  Then rsync
   the collected external muses from the build directory into your
   local external muses directory:

    ```sh
    rsync -a builds/0/project-0/external-muses/ external-muses
    ```

5. Then kick off the Docker build manually:

    ```sh
    docker build . -t argot-server
    ```

[LSP]: https://microsoft.github.io/language-server-protocol/specifications/specification-current/
[pygls]: https://github.com/openlawlibrary/pygls
[sdks]: https://microsoft.github.io/language-server-protocol/implementors/sdks/
[Trinity]: https://gitlab.com/GrammaTech/Mnemosyne/muses/trinity/-/blob/master/muse.py
[TypeWriter]: https://gitlab.com/GrammaTech/Mnemosyne/muses/type-writer/-/blob/master/run_type_writer.py
[black]: https://black.now.sh/?version=stable&state=_Td6WFoAAATm1rRGAgAhARYAAAB0L-Wj4AA-ACxdAD2IimZxl1N_W1ktIvcnCRyzdeeGA586U8RMKbisP9D6xUd8v4usX-jR3lIAACNC8ndFJAQXAAFIPxtdQK4ftvN9AQAAAAAEWVo=
[Mnemosyne documentation]: https://grammatech.gitlab.io/Mnemosyne/docs/
