#!/usr/bin/env python3
"""fork-server

Listen on PORT and fork a new instance of ARGS, communicating via
stdio, for each connection.
"""

import argparse
import atexit
import os
import signal
import socket
import subprocess
import sys


def quiet_exit(sig, _frame):
    print("Got signal", sig, file=sys.stderr)
    sys.exit(128 + sig)


signal.signal(signal.SIGINT, quiet_exit)
signal.signal(signal.SIGQUIT, quiet_exit)
signal.signal(signal.SIGTERM, quiet_exit)
# Automatically reap child processes.
signal.signal(signal.SIGCHLD, signal.SIG_IGN)


def become_process_group():
    # Start a process group.
    if not os.getpgid(0) == os.getpid():
        os.setpgid(0, 0)
    # Kill the whole group on exit.
    atexit.register(lambda: os.killpg(0, signal.SIGTERM))


def prepare_command(prefix, command):
    load_chain = []
    if prefix:
        # We're using bash here to filter stderr with process
        # substitution (not exactly a one-liner in Python) but without
        # the need for escaping by using chain loading: the bash
        # process just does the redirection and execs its argv, there
        # is no parsing.
        load_chain = [
            "bash",
            "-c",
            """prefix="$1"; shift; exec "$@" 2> >(sed -e "s/^/${prefix}/" >&2)""",  # noqa: E501
            ":",
            prefix,
        ]
    return load_chain + command


def handle_connection(command, conn):
    pid = os.fork()

    if pid == 0:
        result = None
        try:
            b = conn.recv(2, socket.MSG_PEEK)
            # The connection is already closed.
            if len(b) == 0:
                return
            childpid = os.getpid()
            prefix = "[" + str(childpid) + "] "
            print("FORKED CHILD PROCESS", childpid, file=sys.stderr)
            prepared_command = prepare_command(prefix, command)
            result = subprocess.run(prepared_command, stdin=conn, stdout=conn)
            print(
                "SUBPROCESS",
                childpid,
                "EXITED",
                result.returncode,
                file=sys.stderr,
            )
        finally:
            # Actually shut down the connection instead of waiting for
            # GC to do it.
            try:
                conn.shutdown(socket.SHUT_WR)
            except socket.error:
                pass
            conn.close()
            # Avoid running atexit handlers in the child.
            os._exit(result and result.returncode or 0)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(add_help=False, description=__doc__)
    parser.add_argument("port", type=int, help="Port to run on")
    parser.add_argument("command", help="Command to execute")
    args, cmd_args = parser.parse_known_args()
    port = args.port
    command = [args.command] + cmd_args

    become_process_group()

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as acceptor:

        acceptor.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        acceptor.setsockopt(socket.IPPROTO_TCP, socket.TCP_QUICKACK, 1)
        acceptor.bind(("0.0.0.0", port))
        acceptor.listen(5)

        atexit.register(lambda: acceptor.close())

        print("Listening on port", port, file=sys.stderr)

        try:
            while 1:
                conn, addr = acceptor.accept()
                handle_connection(command, conn)
        except KeyboardInterrupt:
            sys.exit(130)
