Setup:
a) Start vscode.
b) Argot server is running, with ssr-muse loaded.
c) View->Command Palette, Mnemosyne: Connect to Argot Server

Demo:

1. Open file argot-server/test/etc/crypto.js.
2. Point out the three functions which look similar (md5, sha1, sha256).
3. Under "var crypto" add new line: var context = new Object(); // empty, for now
4. Above exports.md5 declaration, new line: // pass context object to library functions

// modify one function
5. Click on "md5".
6. Select command: click light bulb "Show Fixes"
7. Click "exports.md5 = ..." (expression)
8. At "Modify matching asts" prompt: Keep default string, <Enter>
9. At "Substitute ast..." prompt: add context i.e. md5(s, format, context) <Enter>
10. Verify context added to md5 function only.
11. Edit->Undo to undo change.

// now we generalize to all three functions
11. Click on "md5".
12. Select command: click light bulb "Show Fixes
13. Click "exports.md5 = ..." (expression)
14. At "Modify matching asts" prompt: change each md5 or 'md5' to wildcards
    __1, __2, __3 <Enter>
15. At "Substitute ast..." prompt: add context i.e. __2(s, format, context) <Enter>
16. Verify all 3 functions were modified with the new context addition.
17. Run program: Run->Run Without Debugging, select node.js <Enter>
18. Point out results in Debug Console

// add new log statement to each function
19. Click on "md5".
20. Select command: click light bulb "Show Fixes
21. Click "exports.md5 = ..." (expression)
22. At "Modify matching asts" prompt: change each md5 or 'md5' to wildcards
    __1, __2, __3 <Enter>
23. At "Substitute ast..." prompt: add log statement just before return statement
    i.e. console.log(__3); <Enter>
24. Run program: Run->Run Without Debugging, select node.js <Enter>
25. Point out results in Debug Console


=======================================================================================
crypto.js file contents (at start):
// from https://github.com/node-modules/utility/blob/master/lib/crypto.js
'use strict';

var crypto = require('crypto');

exports.hash = function hash(method, s, format) {
  var sum = crypto.createHash(method);
  var isBuffer = Buffer.isBuffer(s);
  if (!isBuffer && typeof s === 'object') {
    s = JSON.stringify(sortObject(s));
  }
  sum.update(s, isBuffer ? 'binary' : 'utf8');
  return sum.digest(format || 'hex');
};

exports.md5 = function md5(s, format) { return exports.hash('md5', s, format); };

exports.sha1 = function sha1(s, format) { return exports.hash('sha1', s, format); };

exports.sha256 = function sha256(s, format) { return exports.hash('sha256', s, format); };

exports.hmac = function hmac(algorithm, key, data, encoding) {
  encoding = encoding || 'base64';
  var hmac = crypto.createHmac(algorithm, key);
  hmac.update(data, Buffer.isBuffer(data) ? 'binary' : 'utf8');
  return hmac.digest(encoding);
};

exports.base64encode = function base64encode(s, urlsafe) {
  if (!Buffer.isBuffer(s)) {
    s = typeof Buffer.from === 'function' ? Buffer.from(s) : new Buffer(s);
  }
  var encode = s.toString('base64');
  if (urlsafe) {
    encode = encode.replace(/\+/g, '-').replace(/\//g, '_');
  }
  return encode;
};

exports.base64decode = function base64decode(encodeStr, urlsafe, encoding) {
  if (urlsafe) {
    encodeStr = encodeStr.replace(/\-/g, '+').replace(/_/g, '/');
  }
  var buf = typeof Buffer.from === 'function' ? Buffer.from(encodeStr, 'base64') : Buffer.from(encodeStr, 'base64');
  if (encoding === 'buffer') {
    return buf;
  }
  return buf.toString(encoding || 'utf8');
};

function sortObject(o) {
  if (!o || Array.isArray(o) || typeof o !== 'object') {
    return o;
  }
  var keys = Object.keys(o);
  keys.sort();
  var values = [];
  for (var i = 0; i < keys.length; i++) {
    var k = keys[i];
    values.push([k, sortObject(o[k])]);
  }
  return values;
}

console.log(exports.md5(Buffer.from('this is a test'), 'base64'));
console.log(exports.sha1(Buffer.from('this is a test'), 'base64'));
console.log(exports.sha256(Buffer.from('this is a test'), 'base64'));

======================================================================================
crypto.js file contents (at end):

// from https://github.com/node-modules/utility/blob/master/lib/crypto.js
'use strict';

var crypto = require('crypto');
var context = new Object(); // empty, for now

exports.hash = function hash(method, s, format) {
  var sum = crypto.createHash(method);
  var isBuffer = Buffer.isBuffer(s);
  if (!isBuffer && typeof s === 'object') {
    s = JSON.stringify(sortObject(s));
  }
  sum.update(s, isBuffer ? 'binary' : 'utf8');
  return sum.digest(format || 'hex');
};

// pass context object to library functions
exports.md5 = function md5(s, format, context) { console.log('md5'); return exports.hash('md5', s, format); };

exports.sha1 = function sha1(s, format, context) { console.log('sha1'); return exports.hash('sha1', s, format); };

exports.sha256 = function sha256(s, format, context) { console.log('sha256'); return exports.hash('sha256', s, format); };

exports.hmac = function hmac(algorithm, key, data, encoding) {
  encoding = encoding || 'base64';
  var hmac = crypto.createHmac(algorithm, key);
  hmac.update(data, Buffer.isBuffer(data) ? 'binary' : 'utf8');
  return hmac.digest(encoding);
};

exports.base64encode = function base64encode(s, urlsafe) {
  if (!Buffer.isBuffer(s)) {
    s = typeof Buffer.from === 'function' ? Buffer.from(s) : new Buffer(s);
  }
  var encode = s.toString('base64');
  if (urlsafe) {
    encode = encode.replace(/\+/g, '-').replace(/\//g, '_');
  }
  return encode;
};

exports.base64decode = function base64decode(encodeStr, urlsafe, encoding) {
  if (urlsafe) {
    encodeStr = encodeStr.replace(/\-/g, '+').replace(/_/g, '/');
  }
  var buf = typeof Buffer.from === 'function' ? Buffer.from(encodeStr, 'base64') : Buffer.from(encodeStr, 'base64');
  if (encoding === 'buffer') {
    return buf;
  }
  return buf.toString(encoding || 'utf8');
};

function sortObject(o) {
  if (!o || Array.isArray(o) || typeof o !== 'object') {
    return o;
  }
  var keys = Object.keys(o);
  keys.sort();
  var values = [];
  for (var i = 0; i < keys.length; i++) {
    var k = keys[i];
    values.push([k, sortObject(o[k])]);
  }
  return values;
}

console.log(exports.md5(Buffer.from('this is a test'), 'base64'));
console.log(exports.sha1(Buffer.from('this is a test'), 'base64'));
console.log(exports.sha256(Buffer.from('this is a test'), 'base64'));
