(defpackage :argot-server/project-root
  (:use :gt/full
        :argot-server/readtable
        :argot
        :lsp-server
        :lsp-server/protocol
        :lsp-server/utilities/lsp-utilities
        :argot-server/utility
        :argot-server/choices)
  (:import-from :jsonrpc/connection
                :connection
                :*connection*)
  (:export :project-root-for
           :extract-project-root-for
           :read-project-root
           :ask-for-project-root
           :extract-workspace-folders
           :workspace-folders
           :snapshot-project
           :write-project-snapshot))
(in-package :argot-server/project-root)
(in-readtable argot-readtable)

(-> project-root-for (string) (values string &optional))
(defun project-root-for (uri)
  "Get the project root for URI.
If an appropriate root was specified in the initialization options,
use that. Otherwise use the directly containing directory."
  (or (extract-project-root-for uri)
      (file-path-uri
       (fmt "/~{~a~^/~}"
            (rest
             (assure (cons (eql :absolute) t)
               (pathname-directory
                (pathname-directory-pathname
                 (file-uri-pathname uri)))))))))

(defun extract-project-root-for (uri &optional (params (initialize-params-safe)))
  "Extract a project root from the initialization params."
  (when params
    (find-if (op (string^= _ uri))
             ;; Longest matches first.
             (sort-new (extract-workspace-folders params) #'>
                       :key #'length))))

(defun extract-workspace-folders (params)
  "Extract the workspace folder URIs from an InitializeParams instance.
Also include rootUri or rootPath if they are present (but not both)."
  (check-type params |InitializeParams|)
  (nub
   (append
    (mapcar (op (slot-value _ '|uri|))
            (slot-value-safe params '|workspaceFolders|))
    (unsplice
     ;; "If both `rootPath` and `rootUri` are set `rootUri` wins."
     (or (slot-value-safe params '|rootUri|)
         (when-let (root-path (slot-value-safe params '|rootPath|))
           (string+ "file://"
                    ;; TODO Is this enough for Windows?
                    (substitute #\/ #\\ root-path))))))))

(-> workspace-folders (&key (:connection connection))
    (values (soft-list-of string) &optional))
(defun workspace-folders (&key ((:connection *connection*)
                                (current-connection)))
  "Extract the workspace folders of the current connection."
  (when-let (params (initialize-params-safe))
    (extract-workspace-folders params)))

(defun test-uri/default (uri)
  "Default predicate for testing if a URI is one we want."
  (let ((type (pathname-type (file-uri-pathname uri))))
    (or (not (stringp type))
        (string-case type
          (("jpeg" "jpg" "png" "gif"
                   ;; VSCode can't open these.
                   "asar"
                   ;; E.g. the source tree for Git.
                   "gpg")
           nil)
          (t t)))))

;;; NB Currently `snapshot-project' generally clocks in at under a
;;; minute for most reasonably sized projects; for reference
;;; transferring the entire Linux kernel takes ~15 minutes. This is
;;; practical for our current needs but may need to be revisited if we
;;; need to support very large projects.

(-> snapshot-project (&key (:root (or null string))
                           (:test function)
                           (:exclude-default t))
    (values hash-table float &optional))
(defun snapshot-project (&key root
                           (test (constantly t))
                           (exclude-default t))
  "Snapshot the current project by requesting each file individually.

Returns a hash table where the keys are URIs and the values are
plists.

This is meant to be used by muses that wrap external tools that expect
access to an on-disk project.

ROOT can be used to override the default root.

TEST can be a predicate that returns T if a URI is wanted.

EXCLUDE-DEFAULT means to disable any default exclusions."
  (message "Snapshotting project source...")
  (fbind ((test
           (if exclude-default
               (conjoin #'test-uri/default test)
               test)))
    (let ((start-time (get-internal-real-time))
          (snapshot (make-hash-table :test 'equal)))
      (handler-bind ((error
                      (lambda (e)
                        (declare (ignore e))
                        (message "Snapshot failed"))))
        (let ((uris (reshuffle (list-workspace-uris :root root))))
          (do-each (uri uris)
            (when (test uri)
              (ignore-some-conditions (jsonrpc:jsonrpc-error)
                (when-let (contents (get-file-contents uri))
                  (setf (gethash uri snapshot)
                        (list :uri uri
                              :root (project-root-for uri)
                              :string contents))))))))
      (let ((elapsed
             (float (/ (- (get-internal-real-time)
                          start-time)
                       internal-time-units-per-second))))
        (message "Snapshot complete in ~a second~:p (~a file~:p)"
                 elapsed
                 (hash-table-count snapshot))
        (values snapshot elapsed)))))

(-> write-project-snapshot (hash-table directory-pathname)
    directory-pathname)
(defun write-project-snapshot (snapshot dir)
  "Write SNAPSHOT, a hash table from URIs to plists including file contents, into DIR."
  (flet ((get-root ()
           (let ((roots
                  (filter-map [(distinct :test #'equal) {getf _ :root}]
                              (hash-table-values snapshot))))
             (when (rest roots)
               (error "Multiple roots in snapshot: ~a" roots))
             (only-elt roots))))
    (ensure-directories-exist dir)
    (assert (null (directory-files dir)))
    (if (zerop (hash-table-count snapshot)) dir
        (let ((root
               (ensure-directory-pathname
                (file-uri-pathname
                 (get-root)))))
          (do-hash-table (uri plist snapshot dir)
            (let* ((abspath (file-uri-pathname uri)))
              (assert (subpathp abspath root))
              (let* ((relpath (enough-pathname abspath root))
                     (dest (path-join dir relpath)))
                ;; Refuse to write outside the destination directory (e.g. if
                ;; the URI includes a `..' directory component.
                (assert (subpathp dest dir))
                (ensure-directories-exist dest)
                (write-string-into-file (getf plist :string) dest))))))))
