# Set personal or machine-local flags in a file named local.mk
ifneq ("$(wildcard local.mk)","")
include local.mk
endif

.PHONY: konvert chart

all: argot-server

PACKAGE_NAME = argot-server

LISP_DEPS = \
	$(wildcard *.lisp) \
	$(wildcard lens/*.lisp) \
	$(wildcard muses/*.lisp) \
	$(wildcard utility/*.lisp) \

export DOCKER_BUILDKIT = 1
export DOCKER_COMPOSE_CLI_BUILD = 1

include .cl-make/cl.mk

GPU := $${GPU:+-f docker-compose.gpu.yaml}

# We build the argot-server in two steps.  First, we quickload the system
# to compile the requisite fasl files.  Once compiled, we reload the fasl
# files and dump an image.  This works around an issue [1] where
# foreign pointers are serialized as part of the image dumping process
# when these steps are combined, leading to a corrupted image.
#
# [1] https://git.grammatech.com/synthesis/sel/-/issues/178
argot-server: $(LISP_DEPS) $(MANIFEST)
	@rm -f $@
	CC=$(CC) $(LISP_HOME) LISP=$(LISP) $(LISP) $(LISP_FLAGS) \
	--load $(QUICK_LISP)/setup.lisp \
	--eval "(setf uiop/image::*lisp-interaction* nil)" \
	--eval "(ql:quickload :argot-server)" \
	--eval "(quit)"
	CC=$(CC) $(LISP_HOME) LISP=$(LISP) $(LISP) $(LISP_FLAGS) \
	--load $(QUICK_LISP)/setup.lisp \
	--eval "(setf uiop/image::*lisp-interaction* nil)" \
	--eval "(progn #+sb-core-compression (defmethod asdf:perform ((o asdf:image-op) (c asdf:system)) (uiop:dump-image (asdf:output-file o c) :executable t :compression 9)))" \
	--eval "(asdf:make :argot-server :type :program :monolithic t)" \
	--eval "(quit)"

# TODO Konvert cannot currently handle the GPU configuration.
konvert: docker-compose.yaml docker-compose.bridge.yaml
	kompose convert -c -f docker-compose.yaml -f docker-compose.bridge.yaml -o mnemosyne

chart:
	helm package mnemosyne

images:
	docker-compose \
		-f docker-compose.yaml \
		-f docker-compose.host.yaml \
		build

host-up:
	docker-compose pull
	docker-compose \
		-f docker-compose.yaml \
		-f docker-compose.host.yaml \
		$(GPU) \
		up -d --no-build

dev-up:
	docker-compose pull
	docker-compose \
		-f docker-compose.yaml \
		-f docker-compose.host.yaml \
		-f docker-compose.dev.yaml \
		up -d --no-build

up:
	docker-compose pull
	docker-compose \
		-f docker-compose.yaml \
		-f docker-compose.bridge.yaml \
		$(GPU) \
		up -d --no-build

down:
	docker-compose down
	docker-compose rm -f

build-tag-push:
	DOCKER_COMPOSE_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose -f docker-compose.yaml -f docker-compose.host.yaml build $(BUILD_ARGS)
	docker-compose push
	cl -Q -s argot-server/utility "(argot-server/utility:push-latest)"
