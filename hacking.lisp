(defpackage :argot-server/hacking
  (:documentation "User package for hacking (and debugging) at the REPL.")
  (:use :gt/full
        :yason
        :jsonrpc
        :lsp-server
        :lsp-server/protocol
        :software-evolution-library
        :software-evolution-library/software/parseable
        :software-evolution-library/software/tree-sitter
        :software-evolution-library/software/python
        :software-evolution-library/software/javascript
        :software-evolution-library/software/typescript
        :software-evolution-library/software/c
        :software-evolution-library/software/cpp
        :argot-server/utility
        :argot-server/passthrough
        :argot-server/muses/muse
        :argot-server/muses/cl-lsp
        :argot-server/muses/refactoring-mutations
        :argot-server/muses/trinity
        :argot-server/muses/incrementalizer
        #+lambdanet :argot-server/muses/lambdanet
        :argot-server/muses/autocomplete
        :argot-server/muses/hypothesis-tests
        :argot-server/muses/snippet-mining
        #+regel :argot-server/muses/regel
        :argot-server/muses/genpatcher
        :argot-server/muses/dig
        :argot-server/muses/libfuzzer
        :argot-server/muses/type-view
        :argot-server/muses/python-type-view
        :argot-server/muses/typescript-type-view
        :argot-server/muses/jsdoc-type-view
        :argot-server/muses/typewriter
        :argot-server/muses/test-view
        :argot-server/muses/ssr-muse
        :argot-server/muses/pyls
        :argot-server/muses/jsls
        :argot-server/muses/sygus
        :argot-server/muses/herbie
        :argot-server/muses/godbolt-muse
        :argot-server/muses/erroir)
  (:shadowing-import-from :yason :true)
  (:import-from :software-evolution-library/software/tree-sitter
                :whitespace-between
                :whitespace-between/parent
                :pep8
                :output-transformation)
  (:import-from :argot-server/server
                :launch-server
                :server-handle-stop))
(in-package :argot-server/hacking)
