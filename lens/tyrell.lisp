(defpackage :argot-server/lens/tyrell
  (:documentation "Lenses for the Tyrell sexp DSL, used by Trinity.")
  (:use :gt/full
        :fresnel/fresnel
        :software-evolution-library/software/tree-sitter
        :fresnel/readtable)
  (:local-nicknames (:l :fresnel/lens)
                    (:py :argot-server/lens/python)
                    (:syms :argot-server/lens/tyrell-symbols))
  (:shadowing-import-from :fresnel/lens :seq :map)
  (:export :parser :translator
           :python-translator))
(in-package :argot-server/lens/tyrell)
(in-readtable lens-readtable)

(defreadtable tyrell-readtable
  (:merge :standard)
  (:case :preserve))

(defun parser ()
  (let ((p (find-package :argot-server/lens/tyrell-symbols))
        (r (find-readtable 'tyrell-readtable)))
    (l:bij (lambda (c)
             (let ((*package* p)
                   (*readtable* r)
                   (*read-default-float-format* 'double-float)
                   (*read-eval* nil))
               (read-from-string c)))
           (lambda (c)
             (let ((*package* p)
                   (*readtable* r)
                   (*read-default-float-format* 'double-float))
               (prin1-to-string c))))))

(defun param-lens ()
  (l:bij (lambda (x)
           (when (keywordp x)
             (when-let (pos (position x '(|x| |y| |z|)
                                      :test #'string=))
               `(syms::|@param| ,pos))))
         (lambda (x)
           (match x
             (`(syms::|@param| ,n)
               (nth n '(:|x| :|y| :|z|)))))))

(defparameter *term-table*
  '(("+" . syms::|plus|)
    ("-" . syms::|minus|)
    ("*" . syms::|mult|)
    ("/" . syms::|div|)))

(defun op-lens ()
  (alist-lens *term-table* :test #'string=))

(defun binop-lens ()
  (l:match ((op (op-lens))
            (left (translator))
            (right (translator)))
    (list op left right)
    (list op left right)))

(defun bool-lens ()
  (l:match ()
    t
    (list 'syms::|BoolLit| "True")

    :false
    (list 'syms::|BoolLit| "False")))

(defun int-lens ()
  (l:match ((n (l:check-type 'integer)))
    n
    (list 'syms::|IntConst| n)))

(defun const-lens ()
  (l:match ((c (l:or (bool-lens)
                     (int-lens)
                     (read-print-lens 'integer))))
    c
    (list 'syms::|const| c)))

(defun translator ()
  (l:or (const-lens)
        (param-lens)
        (binop-lens)
        (bool-lens)
        (int-lens)))

(defun python-translator ()
  (l:compose (translator)
             (py:translator)))
