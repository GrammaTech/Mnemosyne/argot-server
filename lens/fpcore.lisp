(uiop:define-package :argot-server/lens/fpcore
    (:documentation "Lens for translating between Python sexps and FPCore.")
  (:use :gt/full
        :software-evolution-library/software/tree-sitter
        :software-evolution-library/software/parseable
        :fresnel/readtable)
  (:shadowing-import-from :serapeum :~> :collecting)
  (:import-from :argot-server/lens/fpcore-symbols)
  (:local-nicknames (:l :fresnel/lens)
                    (:fr :fresnel/fresnel)
                    (:fp :argot-server/lens/fpcore-builtins)
                    (:py :argot-server/lens/python))
  (:export :fpcore-lens
           :fpcore-variable?
           :with-fpcore-io
           :fpcore-error))
(in-package :argot-server/lens/fpcore)

(in-readtable lens-readtable)

(def +working-package+
  (find-package :argot-server/lens/fpcore-symbols)
  "Package we can intern parameters in.")

(def +builtins-package+
  (find-package :argot-server/lens/fpcore-builtins)
  "Package that exports FPCore builtins.")

(defreadtable fpcore
  (:merge :standard)
  (:case :preserve))

(define-condition fpcore-error (simple-error)
  ())

(defun call/fpcore-io (fn)
  "Call FN with an appropriate dynamic environment for FPCore I/O."
  (let ((*package* +working-package+)
        (*readtable* (find-readtable 'fpcore))
        (*read-default-float-format* 'double-float)
        (*read-eval* nil)
        (*read-base* 10))
    (funcall fn)))

(defmacro with-fpcore-io ((&key) &body body)
  "Run BODY with an appropriate environment for FPCore I/O."
  (with-thunk (body)
    `(call/fpcore-io ,body)))

(defun fpcore-variable? (x)
  "Is X an variable in an FPCore expression?
That is, a symbol that is not an FPCore builtin."
  (and (symbolp x)
       (eql (symbol-package x) +working-package+)))

(defun fpcore-builtin? (x)
  "Is X something that designates an FPCore builtin?"
  (etypecase x
    (string (find-external-symbol x +builtins-package+))
    (symbol (fpcore-builtin? (string x)))
    (ast (fpcore-builtin? (source-text x)))))

(-> resolve-fpcore-builtin (string &key (:error t)) symbol)
(defun resolve-fpcore-builtin (string &key ((:error errorp) nil))
  "Resolve STRING to an FPCore builtin (symbol)."
  (or (find-symbol string +builtins-package+)
      (and errorp
           (error 'fpcore-error
                  :format-control (formatter "No such FPCore sym as ~a")
                  :format-arguments string))))

(defun intern-fpcore-symbol (string)
  "Intern STRING as an FPCore symbol, either a builtin or a symbol in
the working package."
  (or (resolve-fpcore-builtin string :error nil)
      (values (intern string +working-package+)
              t)))

(defun op-name? (x)
  "Is X the name of a (unary, binary) operator?
An operator name has no alphanumeric characters."
  (and (typep x 'string-designator)
       (not (emptyp (string x)))
       (notany #'alpha-char-p (string x))))

(defun function-name? (x)
  "Is X the name of a function?
Function names are FPCore builtins that are not operator names."
  (and (or (symbolp x) (stringp x))
       (fpcore-builtin? x)
       (not (op-name? x))))

(defun comparison? (x)
  "Is X the name of an FPCore comparison operator?
E.g. <, >, <=, etc.

Note that FPCore uses == for equality and != for inequality \(vs CL =
and /=)."
  (and (typep x 'string-designator)
       (member x '(< <= > >= fp:== fp:!=)
               :test #'string-equal)))

(defun number-lens ()
  "Match a number."
  (l:check (of-type '(or integer float))))

(defun fpcore-symbol-canonizer ()
  "Translate between keywords and FPCore symbols, when possible."
  (l:canonizer
   (lambda (a)
     (and a
          (symbolp a)
          (if (eql (symbol-package a) (find-package :cl))
              a
              (make-keyword (string a)))))
   (lambda (a)
     ;; Second value as intern may return a second value of nil.
     (values (intern-fpcore-symbol (string a))
             t))))

(defun fpcore-math-canonizer (&key (math "math"))
  "Canonize symbols by stripping the `math.' prefix, and converting
Python constant names into Herbie constant names."
  (l:canonizer
   #'identity
   (lambda (a &aux (math. (string+ math ".")))
     (and (symbolp a)
          (string^= math. a)
          (let ((suffix (drop-prefix math. (string a))))
            (values
             (make-keyword
              (string-case suffix
                ;; Python constants to Herbie constants.
                ("pi" "PI")
                ("e" "E")
                ("inf" "INFINITY")
                ("nan" "NAN")
                (t suffix)))
             t))))))

(defun fpcore-symbol-lens ()
  "Converts between uninterned symbols and FPCore symbols."
  (l:rquot
   (fpcore-symbol-canonizer)
   (l:check-type '(and symbol (not null)))))

(defun boolean-lens ()
  "Match a boolean."
  (l:match ()
    t 'fp:true
    :false 'fp:false))

(defun attribute-lens (&key (math "math"))
  "Match an attribute name.
The convention used here is that attribute names are represented on
the FPCore side as variables with dots in their names; e.g. `x.im' on
the Python side becomes `|x.im|' on the FPCore side.

The exception is methods on the `math` object: these lose their prefix
so FPCore recognizes them as built-ins."
  (l:rquot*
   (fpcore-symbol-canonizer)
   (fpcore-math-canonizer :math math)
   (l:bij
    (lambda (a)
      (match a
        ((list :dot
               (and obj (type symbol))
               (and att (type symbol)))
         (values (make-keyword (fmt "~a.~a" obj att))
                 t))))
    (lambda (a)
      (and (symbolp a)
           (position #\. (string a))
           (cons :dot (mapcar #'make-keyword
                              (split-sequence #\. (string a)))))))))

(defun op-lens ()
  "Match an operator name."
  (l:guard #'op-name? (fpcore-symbol-lens)))

(defun unop-lens ()
  "Match a unary operator expression."
  (l:lazy-list (op-lens) (fpcore-lens)))

(defparameter *renamed-binops*
  '((:** . fp:|pow|)
    (:% . fp:|remainder|)
    (:// . fp:|floor|)))

(defun binop-lens ()
  "Match a binary operator expression."
  (l:lazy-list
   (l:or
    (l:guard #'symbolp
             (fr:alist-lens *renamed-binops* :test #'string=))
    (op-lens))
   (fpcore-lens)
   (fpcore-lens)))

(defun fun-lens ()
  "Match the name of a function call."
  (l:compose* (l:check #'function-name?)
              (l:or (fpcore-symbol-lens)
                    (attribute-lens))))

(defun call-lens ()
  "Match a function call."
  (l:lazy-list* (fun-lens) (l:mapcar (fpcore-lens))))

(defun comparison-op-lens ()
  "Match a comparison operator."
  (l:guard #'comparison? (fpcore-symbol-lens)))

(defun comparison-lens ()
  (l:lazy-list (comparison-op-lens) (fpcore-lens) (fpcore-lens)))

;; Note that FPCore `if' is Racket-style, with both branches required.

(defun if-lens ()
  "Match an if-elsif-else statement."
  ;; Should this be expressible with lens combinators? Would it
  ;; be any clearer?
  (l:lquot
   (l:canonizer
    (lambda (c)
      (match c
        (`(cond ,@_)
          (cons 'cond
                (mapcar (lambda (clause)
                          (match clause
                            (`(,test (block nil (progn ,expr)))
                              (list test expr))
                            (otherwise clause)))
                        (cdr c))))))
    (lambda (c)
      (cons 'cond
            (mapcar (op (list (car _1)
                              `(block nil (progn ,(cadr _1)))))
                    (cdr c)))))
   (l:bij (named-lambda rec (c)
            (match c
              (`(cond (t ,expr))
                (l:with-get-lenses ((expr (fpcore-lens)))
                  expr))
              (`(cond (,_ ,_))
                ;; Unrepresentable?
                nil)
              (`(cond (,test ,expr)
                      ,@clauses)
                (l:with-get-lenses ((test (fpcore-lens))
                                    (expr (fpcore-lens)))
                  `(fp:|if| ,test ,expr
                       ,(rec `(cond ,@clauses)))))))
          (lambda (a)
            (nlet rec ((a a)
                       (ifs nil))
              (match a
                (`(fp:|if| ,test ,then ,else)
                  (l:with-create-lenses ((test (fpcore-lens))
                                         (then (fpcore-lens)))
                    (rec else
                         (cons `(,test ,then)
                               ifs))))
                (otherwise
                 (if (no ifs) nil
                     `(cond
                        ,@(reverse ifs)
                        (t ,(l:create (fpcore-lens) a)))))))))))

;;; TODO Get the right prefix from the imports.
(defun fpcore-lens (&key (math "math"))
  (l:or (number-lens)
        (attribute-lens :math math)
        (boolean-lens)
        (fpcore-symbol-lens)
        (unop-lens)
        (comparison-lens)
        (binop-lens)
        (if-lens)
        (call-lens)
        (comparison-lens)))
