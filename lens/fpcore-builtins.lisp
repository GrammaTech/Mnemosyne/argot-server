(defpackage :argot-server/lens/fpcore-builtins
  (:documentation "Package for FPCore builtins only. Do not intern here!")
  ;; We can re-export the non-alphanumeric CL symbols.
  (:use :cl)
  (:export
    :!=
    :*
    :+
    :-
    :/
    :2_SQRTPI                           ;2/sqrt(pi)
    :<
    :<=
    :==
    :>
    :>=
    :E
    :FALSE
    :INFINITY
    :LN10
    :LN2
    :LOG10E
    :LOG2E
    :M_1_PI                             ;1/pi
    :M_2_PI                             ;2/pi
    :M_2_SQRTPI                         ;1/sqrt(2)
    :NAN
    :PI
    :PI_2                               ;pi/2
    :PI_4                               ;pi/4
    :SQRT1_2
    :SQRT2
    :TRUE
    :|FPCore|
    :|acosh|
    :|acos|
    :|and|
    :|and|
    :|asinh|
    :|asin|
    :|atan2|
    :|atanh|
    :|atan|
    :|cbrt|
    :|ceil|
    :|copysign|
    :|cosh|
    :|cos|
    :|erfc|
    :|erf|
    :|exp2|
    :|expm1|
    :|exp|
    :|fabs|
    :|fdim|
    :|floor|
    :|fmax|
    :|fma|
    :|fmin|
    :|fmod|
    :|hypot|
    :|if|
    :|isfinite|
    :|isinf|
    :|isnan|
    :|isnormal|
    :|lgamma|
    :|log10|
    :|log1p|
    :|log2|
    :|log|
    :|nearbyint|
    :|not|
    :|not|
    :|or|
    :|or|
    :|pow|
    :|remainder|
    :|round|
    :|signbit|
    :|sinh|
    :|sin|
    :|sqrt|
    :|tanh|
    :|tan|
    :|tgamma|
    :|trunc|))
(in-package :cl-user)
;;; Package-inferred systems don't allow :lock.
#+sbcl
(cl:eval-when (:compile-toplevel :load-toplevel :execute)
  (sb-ext:lock-package :argot-server/lens/fpcore-builtins))
