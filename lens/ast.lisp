(defpackage :argot-server/lens/ast
  (:use :gt/full
        :software-evolution-library/software/tree-sitter
        :software-evolution-library/software/parseable)
  (:import-from :software-evolution-library
                :genome :software)
  (:import-from :software-evolution-library/software/tree-sitter
                :before-text :after-text :structured-text)
  (:shadowing-import-from :serapeum
                          :collecting)
  (:local-nicknames (:l :fresnel/lens)
                    (:fr :fresnel/fresnel))
  (:export :terminal-symbol-lens
           :translate-partially
           :precedence-table
           :precedence
           :precedence>
           :precedence-canonizer
           :propagate-before-and-after-text
           :save-before-after-text
           :whitespace-canonizer
           :clean-whitespace)
  (:documentation "AST lenses and lens-related functions that are not
  language-specific."))
(in-package :argot-server/lens/ast)
(in-readtable :curry-compose-reader-macros)

(defun save-before-after-text (lens)
  (l:compose lens (propagate-before-and-after-text)))

(defun propagate-before-and-after-text ()
  (l:lens
   #'identity
   (lambda (a c)
     (match* (a c)
       (((type structured-text) (type structured-text))
        (copy a
              :before-text (before-text c)
              :after-text (after-text c)))
       (otherwise a)))
   #'identity))

(defclass precedence-table ()
  ((operators :initarg :operators))
  (:default-initargs :operators nil))

(defgeneric precedence (precedence-table operator)
  (:method ((p precedence-table) o)
    (with-slots (operators) p
      (or (position o operators :test {member _ :test #'string-equal})
          -1))))

(defgeneric precedence> (table operator1 operator2)
  (:method ((p precedence-table) o1 o2)
    (< (precedence p o2) (precedence p o1))))

(defun precedence-canonizer (language precedence-table
                             &key
                               (class
                                (only-elt
                                 (ast-mixin-subclasses
                                  'parenthesized-expression-ast
                                  language))))
  (nest
   (l:canonizer #'identity)
   (lambda (c))
   (flet ((wrap-parens (sub)
            (if-let ((sub-op
                      (typecase sub
                        (binary-ast (operator sub))
                        (unary-ast (string+ (operator sub) "x")))))
              (if (precedence> precedence-table
                               (operator c)
                               sub-op)
                  ;; Chop off any preceding whitespace that would
                  ;; appear after the opening paren.
                  (labels
                      ((trim! (sub)
                         (callf {string-left-trim " "} (before-text sub))
                         (when-let (child (first (children sub)))
                           (trim! child))
                         sub))
                    (make class :children (list (trim! (tree-copy sub)))))
                  sub)
              sub))))
   (ematch c
     ((binary-ast
       (lhs left)
       (rhs right))
      (copy c
            :lhs (wrap-parens left)
            :rhs (wrap-parens right))))))

(defun terminal-symbol-lens (prefix)
  "Lens to translate between terminal symbols (instances of
`sel/sw/ts:terminal-symbol') and strings."
  (assert (not (string$= '- prefix)))
  (l:bij (lambda (x)
           (and (typep x 'terminal-symbol)
                (string-downcase
                 (source-text x))))
         (lambda (x)
           (and-let* (((stringp x))
                      (sym (find-symbol
                            (concatenate 'string
                                         (string prefix)
                                         "-"
                                         ;; E.g. python-or.
                                         (string-upcase x))
                            :sel/sw/ts))
                      (class (find-class sym nil))
                      ((subtypep class 'terminal-symbol)))
             (make class :text (string x))))))

(defgeneric translate-partially (obj lens)
  (:documentation
   "Do partial translation of OBJ using LENS.
Return an alist of paths and translations.

Walk OBJ's AST, looking for subtrees that can be translated by LENS.
When a matching subtree is found, collect a cons of the path of the
subtree and the translation.

One way to use this information might be to do further
operations (e.g. synthesis) on the translation and then use the paths
to reinsert them back into the original AST.")
  (:method ((obj sel:software) (lens l:qlens))
    (translate-partially (sel:genome obj) lens))
  (:method ((root ast) (lens l:qlens))
    (collecting
     (labels ((rec (ast)
                (when (typep ast 'ast)
                  (if-let (abstract (l:get lens ast))
                    (collect (cons (ast-path root ast)
                                   abstract))
                    (mapc #'rec (children ast))))))
       (rec root)))))

(defun whitespace-canonizer ()
  (l:canonizer
   #'identity
   #'patch-whitespace))

(defun clean-whitespace ()
  (whitespace-canonizer))
