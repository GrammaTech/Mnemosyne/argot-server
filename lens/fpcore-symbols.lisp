(uiop:define-package :argot-server/lens/fpcore-symbols
    (:documentation "Package you can freely intern in.")
  (:use-reexport :argot-server/lens/fpcore-builtins))
