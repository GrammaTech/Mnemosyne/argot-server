(uiop:define-package :argot-server/lens/python
    (:documentation
     "Translate Python ASTs into a simple but lossless S-expression
   syntax that can be used as-is or as a basis for further
   translations. Note that Python identifiers are translated as
   keywords to avoid any drama over which package to intern in.")
  (:use :gt/full
        :software-evolution-library/software/tree-sitter
        :software-evolution-library/software/template
        :software-evolution-library/software/python
        :fresnel/readtable
        :argot-server/lens/ast)
  (:import-from :software-evolution-library/software/parseable
                :indentation
                :indent-children
                :source-text)
  (:local-nicknames (:l :fresnel/lens)
                    (:fr :fresnel/fresnel))
  (:shadowing-import-from :fresnel/lens :seq :map)
  (:export :string-lit-term
           :parser
           :bool-lit-term
           :int-lit-term
           :double-lit-term
           :lit-term
           :name-term
           :unop
           :binop
           :lambda-node
           :call-app
           :expr
           :var-decl
           :func-decl
           :decl
           :translator
           :python-precedence-canonizer
           :strip-parens
           :if-statement
           :elif-clause
           :else-clause
           :block-lens
           :expression-lens
           :statement
           :attribute-node
           :term-sym
           :module))
(in-package :argot-server/lens/python)
(in-readtable lens-readtable)

(defparameter *precedence*
  '((":=")
    (or)
    (and)
    (notx)                              ;unary
    (in "not in" is "is not" < <= > >= != ==)
    ("|")
    (^)
    (&)
    (<< >>)
    (+ -)
    (* / @ // %)
    (+x -x ~x)                          ;unary
    (**))
  "Symbols in ascending order of precedence.
We only care about the string value, don't worry about the package.

Unary operators get an `x' suffix before being looked up.")

(defparameter *comparisons*
  '(< <= > >= != ==))

(assert (subsetp *comparisons* (flatten *precedence*)))

(defun stringify (x)
  (etypecase x
    (string-designator (string x))
    (tree-sitter-ast (source-text x))))

(defun comparison? (x)
  (member (stringify x) *comparisons* :test #'string=))

(defun boolean-op? (x)
  (member (stringify x) '(or and not) :test #'string-equal))

(defun python-precedence-canonizer ()
  "Canonize a binary operator by wrapping left or right
operations (unary or binary) of lower precedence in parens."
  (precedence-canonizer
   'python
   (make 'precedence-table :operators *precedence*)
   :class (class-of (convert 'python-ast "(2+2)" :deepest t))))

(defun parser ()
  "Return a lens to convert between Python source code strings and ASTs."
  (l:bij {convert 'python-ast} #'source-text))

(defun strip-parens ()
  "Canonizer that strips parentheses."
  (l:canonizer (lambda (c)
                 (match c
                   ((parenthesized-expression-ast
                     :children `(,child))
                    child)))
               #'identity))

(def +string-quotes+
  '("\"\"\"" "'''" "\"" "'"))

(defun string-quotes (string)
  (let ((delim (find-if {string$= _ string} +string-quotes+)))
    (values
     ;; Capture prefixes like u'', r'', etc.
     (subseq string 0 (+ (search delim string) (length delim)))
     delim)))



;;; Lenses

(defun string-lit-term ()
  "Match Python string literals.
Note that this translates string literals with their surrounding
quotes intact."
  (l:match ((string (l:check-type 'string)))
    (make 'python-string :text string)
    string))

(defun quoted-string-term (&key (default "\""))
  "Match a Python string literal (without its quotes)."
  ;; TODO It would be nice to have more combinators/patterns for
  ;; working with strings.
  (l:compose
   (l:lens (lambda (c)
             (when (stringp c)
               (mvlet ((q1 q2 (string-quotes c)))
                 (slice c (length q1) (- (length q2))))))
           (lambda (a c)
             (when (stringp a)
               (if (stringp c)
                   (mvlet ((q1 q2 (string-quotes c)))
                     (string+ q1 a q2))
                   (string+ default a default))))
           (lambda (a)
             (when (stringp a)
               (string+ default a default))))
   (string-lit-term)))

(defun bool-lit-term ()
  "Return a lit-term to convert between Python and Lisp boolean literals.
Note that false is represented as `:false' rather than nil."
  (l:match ()
    (python "True")
    t

    (python "False")
    :false))

(defun int-lit-term ()
  "Return a lens to convert between Python and Lisp integer literals."
  (l:match ((n (fr:read-print-lens 'integer)))
    (make 'python-integer :text n)
    n))

(defun double-lit-term ()
  "Return a lens to convert between Python and Lisp floating-point literals."
  (l:match ((double-float (fr:read-print-lens 'double-float)))
    (make 'python-float :text double-float)
    double-float))

(defun lit-term ()
  "Return a lens to convert between Python and Lisp literals."
  (l:or (quoted-string-term)
        (bool-lit-term)
        (int-lit-term)
        (double-lit-term)))

(defun name-term ()
  "Translate Python identifiers and keywords."
  (l:match ((name (fr:keyword-lens)))
    (make 'python-identifier :text name)
    name))

(defun term-sym ()
  "Match a Python terminal symbol (subclass of `terminal-symbol')."
  (l:rquot
   (l:canonizer
    (lambda (x)
      (and (symbolp x)
           (string-downcase x)))
    (lambda (x)
      (and (stringp x)
           (values (or (find-symbol x :cl)
                       (make-keyword x))
                   t))))
   (terminal-symbol-lens 'python)))

(defun unop ()
  "Match Python unary expressions."
  (l:match ((arg (expr)))
    (python "-$1" arg) `(- ,arg)
    (python "+$1" arg) `(+ ,arg)
    (python "~$1" arg) `(lognot ,arg)))

(defun not-lens ()
  "Match Python unary expressions."
  (l:match ((arg (expr)))
    (python "not $1" arg)
    `(not ,arg)))

(defun binop ()
  "Match Python binary expressions."
  (l:lquot
   (python-precedence-canonizer)
   (l:match ((left (expr))
             (op (term-sym))
             (right (expr)))
     (make 'python-binary-operator
           :python-left left
           :python-right right
           :python-operator op)
     `(,op ,left ,right))))

(defun boolean-binop ()
  (l:lquot*
   (python-precedence-canonizer)
   (l:match ((left (expr))
             (right (expr)))
     (python "$1 and $2" left right)
     `(and ,left ,right)

     (python "$1 or $2" left right)
     `(or ,left ,right))))

;; TODO Support chained comparisons.
(defun comparison ()
  "Match Python (binary) comparisons."
  (l:match ((left (expr))
            (op (l:rguard #'comparison? (term-sym)))
            (right (expr)))
    (make 'python-comparison-operator
          :children `(,left ,right)
          :python-operators `(,op))
    `(,op ,left ,right)))

(defun lambda-node ()
  "Match Python lambdas."
  (l:match ((params (l:mapcar (name-term)))
            (body (expr)))
    (python "lambda @PARAMS: $BODY"
            :body body
            :params params)
    `(lambda (,@params) ,body)))

(defun parameters-lens ()
  "Translate Python function parameters."
  (l:or
   (l:match ()
     (make 'python-empty-parameters)
     (list))
   (l:match ((params (l:mapcar (name-term))))
     (make 'python-parameters
           :children params)
     `(,@params))))

(defun pass-lens ()
  "Translate Python `pass' statements."
  (l:match ()
    (python "pass")
    '(:pass)))

(defun call-app ()
  "Translate Python function/method calls."
  (l:match ((callee (expr))
            (arguments (l:mapcar (expr))))
    (python "$CALLEE(@ARGUMENTS)"
            :callee callee
            :arguments arguments)
    `(,callee ,@arguments)))

(defun attribute-node ()
  "Translate Python attribute nodes."
  (l:match ((o (translator))
            (a (translator)))
    (python "$O.$A" :o o :a a)
    (list :dot o a)))

(defun term ()
  "Translate Python terminals."
  (l:or (lit-term)
        (name-term)))

(defun expr ()
  "Translate Python expressions."
  (l:lquot
   (strip-parens)
   (l:or (term)
         (attribute-node)
         (not-lens)
         (unop)
         (lambda-node)
         ;; These should be disjoint.
         (l:or!
          (comparison)
          (boolean-binop))
         (binop)
         (call-app))))

(defun func-decl ()
  "Translate Python function declarations."
  (l:match ((name (expr))
            (params (parameters-lens))
            (body (block-lens)))
    (make 'python-function-definition
          :python-name name
          :python-parameters params
          :python-body body)
    `(defun ,name (,@params)
       ,body)))

(defun decl ()
  "Return a lens to convert Python declarations."
  (l:or (func-decl)))

(defun if-statement ()
  (l:match ((condition (translator))
            (consequence (block-lens))
            (alternative (l:mapcar
                          (l:or (else-clause) ;Yes, else first.
                                (elif-clause)))))
    (make 'python-if-statement
          :python-condition condition
          :python-alternative alternative
          :python-consequence consequence)
    `(cond (,condition ,consequence)
           ,@alternative)))

(defun elif-clause ()
  (save-before-after-text
   (l:match ((condition (translator))
             (consequence (block-lens)))
     (make 'python-elif-clause
           :python-condition condition
           :python-consequence consequence)
     `(,condition ,consequence))))

(defun else-clause ()
  (save-before-after-text
   (l:match ((body (block-lens)))
     (make 'python-else-clause
           :python-body body)
     `(t ,body))))

(defun block-lens ()
  (save-before-after-text
   (l:match ((children (l:mapcar (translator)))
             ;; TODO There may be some issues if the 4/-4 are ever
             ;; used in a file with indentation other than that.
             (indent-children (l:identity!) 4)
             (indent-adjustment (l:identity!) -4))
     (make 'python-block
           :children children
           :indent-adjustment indent-adjustment
           :indent-children indent-children)
     `(block nil ,@children))))

(defun expression-lens ()
  (l:match ((child (translator)))
    (make 'python-expression-statement
          :children (list child))
    `(progn ,child)))

(defun assignment ()
  (l:match ((left (translator))
            (right (translator)))
    (python "$1 = $2" left right)
    `(setf ,left ,right)))

(defun return-statement ()
  (l:match ((child (translator)))
    (python "return $1" child)
    `(return ,child)))

(defun statement ()
  (l:or (if-statement)
        (pass-lens)
        (expression-lens)
        (block-lens)
        (assignment)
        (return-statement)))

(defun module ()
  (l:match ((children (l:mapcar (translator))))
    (make 'python-module :children children)
    `(:module ,@children)))

(defun translator ()
  (save-before-after-text
   (l:lquot*
    (whitespace-canonizer)
    (l:or (module) (decl) (statement) (expr)))))
