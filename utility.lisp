(defpackage :argot-server/utility
  (:use :gt/full :lsp-server.lem-base
        :argot-server/readtable
        :argot-server/config
        :argot
        :software-evolution-library/software/lisp
        :software-evolution-library/software/python
        :software-evolution-library/software/javascript
        :software-evolution-library/software/typescript
        :software-evolution-library/software/c
        :software-evolution-library/software/cpp
        :software-evolution-library/software/tree-sitter
        :software-evolution-library/utility/range
        :lsp-server
        :lsp-server/protocol
        :lsp-server/utilities/lsp-utilities
        :cmd)
  (:import-from :software-evolution-library/software/tree-sitter
                :before-text :after-text
                :before-asts :after-asts)
  (:import-from :software-evolution-library/software/project
                :text-file-p)
  (:import-from :lsp-server :*server* :lsp-server)
  (:import-from :jsonrpc
                :notify
                :client-connect)
  (:import-from :jsonrpc/errors
                :jsonrpc-callback-error)
  (:import-from :quri
                :uri-port
                :uri-host
                :uri)
  (:import-from :usocket
                :socket-listen
                :connection-refused-error
                :socket-connect
                :socket-close)
  (:import-from :lsp-server.lem-base
                :make-point
                :point-charpos
                :point-linum
                :copy-point
                :buffer-point)
  (:import-from :lsp-server/protocol-util
                :move-to-lsp-position)
  (:import-from :lsp-server
                :get-buffer-from-uri)
  (:import-from :uiop/run-program :run-program)
  (:import-from :software-evolution-library
                :from-string
                :genome)
  (:import-from :jsonrpc/connection
                :connection
                :*connection*)
  (:import-from :quri)
  (:local-nicknames
   (:bt :bordeaux-threads)
   (:lp :lparallel))
  (:shadowing-import-from :lsp-server.lem-base :add-hook :remove-hook :run-hooks)
  (:export :guess-language
           :base-language
           :with-text
           :point-to-position
           :position->point
           :client-connect/retries
           :random-port
           :argot-readtable
           :argot-registry
           :argot-image-exists?
           :point->source-location
           :points->source-range
           :uri-buffer-software
           :buffer-software
           :message
           :dockerized?
           :lang-software
           :get-file-contents-from-client
           :get-file-contents
           :list-workspace-uris
           :await-uri-buffer
           :annotate-workspace-edit
           :muse-server
           :current-server
           :file-uri-path
           :+localhost+
           :service-host
           :network
           :port-available-p
           :port-listening-p
           :server-network
           :*network*
           :with-deadline
           :+visual-studio-code+
           :get-argot-init-option
           :argot-method-unsupported
           :check-argot-support
           :tag-images-latest
           :push-latest
           :images.tags
           :edit-redundant?
           :command-keyword
           :project-root-for
           :clear-diagnostics
           :path-from-params
           :safe-current-connection
           :goto-position
           :guess-slug-language
           :argot-support?
           :with-thread-name
           :with-resetting
           :*reset-hook*
           :define-reset-hook))
(in-package :argot-server/utility)
(in-readtable argot-readtable)

(deftype promise ()
  '#.(type-of (lp:promise)))

(deftype base-language ()
  '(member python lisp javascript typescript-ts typescript-tsx c cpp))

(defvar *reset-hook* ()
  "Hook for clearing muse state.")

(defmacro define-reset-hook (name args &body body)
  `(progn
     (defun ,name ,args ,@body)
     (add-reset-hook ',name)
     ',name))

(defun add-reset-hook (fn)
  (serapeum:add-hook '*reset-hook* fn))

(defconst +localhost+ "127.0.0.1")

(def +visual-studio-code+
  "Visual Studio Code"
  "Name that VS Code sends for itself.")

(define-condition argot-method-unsupported ()
  ((connection :initarg :connection)
   (method :initarg :method
     :type string
     :reader :argot-method-unsupported-method))
  (:default-initargs :connection (current-connection))
  (:report (lambda (c s)
             (with-slots (connection method) c
               (format s "Client ~a does not support method ~a"
                       connection method)))))

(-> service-host (string) (values string &optional))
(defun service-host (service-name)
  (ecase-of network *network*
    (:host +localhost+)
    (:bridge service-name)))

(defconst argot-registry
  "registry.gitlab.com/grammatech/mnemosyne/argot-server/")

(defun guess-slug-language (slug)
  "Guess SLUG's language.
SLUG could be a file extension or an LSP language identifier."
  (string-case (string-downcase slug)
    (("py" "python") 'python)
    (("lisp" "cl" "asd") 'lisp)
    (("js" "javascript") 'javascript)
    ("json" 'json)
    (("ts" "typescript") 'typescript-ts)
    ("tsx" 'typescript-tsx)
    (("c" "h") 'c)
    (("cpp" "cc" "c++" "hpp") 'cpp)))

(defgeneric guess-language (file)
  (:method ((file buffer))
    (guess-language (buffer-filename file)))
  (:method ((file pathname))
    (when-let (type (pathname-type file))
      (guess-slug-language type)))
  (:method ((file string))
    (let ((file
           (if (string*= "://" file)
               (file-uri-path file)
               file)))
      (guess-language (pathname file)))))

(defun lang-software (lang)
  (case-of base-language lang
    (javascript 'javascript)
    (python 'python)
    (typescript-ts 'typescript-ts)
    (typescript-tsx 'typescript-tsx)
    (lisp 'lisp)
    (c 'c)
    (cpp 'cpp)
    (otherwise nil)))

(defmacro with-text ((text-symbol start end) &body body)
  `(with-point ((,start ,start)
                (,end ,end))
     (let ((,text-symbol (points-to-string ,start ,end)))
       ,@body)))

;;; There are /three/ representations of a location we have to deal
;;; with: Lem points (1-based lines, 0-based columns), LSP positions
;;; (0-based lines and columns), and `source-location' objects from
;;; SEL (1-based lines and columns). This is further complicated by
;;; the fact that points are only meaningful when associated with
;;; buffers.

(defmethod convert ((type (eql 'point)) (p point) &key)
  p)

(defmethod convert ((type (eql 'Position)) (p Position) &key)
  p)

(defmethod convert ((type (eql 'source-location)) (s source-location) &key)
  s)

(defmethod convert ((type (eql 'Position)) (point point) &key)
  (point-to-position point))

(defmethod convert ((type (eql 'Position)) (s source-location) &key)
  (make 'Position
        :line (1- (line s))
        :character (1- (column s))))

(defmethod convert ((type (eql 'source-location)) (point point) &key)
  (point->source-location point))

(defmethod convert ((type (eql 'source-location)) (pos Position) &key)
  (ematch pos
    ((Position :line line :character character)
     (make 'source-location
           :line (1+ line)
           :column (1+ character)))))

(defmethod convert ((type (eql 'point))
                    (p Position)
                    &key buffer kind)
  (position->point buffer p kind))

(defmethod convert ((type (eql 'point))
                    (s source-location)
                    &key buffer kind)
  (source-location->point buffer s kind))

(defmethod convert ((type (eql 'Range)) (range source-range) &key)
  (make 'Range
        :start (convert 'Position (begin range))
        :end (convert 'Position (end range))))

(defmethod convert ((type (eql 'source-range)) (range Range) &key)
  (make 'source-range
        :begin (convert 'source-location (slot-value range '|start|))
        :end (convert 'source-location (slot-value range '|end|))))

(defun position->point (buffer position &optional kind)
  (let ((point (copy-point (buffer-point buffer) kind)))
    (move-to-lsp-position point position)
    point))

(defun source-location->point (buffer location &optional kind)
  (position->point buffer (convert 'Position location) kind))

(-> point-to-position (point) (values Position &optional))
(defun point-to-position (point)
  (make-instance '|Position|
                 ;; NB LSP uses zero-based line and character offsets.
                 :|line| (1- (point-linum point))
                 :|character| (point-charpos point)))

(-> point->source-location (point) (values source-location &optional))
(defun point->source-location (point)
  (make 'source-location
        :line (point-linum point)
        :column (1+ (point-charpos point))))

(-> points->source-range (point point)
    (values source-range &optional))
(defun points->source-range (start end)
  (make 'source-range
        :begin (point->source-location start)
        :end (point->source-location end)))

(defmethod convert ((type (eql 'string))
                    (buffer buffer)
                    &key)
  (buffer-to-string buffer))

#+debug
(defun show-hash (hash)
  (typecase hash
    (hash-table (let ((result nil))
                  (maphash (lambda (key value) (push (cons key (show-hash value))
                                                result))
                           hash)
                  result) )
    (list (mapcar #'show-hash hash))
    (t hash)))

(defun port-available-p (host port)
  (handler-case
      (progn
        (socket-close (socket-listen host port :reuse-address t))
        t)
    (usocket:address-in-use-error ()
      nil)))

(defun random-port ()
  (flet ((random-port () (random-in-range 1024 65536)))
    (iter (for port = (random-port))
          (until (port-available-p +localhost+ port))
          (finally (return port)))))

(defun port-listening-p (host port)
  (handler-case
      (progn
        (socket-close (socket-connect host port))
        t)
    ((or usocket:socket-error
      usocket:ns-error
      usocket:ns-condition) ()
      nil)))

(defun client-connect/retries (client
                               &rest initargs
                               &key (tries 10)
                                 url
                                 (host +localhost+)
                                 port
                                 (mode :tcp)
                               &allow-other-keys)
  "Connect to a server that may or may not be running yet (e.g. if it has been started in another thread)."
  (when url
    (setf (values host port)
          (let ((uri (uri url)))
            (values (uri-host uri)
                    (uri-port uri)))))
  (assert (and host port))
  (nlet connect ((tries tries)
                 (wait 0.1))
    (unless (port-listening-p host port)
      (if (plusp (1- tries))
          (progn
            (sleep wait)
            (connect (1- tries)
                     (* 2 wait)))
          (error "Could not connect to ~a:~a" host port))))
  ;; NB It is not safe to call client-connect more than once on the
  ;; same client.
  (apply #'client-connect client
         :mode mode
         (remove-from-plist initargs :tries)))

(defun image-exists? (name)
  (multiple-value-bind (stdout stderr errno)
      (run-program (format nil "docker image inspect ~a" name)
                   :ignore-error-status t)
    (declare (ignorable stdout stderr))
    (zerop errno)))

(defun argot-image-exists? (name)
  (image-exists? (string+ argot-registry name)))

(defun buffer-software (buffer)
  (declare (type buffer buffer))
  (let* ((lang (guess-language buffer))
         (software-class (lang-software lang))
         (text (buffer-to-string buffer)))
    (when software-class
      (from-string (make software-class) text))))

(defun uri-buffer-software (uri &key lang)
  "Retrieve the buffer for URI and parse it into a software object."
  (when-let* ((buffer (get-buffer-from-uri uri))
              (string (buffer-to-string buffer))
              (lang (or lang (guess-language buffer)))
              (software (from-string (make lang) string)))
    ;; Parse the AST eagerly.
    (genome software)
    software))

(defun message (&rest args)
  "Display a message in the connected editor.

ARGS should be a format control string, followed by arguments, except
that the first element can be a keyword representing a message type
\(see `message-type-keyword')."
  (ematch args
    ((list* (and type (type keyword))
            (and control (type (or string function)))
            args)
     (show-message (format nil "~?" control args) :type type))
    ((list* (and control (type (or string function)))
            args)
     (apply #'message :info control args))))

(define-compiler-macro message (&whole call &rest args)
  "Wrap a literal format control string with `formatter'."
  (match args
    ((list* (and control (type string)) args)
     `(message (formatter ,control) ,@args))
    ((list* (and type (type keyword)) (and control (type string)) args)
     `(message ,type (formatter ,control) ,@args))
    (otherwise call)))

(defun dockerized? ()
  "Is the server running in a Docker image?"
  (uiop:getenv "MNEMOSYNE_DOCKER"))

(defgeneric muse-server (muse)
  (:method ((server lsp-server)) server)
  (:documentation "Get the server for MUSE."))

(defun current-server ()
  "Get the current server.
If the current server is actually a muse, get the root server."
  (muse-server *server*))

(def +client-lock+ (bt:make-recursive-lock "Client lock")
  "Lock to prevent contention when sending requests to client.")

(-> get-file-contents-from-client (string) string)
(defun get-file-contents-from-client (uri)
  "Request the contents on disk of the file at URI from the client.
Note that this may differ from the contents of URI's buffer, if there
is one, if the file is being edited but has not yet been saved."
  (nest
   (let ((*connection* (current-connection))))
   (values)
   (gethash "text")
   (bt:with-recursive-lock-held (+client-lock+))
   (jsonrpc:call (current-server) +argot/textDocument/content+)
   (make 'ArgotContentParams
         :textDocument
         (make 'TextDocumentIdentifier
               :uri uri))))

(-> get-file-contents
    (string &key (:connection connection)) (values (or string null) &optional))
(defun get-file-contents (uri &key ((:connection *connection*)
                                    (current-connection)))
  "Get the contents of URI either from a buffer (if there is one), or
from disk, or by requesting it from the client with
`get-file-contents-from-client'.

This may return nil if URI is a binary file."
  (let ((*connection* (current-connection)))
    (if-let (b (get-buffer-from-uri uri))
      (buffer-to-string b)
      (if-let (file
               (and *allow-local*
                    (file-exists-p
                     (parse-unix-namestring
                      (drop-prefix "file://" uri)))))
        (when (text-file-p file)
          (read-file-into-string file))
        (if (argot-support? +argot/textDocument/content+)
            (get-file-contents-from-client uri)
            (error "Cannot load ~a" uri))))))

(defun list-workspace-uris-from-disk (dir)
  (with-collectors (collect)
    (walk-directory dir
                    (lambda (p)
                      (let ((uri (string+ "file://" (unix-namestring p))))
                        (unless (ignore-workspace-uri? uri)
                          (collect uri)))))))

(defun list-workspace-uris-from-client (base)
  (nest
   (and (argot-support? +argot/workspace/files+))
   (remove-if #'ignore-workspace-uri?)
   (mapcar {gethash "uri"})
   (bt:with-recursive-lock-held (+client-lock+))
   (jsonrpc:call (current-server) +argot/workspace/files+)
   (multiple-value-call #'make 'ArgotFilesParams
     (if base (values :base base)
         (values)))))

(defun list-workspace-uris (&key uri
                              (root (and uri (project-root-for uri)))
                              (base nil)
                              ((:connection *connection*)
                               (current-connection)))
  "List the URIs of all the files in the workspace of URI.
BASE can be specified to restrict the list to a subdirectory.

This omits the files that VS Code ignores by default."
  (declare (type (or string null) base))
  (let ((root
         (and root
              (string+ (drop-suffix "/"
                                    (namestring
                                     (file-uri-pathname root)))
                       (and base
                            (string+ "/" base))))))
    (if-let (dir
             (and *allow-local*
                  root
                  (directory-exists-p root)))
      (list-workspace-uris-from-disk dir)
      (list-workspace-uris-from-client base))))

(defun ignore-workspace-uri? (uri)
  "Is URI a file that VS Code would ignore by default?"
  (flet ((ignore-namestring? (namestring)
           (or (string$= namestring "~")
               (string$= namestring "#")
               (search ".min." namestring)))
         (ignore-dir? (dir)
           (string-case dir
             ((".DS_Store"
               ".code_search"
               ".git"
               ".hg"
               ".svn"
               "CVS"
               "Thumbs.db"
               "bower_components"
               "node_modules"
               ".vscode")
              dir)))
         (ignore-name? (name)
           (and (stringp name)
                (string-case name
                  (("TAGS") name))))
         (ignore-extension? (ext)
           (and (stringp ext)
                (string-case ext
                  (("a"
                    "bak"
                    "dll"
                    "dvi"
                    "o"
                    "so")
                   ext)))))
    (let ((namestring (drop-prefix "file://" uri)))
      (or (ignore-namestring? namestring)
          (let ((pathname (parse-unix-namestring namestring)))
            (or (iter (for dir in (rest (pathname-directory pathname)))
                      (thereis (ignore-dir? dir)))
                (ignore-name? (pathname-name pathname))
                (ignore-extension? (pathname-type pathname))))))))

(-> show-document (string &rest t &key &allow-other-keys)
    boolean)
(defun show-document (uri &rest args &key &allow-other-keys)
  "Make the editor open URI."
  (nest
   (let ((*connection* (current-connection))))
   (values)
   (gethash "success")
   (bt:with-recursive-lock-held (+client-lock+))
   (jsonrpc:call (current-server) "window/showDocument")
   (apply #'make 'ShowDocumentParams
          :uri uri
          args)))

(-> await-uri-buffer (string &key (:async t))
    (or buffer promise))
(defun await-uri-buffer (uri &key async)
  "Get the buffer for URI.
If there is no buffer for URI, ask the client to open it and return
the resulting buffer.

By default, synchronous (waits until the client has sent a didOpen
notification). If ASYNC is non-nil, it returns a promise instead."
  (values
   (let ((p (uri-buffer-promise uri)))
     (if (lp:fulfilledp p)
         (lp:force p)
         (progn
           (show-document uri)
           (if async p
               (lp:force p)))))))

(defun client-supports-annotated-edits? ()
  (let ((initialize-params
         (and (boundp '*connection*)
              (initialize-params*))))
    (match initialize-params
      ((InitializeParams
        :capabilities
        (ClientCapabilities
         :workspace
         (WorkspaceClientCapabilities
          :workspace-edit caps)))
       (slot-boundp caps '|changeAnnotationSupport|)))))

(-> annotate-workspace-edit
    (string (or WorkspaceEdit ApplyWorkspaceEditParams)
            &key (:annotation-id string)
            (:needs-confirmation boolean)
            (:description (or null string))
            (:always boolean))
    (or WorkspaceEdit ApplyWorkspaceEditParams))
(defun annotate-workspace-edit (label workspace-edit
                                &rest kwargs
                                &key
                                  (annotation-id (string (gensym "edit")))
                                  (needs-confirmation t)
                                  description
                                  always)
  "Give WorkSpace edit a ChangeAnnotations property with LABEL and
make the TextEdit instances refer to it.

If the current client (based on `*connection*`) does not support
change annotations then WORKSPACE-EDIT is returned (unless ALWAYS is non-nil)."
  (unless (or always (client-supports-annotated-edits?))
    (return-from annotate-workspace-edit
      workspace-edit))
  (ematch workspace-edit
    ((and params
          (ApplyWorkspaceEditParams
           :edit edit))
     (make 'ApplyWorkspaceEditParams
           :label (if (slot-boundp params '|label|)
                      (slot-value params '|label|)
                      label)
           :edit (apply #'annotate-workspace-edit label edit kwargs)))
    ((WorkspaceEdit
      :document-changes edits)
     (make 'WorkspaceEdit
           :change-annotations
           (dict annotation-id
                 (multiple-value-call #'make 'ChangeAnnotation
                   :label label
                   :needs-confirmation needs-confirmation
                   (if description
                       (values :description description)
                       (values))))
           :document-changes
           (mapcar
            (named-lambda annotate-edit (edit)
              (ematch edit
                ((or (CreateFile)
                     (DeleteFile)
                     (RenameFile))
                 (copy edit :annotation-id annotation-id))
                ((TextEdit :range range :new-text new-text)
                 (make 'AnnotatedTextEdit
                       :range range
                       :new-text new-text
                       :annotation-id annotation-id))
                ((TextDocumentEdit
                  :text-document doc
                  :edits edits)
                 (make 'TextDocumentEdit
                       :text-document doc
                       :edits (mapcar #'annotate-edit edits)))))
            edits)))))

(defmacro with-deadline ((seconds) &body body)
  "Run BODY with a timeout of SECONDS.
This uses I/O deadlines when possible, timeouts otherwise.

If seconds is 0, there is no deadline."
  (with-unique-names (thunk _seconds)
    ;; Not declaring the thunk dynamic-extent since threads are in
    ;; play.
    `(flet ((,thunk () ,@body))
       (let ((,_seconds ,seconds))
         (if (zerop ,_seconds) (,thunk)
             #+sbcl
             (sb-sys:with-deadline (:seconds ,_seconds)
               (,thunk))
             #-sbcl
             (with-timeout (,seconds)
               (,thunk)))))))

(defun get-argot-init-option (key &aux (*connection* (current-connection)))
  "Is KEY set in the Argot initialization options?"
  (check-argot-option key)
  (match (initialize-params-safe)
    ((InitializeParams :initialization-options
                       (dict "argot" (and options (type hash-table))))
     (gethash key options))))

(defun argot-support? (method &key (option (argot-method-option method)))
  (get-argot-init-option option))

(defun check-argot-support (method &key (option (argot-method-option method)))
  "Check whether an Argot method is supported by the current client."
  (unless option
    (error "No such Argot method as ~a" method))
  (unless (get-argot-init-option option)
    (cerror "Send it anyway"
            'argot-method-unsupported
            :method method)))

(defun images.tags-1 ()
  (mapcar (op (apply #'cons
                     (mapcar #'trim-whitespace
                             (drop 1 (split-sequence #\: _)))))
          (filter (op (string^= "image: " (trim-whitespace _)))
                  (lines (read-file-into-string
                          (path-join
                           (asdf:system-relative-pathname
                            "argot-server" "docker-compose.yaml")))))))

(defun images.tags ()
  "Return all the current Mnemosyne images an alist of (image-name . tag)."
  (let ((images.tags (images.tags-1)))
    (assert (every (distinct :test #'equal :key #'car) images.tags))
    images.tags))

(defun tag-images-latest (&optional (images.tags (images.tags)))
  "Make sure the current Mnemosyne images are also tagged as latest."
  (iter (for (image . tag) in images.tags)
        (cmd "docker tag" (fmt "~a:~a" image tag) "latest")))

(defun push-latest (&optional (images.tags (images.tags)))
  "Push the latest Mnemosyne images."
  (tag-images-latest images.tags)
  (dolist (image (mapcar #'car images.tags))
    (cmd "docker push" image)))

(defgeneric edit-redundant? (doc edit)
  (:documentation "Return T if EDIT would insert duplicate text.")
  (:method ((doc TextDocumentIdentifier) edit)
    (edit-redundant? (slot-value doc '|uri|) edit))
  (:method ((uri string) edit)
    (mvlet* ((new-text start end line1 char1 line2 char2
              (ematch edit
                ((TextEdit
                  :range (Range :start (and start
                                            (Position :line line1
                                                      :character char1))
                                :end (and end
                                          (Position :line line2
                                                    :character char2)))
                  :newText new-text)
                 (values new-text start end line1 char1 line2 char2))))
             (buffer (get-buffer-from-uri uri))
             (start-point (position->point buffer start))
             (end-point (position->point buffer end))
             (before (points-to-string
                      (buffer-start-point buffer)
                      end-point))       ;sic
             (after (points-to-string
                     start-point        ;sic
                     (buffer-end-point buffer))))
      (declare (ignore line1 line2 char1 char2))
      (or (string$= new-text before)
          (string^= new-text after)))))

(-> command-keyword (t symbol) (values symbol t &optional))
(defun command-keyword (muse command)
  "Intern a keyword for COMMAND incorporating the name of MUSE."
  (make-keyword (string+ (type-of muse) "." command)))

(defun clear-diagnostics (uri)
  "Remove all diagnostics for URI."
  (jsonrpc:notify *server*
                  "textDocument/publishDiagnostics"
                  (make 'PublishDiagnosticsParams
                        :uri uri
                        :diagnostics '())))

(defun goto-position (uri &key (line 0) (char 0))
  "Tell the editor to jump to a particular LINE and CHAR in URI."
  (declare (string uri)
           ((integer 0 *) line char))
  (check-argot-support |+argot/window/goto+|)
  (let ((*connection* (current-connection)))
    (await-uri-buffer uri)
    (show-document uri :take-focus t)
    (bt:with-recursive-lock-held (+client-lock+))
    (jsonrpc:call (current-server)
                  "argot/window/goto"
                  (make 'gotoOptions
                        :uri uri
                        :position
                        (make 'Position :line line :character char)))))
