;;; passthrough.lisp --- Muse which passes thru requests and responses
;;;                      to an external LSP server.
(defpackage :argot-server/muses/passthrough
  (:use :gt/full
        :jsonrpc
        :lsp-server/utilities/lsp-utilities
        :argot-server/muses/muse
        :argot-server/muses/cli-muses
        :argot-server/utility
        :lsp-server/protocol)
  (:import-from :yason
                :*parse-json-booleans-as-symbols*
                :parse)
  (:import-from :jsonrpc/class
                :server-client-connections)
  (:import-from :lsp-server
                :server-buffers
                :buffer-version
                :buffer-language-id
                :buffer-uri
                :handle)
  (:import-from :jsonrpc/transport/tcp)
  (:import-from :jsonrpc/transport/stdio)
  (:import-from :lsp-server/logger
                :log-format)
  (:export :muse-server
           :muse-lang
           :muse-started-p
           :muse-stop
           :passthrough-client
           :external-muse
           :external-code-actor-muse
           :tcp-muse
           :muse-host
           :muse-port
           :stdio-muse
           :client-dispatch-notification
           :client-dispatch-request))
(in-package :argot-server/muses/passthrough)
(in-readtable argot-readtable)

(defclass external-muse (muse)
  ((client :type client :reader muse-client
           :documentation "The lazily initialized JSON-RPC client.")
   (initialize-result
    :initform nil
    :accessor muse-initialize-result))
  (:documentation "A muse that is actually another LSP server."))

(defmethod reinitialize-muse ((muse external-muse)
                              &key initialize-params)
  "Reinitialize an external MUSE by sending it INITIALIZE-PARAMS, then updating it with the state of each of the server's buffers."
  (with-slots (client) muse
    (when initialize-params
      (jsonrpc:call client
                    "initialize"
                    initialize-params)
      (jsonrpc:call client "initialized"))
    ;; Wait until after the initialization params have been sent
    ;; (which can be slow) to send the list of buffers. Sending the
    ;; files must be synchronized to prevent lost updates, either
    ;; from `didChange` messages for particular files or from new
    ;; files being opened.
    (nest
     (with-deadline (60))
     (synchronized ((muse-server muse)))
     (dolist (buffer (server-buffers (muse-server muse))))
     (jsonrpc:call client
                   "textDocument/didOpen"
                   (dict "textDocument"
                         (dict
                          "uri" (buffer-uri buffer)
                          "languageId" (buffer-language-id buffer)
                          "version" (buffer-version buffer)
                          "text" (buffer-to-string buffer)))))))

(defmethod muse-timed-out ((muse external-muse) callback &key initialize-params)
  "Handle an external muse that has timed out by stopping it, restarting it, and reinitializing it (with `reinitialize-muse`)."
  (log-format "# Muse ~a has timed out" (muse-name muse))
  (muse-stop muse :hard t :allow-other-keys t)
  (muse-start muse)
  (handler-case
      (progn
        (reinitialize-muse muse :initialize-params initialize-params)
        (funcall callback))
    (serious-condition (c)
      (format *error-output*
              "Failed to reinitialize muse ~a because: ~a"
              (muse-name muse)
              c))))

(defvar *id* nil)

(defmethod dispatch :around ((self external-muse) request)
  (let* ((*id* (request-id request))
         (response (call-next-method)))
    (when (equal "initialize" (request-method request))
      (setf (muse-initialize-result self)
            (convert 'InitializeResult (response-result response))))
    response))

(defmethod handle ((self external-muse) method params)
  (with-slots (client) self
    (let ((method (string method)))
      (if *id*
          (jsonrpc:call client (string method) params)
          (notify client method params)))))

(defmethod muse-started-p and ((muse external-muse))
  (slot-boundp muse 'client))

(defmethod muse-stop progn ((muse external-muse) &key (hard 5))
  (when-let (client (slot-value-safe muse 'client))
    (flet ((soft-shutdown ()
             ;; The handler is here because many LSP servers implement the
             ;; LSP shutdown/exit dance incorrectly.
             (handler-case
                 (progn
                   (jsonrpc:call client "shutdown")
                   (jsonrpc:notify client "exit"))
               (timeout ())
               (error (e)
                 (format *error-output*
                         "~&Error in shutdown of ~a:~%~a~%" muse e)))))
      (etypecase-of (or boolean (real (0) *)) hard
        (null (soft-shutdown))
        ;; Empty case.
        ((eql t))
        ((real (0) *)
         (ignore-some-conditions (timeout)
           (with-deadline (hard)
             (soft-shutdown))))))
    (client-disconnect client))
  (slot-makunbound muse 'client))

(defmethod muse-commands append ((muse external-muse))
  (nlet retry ((result (muse-initialize-result muse)))
    (match result
      ((type hash-table)
       (retry (convert 'InitializeResult result)))
      ((InitializeResult :capabilities (and caps (ServerCapabilities)))
       (match (slot-value-safe caps '|executeCommandProvider|)
         ((ExecuteCommandOptions :commands commands)
          commands))))))

(defclass passthrough-client (client)
  ((server :initarg :server :type lsp-server :reader client-server)
   (muse :initarg :muse :reader client-muse))
  (:documentation "A client that passes through any messages it receives
to the clients connected to a specified server.")
  (:default-initargs
   :parser (lambda (obj)
             (let ((*parse-json-booleans-as-symbols* t))
               (parse obj)))))

(defmethod print-object ((self passthrough-client) stream)
  (print-unreadable-object (self stream :type t)
    (with-slots (server) self
      (format stream "~a" server))))

(defgeneric client-dispatch-notification (client muse method notification)
  (:method ((self passthrough-client) muse method notification)
    (declare (ignore muse method))
    (with-slots (server) self
      ;; TODO Only the connections for this muse!
      (dolist (conn (server-client-connections server))
        (notify-to server conn
                   (request-method notification)
                   (request-params notification))))))

(defgeneric client-dispatch-request (client muse method request)
  (:method ((self passthrough-client) muse method request)
    (declare (ignore muse))
    (with-slots (server) self
      ;; TODO Only the connections for this muse!
      (dolist (conn (server-client-connections server))
        (apply #'call-to
               server conn
               (request-method request)
               (request-params request)
               (when (eql method :|workspace/applyEdit|)
                 (list :timeout nil)))))))

(defmethod dispatch :before ((self passthrough-client) message)
  "Pass through server notifications."
  (unless (request-id message)
    (client-dispatch-notification self
                                  (client-muse self)
                                  (find-keyword (request-method message))
                                  message)))

(defmethod dispatch ((self passthrough-client) message)
  "Pass through server requests."
  (if (not (request-id message))
      (call-next-method)
      (client-dispatch-request self
                               (client-muse self)
                               (find-keyword (request-method message))
                               message)))

(defclass external-code-actor-muse (external-muse)
  ((busy-lock :initform (make-lock "External muse lock") :reader busy-lock))
  (:documentation "An external muse which creates two-round code actions."))

(defmacro when-can-acquire-lock ((place) &body body &aux (held (gensym)))
  "Execute BODY when the lock named by PLACE can be acquired."
  `(let (,held)
     (unwind-protect
          (when (acquire-lock ,place nil)
            (setf ,held t)
            ,@body)
       (when ,held
         (setf ,held nil)
         (release-lock ,place)))))

;;; The following methods workaround an issue in Mnemosyne
;;; where, if during the partial completion of a applyEdit request
;;; a codeAction request is created (automatically by VSCode),
;;; Mnemosyne and the external muse will deadlock.  The external muse
;;; will be awaiting confirmation the applyEdit has completed,
;;; while Mnemosyne will be waiting for a response for the
;;; codeAction request.  To hack around this deadlock, we use
;;; a `busy-lock` here to avoid sending codeAction requsts while
;;; we are applying an edit.  The methods below accomplish this.

(defmethod dispatch :around ((s passthrough-client) request)
  (if (and (eql (make-keyword (request-method request))
                :|workspace/applyEdit|)
           (typep (client-muse s) 'external-code-actor-muse))
      (with-lock-held ((busy-lock (client-muse s)))
        (call-next-method))
      (call-next-method)))

(defmethod handle :around ((s external-code-actor-muse)
                           (m (eql :|textDocument/codeAction|))
                           params)
  (when-can-acquire-lock ((busy-lock s))
    (call-next-method)))

(defmethod handle :around ((s external-code-actor-muse)
                           (m (eql :|textDocument/hover|))
                           params)
  (when-can-acquire-lock ((busy-lock s))
    (call-next-method)))

(defclass tcp-muse (external-muse port-muse)
  ((host :initarg :host))
  (:default-initargs
   :host "127.0.0.1"))

(defmethod muse-host ((muse tcp-muse))
  (with-slots (host) muse
    (service-host host)))

(defmethod muse-started-p and ((muse tcp-muse))
  (and (slot-boundp muse 'client)
       (with-slots (client) muse
         (jsonrpc/class:jsonrpc-transport client))))

(defmethod print-object ((self tcp-muse) stream)
  (print-unreadable-object (self stream :type t)
    (format stream "~a ~a:~a"
            (muse-name self)
            (muse-host self)
            (muse-port self))))

(defmethod muse-available? and ((self tcp-muse))
  (with-accessors ((port muse-port) (host muse-host)) self
    (port-listening-p host port)))

(defclass stdio-muse (external-muse process-muse)
  ())

;;; These have to be :after methods in case the primary method is
;;; needed to get the server running.

(defmethod muse-start :after ((s tcp-muse))
  ;; Lazily initialize actual server connnection.
  (nest
   (with-accessors ((server muse-server)) s)
   (with-slots (client) s
     (setf client (make 'passthrough-client
                        :server server
                        :muse s))
     (client-connect/retries client :host (muse-host s)
                                    :port (muse-port s)))))

(defmethod muse-start :after ((s stdio-muse))
  ;; Lazily initialize actual server connnection.
  (nest
   (with-accessors ((server muse-server)
                    (process muse-process)) s)
   (with-slots (client) s
     (setf client (make 'passthrough-client
                        :server server
                        :muse s))
     (client-connect client
                     :mode :stdio
                     ;; Our input is their output, and v.v.
                     :input (process-info-output process)
                     :output (process-info-input process)))))
