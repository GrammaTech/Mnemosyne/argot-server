;;; snippet-mining.lisp --- Muse for snippets of example API usages
;;;
;;; This muse displays to clients snippets of API usages
;;; using a collection of techniques, including mining a corpus
;;; of code for common, idiomatic usage examples.
(defpackage :argot-server/muses/snippet-mining
  (:use :gt/full
        :argot-server/muses/base
        :argot-server/muses/pygls
        :argot-server/muses/passthrough)
  (:export :snippet-mining
           :+snippet-mining-command+))
(in-package :argot-server/muses/snippet-mining)

(defclass snippet-mining (tcp-muse pygls) ()
  (:default-initargs
   :port 7070
   :name "Snippet Mining"
   :lang 'python
   :host "snippet-mining"))
