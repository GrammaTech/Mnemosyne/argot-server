(defpackage :argot-server/muses/repeater
  (:use :gt/full
        :argot-server/readtable
        :argot-server/passthrough
        :argot-server/muses/muse
        :argot-server/utility)
  (:import-from :jsonrpc
                :copy-response
                :copy-request
                :dispatch
                :request
                :request-id
                :request-params
                :request-method)
  (:shadow :add-hook :remove-hook :run-hooks)
  (:export :repeater :forward-request :next-muse))
(in-package :argot-server/muses/repeater)
(in-readtable argot-readtable)

(defclass repeater (muse)
  ((next :initarg :next :type lsp-server :accessor next-muse))
  (:documentation "A muse that forwards messages to another muse."))

(defgeneric forward-request (repeater next-muse request)
  (:method ((repeater repeater) next-muse request)
    (pass-through repeater next-muse request)))

(defmethod initialize-instance :after ((server repeater) &key)
  (with-slots (next) server
    (setf (muse-server next) server)))

(defmethods repeater (r (muse next))
  (:method dispatch :around (r (request request))
    (ignore-some-conditions (jsonrpc:jsonrpc-method-not-found)
      (call-next-method))
    (forward-request r muse request))
  (:method muse-name (r) (muse-name muse))
  (:method muse-lang (r) (muse-lang muse))
  (:method muse-start (r) (muse-start muse))
  (:method muse-stop :around (r &rest args &key)
    (apply #'muse-stop muse args))
  (:method muse-available? and (r) (muse-available? muse))
  (:method muse-lang (r) (muse-lang muse))
  (:method muse-started-p and (r) (muse-started-p muse))
  (:method muse-supports? (r doc) (muse-supports? muse doc))
  (:method muse-host (r) (muse-host muse))
  (:method muse-port (r) (muse-port muse)))
