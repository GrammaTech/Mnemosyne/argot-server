;;; autocomplete.lisp --- Muse for autocompletion of source code.
;;;
;;; This muse currently synthesizes results from a technique tailored to
;;; callarg prediction and a more general ml-based solution forked from
;;; the galois autocomplete project.
(defpackage :argot-server/muses/autocomplete
  (:use :gt/full
        :jsonrpc
        :argot-server/readtable
        :argot-server/passthrough
        :argot-server/muses/base
        :argot-server/muses/passthrough
        :argot-server/muses/argument-predictor
        :argot-server/muses/galois-autocomplete)
  (:export :autocomplete))
(in-package :argot-server/muses/autocomplete)
(in-readtable argot-readtable)

(defclass autocomplete (muse)
  ((argument-predictor-muse :initarg :argument-predictor-muse
                            :reader argument-predictor-muse)
   (galois-autocomplete-muse :initarg :galois-autocomplete-muse
                             :reader galois-autocomplete-muse))
  (:default-initargs
   :lang 'python
   :argument-predictor-muse (make-instance 'argument-predictor)
   :galois-autocomplete-muse (make-instance 'galois-autocomplete))
  (:documentation "Autocompletion muse with sub-muses for ~
                   various code completion techniques."))


;;; Overrides to call the given method on each autocomplete sub-muse.
(defmethod muse-started-p and ((obj autocomplete))
  (and-results #'muse-started-p obj))

(defmethod muse-available? and ((obj autocomplete))
  (and-results #'muse-available? obj))

(defmethod muse-start ((obj autocomplete))
  (progn-results #'muse-start obj))

(defmethod muse-stop progn ((obj autocomplete) &key)
  (progn-results #'muse-stop obj))

(defmethod muse-timed-out ((obj autocomplete) callback &rest args)
  (apply #'progn-results #'muse-timed-out obj callback args))

(defmethod dispatch ((obj autocomplete) request)
  (string-case (request-method request)
    ("textDocument/completion"
     (or-responses {pass-through obj} obj request))
    ;; Filter requests autocomplete doesn't know how to handle.
    ;; TODO Use the muse's initialization parameters to filter out
    ;; notifications and requests it doesn't support.
    (("textDocument/codeAction"
      "workspace/executeCommand"
      "textDocument/hover")
     nil)
    (t (progn-results {pass-through obj} obj request))))


;;; Helper functions
(defun and-results (method obj &rest args)
  "Call METHOD with ARGS on the each of OBJ's sub-muses,
ANDing the results."
  (and (apply method (argument-predictor-muse obj) args)
       (apply method (galois-autocomplete-muse obj) args)))

(defun progn-results (method obj &rest args)
  "Call METHOD with ARGS on the each of OBJ's sub-muses,
returning the last result."
  (apply method (argument-predictor-muse obj) args)
  (apply method (galois-autocomplete-muse obj) args))

(defun or-responses (method obj &rest args)
  "Call METHOD with ARGS on the each of OBJ's sub-muses,
ORing the JSON response results."
  (let ((muses (list (argument-predictor-muse obj)
                     (galois-autocomplete-muse obj)))
        (response nil))
    (iter (for muse in muses)
          (setf response (apply method muse args))
          (until
           (match (response-result response)
             ;; Ignore an empty list of items.
             ((dict "items" nil) nil)
             (response response)))
          (finally (return response)))))
