(defpackage :argot-server/muses/pyls
  (:use :gt/full
        :argot-server/muses/base
        :argot-server/muses/passthrough)
  (:export :pyls)
  (:documentation "Muse that provides baseline LSP support for Python."))
(in-package :argot-server/muses/pyls)

(defclass pyls (tcp-muse)
  ()
  (:default-initargs
   :name "Python Language Server"
   :lang 'python
   :host "pyls"
   :port 3050))

(defmethod muse-disabled? or ((muse pyls) params)
  (equal +visual-studio-code+ (client-info-name :params params)))
