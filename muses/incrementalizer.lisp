(defpackage :argot-server/muses/incrementalizer
  (:use :gt/full
        :argot-server/readtable
        :argot-server/passthrough
        :lsp-server/lsp-server
        :lsp-server/protocol
        :lsp-server/protocol-util
        :lsp-server/utilities/lsp-utilities
        :lsp-server.lem-base
        :argot-server/muses/muse
        :argot-server/muses/repeater
        :argot-server/utility)
  (:import-from :jsonrpc
                :copy-response
                :copy-request
                :dispatch
                :request
                :request-id
                :request-params
                :request-method
                :response-id
                :response-result
                :response-error
                :make-response)
  (:shadow :add-hook :remove-hook :run-hooks)
  (:export :incrementalize :incrementalizer))
(in-package :argot-server/muses/incrementalizer)
(in-readtable argot-readtable)

(defclass incrementalizer (repeater)
  ()
  (:documentation "A muse that translates incremental updates to full updates for another muse."))

(defmethod initialize-instance :after ((server incrementalizer) &key)
  (with-slots (next) server
    (setf (muse-server next) server)))

(defun incrementalize (next)
  "Wrap NEXT, a muse that only supports full sync, so that it supports incremental sync."
  (make 'incrementalizer :next next))

(defmethod forward-request ((server incrementalizer)
                            muse
                            (request request))
  (nest
   (with-accessors ((method request-method)
                    (params request-params))
       request)
   (string-case method
     ("initialize"
      (let ((response (call-next-method)))
        (copy-response response
                       :result (incrementalize-result!
                                (response-result response)))))
     ("textDocument/didChange"
      (call-next-method server muse
                        (copy-request request
                                      :params (undelta params))))
     (otherwise (call-next-method)))))

(defun incrementalize-result! (result)
  "Rewrite RESULT to claim support for incremental updates."
  (match result
    ;; I should be able to use `trivia:place' here, but it doesn't
    ;; work.
    ((dict "capabilities"
           (dict "textDocumentSync"
                 (dict "changes" (and _ (type number)))))
     (setf (href result "capabilities" "textDocumentSync" "changes")
           TextDocumentSyncKind.Incremental)
     result)
    ((dict "capabilities"
           (dict "textDocumentSync"
                 (and _ (type number))))
     (setf (href result "capabilities" "textDocumentSync")
           TextDocumentSyncKind.Incremental)
     result)
    ((type InitializeResult)
     (incrementalize-result! (convert 'hash-table result)))
    (otherwise result)))

(defun undelta (params)
  "Rewrite PARAMS from a delta to a new version."
  (nest
   (convert 'hash-table)
   (ematch params
     ((type hash-table)
      (undelta
       (convert 'DidChangeTextDocumentParams params)))
     ((DidChangeTextDocumentParams
       :textDocument
       (and doc
            (VersionedTextDocumentIdentifier
             :uri uri)))
      (let ((buffer (get-buffer-from-uri uri)))
        (make 'DidChangeTextDocumentParams
              :textDocument doc
              :contentChanges (list
                               (make 'TextDocumentContentChangeEvent
                                     :text (buffer-to-string buffer)))))))))
