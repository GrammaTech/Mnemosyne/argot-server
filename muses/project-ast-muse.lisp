(defpackage :argot-server/muses/project-ast-muse
  (:documentation "Mixin to maintain a project AST.")
  (:use :gt/full
        :argot-server/muses/base
        :lsp-server/protocol
        :argot-server/merge-results
        :argot-server/readtable)
  (:import-from :argot-server/utility
                :*reset-hook*
                :message
                :with-thread-name
                :guess-slug-language)
  (:import-from :lsp-server/utilities/worker-thread
                :with-lsp-worker-thread)
  (:import-from :argot-server/muses/muse
                :contribute-server-capabilities)
  (:local-nicknames
   (:cli :software-evolution-library/command-line)
   (:dir :software-evolution-library/software/directory)
   (:lsp :lsp-server)
   (:project :software-evolution-library/software/project)
   (:snapshot :argot-server/project-root)
   (:ts :software-evolution-library/software/tree-sitter))
  (:export :project-ast-muse
           :ensure-project-ast
           :project-ast
           :lookup-project-uri))
(in-package :argot-server/muses/project-ast-muse)
(in-readtable argot-readtable)

(defstruct (project-ast-cell (:conc-name project-ast-cell.))
  "Stores the root of a project, along with a lock to mutate it."
  (ast nil :type (or null dir:directory-project))
  (root (error "No root") :read-only t :type string)
  (lock (bt:make-lock "Cell lock") :read-only t))

(defmethod monitor ((cell project-ast-cell))
  (project-ast-cell.lock cell))

(declaim (type fset:map *project-asts*))
(defvar *project-asts* (empty-map)
  "Tracked project ASTs, by root.")

(define-reset-hook clear-project-asts ()
  (synchronized ('*project-asts*)
    (setf *project-asts* (empty-map))))

(defplace project-ast (uri)
  "Get/set the project AST for URI's project root."
  (assert (string^= "file://" uri))
  (project-ast-cell.ast
   (ensure-project-ast-cell
    (project-root-for uri))))

(-> ensure-project-ast-cell (string)
    (values project-ast-cell &optional))
(defun ensure-project-ast-cell (root)
  "Ensure a project AST cell has been allocated for ROOT."
  (assert (string^= "file://" root))
  (values
   (let ((map *project-asts*))
     (or (lookup map root)
         (let ((new-cell (make-project-ast-cell :root root)))
           (synchronized ('*project-asts*)
             (let ((map *project-asts*))
               (or (lookup map root)
                   (progn
                     (setf *project-asts*
                           (with map root new-cell))
                     new-cell)))))))))

(defun guess-project-class-name (dir)
  "Return the project class name for DIR."
  (assert (directory-pathname-p dir))
  (when-let (guess (cli:guess-language dir))
    (if (proper-subtype-p guess 'project:project)
        guess
        (error "Cannot recognize type of project"))))

(-> snapshot->ast (hash-table &key (:class symbol))
    (values dir:directory-project &optional))
(defun snapshot->ast (snapshot &key class)
  "Dump SNAPSHOT to disk and convert it into a project AST."
  (with-temporary-directory (:pathname dir)
    (let ((p (assure directory-pathname (pathname dir))))
      (message "Loading project")
      (snapshot:write-project-snapshot snapshot p)
      (let ((class-name (or class
                            (guess-project-class-name p)
                            (error "Cannot find class for project"))))
        (prog1 (sel:from-file class-name p)
          (message "Loaded project as ~a" class-name))))))

(defun compute-project-ast (&key root class)
  "Snapshot the current project and convert it to a project AST."
  (snapshot->ast (snapshot:snapshot-project :root root)
                 :class class))

(-> ensure-project-ast (string &key (:root string) (:class symbol))
    (values dir:directory-project))
(defun ensure-project-ast (uri &key (root (project-root-for uri))
                                 class)
  "Get the project AST, computing it if necessary."
  (let ((cell (ensure-project-ast-cell root)))
    (or (project-ast-cell.ast cell)
        (synchronized (cell)
          (or (project-ast-cell.ast cell)
              (setf (project-ast-cell.ast cell)
                    ;; Compute inside the lock to avoid redundant
                    ;; work.
                    (compute-project-ast
                     :root root
                     :class class)))))))

(defun drop-root (root uri)
  (if (string^= root uri)
      (string-left-trim "/" (drop-prefix root uri))
      uri))

(defun canonical-path (root uri)
  (when root
    (let ((root-path (file-uri-pathname root))
          (uri-path (file-uri-pathname uri)))
      (drop-root (namestring root-path)
                 (namestring uri-path)))))

(defun uri-project-relative-path (uri &key (root (project-root-for uri)))
  (canonical-path root uri))

(-> lookup-project-uri (string &key (:project-ast t))
    (values (or null ast) ))
(defun lookup-project-uri (uri &key project-ast)
  "Lookup URI in the project AST."
  (let* ((root (project-root-for uri))
         (project-ast
          (or project-ast
              (ensure-project-ast uri :root root)))
         (canonical-path (canonical-path root uri)))
    (let ((result (lookup project-ast canonical-path)))
      (etypecase result
        (null)
        (software (genome result))
        (dir:file-ast
         (only-elt (dir:contents result)))))))

(defun update-project-ast (uri &key delete)
  "Update URI in the project AST with its new value."
  (nest
   (with-thread-name (:name "Update project AST"))
   (let* ((root (project-root-for uri))
          (initial-project-ast (ensure-project-ast uri :root root))))
   ;; To avoid contention, we update the project optimistically,
   ;; doing as much work as possible outside of the lock and only
   ;; committing if the project has not changed.
   (nlet retry ())
   (flet ((hold-project-ast ()
            "Get the project AST atomically."
            (let ((current-project-ast (project-ast uri)))
              (if (eql initial-project-ast current-project-ast)
                  current-project-ast
                  (progn
                    (setf initial-project-ast current-project-ast)
                    (retry)))))))
   (let ((old-ast (lookup-project-uri uri))))
   (econd
    ;; Deleting a non-exist file, do nothing.
    ((and (no old-ast) delete)
     initial-project-ast)
    ;; Adding a new file.
    ((no old-ast)
     (when-let (buffer (lsp:uri-buffer uri))
       (let* ((target-lang
               (guess-slug-language
                (lsp:buffer-language-id buffer)))
              (new-string (buffer-to-string buffer))
              (new-path (canonical-path root uri))
              (new-software
               (make target-lang
                     :original-path
                     (pathname new-path)))
              (new-software (from-string new-software new-string))
              (new-project
               (with initial-project-ast
                     (canonical-path root uri)
                     new-software)))
         (synchronized ('*project-asts*)
           (hold-project-ast)
           (setf (project-ast uri) new-project)))))
    ;; Deleting an old file.
    ((and delete old-ast)
     (let* ((path (canonical-path root uri))
            (new-project (less initial-project-ast path)))
       (synchronized ('*project-asts*)
         (hold-project-ast)
         (setf (project-ast uri) new-project))))
    ;; Updating an old file.
    (old-ast
     (when-let (buffer (lsp:uri-buffer uri))
       (let* ((target-lang (ts:ast-language-class old-ast))
              (new-string (buffer-to-string buffer))
              (new-ast (genome (from-string target-lang new-string)))
              (new-project (with initial-project-ast old-ast new-ast)))
         (synchronized ('*project-asts*)
           (hold-project-ast)
           (setf (project-ast uri) new-project))))))))

(defun delete-project-asts (uris)
  "Update URI in the project AST with its new value."
  (dolist (uri uris)
    (update-project-ast uri :delete t)))

(defclass project-ast-muse (lsp-server)
  ()
  (:documentation "Muse that maintains a project AST in the background."))

(defmethod contribute-server-capabilities list ((muse project-ast-muse))
  (make 'ServerCapabilities
        :workspace
        (nest
         (dict "fileOperations")
         (dict "didDelete")
         (make 'FileOperationRegistrationOptions :filters)
         (list (make 'FileOperationFilter
                     :pattern
                     (make 'FileOperationPattern
                           :glob "**.*"))
               (make 'FileOperationFilter
                     :pattern
                     (make 'FileOperationPattern
                           :glob "**"))))))

(defmethod on-change progn ((muse project-ast-muse)
                            (uri string)
                            &key)
  "Update URI in the project AST."
  (and-let* (((string^= "file://" uri))
             ;; TODO Sometimes (why?) VS Code sends system header
             ;; files as if they were part of the project.
             ((not (search "/usr/include/" uri)))
             (root (project-root-for uri))
             (project-ast (ensure-project-ast uri :root root)))
    (update-project-ast uri)))

(defun delete-files-uris (params)
  (match params
    ((DeleteFilesParams :files params)
     (iter (for param in params)
           (match param
             ((FileDelete :uri uri)
              (collect uri)))))))

(defun handle-delete (params)
  (let ((uris (delete-files-uris params)))
    (delete-project-asts uris)))

(define-class-method "workspace/didDeleteFiles" project-ast-muse (params DeleteFilesParams)
  (handle-delete params)
  nil)

(defmethod muse-stop progn ((muse project-ast-muse) &key)
  (synchronized ('*project-asts*)
    (setf *project-asts* (empty-map))))

(define-class-method "shutdown" project-ast-muse (params)
  (clear-project-asts)
  nil)

(define-class-method "exit" project-ast-muse (params)
  (clear-project-asts)
  nil)
