(defpackage :argot-server/muses/erroir
  (:documentation "Context-sensitive syntax error repair.
It's pronounced er-WAHR.")
  (:use :gt/full
        :argot-server/muses/base
        :argot-server/muses/code-actor
        :argot-server/lsp-diff)
  (:export :erroir
           :fix-source))
(in-package :argot-server/muses/erroir)
(in-readtable argot-readtable)

(defclass erroir (code-actor)
  ()
  (:default-initargs
   :lang 'python))

;;; TODO Tree-sitter uses regexes for terminals. It might be useful it
;;; we could automatically expose those regexes as PPCRE parse tree
;;; synonyms when translating the grammars.

(define-parse-tree-synonym python-identifier
    (:regex "[a-zA-Z_][0-9a-zA-Z_]+"))

;;; This is where Erroir gets interesting: we can take advantage of
;;; PPCRE's filter functions to mix regexes with AST parsing.

(defun python-expression-filter (pos)
  "Filter to match a single Python expression in a regex."
  (let* ((subseq (nsubseq ppcre::*string* pos))
         (tree (convert 'python-ast subseq)))
    (nlet rec ((tree tree))
      (if (typep tree 'expression-ast)
          (+ pos (length (source-text tree)))
          (if-let (children (children tree))
            (rec (first children))
            nil)))))

(define-parse-tree-synonym whitespace
    (:greedy-repetition 1 nil :whitespace-char-class))

(define-parse-tree-synonym whitespace?
    (:greedy-repetition 0 nil :whitespace-char-class))

(defmethod muse-code-action-kinds append ((self erroir))
  (list CodeActionKind.QuickFix))

(defmethod muse-commands append ((self erroir))
  (list :erroir.fix))

(defclass fix ()
  ((source :initarg :source :type string)
   (lang :initarg :lang :type symbol)
   (start :initarg :start :type array-index)
   (end :initarg :end :type array-length)))

(defparameter *fixes*
  '(insert-colon-before-block-with-expression
    insert-colon-after-keyword-same-line
    insert-colon-after-sig
    insert-colon-after-def))

;; NB This uses the entire buffer (rather than just the error AST)
;; since in the presence of errors the boundaries of ASTs can be
;; unstable.

(defun fix-source (lang source &key (start 0) (end (length source)))
  (declare (array-index start) (array-length end))
  (assert (subtypep lang 'parseable))
  (some #'apply-fix
        (mapcar (op (make _ :lang lang :source source :start start :end end))
                *fixes*)))

(defmethod applicable-commands ((self erroir) &key start end text-document)
  (nest
   (with-document-software ((start end)
                            :buffer buffer
                            :software sw
                            :asts asts))
   (when-let* ((ast (find-if (op (some #'contains-error-ast-p (children _)))
                             (reverse asts)))))
   (multiple-value-bind (start end) (ast-start+end sw ast))
   (let ((source (buffer-to-string buffer))))
   (when-let ((fix (fix-source (type-of sw)
                               source
                               :start (source-location->position source start)
                               :end (source-location->position source end)))))
   (list)
   (make 'Command
         :title "Fix Syntax Error"
         :command (string :erroir.fix)
         :arguments)
   (list)
   (make 'CodeAction
         :title "Fix Syntax Error"
         :kind CodeActionKind.QuickFix
         :edit)
   (annotate-workspace-edit "Erroir syntax fix")
   (make 'WorkspaceEdit
         :documentChanges)
   (list)
   (make 'TextDocumentEdit
         :text-document
         (make 'OptionalVersionedTextDocumentIdentifier
               :uri (slot-value text-document '|uri|)
               :version (text-document-version text-document))
         :edits
         (lsp-edit-diff-strings (buffer-to-string buffer)
                                fix))))

(defmethod apply-command ((self erroir)
                          (cmd (eql :erroir.fix))
                          args)
  (make 'ApplyWorkspaceEditParams
        :label "quick syntax fix"
        :edit
        (slot-value (convert 'CodeAction (first args))
                    '|edit|)))

(defgeneric apply-fix (fix)
  (:documentation "If the error can be fixed, return a new, fixed
  version of FIX's document." )
  (:method :around ((fix fix))
    (with-slots (lang) fix
      (let ((new (call-next-method)))
        (unless (contains-error-ast-p (genome (from-string lang new)))
          new)))))

(defgeneric fix-ast (fix ast)
  (:method ((fix fix) (ast ast))
    nil))

(defclass insert-colon-after-sig (fix)
  ())

(defun regex-replace-all* (from source to &rest args
                           &key
                             (start 0)
                             (end (length source))
                           &allow-other-keys)
  "Like `regex-replace-all', but retain the portions outside of START
and END."
  (string+ (nsubseq source 0 start)
           (apply #'regex-replace-all from source to
                  :start start :end end
                  args)
           (nsubseq source end)))

(define-compiler-macro regex-replace-all*
    (&whole call from source to &rest args &key &allow-other-keys
            &environment env)
  (if (constantp from env)
      `(regex-replace-all* (load-time-value (create-scanner ,from))
                           ,source
                           ,to
                           ,@args)
      call))

;;; TODO Develop a more succinct sexp encoding for regexes, along the
;;; lines of Esrap. E.g. `(and "->" python-identifier (!> ":"))`. *
;;; and + for greedy repetition.

(defmethod apply-fix ((fix insert-colon-after-sig))
  (with-slots (source start end) fix
    (regex-replace-all*
     '(:sequence " -> " python-identifier
       (:negative-lookahead ":"))
     source
     "\\&:"
     :start start :end end)))

(defclass insert-colon-after-def (fix)
  ())

(defmethod apply-fix ((fix insert-colon-after-def))
  (with-slots (source start end) fix
    (regex-replace-all*
     '(:sequence
       "def" whitespace python-identifier whitespace?
       (:filter python-expression-filter)
       (:negative-lookahead ":"))
     source
     "\\&:")))

(defclass insert-colon-after-keyword-same-line (fix)
  ())

(defmethod apply-fix ((fix insert-colon-after-keyword-same-line))
  (with-slots (source start end) fix
    (regex-replace-all*
     '(:sequence
       :multi-line-mode-p
       (:alternation "else" "try")
       :end-anchor)
     source
     "\\&:"
     :start start :end end)))

(defclass insert-colon-before-block-with-expression (fix)
  ())

(defmethod apply-fix ((fix insert-colon-before-block-with-expression))
  (with-slots (source start end) fix
    (regex-replace-all*
     '(:sequence
       (:alternation "if" "elif" "while")
       whitespace
       (:filter python-expression-filter)
       (:negative-lookahead ":"))
     source
     "\\&:"
     :start start :end end)))
