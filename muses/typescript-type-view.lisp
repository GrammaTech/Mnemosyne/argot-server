(defpackage :argot-server/muses/typescript-type-view
  (:use :gt/full
        :argot-server/readtable
        :argot-server/muses/base
        :argot-server/muses/type-view
        :argot-server/ast-utils
        :lsp-server/protocol
        :argot)
  (:export :typescript-type-view))
(in-package :argot-server/muses/typescript-type-view)
(in-readtable argot-readtable)

(defclass typescript-type-view (type-view)
  ()
  (:documentation "Muse for inserting Typescript types.")
  (:default-initargs
   :force-two-round-code-actions t
   :lang 'typescript))

(defmethod contrib-text-edits ((muse typescript-type-view)
                               (data FunctionReturnType)
                               &key)
  (lret ((edits (call-next-method)))
    (dolist (edit edits)
      (setf (slot-value edit '|newText|)
            (string+ ": " (slot-value edit '|newText|))))))

(defmethod extract-ast-type :around ((muse typescript-type-view) software ast)
  (lret ((result (call-next-method)))
    (typecase result
      (FunctionReturnType
       (callf (op (drop-prefix ": " _))
              (slot-value result '|returns|)))
      (TypeCandidate
       (callf (op (drop-prefix ": " _))
              (slot-value result '|typeName|)))
      (otherwise result))))
