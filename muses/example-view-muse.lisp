(defpackage :argot-server/muses/example-view-muse
  (:use :gt/full
        :argot-server/readtable
        :argot-server/lsp-diff
        :argot-server/muses/base
        :argot-server/muses/code-actor
        :lsp-server
        :lsp-server/protocol
        :argot
        :argot-server/ast-utils
        :software-evolution-library)
  (:import-from :argot-server/annotation-host
                :contributions-by-kind)
  (:export :comment-tests-for
           :pack-tested-ast
           :with-tested-ast-pack
           :test-examples-for))
(in-package :argot-server/muses/example-view-muse)
(in-readtable argot-readtable)

(defgeneric line-comment-prefix (software)
  (:documentation
   "Get the string prefix for a line comment for SOFTWARE.
If the prefix is unknown, return nil.")
  (:method ((software software)) nil)
  (:method ((software python)) "#")
  (:method ((software c/cpp)) "//")
  (:method ((software javascript)) "//"))

(-> parse-io-comment (string &key
                             (:prefix (or string null))
                             (:name t))
    (or null example-call))
(defun parse-io-comment (string &key prefix (name ""))
  "Parse a string having the form `[inputs] -> [outputs]' into an
example-call instance."
  (setf name (or name ""))
  (let ((string (trim-whitespace string)))
    (when prefix
      (unless (string^= prefix string)
        (return-from parse-io-comment nil))
      (setf string (drop-prefix prefix string)))
    (match (split "->" (trim-whitespace string))
      ((list args out)
       (let* ((args (split "[, ]+" args))
              (args (mapcar (op (parse-integer _ :junk-allowed t))
                            args))
              (out (parse-integer out :junk-allowed t)))
         (and (integerp out)
              (every #'integerp args)
              (make 'example-call
                    :comparator "=="
                    :name name
                    :arguments args
                    :output out)))))))

(defgeneric comment-tests-for (software ast)
  (:documentation "Return asts of TYPE in SOFTWARE with associated comment tests.")
  (:method ((software software) (ast ast))
    (append
     (nest
      (let ((prefix (line-comment-prefix software))))
      (filter-map
       {parse-io-comment _ :prefix prefix :name (function-name ast)})
      (mapcar #'source-text)
      (comments-for-strict software ast)))))

(defun test-examples-for (uri software ast
                          &key (muse *server*)
                            (contribs (contributions-by-kind
                                       (muse-server muse)
                                       uri
                                       ArgotKind.Tests)))
  "Extract examples from the tests for enclosing function of AST.
The tests are the ones previously extracted by the test view muse(s)."
  (when-let* ((function (find-enclosing 'function-ast software ast))
              (name (function-name function))
              (relevant-test-prefix (string+ "test_" name))
              (contribs contribs)
              (tests
               (nest
                (filter (op (string^= relevant-test-prefix (name _))))
                (mapcar {convert 'TestCase})
                (mappend (op (gethash "tests" (convert 'hash-table (data _))))
                         contribs))))
    (filter (op (member (slot-value _ '|comparator|)
                        '("=" "==" "->" "!=" "!==")
                        :test #'equal))
            (mappend #'test-case-examples tests))))

;;; TODO Should this be the default behavior of comments-for?
(defun comments-for-strict (software ast)
  "Like `comments-for', but drop comments that aren't specificially for the node."
  (flet ((next-relevant-node (comment)
           (first
            (find-following
             '(or control-flow-ast function-ast)
             software
             comment))))
    (let ((comments (reverse (comments-for software ast))))
      (reverse (first (assort comments :key #'next-relevant-node))))))


;;; Software object packing and unpacking for LSP communication
(defun pack-tested-ast (software ast examples)
  (dict "software-class" (symbol-name (type-of software))
        "position" (serialize-path (position ast software))
        "text" (genome-string software)
        "examples" (mapcar {convert 'hash-table} examples)))

(defmacro with-tested-ast-pack
    ((arguments
      &key (doc (gensym)) (software (gensym)) (ast (gensym)) (examples (gensym)) (text (gensym))
      &aux (params (gensym)))
     &body body)
  `(destructuring-bind (,doc ,params) ,arguments
     (declare (ignorable ,doc))
     (let* ((,text (gethash "text" ,params))
            (,examples (gethash "examples" ,params))
            (,software (from-string
                        (make-instance (intern (gethash "software-class" ,params) :sel/sw/ts))
                        ,text))
            (,ast (@ ,software (deserialize-path (gethash "position" ,params)))))
       (declare (ignorable ,software ,ast ,examples ,text))
       ,@body)))
