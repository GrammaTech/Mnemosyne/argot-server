(defpackage :argot-server/muses/godbolt-muse
  (:use :gt/full
        :argot-server/muses/base
        :argot-server/muses/code-actor
        :argot-server/muses/http-muse
        :drakma
        :argot-server/ast-utils
        :argot-server/utility/json-api
        :cmd)
  (:local-nicknames (:g :argot-server/utility/godbolt))
  (:import-from :lsp-server
                :connection-prop)
  (:export :godbolt-muse)
  (:documentation "Muse that integrates Compiler Explorer via LSP."))
(in-package :argot-server/muses/godbolt-muse)
(in-readtable argot-readtable)

(defclass godbolt-muse (code-actor port-muse http-muse)
  ((langs))
  (:default-initargs
   :name "Compiler Explorer"
   :force-two-round-code-actions t
   :port 7171))

(defmethod muse-start ((m godbolt-muse))
  (setf (muse-client m)
        (make 'api
              :host (service-host "godbolt")
              :port (muse-port m)
              :scheme :http)))

(defmethod muse-stop progn ((m godbolt-muse) &key)
  (setf (muse-client m) nil))

(defun id-lang (id)
  "Convert a Compiler Explorer language ID to a tree-sitter class name."
  (when (stringp id)
    (string-case id
      ("c++" 'cpp)
      (otherwise
       (find-symbol (string-upcase id)
                    :sel/sw/tree-sitter)))))

(defun supported-languages ()
  "Return a list of supported languages, as tree-sitter class names."
  (filter-map #'id-lang (mapcar #'g:id (g:languages))))

(defmethod muse-lang ((muse godbolt-muse))
  (or (slot-value-safe muse 'langs)
      (let ((langs
             (let ((g:*godbolt* (muse-client muse)))
               (if (muse-started-p muse)
                   (cons 'or (supported-languages))
                   '(or c cpp)))))
        (synchronized (muse)
          (or (slot-value-safe muse 'langs)
              (setf (slot-value muse 'langs) langs))))))

(defun default-compiler ()
  "Return the default compiler for the current connection."
  (or (connection-meta 'default-compiler)
      (find "clangdefault" (g:compilers)
            :key #'g:id
            :test #'equal)))

(defun (setf default-compiler) (value)
  "Set the default compiler for the current connection."
  (setf (connection-meta 'default-compiler)
        (assure g:compiler value)))

(defmethod muse-commands append ((muse godbolt-muse))
  (list :godbolt.dis))

(defmethod applicable-commands ((muse godbolt-muse)
                                &key start end text-document)
  (declare (ignore text-document))
  (nest
   (let ((buffer (point-buffer start))))
   (when-let ((lang-sym (guess-language buffer))))
   (let ((text (buffer-to-string buffer))))
   (ignore-errors)
   (let ((sw (from-string (make lang-sym) text))))
   (when (ast-valid? sw))
   (let ((start (convert 'source-location start))
         (end (convert 'source-location end))))
   (when-let (fun (nearest-function-node sw start end)))
   ;; TODO One action per compiler?
   (list)
   (make 'Command
         ;; TODO Mention compiler?
         :title (fmt "Disassemble ~a"
                     (function-name fun))
         :command (string :godbolt.dis)
         :arguments)
   (when-let (range (node-source-range sw fun)))
   (list (buffer-uri buffer)
         (convert 'Position (begin range))
         (convert 'Position (end range)))))

(defmethod apply-command ((muse godbolt-muse)
                          (cmd (eql :godbolt.dis))
                          arguments
                          &aux (g:*godbolt* (muse-client muse)))
  (mvlet* ((uri start end
            (destructuring-bind (uri start end) arguments
              (values uri start end)))
           (start-line
            (ematch (convert 'Position start)
              ((|Position| :line line)
               line)))
           (end-line
            (ematch (convert 'Position end)
              ((|Position| :line line)
               (values line))))
           ;; TODO Just grab the function in the range and filter out
           ;; everything else? You still needs imports, struct
           ;; definitions, etc. Also across files?
           (asm (asm-by-lines (buffer-to-string (uri-buffer uri)))))
    (iter (for (source-line . asm-lines) in asm)
          (for line from 0)
          (while (<= line end-line))
          (when (>= line start-line)
            (unless (emptyp asm-lines)
              (collecting
               (make 'Diagnostic
                     :severity DiagnosticSeverity.Hint
                     ;; TODO
                     :source (g:name (default-compiler))
                     :message asm-lines
                     :range (make 'Range
                                  :start (make 'Position
                                               :character 0
                                               :line line)
                                  :end (make 'Position
                                             :character
                                             ;; TODO UTF-16
                                             (length source-line)
                                             :line line)))
               into diagnostics)))
          (finally
           (jsonrpc:notify *server*
                           "textDocument/publishDiagnostics"
                           (convert 'hash-table
                                    (make 'PublishDiagnosticsParams
                                          :uri uri
                                          :diagnostics diagnostics)))))))

(defmethod on-change progn ((muse godbolt-muse) uri &key)
  (clear-diagnostics uri))

(-> asm-string (string) string)
(defun asm-string (string)
  "Given STRING, a string of code, compile it with the default
compiler and return the resulting assembly as a string."
  (nest
   (princ-to-string)
   (g:compile (default-compiler))
   string))

(-> asm-by-lines (string) list)
(defun asm-by-lines (string)
  "Return an alist of (line . disassembly) for each line in STRING."
  (flet ((asm-text (asm)
           (trim-whitespace (g:asm-text asm)))
         (asm-line (asm)
           (g:source-line (g:asm-source asm))))
    (let* ((output (g:compile (default-compiler) string))
           (lines (lines string))
           (asms (g:output-asm output))
           ;; Remove asm nodes without source (like the function name).
           (asms (remove-if #'null asms :key #'g:asm-source))
           (runs (runs asms :key #'asm-line))
           ;; NB Is it NOT the case that ASM lines for the same source
           ;; line are continuous!
           (lines.runs
            (iter (for run in runs)
                  (for i = (asm-line (first run)))
                  (collect (cons i (string-join
                                    (mapcar #'asm-text run)
                                    #\Newline))))))
      (iter (for line in lines)
            (for i from 1)
            (for relevant-runs = (keep i lines.runs :key #'car))
            (for text = (mapconcat #'cdr relevant-runs (fmt "~%...~%")))
            (collect (cons line text))))))
