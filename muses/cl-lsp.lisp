(defpackage :argot-server/muses/cl-lsp
  (:use :gt/full
        :argot-server/readtable
        :argot-server/muses/base
        :argot-server/muses/passthrough
        :argot-server/muses/background-thread-muse)
  (:import-from :cl-lsp/main)
  (:export :cl-lsp)
  (:documentation "Muse that controls CL-LSP (running in the same image)."))
(in-package :argot-server/muses/cl-lsp)

(defclass cl-lsp (tcp-muse background-thread-muse)
  ()
  (:documentation "Handle for a CL-LSP instance.

Althrough the CL-LSP instance is running in the same Lisp image, we
still communicate with it over TCP as that is how it is designed to
run.")
  (:default-initargs
   :name "CL-LSP"
   :lang 'lisp))

(defmethod muse-available? :around ((muse cl-lsp))
  t)

(defmethod muse-foreground ((muse cl-lsp))
  (let ((port (muse-port muse)))
    (cl-lsp/main:run-tcp-mode :port port)))
