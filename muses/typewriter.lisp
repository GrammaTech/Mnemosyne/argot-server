(defpackage :argot-server/muses/typewriter
  (:use :gt/full
        :argot-server/readtable
        :argot-server/muses/base
        :argot-server/muses/jsonrpc-muse
        :argot-server/muses/work-queue
        :argot
        :argot-server/ast-utils
        :argot-server/annotation-host)
  (:import-from :moira)
  (:import-from :jsonrpc/transport/http)
  (:export :typewriter-muse
           :*typewriter-script*
           :string-predictions
           :predictions->annotations
           :annotations->predictions))
(in-package :argot-server/muses/typewriter)
(in-readtable argot-readtable)

(defclass typewriter-muse (work-queue-muse port-muse jsonrpc-muse) ()
  (:default-initargs
   :name "TypeWriter"
   :lang 'python
   :port 4000)
  (:documentation "A muse that talks to a TypeWriter server."))

(defmethod process-uri ((muse typewriter-muse) (uri string))
  "Perform the work of the update loop."
  (handler-bind
      ((error
        (lambda (e)
          (princ e *error-output*)
          (continue e))))
    (with-simple-restart (continue "Skip ~a" uri)
      (show-message (fmt "Synthesizing types for ~a" uri) :type :log)
      (annotate-uri muse uri))))

(defun annotate-uri (muse uri)
  "Compute and save the annotations for URI."
  (save-annotations muse (compute-annotations muse uri)))

(defun compute-annotations (muse uri)
  "Compute the annotations for URI."
  (let* ((buffer (get-buffer-from-uri uri))
         (string (buffer-to-string buffer))
         (predictions (string-predictions muse string)))
    (and (stringp predictions)
         (predictions->annotations uri predictions))))

(defun save-annotations (muse annotations)
  "Save ANNOTATIONS."
  (check-type annotations list)
  (when annotations
    (match (first annotations)
      ((Annotation :textDocument (TextDocumentIdentifier :uri |uri|))
       (show-message (fmt "Type candidates available for ~a" |uri|) :type :log)))
    (did-annotate muse annotations)))

(-> string-predictions (muse string) (or string null))
(defun string-predictions (muse string)
  "Get the predictions for STRING."
  (nest
   (when (valid-python? string))
   (gethash "prediction list")
   (jsonrpc:call (muse-client muse) "run_type_writer")
   (dict "source" string)))

(-> predictions->annotations (string (or string hash-table)
                                     &key (:software (or nil software)))
    (soft-list-of Annotation))
(defun predictions->annotations (uri predictions &key software)
  "Convert PREDICTIONS, a JSON string or hash table, into annotations for URI."
  (etypecase predictions
    (string
     (predictions->annotations
      uri
      (let ((yason:*parse-object-as* :hash-table))
        (yason:parse predictions))))
    (hash-table
     (let ((software (or software (uri-buffer-software uri))))
       ;; "The predictions Dictionary, in full, has the type: Dict[str,
       ;; Dict[str, Tuple[Dict[str, int],int]]]. This is a Dictionary
       ;; from function name to a Dictionary of variable names, to Tuple
       ;; containing a Dictionary of string (a predicted type assignment)
       ;; to int (a predicted assignments rank, lower is more probable)
       ;; and an integer (the variables type assignment location)"
       (iter annotations
             (for (fn-name var-preds) in-hashtable predictions)
             (iter (for (var-name (ranks (ln col)))
                        in-hashtable var-preds)
                   (nest
                    (in annotations)
                    (collect)
                    (assure Annotation)
                    (if (equal var-name "return")
                        (function-node-annotation uri software fn-name ranks)
                        (fval-annotation uri var-name ranks ln col)))))))))

(-> annotations->predictions ((soft-list-of Annotation)
                              &key (:software (or software null)))
    hash-table)
(defun annotations->predictions (annotations &key software)
  (lret ((dict (dict)))
    (labels ((function-predictions (name)
               (ensure-gethash name dict (dict)))
             (variable-predictions (fname name ln col)
               (ensure-gethash name (function-predictions fname)
                               (list (dict) (list ln col))))
             (add-prediction (fname name type n ln col)
               (setf (gethash type
                              (first (variable-predictions fname name ln col)))
                     n)))
      (dolist (a (filter (op (slot-boundp _ '|range|)) annotations))
        (match a
          ((Annotation
            :text-document (TextDocumentIdentifier :uri uri)
            :range (and range
                        (Range :start (Position :line line :character char)))
            :contributions contributions)
           (let* ((ln (1+ line))
                  (col char)
                  (software (or software (uri-buffer-software uri)))
                  (subseq (range-subseq software range))
                  (function-name (function-name-at software range)))
             (dolist (c contributions)
               (match c
                 ((Contribution
                   :source "TypeWriter"
                   :kind #.ArgotKind.Types
                   :data data)
                  (match data
                    ((TypeCandidates :candidates candidates)
                     (dolist (c candidates)
                       (ematch c
                         ((TypeCandidate :type-name type :probability n)
                          (add-prediction function-name
                                          subseq
                                          type
                                          n
                                          ln
                                          col)))))
                    ((FunctionReturnTypeCandidates :candidates candidates)
                     (dolist (c candidates)
                       (ematch c
                         ((FunctionReturnTypeCandidate
                           :probability n
                           :function-name fname
                           :returns type)
                          (add-prediction fname "return" type n
                                          ln col))))))))))))))))

(defun range-subseq (software range)
  (source-text
   (first
    (asts-contained-in-source-range
     software
     (convert 'source-range range)))))

(defun function-name-at (software range)
  (let* ((asts
          (asts-containing-source-location
           software
           (begin (convert 'source-range range))))
         (functions (find-if (of-type 'function-ast)
                             asts
                             :from-end t)))
    ;; In case it's in a lambda.
    (some #'function-name functions)))

(defun fval-annotation (uri var-name types.ranks ln col)
  "Build an annotation for a variable."
  (let* ((start
          (make 'Position
                :line (1- ln)
                :character col))
         (end
          (make 'Position
                :line (1- ln)
                :character (+ col (length var-name))))
         (range
          (make 'Range :start start :end end))
         (contributions
          (make 'Contribution
                :source "TypeWriter"
                :kind ArgotKind.Types
                :data
                (make 'TypeCandidates
                      :candidates
                      (etypecase types.ranks
                        (hash-table
                         (iter (for (type-name prob) in-hashtable types.ranks)
                               (collect
                                (make 'TypeCandidate
                                      :type-name type-name
                                      :probability prob))))
                        (string
                         (list
                          (make 'TypeCandidate
                                :type-name types.ranks
                                :probability 1))))))))
    (make 'Annotation
          :textDocument (make 'TextDocumentIdentifier
                              :uri uri)
          :range range
          :contributions (list contributions))))

(defun function-node-annotation (uri software fn-name types.ranks)
  "Build an annotation for a function return type."
  (let* ((ast (genome software))
         (node (find fn-name ast
                     :key #'function-name
                     :test #'equal))
         (source-range (node-source-range ast node))
         (lsp-range (convert 'Range source-range)))
    (make 'Annotation
          :textDocument (make 'TextDocumentIdentifier :uri uri)
          :range lsp-range
          :contributions
          (list
           (make 'Contribution
                 :source "TypeWriter"
                 :kind ArgotKind.Types
                 :data
                 (make 'FunctionReturnTypeCandidates
                       :candidates
                       (etypecase types.ranks
                         (hash-table
                          (iter (for (returns prob) in-hashtable types.ranks)
                                (collect
                                 (make
                                  'FunctionReturnTypeCandidate
                                  :function-name fn-name
                                  :returns returns
                                  :probability prob))))
                         (string
                          (list
                           (make
                            'FunctionReturnTypeCandidate
                            :function-name fn-name
                            :returns types.ranks
                            :probability 1))))))))))
