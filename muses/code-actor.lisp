(defpackage :argot-server/muses/code-actor
  (:use :gt/full
        :argot-server/readtable
        :argot-server/lsp-diff
        :argot-server/muses/base
        :lsp-server/protocol
        :argot-server/merge-results
        :argot-server/utility
        :lsp-server/utilities/worker-thread
        :software-evolution-library)
  (:import-from :argot-server/muses/muse
                :contribute-server-capabilities)
  (:export :code-actor
           :apply-command
           :applicable-commands
           :code-actions
           :muse-code-action-kinds
           :muse-commands
           :force-two-round-code-actions*
           ;; Helpers
           :with-document-software
           :make-workspace-edit-diff
           :serialize-path
           :deserialize-path
           :pack-ast
           :with-ast-pack
           :relevant-asts
           :undeclared-commands)
  (:documentation "Define a base class for muses that provide code actions."))
(in-package :argot-server/muses/code-actor)
(in-readtable argot-readtable)

;;;; LSP Methods: codeAction requested synthesis.
;;;
;;; https://microsoft.github.io/language-server-protocol/specification#textDocument_codeAction
;;;

;;; "Two-round" code actions are when we send back a list of commands
;;; and the client selects one to execute.

(defclass code-actor (muse)
  ((force-two-round-code-actions
    :initarg :force-two-round-code-actions
    :accessor force-two-round-code-actions))
  (:documentation "A muse able to respond to requests for code
actions.  All code actors should provide defmethods for the
`applicable-commands' generic function.")
  (:default-initargs
   :force-two-round-code-actions nil))

(defplace force-two-round-code-actions* ()
  (force-two-round-code-actions *server*))

(defgeneric muse-code-action-kinds (muse)
  (:method-combination append)
  (:method append ((muse t))
    nil)
  (:method append ((muse code-actor))
    (list CodeActionKind.Empty "synthesis.code"))
  (:documentation "A list of code action kinds supported by the muse."))

(define-condition undeclared-commands (warning)
  ((muse :initarg :muse)
   (commands :initarg :commands :type list))
  (:report
   (lambda (c s)
     (with-slots (muse commands) c
       (format s "Returning undeclared command names from ~a: ~a~2%~
Command names should be declared in advance in ~s."
               muse commands 'muse-commands)))))

#+type-for-documentation-only
(-> applicable-commands
    (t &key (:start point) (:end point) (:text-document |TextDocumentIdentifier|))
    (trivial-types:proper-list |Command|))
(defgeneric applicable-commands (code-actor &key start end text-document)
  (:documentation
   "Return the applicable commands of CODE-ACTOR to request, as instances of Command.

For two-round code actions, these are sent to the client directly. For
one-round code actions, they are nested in instances of CodeAction.

Commands names must be declared in advance with `muse-commands'. A
warning will be signaled if undeclared commands are returned.")
  (:method-combination standard/context)
  (:method :context ((muse code-actor) &key start end text-document)
    (declare (ignore start end))
    (when text-document
      (unless (string^= "file://" (slot-value text-document '|uri|))
        (return-from applicable-commands)))
    (let* ((result (call-next-method))
           (command-names
            (mapcar (op (slot-value _ '|command|)) result))
           (undeclared
            (set-difference command-names (muse-commands muse)
                            :test #'string=)))
      (when undeclared
        (warn 'undeclared-commands
              :commands undeclared
              :muse muse))
      result)))

#+type-for-documentation-only
(-> apply-command (t keyword t) |ApplyWorkspaceEditParams|)
(defgeneric apply-command (muse command arguments)
  (:documentation "Return ApplyWorkspaceEditParams resulting from COMMAND.")
  (:method (muse command args)
    (declare (ignore muse command args))
    nil))

#+type-for-documentation-only
(-> code-actions
    (t &key (:start point) (:end point) (:text-document |TextDocumentIdentifier|))
    (trivial-types:proper-list |CodeAction|))
(defgeneric code-actions (code-actor &key start end text-document)
  (:documentation
   "Return results of running applicable commands of CODE-ACTOR to request.")
  (:method ((muse code-actor) &key start end text-document)
    (mapcar (lambda (cmd)
              (with-slots (|title| |arguments|) cmd
                (match |arguments|
                  ((list* (and action (type CodeAction)) _)
                   (make 'CodeAction
                      :|title| |title|
                      :|edit| (slot-value action '|edit|)))
                  (otherwise
                   (make 'CodeAction :|title| |title|)))))
            (assure list
              (applicable-commands muse
                                   :start start
                                   :end end
                                   :text-document text-document)))))

(defmethod contribute-server-capabilities list ((muse code-actor))
  (make 'ServerCapabilities
        :|textDocumentSync| |TextDocumentSyncKind.Incremental|
        :|codeActionProvider|
        (make '|CodeActionOptions|
              :|codeActionKinds|
              (nest (nub)
                    (remove-if #'emptyp)
                    (mapcar #'string)
                    (muse-code-action-kinds muse)))
        :executeCommandProvider
        (make 'ExecuteCommandOptions
              :commands (nest (nub)
                              (mapcar #'string)
                              (muse-commands muse)))))

(define-class-method "textDocument/codeAction" code-actor
    (params |CodeActionParams|)
  (let ((document-changes-p
          ;; Does this client support workspace/documentChanges?
          (handler-case
              (reduce
               (lambda (acc name)
                 (slot-value acc name))
               '(|capabilities| |workspace| |workspaceEdit| |documentChanges|)
               :initial-value (initialize-params*))
            (unbound-slot () nil))))
    (with-slots (|textDocument| |range|) params
      (with-slots (|start| |end|) |range|
        (with-document-position (start (slot-value |textDocument| '|uri|) |start|)
          (with-point ((end start))
            (move-to-lsp-position end |end|)
            (if (or (force-two-round-code-actions*) document-changes-p)
                ;; If this client supports workspace/documentChanges return commands.
                (applicable-commands *server* :start start :end end
                                              :text-document |textDocument|)
                ;; If not return CodeActions to be run directly.
                (code-actions *server* :start start :end end
                                       :text-document |textDocument|))))))))

(define-class-method "workspace/executeCommand"
    code-actor
    (params |ExecuteCommandParams|)
  (let ((muse *server*))
    (with-slots (|command| |arguments|) params
      (with-lsp-worker-thread (:name (fmt "Executing command ~a" |command|))
        (when-let* ((result
                     (with-simple-restart (abort "Abort command")
                       (apply-command muse
                                      (make-keyword |command|)
                                      |arguments|)))
                    (request-params
                     (convert 'hash-table result)))
          (lsp-server:request-log
           '|workspace/executeCommand| request-params)
          ;; TODO Invalidate if the buffer has changed? Or do versions
          ;; cover that?
          (jsonrpc:call-async muse "workspace/applyEdit" request-params)))))
  ;; Empty reply to the client as we acted through an explicit client request.
  nil)


;;;; Helper functions for writing muses

(-> relevant-asts (point point &key (:software (or software nil)))
    (values list software &optional))
(defun relevant-asts
    (start end &key (software (buffer-software (point-buffer start))))
  "Return the relevant ASTs for the region between START and END.
That is, return the ASTs that that fall inside the region, or, if
there are no such ASTs, then return the ASTs that the region falls
inside.

Practically this means that if there is no selection we act on the
surrounding ASTs rather than doing nothing."
  (declare (type point start end))
  (values
   (ignore-some-conditions (reader-error)
     (or (asts-contained-in-source-range
          software
          (points->source-range start end))
         (asts-containing-source-location
          software
          (convert 'source-location start))))
   software))

(defun uri-original-path (uri)
  (when uri
    (when-let (root (project-root-for uri))
      (string-left-trim "/" (drop-prefix root uri)))))

(defmacro with-document-software
    (((start end)
      &key (software (gensym)) (text (gensym))
        (uri (gensym))
        asts
        (buffer (gensym))
        (language (gensym))
        (software-class (gensym)))
     &body body)
  "Load an SEL software object from a document pointer."
  (once-only (start end)
    `(when-let* ((,buffer (point-buffer ,start))
                 (,uri (buffer-uri ,buffer))
                 (,language (guess-language ,buffer))
                 (,software-class (lang-software ,language))
                 (,text (buffer-to-string ,buffer))
                 (,software (from-string
                             (make ,software-class
                                   :original-path (uri-original-path ,uri))
                             ,text))
                 ,@(and asts
                        `((,asts (relevant-asts ,start ,end
                                                :software ,software)))))
       (progn ,end)                     ;Allow end to be ignored.
       ,@body)))

(defun make-workspace-edit-diff
    (doc old-text new-text &key (label (error "Label is required")))
  (nest
   (make 'ApplyWorkspaceEditParams :|label| label :|edit|)
   (make-instance 'WorkspaceEdit :documentChanges) (list)
   (make-instance 'TextDocumentEdit
                  :textDocument (make 'OptionalVersionedTextDocumentIdentifier
                         :uri (gethash "uri" doc)
                         :version nil)
     :|edits|)
   (with-temporary-file-of (:pathname old-file) old-text)
   (with-temporary-file-of (:pathname new-file) new-text)
   (lsp-edit-diff old-file new-file)))

(defun serialize-path (path)
  (mapcar (named-lambda rec (step)
            (etypecase step
              (number step)
              (symbol (symbol-name step))
              ;; Index into a slot of variable arity that is not the
              ;; children slot.
              ((cons symbol number)
               (list (rec (car step))
                     (rec (cdr step))))))
          path))

(defun deserialize-path (path)
  (flet ((find-slot-name (string)
           (find-symbol string :sel/sw/ts)))
    (mapcar (lambda (step)
              (etypecase step
                (number step)
                (string (find-slot-name step))
                ((tuple string number)
                 (cons (find-slot-name (first step))
                       (second step)))))
            path)))

(defun pack-ast (software ast)
  (dict "software-class" (symbol-name (type-of software))
        "position" (serialize-path (position ast software))
        "text" (genome-string software)))

(defmacro with-ast-pack
    ((arguments
      &key (doc (gensym)) (software (gensym)) (ast (gensym)) (text (gensym)) uri
      &aux (params (gensym)))
     &body body)
  `(destructuring-bind (,doc ,params) ,arguments
     (let* (,@(when uri `((,uri (gethash "uri" ,doc))))
            (,text (gethash "text" ,params))
            (,software (from-string
                        (make-instance (intern (gethash "software-class" ,params) :sel/sw/ts))
                        ,text))
            (,ast (@ ,software (deserialize-path (gethash "position" ,params)))))
       ,@body)))
