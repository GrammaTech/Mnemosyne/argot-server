(defpackage :argot-server/muses/http-muse
  (:use :gt/full
        :argot-server/muses/base)
  (:export :http-muse :make-http-muse-client))
(in-package :argot-server/muses/http-muse)

(defclass http-muse (muse)
  ((client :initform nil :accessor muse-client))
  (:documentation
   "Mixin for muses that run over HTTP but don't use LSP."))

(defmethod muse-available? and ((muse http-muse))
  (port-listening-p (muse-host muse)
                    (muse-port muse)))

(defmethod muse-started-p and ((muse http-muse))
  (muse-client muse))
