(defpackage :argot-server/muses/python-type-view
  (:use :gt/full
        :argot-server/readtable
        :argot-server/muses/base
        :argot-server/muses/type-view
        :argot-server/ast-utils
        :lsp-server/protocol
        :argot)
  (:export :python-type-view))
(in-package :argot-server/muses/python-type-view)
(in-readtable argot-readtable)

(defclass python-type-view (type-view)
  ()
  (:documentation "Muse for inserting Python types.")
  (:default-initargs
   :force-two-round-code-actions t
   :lang 'python))

(defmethod contrib-text-edits ((muse python-type-view)
                               (data FunctionReturnType)
                               &key)
  (let ((edits (call-next-method)))
    (dolist (edit edits edits)
      (setf (slot-value edit '|newText|)
            (string+ " -> " (slot-value edit '|newText|))))))
