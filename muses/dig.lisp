(defpackage :argot-server/muses/dig
  (:use :gt/full
        :argot-server/readtable
        :argot-server/lsp-diff
        :argot-server/muses/base
        :argot-server/muses/code-actor
        :lsp-server
        :lsp-server/protocol
        :argot
        :argot-server/ast-utils
        :software-evolution-library
        :argot-server/muses/example-view-muse
        :dig)
  (:export :dig))
(in-package :argot-server/muses/dig)
(in-readtable argot-readtable)

(defclass dig (code-actor) ()
  (:documentation "Integrate DIG (see https://github.com/unsat/dig) as a code actor.")
  (:default-initargs :force-two-round-code-actions t))

(defmethod muse-code-action-kinds append ((dig dig))
  (list "CodeActionKind.InferInvariant"))

(defmethod muse-commands append ((dig dig))
  (list :argot.dig))

(defmethod applicable-commands ((dig dig) &key start end text-document)
  "Return a list of potential invariant inference locations."
  (with-document-software ((start end)
                           :software software
                           :asts asts
                           :uri uri)
    (filter-map
     (lambda (ast)
       (when-let ((tests
                   (append (comment-tests-for software ast)
                           (test-examples-for uri software ast))))
         (when (typep ast 'control-flow-ast)
           (make 'Command
                 :|title|
                 (format nil "Infer invariant at ~s from ~d examples"
                         (concatenate 'string (take 8 (source-text ast)) "...")
                         (length tests))
                 :|command| (string :argot.dig)
                 :|arguments|
                 (list text-document (pack-tested-ast software ast tests))))))
     asts)))

(defmethod apply-command ((dig dig)
                          (cmd (eql :argot.dig))
                          arguments)
  (declare (ignorable cmd))
  (flet ((show (message) (show-message message :server dig)))
    (with-tested-ast-pack (arguments
                           :doc doc :software software
                           :ast ast :examples tests)
      (show "Starting Dynamic Invariant Generator...")
      (annotate-workspace-edit
       "DIG inferred invariants"
       (make-workspace-edit-diff
        doc
        (genome-string software)
        (genome-string
         (infer-and-inject-assertion-at
          software
          (mapcar {gethash "arguments"} tests)
          ast
          :before-trace {show "Collecting dynamic traces..."}
          :before-dig {show "Generating invariants..."}
          :before-check {show "Verifying invariants..."}))
        :label "insert invariant")))))
