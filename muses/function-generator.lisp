;;; function-generator.lisp --- Muse for function body generation.
;;;
;;; This muse leverages OpenAI's Codex machine learning model to
;;; automatically generate function bodies when a function prototype
;;; and docstring are given and offer them to clients as code actions.
(defpackage :argot-server/muses/function-generator
  (:use :gt/full
        :argot-server/muses/base
        :argot-server/muses/pygls
        :argot-server/muses/passthrough)
  (:export :function-generator))
(in-package :argot-server/muses/function-generator)

(defclass function-generator (external-code-actor-muse tcp-muse pygls) ()
  (:default-initargs
   :name "Function Generator"
   :lang 'python
   :host "function-generator"
   :port 8081))
