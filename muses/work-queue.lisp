;;; work-queue.lisp - mixin muse with a thread-safe queue of URIs to
;;; process in a background thread

(defpackage :argot-server/muses/work-queue
  (:use :gt/full
        :lsp-server/utilities/lsp-utilities
        :argot-server/readtable
        :argot-server/muses/muse
        :argot-server/muses/background-thread-muse
        :argot-server/utility)
  (:import-from :lsp-server
                :get-buffer-from-uri)
  (:local-nicknames (:lp.q :lparallel.queue)
                    #+sbcl (:md5 :sb-md5))
  ;; Use SBCL's internal MD5 if available, as it's (hopefully) faster.
  #-sbcl (:import-from :md5 :md5sum-string)
  (:shadow :never)
  (:export :work-queue-muse
           :process-uri))
(in-package :argot-server/muses/work-queue)
(in-readtable argot-readtable)


;;; Definition of the work-queue-muse mixin.
(defclass work-queue-muse (background-thread-muse)
  ((update-queue :initform (lp.q:make-queue)
                 :reader update-queue)
   (uri-stamps :initform (make-hash-table :test 'equal)
               :reader uri-stamps)
   (uri-stamps-lock :initform (make-recursive-lock "Work queue lock")
                    :reader uri-stamps-lock))
  (:documentation "Mixin muse with a thread-safe queue or URIs to process
in a background thread."))

(defgeneric process-uri (muse uri)
  (:documentation "Entry point for work-queue muses which should be overriden
with the implementation-specific processing of a URI to be performed."))

(defmethod process-uri :before ((muse work-queue-muse) (uri string))
  "Update the uri-stamp associated with URI with the current file contents
after processing."
  (update-stamp muse uri))

(defmethod muse-foreground ((muse work-queue-muse))
  "Main processing loop of the back-ground thread.  Pop URIs from the update
queue and if the file contents of URI have changed since last update, dispatch
to `process-uri`."
  (iter (and-let* ((uri (lp.q:pop-queue (update-queue muse)))
                   ((should-process-uri? muse uri)))
          (process-uri muse uri))))

(defun should-process-uri? (muse uri)
  "Return T if URI has changed and should be sent to the background
thread for processing."
  (and uri
       (muse-started-p muse)
       (has-lsp-buffer? uri)
       (needs-update? muse uri)))

(defun has-lsp-buffer? (uri)
  "Return non-NIL if there is an LSP buffer open for URI."
  (get-buffer-from-uri uri))

(defmethod on-change progn ((muse work-queue-muse) (uri string) &key)
  "LSP hook to push URI to the update queue upon changes to URI."
  (enq-uri-for-update muse uri))

(defun enq-uri-for-update (muse uri)
  "Push URI to the queue for updating."
  (lp.q:push-queue uri (update-queue muse)))


;;; Creation and management of uri-stamps representing file contents.
(defstruct-read-only uri-stamp
  "A value representing the version of a uri."
  (hash :type octet-vector)
  (size :type (integer 0 *)))

(def never
  (make-uri-stamp :hash (make-octet-vector 0)
                  :size 0)
  "Initial \"stamp\" for a file that has never been annotated.")

(defun hash-string (string)
  "Get the MD5 hash of a string."
  (md5:md5sum-string string :external-format :utf-8))

(defun string-stamp (string)
  "Get the uri-stamp of a string."
  (make-uri-stamp :hash (hash-string string)
                  :size (length string)))

(defun uri-stamp (uri)
  "Get a value representing the state of a file managed by LSP.
This is intended to be opaque, but at the moment consists of (1) the
MD5 of the file's contents and (2) the length of the file."
  (if-let ((buffer (get-buffer-from-uri uri)))
    (string-stamp (buffer-to-string buffer))
    never))

(defun needs-update? (muse uri)
  "Return T if the uri-stamp for URI is different from the last
time it was processed."
  (not (equalp (uri-stamp uri)
               (last-update muse uri))))

(defun last-update (muse uri)
  "Get the uri-stamp associated with URI the last time it was
processed."
  (with-lock-held ((uri-stamps-lock muse))
    (gethash uri (uri-stamps muse) never)))

(defun update-stamp (muse uri)
  "Update the uri-stamp associated with URI with the current file contents."
  (with-lock-held ((uri-stamps-lock muse))
    (setf (gethash uri (uri-stamps muse))
          (uri-stamp uri))))
