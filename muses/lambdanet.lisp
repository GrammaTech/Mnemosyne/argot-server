(defpackage :argot-server/muses/lambdanet
  (:use :gt/full
        :argot-server/muses/base
        :argot-server/muses/cli-muses
        :argot-server/readtable
        :lsp-server
        :lsp-server/protocol
        :argot)
  (:import-from :uiop/run-program :run-program)
  (:import-from :esrap
                :character-ranges
                :text
                :parse
                :defrule)
  (:import-from :moira)
  (:import-from :trivial-file-size :file-size-in-octets)
  (:import-from :jsonrpc
                :make-request
                :dispatch)
  (:import-from :argot-server/annotation-host :did-annotate)
  (:import-from :argot-server/merge-results
                :merge-results)
  (:import-from :argot-server/utility
                :*reset-hook*)
  (:local-nicknames (:i :iterate))
  (:shadow :integer :float :range :position)
  (:export :lambdanet-muse))
(in-package :argot-server/muses/lambdanet)
(in-readtable argot-readtable)

(defvar *lambdanet-verbose* nil
  "Whether LambdaNet should print debugging information.")

(defvar *roots* '()
  "A list of cached project roots.")

(defvar *file-versions* (empty-map)
  "An FSet map from file to version.")

(defvar *last-run-file-versions* (empty-map)
  "The value of `*file-versions*' at the start of the last run.
This can be compared to `*file-versions*' to check if the results are
still valid.")

(define-reset-hook reset-lambdanet ()
  (setf *roots* nil
        *file-versions* (empty-map)
        *last-run-file-versions* (empty-map)))

(defun out-of-date? (file)
  (assert (not (string^= "file:" (namestring file))))
  (let ((current-version (lookup *file-versions* file))
        (old-version (lookup *last-run-file-versions* file)))
    (not (eql current-version old-version))))

(defun locate-dominating-file (file name)
  (nlet rec ((dir (pathname-directory-pathname file))
             (name (pathname name)))
    (if (equal dir (user-homedir-pathname))
        nil
        (let ((file (make-pathname :defaults dir
                                   :name (pathname-name name)
                                   :type (pathname-type name))))
          (flet ((rec ()
                   (let ((parent (pathname-parent-directory-pathname dir)))
                     (if (equal parent dir)
                         nil
                         (rec parent name)))))
            (if (wild-pathname-p file)
                (let ((matches (directory file)))
                  (if matches
                      (values (first matches) (rest matches))
                      (rec)))
                (or (file-exists-p file)
                    (rec))))))))

(defun file-project-root (file)
  "Find the root directory of FILE's project.
This is either (1) a cached project root that FILE is below, or (2)
the result of looking backward in the directory hierarchy for the
nearest `package.json' file."
  (assure (or null directory-pathname)
    (let ((file (pathname (file-uri-path file))))
      (or (find file *roots* :test #'subpathp)
          (and-let* ((package.json
                      (locate-dominating-file file #p"package.json"))
                     (root (pathname-directory-pathname package.json)))
            (synchronized ('*roots*)
              (pushnew root *roots* :test 'equal))
            root)))))

;;; Muse.

(defclass lambdanet-muse (launcher-muse)
  ((updating :initform nil :type boolean))
  (:default-initargs
   :name "LambdaNet"
   :lang '(or javascript typescript))
  (:documentation "A muse that wraps an attached LambdaNet process."))

(defmethod muse-start :before ((muse lambdanet-muse))
  (when *lambdanet-verbose*
    (format *error-output* "~&Starting LambdaNet...~%")))

(defmethod muse-start :after ((muse lambdanet-muse))
  (moira:spawn-thread
   (muse-worker-function
    (lambda ()
      (sleep 60)
      (update-loop muse)))
   :name "LambdaNet updater"))

(defvar *lambdanet-script*
  #p"/usr/local/bin/lambdanet-muse"
  "Absolute location of the lambdanet script.
The default value is a location in the Argot Server Docker image.")

(defmethod muse-available? and ((muse lambdanet-muse))
  ;; (file-exists-p *lambdanet-script*)
  (argot-image-exists? "lambdanet"))

(define-class-method "initialize" lambdanet-muse (params |InitializeParams|)
  (merge-results
   (call-next-method)
   (make 'InitializeResult
         :capabilities
         (make 'ServerCapabilities
               :textDocumentSync |TextDocumentSyncKind.Incremental|
               :hoverProvider t))))

(define-class-method "textDocument/didOpen" lambdanet-muse
    (params DidOpenTextDocumentParams)
  "Detect new projets."
  (ematch params
    ((DidOpenTextDocumentParams
      :textDocument
      (TextDocumentItem :uri uri))
     (file-project-root uri)))
  (call-next-method))

(define-class-method "textDocument/didChange" lambdanet-muse
    (params DidChangeTextDocumentParams)
  "Update the file version after every change."
  (multiple-value-prog1 (call-next-method)
    (ematch params
      ((DidChangeTextDocumentParams
        :textDocument
        (VersionedTextDocumentIdentifier
         :version version
         :uri uri))
       (let ((uri (file-uri-path uri)))
         (file-project-root uri)
         (synchronized ('*file-versions*)
           (withf *file-versions* uri version)))))))

(define-class-method "textDocument/hover" lambdanet-muse
    (params HoverParams)
  "Trigger rebuilding if necessary on hover."
  (ematch params
    ((HoverParams
      :textDocument (TextDocumentIdentifier :uri uri))
     (file-project-root uri)))
  nil)

(defun file-predictions-as-annotations (uri predictions)
  (iter (for (((start-line . start-char)
               . (stop-line . stop-char))
              . guesses)
             in predictions)
        (let* ((start
                (make 'Position
                      :line (1- start-line)
                      :character (1- start-char)))
               (end
                (make 'Position
                      :line (1- stop-line)
                      :character (1- stop-char)))
               (range
                (make 'Range :start start :end end))
               (contributions
                (make 'Contribution
                      :source "LambdaNet"
                      :kind ArgotKind.Types
                      :data (make 'TypeCandidates
                                  :candidates
                                  (iter (for (prob name) in guesses)
                                        (collect
                                         (make 'TypeCandidate
                                               :type-name name
                                               :probability prob))))))
               (annotation
                (make 'Annotation
                      :textDocument (make 'TextDocumentIdentifier
                                          :uri uri)
                      :range range
                      :contributions (list contributions))))
          (collect annotation))))

(defun project-predictions-as-annotations (root predictions)
  (iter (for (file . sigs) in predictions)
        (for js-file = (ts->js file))
        (for uri = (fmt "file://~a~a" root js-file))
        (appending (file-predictions-as-annotations uri sigs))))

(defun save-predictions-as-annotations (muse root predictions)
  (let ((annotations (project-predictions-as-annotations root predictions)))
    (did-annotate muse annotations)))

(defmethod muse-program ((muse lambdanet-muse))
  `("docker" "run" "--rm"
             "-i"
             "--cap-drop=ALL"
             "-v" "/dev/shm/:/dev/shm/:ro"
             "--memory=14g"
             "--network=host"
             ,(string+ argot-registry "lambdanet")
             "nice" "-1" "sbt" "runMain driver.JavaDriver"))

(defmethod parse-from ((muse lambdanet-muse) stream
                       &aux (verbose *lambdanet-verbose*))
  (read-line stream nil nil)
  (iter (for line = (read-line stream nil nil))
        (while line)
        (until (string*= "DONE" line))
        (when verbose
          (print line))
        (when (string^= "[error] java.lang.OutOfMemoryError" line)
          (terminate-process (muse-process muse))
          (return))
        (collect line)))

(defmethod drive :around ((muse lambdanet-muse) (prompt t))
  "Parse the predictions returned by LambdaNet."
  (nest
   (fbind ((file-line? (curry #'string^= "[info] === File: "))
           (prediction? (curry #'string^= "[info] ("))))
   (let* ((lines (call-next-method))
          (lines
           (filter (lambda (line)
                     (or (file-line? line)
                         (prediction? line)))
                   lines))
          (runs
           (runs lines
                 :test (lambda (line1 line2)
                         (and (file-line? line1)
                              (prediction? line2)))))
          (runs
           (mapcar (lambda (run)
                     (cons (parse-file-line (first run))
                           (mapcar #'parse-prediction-line
                                   (rest run))))
                   runs)))
     runs)))

(defun list-js-ts-files (dir)
  "List the JavaScript or TypeScript files (but not type definition files!) in DIR."
  (nest
   (let ((dir (pathname-as-directory dir))))
   (remove-if (op (string$= ".d.ts" (namestring _))))
   (mapcar (op (path-join dir (drop-prefix "./" _))))
   (remove-if #'emptyp)
   (split-sequence #\Null)
   (run-program
    "find . -type f \\( -name '*.js' -o -name '*.ts' \\) -print0"
    :output '(:string :stripped t)
    :directory dir)))

(defun file-as-suffix (file)
  "Given FILE, an absolute pathname, convert it into a relative pathname with the same directory path components, suitable for appending to a base pathname."
  (assert (absolute-pathname-p file))
  (let* ((suffix
          (nlet rec ((file file))
            (ematch (pathname-directory file)
              ((list* :absolute :home suffix)
               (rec
                (make-pathname
                 :directory (append
                             (pathname-directory (user-homedir-pathname))
                             suffix)
                 :defaults file)))
              ((list* :absolute suffix)
               (make-pathname :directory `(:relative ,@suffix)
                              :defaults file))
              ((list* :relative _) file)
              (() file)))))
    (assure relative-pathname suffix)))

(defun change-pathname-type (file from to)
  "Change the pathname type (extension) of FILE."
  (declare (string from to))
  (if (equal (pathname-type file) from)
      (make-pathname :type to
                     :defaults file)
      file))

(defun js->ts (file)
  "Change extension from .js to .ts."
  (change-pathname-type file "js" "ts"))

(defun ts->js (file)
  "Change extension from .ts to .js."
  (change-pathname-type file "ts" "js"))

(defun copy-js-project (root dest)
  "Copy ROOT into a subdirectory of DEST, without collisions, updating
files only when necessary."
  (assert (absolute-pathname-p dest))
  (let* ((dest (pathname-as-directory dest))
         (files (list-js-ts-files root))
         (suffixes (mapcar #'file-as-suffix files))
         (dests (mapcar (op (path-join dest _)) suffixes))
         (dests (mapcar #'js->ts dests)))
    (iter (for src in files)
          (for dest in dests)
          (ensure-directories-exist dest)
          (when (or (not (file-exists-p dest))
                    (< (file-write-date dest)
                       (file-write-date src))
                    (/= (file-size-in-octets dest)
                        (file-size-in-octets src)))
            (copy-file src dest)))))

(defun compute-predictions (muse project-root
                            &aux (base #p"/dev/shm/"))
  "Run LambdaNet on PROJECT-ROOT and return the predictions."
  (copy-js-project project-root base)
  (drive muse (string+ base "/" project-root)))

(-> project-file-versions ((or string pathname) &key (:map fset:map)) fset:map)
(defun project-file-versions (root &key (map *file-versions*))
  "Return a map from file->version for files included in ROOT."
  (iter (i:with root = (pathname root))
        (for (k v) in-map map)
        (when (subpathp k root)
          (map-collect k v))))

(defun project-obsolete? (root)
  (let ((new (project-file-versions root :map *file-versions*))
        (old (project-file-versions root :map *last-run-file-versions*)))
    (or (empty? old)
        (not (equal? old new)))))

(defun update-obsolete-projects (muse)
  "Update the predictions for any projects that have changed since the last run."
  (let ((obsolete (filter #'project-obsolete? *roots*)))
    (setf *last-run-file-versions* *file-versions*)
    (dolist (project obsolete)
      (update-predictions muse project))))

(defun update-predictions (muse root)
  "Compute and save predictions for ROOT."
  (when *lambdanet-verbose*
    (format *error-output* "~&~%Updating predictions for ~a" root))
  (let ((predictions (compute-predictions muse root)))
    (save-predictions-as-annotations muse root predictions)))

(defun update-loop (muse)
  "Every 5 minutes, update known projects."
  (iter (for now = (get-universal-time))
        (for last-run previous now initially 0)
        (when (> (- now last-run) (* 60 5))
          (update-obsolete-projects muse))
        (sleep (* 60 5))
        (while (muse-started-p muse))))

;;; Parser.

(defun parse-file-line (line)
  "Parse a file line in the input."
  (ematch line
    ((ppcre "=== File: (.*?) ===" file)
     file)))

(defun parse-prediction-line (line)
  "Parse a prediction."
  (parse 'prediction (drop-prefix "[info] " line)))

;;; Define a parser for predictions.

(defrule integer (+ #.`(or ,@(coerce "0123456789" 'list)))
  (:text t)
  (:function parse-integer))

(defrule float (and integer "." integer)
  (:lambda (x)
    (parse-float (apply #'string+ x))))

(defrule percent (and float "%")
  (:function first))

(defrule position (and "(" integer "," integer ")")
  (:lambda (l)
    (ematch l
      ((list "(" line "," col ")")
       (cons line col)))))

(defrule range (and position "-" position)
  (:lambda (l)
    (ematch l
      ((list start "-" end)
       (cons start end)))))

(defrule guess.index (and "[" integer "]")
  (:function second))

(defrule guess.likelihood (and "(" percent ")")
  (:function second))

(defrule spaces (+ " ")
  (:constant nil))

(defrule type-char (character-ranges
                    (#\a #\z)
                    (#\A #\Z)
                    (#\0 #\9)
                    #\:)
  (:text t))

(defrule guess.typename (+ (character-ranges
                            (#\a #\z)
                            (#\A #\Z)
                            (#\0 #\9)
                            #\:))
  (:text t)
  (:lambda (text)
    (first (split-sequence #\: text :count 1))))

(defrule guess.type (and spaces guess.typename)
  (:function second))

(defrule guess (and guess.index guess.likelihood guess.type)
  (:function rest))

(defrule guess-list (and guess (* (and "," spaces guess)))
  (:lambda (l)
    (cons (first l)
          (mappend #'cddr (second l)))))

(defrule prediction (and range (and ":" spaces) guess-list)
  (:lambda (l)
    (cons (first l) (third l))))
