;;; galois-autocomplete.lisp --- Muse for a general machine learning
;;; autocompleter based on a fork of the Galois project.
;;;
;;; For now, this muse launches the language server directly on the
;;; client's machine.  It is expected that later the language server
;;; will be run on dedicated hardware optimized for ml models which
;;; may be connected to a tcp-muse.
(defpackage :argot-server/muses/galois-autocomplete
  (:use :gt/full
        :argot-server/readtable
        :argot-server/muses/base
        :argot-server/muses/passthrough
        :argot-server/muses/pygls)
  (:export :galois-autocomplete
           :+galois-autocomplete-command+))
(in-package :argot-server/muses/galois-autocomplete)
(in-readtable argot-readtable)

(defclass galois-autocomplete (tcp-muse pygls) ()
  (:default-initargs
   :name "Galois Autocomplete"
   :lang 'python
   :host "autocomplete"
   :port 3030))
