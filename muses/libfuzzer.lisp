(defpackage :argot-server/muses/libfuzzer
  (:use :gt/full
        :argot-server/readtable
        :argot-server/lsp-diff
        :argot-server/muses/base
        :argot-server/muses/code-actor
        :lsp-server
        :lsp-server/protocol
        :argot
        :argot-server/ast-utils
        :software-evolution-library
        :argot-server/muses/example-view-muse
        :libfuzzer)
  (:import-from :argot-server/annotation-host
                :did-annotate)
  (:export :libfuzzer))
(in-package :argot-server/muses/libfuzzer)
(in-readtable argot-readtable)

(defclass libfuzzer (code-actor) ()
  (:documentation "Integrate LIBFUZZER (see https://llvm.org/docs/LibFuzzer.html) as a code actor.")
  (:default-initargs
   :force-two-round-code-actions t
   :lang '(or c)
   :name "LibFuzzer"))

(defmethod muse-available? and ((muse libfuzzer))
  ;; This muse is currently disabled.
  nil)

(defmethod muse-code-action-kinds append ((libfuzzer libfuzzer))
  (list "CodeActionKind.FuzzTest"))

(defmethod muse-commands append ((libfuzzer libfuzzer))
  (list :argot.libfuzzer))

(defmethod applicable-commands ((libfuzzer libfuzzer) &key start end text-document)
  "Return a list of potential fuzz testing locations."
  (with-document-software ((start end) :software software :asts asts)
    (filter-map
     (lambda (ast)
       (make 'Command
             :|title|
             (format nil "libfuzzer: Fuzz test at ~s"
                     (concatenate 'string (take 8 (source-text ast)) "..."))
             :|command| (string :argot.libfuzzer)
             :|arguments|
             (list text-document (pack-tested-ast software ast nil))))
     (remove-if-not (of-type '(or c-function-definition cpp-function-definition))
                    asts))))

(defclass libfuzzer-test-specification (TestCase)
  ()
  (:documentation "A specification for a libfuzzer test."))

(defun fuzz-test-contribution (muse function-ast fuzz-results)
  "Make a new contribution for URI based on FUZZ-RESULT for FUNCTION-NAME."
  (make 'Contribution
        :source (muse-name muse)
        :kind ArgotKind.Tests
        :data
        (convert 'hash-table
                 (make 'libfuzzer-test-specification
                       ;; TODO: switch this to a location to insert at the
                       ;;       right spot?
                       :target (function-name function-ast)
                       ;; NOTE: This is the hierarchical path of a
                       ;;       test in its test suite. It's a
                       ;;       required value but doesn't have use
                       ;;       in this case.
                       :path (list "")
                       :arguments fuzz-results))))

(defmethod apply-command ((libfuzzer libfuzzer)
                          (cmd (eql :argot.libfuzzer))
                          arguments)
  (declare (ignorable cmd))
  (with-tested-ast-pack (arguments :doc doc :software software :ast ast)
    (did-annotate
     libfuzzer
     (list (make 'Annotation
                 :textDocument
                 (make 'TextDocumentIdentifier
                       :uri (gethash "uri" doc))
                 :contributions
                 (list (fuzz-test-contribution
                        libfuzzer
                        ast
                        (crashing-fuzz-inputs software ast))))))))
