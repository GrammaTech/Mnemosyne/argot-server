(defpackage :argot-server/muses/jsdoc-type-view
  (:documentation "Type view for JavaScript, using JSDoc.")
  (:use :gt/full
        :argot-server/readtable
        :argot-server/muses/base
        :argot-server/muses/type-view
        :argot-server/ast-utils
        :lsp-server/protocol
        :argot)
  (:export :jsdoc-type-view))
(in-package :argot-server/muses/jsdoc-type-view)
(in-readtable argot-readtable)

(defclass jsdoc-type-view (type-view)
  ()
  (:documentation "Muse for inserting JSDoc types.")
  (:default-initargs
   :force-two-round-code-actions t
   :name "JSDoc Type View"
   :lang 'javascript))

;;; TODO Enable.
(defmethod muse-available? and ((muse jsdoc-type-view))
  nil)
