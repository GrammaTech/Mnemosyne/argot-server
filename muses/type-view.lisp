(defpackage :argot-server/muses/type-view
  (:use :gt/full
        :argot-server/readtable
        :argot-server/muses/base
        :argot-server/muses/code-actor
        :argot-server/ast-utils
        :lsp-server/protocol
        :argot)
  (:import-from :argot-server/annotation-host
                :did-annotate
                :dump-string-annotations)
  (:import-from :software-evolution-library
                :mutate)
  (:import-from :argot-server/annotation-host
                :get-annotations
                :has-type-contributions?)
  (:export :type-view
           :contrib-text-edits
           :extract-type-annotations
           :extract-type-contribs
           :extract-ast-type
           :dump-type-view-string-annotations))
(in-package :argot-server/muses/type-view)
(in-readtable argot-readtable)

(defclass type-view (code-actor)
  ()
  (:documentation "Base classes for muses that read and insert types.")
  (:default-initargs
   :force-two-round-code-actions t
   ;; Base muse, applies to no language.
   :lang nil))

(defmethod muse-code-action-kinds append ((tv type-view))
  (list CodeActionKind.Source))

(defmethod muse-commands append ((tv type-view))
  (mapcar (op (command-keyword tv _))
          '(:collect-types :insert-types)))

;;; TODO Command for collecting types, which signals the appropriate
;;; type collector?

(defmethod applicable-commands ((tv type-view) &key text-document start end)
  (declare (ignore start end))
  (let ((uri (slot-value text-document '|uri|)))
    (when (and (has-type-contributions? (muse-server tv) uri)
               ;; Don't show action if nothing would happen.
               (compute-edits tv text-document))
      (list
       (make 'Command
             :title "Insert Types in File"
             :command (string (command-keyword tv :insert-types))
             :arguments (list text-document))))))

(defun compute-edits (muse doc)
  (let* ((uri (slot-value doc '|uri|))
         (annotations (get-annotations (muse-server muse) uri)))
    (nest
     (remove-if {edit-redundant? uri})
     (iter outer
           (for a in annotations)
           (with-slots (|range| |contributions|) a
             (with-slots (|start| |end|) |range|
               (iter (for c in (slot-value a '|contributions|))
                     (with-slots (|kind| |data| |source|) c
                       (when (and (eql |kind| ArgotKind.Types)
                                  ;; Ignore embedded types.
                                  (not (equal |source| (muse-name muse))))
                         (in outer
                             (appending
                              (assure list
                                (contrib-text-edits muse
                                                    |data|
                                                    :uri uri
                                                    :document doc
                                                    :start |start|
                                                    :end |end|)))))))))))))

(defmethod apply-command ((muse type-view)
                          (cmd (eql :type-view.insert-types))
                          arguments)
  (let* ((doc (convert 'OptionalVersionedTextDocumentIdentifier
                       (first arguments))))
    (nest
     (ignore-some-conditions (mutate))
     (make 'ApplyWorkspaceEditParams
           :label "insert types"
           :edit)
     (annotate-workspace-edit "Inserted types")
     (make 'WorkspaceEdit :documentChanges)
     (list)
     (make 'TextDocumentEdit :textDocument doc :edits)
     (compute-edits muse doc))))

(defgeneric contrib-text-edits (muse data &key &allow-other-keys)
  (:documentation "Compute text edits for contrib data DATA.")
  (:argument-precedence-order data muse)
  (:method ((muse type-view) (data hash-table) &rest args &key)
    (and (gethash "interface" data)
         (apply #'contrib-text-edits muse (convert 'Data data) args)))
  (:method ((muse type-view) (data TypeCandidate) &key end)
    (let ((name (slot-value data '|typeName|)))
      (list
       (make 'TextEdit
             :range (make 'Range :start end :end end)
             :newText (string+ ": " name)))))
  (:method ((muse type-view) (data FunctionReturnTypeCandidate) &rest args &key)
    (ematch data
      ((FunctionReturnTypeCandidate
        :function-name name
        :returns returns)
       (apply #'contrib-text-edits
              muse
              (make 'FunctionReturnType
                    :function-name name
                    :returns returns)
              args))))
  (:method ((muse type-view) (data TypeCandidates) &rest args &key)
    (let ((best
           (iter (for candidate in (slot-value data '|candidates|))
                 (ematch candidate
                   ((TypeCandidate :probability probability)
                    (finding candidate maximizing probability))))))
      (when (= 1 (slot-value best '|probability|))
        (apply #'contrib-text-edits muse best args))))
  (:method ((muse type-view) (data FunctionReturnTypeCandidates)
            &rest args &key)
    (let ((best
           (iter (for candidate in (slot-value data '|candidates|))
                 (ematch candidate
                   ((FunctionReturnTypeCandidate
                     :probability probability)
                    (finding candidate maximizing probability))))))
      (when (= 1 (slot-value best '|probability|))
        (apply #'contrib-text-edits muse best args))))
  (:method ((muse type-view) (data FunctionReturnType) &key uri start end)
    (let ((software (uri-buffer-software uri))
          (return-type (slot-value data '|returns|)))
      (when-let* ((language (guess-language uri))
                  (function-node (function-node-in-range software start end))
                  (spot (end-of-parameter-list software function-node)))
        (unless (typep function-node 'lambda-ast)
          (list
           (make 'TextEdit
                 :range (make 'Range
                              :start (convert 'Position spot)
                              :end (convert 'Position spot))
                 :newText return-type)))))))

(defgeneric extract-type-annotations (muse document &key &allow-other-keys)
  (:documentation "Extract types present in DOCUMENT, as annotations.")
  (:method (muse document &key)
    nil)
  (:method ((muse type-view)
            (document TextDocumentIdentifier)
            &key)
    (let ((software (uri-buffer-software (slot-value document '|uri|))))
      (extract-type-annotations muse software :document document)))
  (:method ((muse type-view)
            (software parseable)
            &key document)
    (iter (for (range . contrib) in
               (extract-type-contribs muse software))
          (collect (make 'Annotation
                         :text-document document
                         :range range
                         :contributions (list contrib))))))

(defgeneric extract-type-contribs (muse software)
  (:documentation "Extract types present in SOFTWARE, as Argot contributions.")
  (:method (muse software)
    nil)
  (:method ((muse type-view) (software parseable))
    (iter
     (for (ast . range) in (ast-source-ranges (genome software)))
     (when-let (data (extract-ast-type muse software ast))
       (collect (cons (convert 'Range range)
                      (make 'Contribution
                            :source (muse-name muse)
                            :kind ArgotKind.Types
                            :data data)))))))

(defgeneric extract-ast-type (muse software ast)
  (:documentation "Extract a type for AST, as an Argot datum.")
  (:method (muse software ast)
    nil)
  (:method (muse (software parseable) (ast function-ast))
    (when-let (return-type (return-type ast))
      (make 'FunctionReturnType
            :function-name (function-name ast)
            :returns (source-text return-type))))
  (:method (muse (software parseable) (ast parameter-ast))
    (when-let (type (parameter-type ast))
      (make 'TypeCandidate
            :type-name (source-text type)
            :probability 1))))

(defmethod on-change progn ((muse type-view) uri &key document)
  ;; TODO If this ends up making requests it should be run in another
  ;; thread (cf. test-view).
  (update-extracted-types muse document))

(defun update-extracted-types (muse document)
  (did-annotate muse
                (extract-type-annotations muse document)))

(defun dump-type-view-string-annotations (muse software)
  "For debugging and testing purposes, extract type annotations from
SOFTWARE and return an alist of where the values are the extracted
annotations and the keys are the strings from SOFTWARE the annotations
apply to."
  (let* ((fake-doc (make 'TextDocumentIdentifier :uri "file://tmp/file"))
         (annotations
          (extract-type-annotations muse software :document fake-doc)))
    (dump-string-annotations software annotations)))
