(defpackage :argot-server/muses/background-thread-muse
  (:use :gt/full
        :lsp-server/utilities/worker-thread
        :argot-server/readtable
        :argot-server/muses/base
        :argot-server/muses/passthrough)
  (:export :background-thread-muse
           :muse-foreground
           :background-thread-muse-thread)
  (:documentation "Support for muses with an associated background thread."))
(in-package :argot-server/muses/background-thread-muse)

(defclass background-thread-muse (muse)
  ((thread :initform nil
           :reader background-thread-muse-thread))
  (:documentation
   "Base class for a muse with an associated background thread."))

(defgeneric muse-foreground (muse)
  (:documentation "Function to run in the foreground of MUSE's thread.
Expected to be a loop that exits when MUSE is stopped.

A `:quit` tag is bound around the call so your loop can always exit
with `\(throw :quit ...)`."))

(defmethod muse-started-p and ((muse background-thread-muse))
  (not (null (background-thread-muse-thread muse))))

(defmethod muse-start :before ((muse background-thread-muse) &aux ready)
  (with-slots (thread fn) muse
    (setf thread
          (let ((thread
                 (with-lsp-worker-thread (:name (muse-worker-name muse))
                   (setf ready t)
                   (catch :quit
                     (let ((*server* muse))
                       (muse-foreground muse))))))
            ;; Wait until the thread is actually running before returning.
            (loop until ready do (sleep 0.1))
            thread))))

(defmethod muse-stop progn ((muse background-thread-muse) &key)
  (with-slots (thread) muse
    (ignore-errors
     (bt:interrupt-thread thread (lambda () (throw :quit nil))))
    ;; Wait until the thread has actually finished before returning.
    (loop while (bt:thread-alive-p thread) do (sleep 0.1))
    ;; Clear the slot.
    (setf thread nil)))
