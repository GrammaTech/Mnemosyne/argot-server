(defpackage :argot-server/muses/test-view
  (:use :gt/full
        :argot-server/readtable
        :argot-server/ast-utils
        :argot-server/muses/base
        :argot-server/muses/code-actor
        :argot-server/utility/testing
        :argot-server/muses/example-view-muse
        :argot-server/lsp-diff
        :lsp-server/protocol
        :lsp-server/utilities/worker-thread
        :argot
        :lsp-server
        :software-evolution-library/software/cpp-project)
  (:import-from :argot-server/annotation-host
                :get-annotations
                :did-annotate
                :contributions-by-kind)
  (:import-from :software-evolution-library/software/project
                :collect-evolve-files)
  (:import-from :software-evolution-library/components/file
                :original-path)
  (:import-from :argot-server/utility
                :command-keyword)
  (:export :test-view
           :inline-test-framework
           :test-node?
           :extract-examples
           :update-extracted-tests))
(in-package :argot-server/muses/test-view)
(in-readtable argot-readtable)

(defconst +muse-name+ "Test View")

(defclass inline-test-framework (test-framework)
  ()
  (:default-initargs
   :test-framework-case-class 'inline-test-case))

(defclass inline-test-case (unit-test-case parsed-test)
  ())

(defmethod tests-for ((self inline-test-framework) (sw software))
  (nest
   (when-let* ((genome (ignore-errors (genome sw)))
               (ranges (ast-source-ranges genome))))
   (iter (for node in-tree genome))
   (when (and (typep node 'function-ast)
              (test-node? node)))
   (let ((name (function-name node))))
   (collect)
   (make 'inline-test-case
         :source-text (test-source-text sw node)
         :software sw
         :object node
         :name name
         :conditions)
   (list)
   (make 'fatal-test-condition
         :name name
         :examples
         (extract-examples sw node))))

(defgeneric test-source-text (software test-ast)
  (:documentation "Extract the source text of TEST-AST.

This may take special handling for certain languages. E.g. for a
Python test definition we want not just the function definition but
also its decorators.")
  (:method ((software t) ast)
    (source-text ast))
  (:method ((sw python) (ast python-function-definition))
    (let ((deco (find-enclosing 'python-decorated-definition sw ast)))
      (if (and deco
               (eql ast (python-definition deco)))
          (source-text deco)
          (call-next-method)))))

(defgeneric literal->json (literal)
  (:method ((x string-ast)) (source-text x))
  (:method ((x float-ast)) (parse-float (source-text x)))
  (:method ((x number-ast)) (parse-number (source-text x)))
  (:method ((x boolean-true-ast)) t)
  (:method ((x boolean-false-ast)) 'yason:false))

(defgeneric call-plist (call)
  (:method ((ast ast))
    nil)

  (:method ((ast python-subscript))
    (let ((subscripts (python-subscript ast)))
      (when (every (of-type 'literal-ast) subscripts)
        (when-let (plist (copy-list (call-plist (python-value ast))))
          (appendf (getf plist :subscripts)
                   (mapcar #'literal->json subscripts))
          plist))))

  (:method ((call call-ast))
    (match call
      ((call-ast (call-arguments args)
                 (call-function name))
       (when (every (of-type 'literal-ast) args)
         (list
          :name (source-text name)
          :arguments (mapcar #'literal->json args)))))))

(defgeneric extract-examples (software node)
  (:method ((software t) node)
    (nest
     (when-let ((name (function-name node))))
     (let* ((comparisons (collect-if #'comparisonp node))))
     (iter (for cmp in comparisons))
     ;; Not every comparison has an lhs and rhs (e.g. chained
     ;; comparison in Python).
     (when-let* ((lhs (ignore-errors (lhs cmp)))
                 (rhs (ignore-errors (rhs cmp))))
       (when (typep lhs 'literal-ast)
         (rotatef lhs rhs)))
     (when (typep rhs 'literal-ast))
     (when-let (call-plist (call-plist lhs))
       (collecting
        (apply #'make 'example-call
               :comparator (string (operator cmp))
               :output (literal->json rhs)
               call-plist))))))

(defclass test-view (code-actor)
  ((framework :initarg :framework :reader framework))
  (:documentation "A view that extracts tests from files and makes
  them available as annotations.")
  (:default-initargs
   :force-two-round-code-actions t
   :name +muse-name+
   :framework (make 'inline-test-framework :name +muse-name+)
   :lang '(or python javascript c cpp)))

(defmethod muse-code-action-kinds append ((test-view test-view))
  (list "CodeActionKind.AddTest"))

(defmethod muse-commands append ((test-view test-view))
  (mapcar (op (command-keyword test-view _))
          '(:add-test)))

(defmethod initialize-instance :after ((self test-view) &key framework)
  ;; Default the test framework's name to match the muse's.
  (setf (slot-value self 'framework)
        (or framework
            (make 'test-framework :name (muse-name self)))))

(defun test-node? (node)
  "Is NODE a test node?"
  (let ((name (function-name node)))
    (when (and (stringp name)
               (string^= "test_" name))
      node)))

;;; TODO Retained for testing purposes. Rewrite tests.
(defun extract-test-nodes (language string)
  "Parse STRING into an AST and return the test nodes."
  (when-let* ((software
               (ignore-errors
                (from-string (make language) string)))
              (genome
               (ignore-errors
                (genome software)))
              (function-nodes
               (iter (for node in-tree genome)
                     (when (typep node 'function-ast)
                       (collect node))))
              (test-nodes
               (filter #'test-node? function-nodes)))
    test-nodes))

(defun extract-test-texts (language string)
  "Return the source text for the test nodes in STRING, given LANGUAGE."
  (mapcar #'source-text (extract-test-nodes language string)))

(defun test-node-contribution (muse uri)
  "Extract the tests from the document at URI and wrap them as a
contribution."
  (nest
   (make 'Contribution
         :source (muse-name muse)
         :kind ArgotKind.Tests
         :data)
   (convert 'hash-table)
   (make 'Tests :tests)
   (when-let* ((buffer (get-buffer-from-uri uri))
               (lang (guess-language buffer))
               (string (buffer-to-string buffer))
               (software
                (ignore-errors
                 (from-string (make lang) string))))
     (tests-for (framework muse)
                software))))

(defun save-contribution (muse uri contribution)
  "Save CONTRIBUTION for URI."
  (did-annotate muse
                (list
                 (make 'Annotation
                       :textDocument
                       (make 'TextDocumentIdentifier
                             :uri uri)
                       :contributions (list contribution)))))

(defgeneric update-extracted-tests (muse uri &key)
  (:documentation "Extract tests from URI and send them back as an annotation.")
  (:method (muse uri &key)
    (save-contribution muse uri (test-node-contribution muse uri))))

(defmethod on-change progn ((muse test-view) uri &key)
  ;; VS Code can hang if this results in interleaved requests.
  (with-lsp-worker-thread (:name (muse-worker-name muse))
    (synchronized ()
      (update-extracted-tests muse uri))))

(defmethod add-test ((framework inline-test-framework)
                     (software c)
                     (specification (eql :libfuzzer-test-specification))
                     &key data)
  (let* ((genome (genome software))
         (function-name (car data))
         (arguments (cdr data))
         (target-function (find-if (lambda (ast)
                                     (and (typep ast 'function-ast)
                                          (equal function-name (function-name ast))))
                                   genome))
         (new-function
           ;; TODO: this is currently assuming an identation. Should probably
           ;;       set the indentation manually.
           (convert 'c-ast (fmt "void test_~a() {~%    ~a(~{~a~^, ~});~%}~%"
                                ;; NOTE: arguments can have multiple sets of
                                ;;       args. Consider putting them all in the
                                ;;       test. Assumes only one since there will
                                ;;       only be one currently.
                                function-name function-name (car arguments))
                    :deepest t))
         ;; TODO: The following is a hack to insert after the target function
         ;;       by first inserting the new test, removing the target function,
         ;;       and then reinserting the test again.
         ;;       This should instead use a one-shot #'insert-after once one is
         ;;       added to functional-trees.
         (new-software (insert software target-function new-function))
         ;; TODO: this can be removed when argot-server is moved to the latest
         ;;       functional-trees.
         (new-function
           (find-if (lambda (ast)
                      (and (typep ast 'function-ast)
                           (equal (fmt "test_~a" function-name) (function-name ast))))
                    (genome new-software))))
    (insert (less new-software target-function)
            new-function target-function)))

(defmethod applicable-commands ((test-view test-view) &key start end text-document)
  (labels ((get-edits (software data)
             "Get the edits needed to change the text document such that it has
              a new test."
             (setf data (convert 'hash-table data))
             (when-let ((test
                         (add-test
                          (framework test-view)
                          software
                          (make-keyword (gethash "interface" data))
                          :data
                          (list* (gethash "target" data)
                                 (gethash "arguments" data)))))
               (lsp-edit-diff-strings
                (genome-string software)
                (genome-string test)))))
    (with-document-software ((start end) :software software :asts asts :uri uri)
      (remove-duplicates
       (iter
        (for contribution in (contributions-by-kind (muse-server test-view) uri ArgotKind.Tests))
        (for data = (data contribution))
        (when-let ((edits (get-edits software data)))
          (collect
           (make 'Command
                 :|title|
                 (format nil "~a: Insert test for function ~a"
                         (slot-value contribution '|source|)
                         (slot-value data '|target|))
                 :|command| (string (command-keyword test-view :add-test))
                 :|arguments|
                 ;; TODO: this may be inefficient computing it before it
                 ;;       is known to be needed.
                 (list text-document edits)))))
       :test #'equal?
       :key (op (slot-value _ '|arguments|))))))

(defun apply-add-test (arguments)
  (destructuring-bind (text-document edits) arguments
    (make-instance
     'ApplyWorkspaceEditParams
     :|label| "add test"
     :|edit|
     (annotate-workspace-edit
      "test-view test"
      (make-instance
       'WorkspaceEdit
       :|documentChanges|
       (list
        (make-instance
         'TextDocumentEdit
         :|textDocument|
         (convert 'OptionalVersionedTextDocumentIdentifier text-document)
         :|edits|
         (mapcar {convert 'lsp-server/protocol:|TextEdit|} edits))))))))

(defun dispatch-command (cmd arguments)
  (cond
    ((string-suffix-p 'add-test (string cmd))
     (apply-add-test arguments))))

(defmethod apply-command ((test-view test-view)
                          (cmd symbol)
                          arguments)
  (dispatch-command cmd arguments))
