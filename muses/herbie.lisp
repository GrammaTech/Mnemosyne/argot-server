(defpackage :argot-server/muses/herbie
  (:use :gt/full
        :argot-server/muses/base
        :argot-server/muses/code-actor
        :argot-server/utility/fpcore
        :cmd)
  (:import-from :trivia.fail :fail)
  (:import-from :argot-server/lsp-diff
                :lsp-edit-diff-strings)
  ;; For debugging.
  (:local-nicknames (:l :fresnel/lens))
  (:export :herbie-muse)
  (:documentation "Muse that provides baseline LSP support for JavaScript."))
(in-package :argot-server/muses/herbie)
(in-readtable argot-readtable)

(def herbie (resolve-executable "herbie"))

(deftype seed ()
  '(or null (integer 1 (#.(expt 2 31)))))

(defun get-timeout ()
  (and (resolve-executable "timeout")
       '("timeout" "--foreground" "4m")))

;;; TODO This should use a persistent Herbie shell.
(-> herbie-improve (fpcore &key (:seed seed)) (values fpcore &optional))
(defun herbie-improve (fpcore &key (seed (herbie-seed*)))
  "Run Herbie in one-shot mode to improve FPCORE and return the
results as a string."
  (nest
   (with-temporary-file-of (:pathname in) (convert 'string fpcore))
   (with-temporary-file (:pathname out))
   (progn
     (if seed
         (cmd (get-timeout) herbie "improve --seed" seed in out)
         (cmd (get-timeout) herbie "improve" in out))
     (convert 'fpcore
              (only-elt
               (remove-if (op (or (emptyp _1) (string^= ";;" _1)))
                          (lines
                           (read-file-into-string out))))))))

(defgeneric herbie-seed (server)
  (:method ((server t)) nil))

(defun herbie-seed* ()
  "Get the Herbie seed from the current server."
  (herbie-seed *server*))

(defclass herbie-muse (code-actor)
  ((seed :initarg :seed :reader herbie-seed :type seed)
   (temp :initarg :temp :reader herbie-temp :type (or null string)))
  (:default-initargs
   :force-two-round-code-actions t
   :seed nil
   :temp nil
   :name "Herbie")
  (:documentation "Integrate Herbie as a code actor."))

(defmethod muse-code-action-kinds append ((h herbie-muse))
  (list :herbie.improve))

(defmethod muse-commands append ((h herbie-muse))
  (list :herbie.improve))

(defun fpcore-command (node fpcore doc range)
  "Make an LSP command to improve an FPCore expression."
  (let* ((start (convert 'Position (begin range)))
         (end (convert 'Position (end range))))
    (make 'Command
          :title (fmt "Improve floating-point math @~a"
                      (ellipsize (source-text node) 20))
          :command #.(string :herbie.improve)
          :arguments (list "improve"
                           doc start end
                           (convert 'string fpcore)))))

(-> inline-fpcore-variables (software ast fpcore)
    (values fpcore &optional))
(defun inline-fpcore-variables (software ast fpcore)
  (if (contains-error-ast-p ast) fpcore
      (match fpcore
        ((fpcore vars props orig-expr)
         (let* ((replacements
                 (nest
                  (iter scopes
                        (for scope in (scopes software ast)))
                  (iter (for var-alist in scope))
                  (let ((name (assocdr :name var-alist))
                        (decl (assocdr :decl var-alist))))
                  (when-let* ((var (find name vars :test #'string=))
                              (rhs
                               (and (typep decl 'variable-declaration-ast)
                                    (ignore-errors
                                     (rhs decl))))
                              (fpcore
                               (unless (typep rhs 'function-ast)
                                 (convert 'fpcore rhs)))
                              (subtree (fpcore-expr fpcore))))
                  ;; Should we recurse?
                  (in scopes (collect (cons var subtree)))))
                (new-expr (sublis replacements orig-expr)))
           (copy-fpcore fpcore
                        :symbols (fpcore-expression-symbols new-expr)
                        :properties props
                        :expr new-expr))))))

(defun math-nodes-alist (software ast)
  "Find all the nodes in AST that can be translated into FPCore math
expressions and return an alist of nodes and translations."
  (mapcar (lambda (node)
            (cons node
                  (nest
                   (add-herbie-precondition software node)
                   (inline-fpcore-variables software node)
                   (convert 'fpcore node))))
          (extract-fpcore-math-nodes ast)))

(defun herbie-precondition (software node)
  "Look for an Herbie specific precondition annotation \(a comment
that starts with `herbie:`)."
  ;; TODO Once the test-view can do it we should just be looking for
  ;; assertions.
  (when-let (comment (lastcar (comments-for software node)))
    (let ((text (drop-while {find _ "# "} (source-text comment))))
      (when (string^= "herbie: " text)
        (let* ((text (drop-prefix "herbie: " text))
               (ast (convert 'python-ast text)))
          (fpcore-expr
           (convert 'fpcore (only-elt (extract-fpcore-math-nodes ast)))))))))

(defun add-herbie-precondition (software node fpcore)
  "Add the precondition in NODE to FPCORE."
  (nest
   (let ((precondition (herbie-precondition software node))))
   (if (no precondition) fpcore)
   (copy-fpcore fpcore :properties)
   (list* :pre precondition)
   (fpcore-properties fpcore)))

(defun uniquify-ast (ast)
  "Generate a unique variable name and substitute it for the
identifier named herbie_temp in AST."
  (let ((herbie-temp
         (or (herbie-temp *server*)
             (string (gensym "herbie_temp")))))
    (values
     (mapcar (lambda (node)
               (match node
                 ((python-identifier :text "herbie_temp")
                  ;; Copying violates some obscure functional-trees
                  ;; assertion.
                  (make 'python-identifier :text herbie-temp))
                 (otherwise node)))
             ast)
     herbie-temp)))

(defgeneric make-assignment (name ast)
  (:method ((name string) (ast python-ast))
    ;; There's no (Pythonic?) way to nest an if-elif chain in a Python
    ;; expression, so if an AST is not an expression we need to push
    ;; the assignment down to the leaves.
    (if (typep ast 'expression-ast)
        (make 'python-expression-statement
              :children (list (python "$1 = $2" name ast)))
        (mapcar (lambda (node)
                  (match node
                    ((python-expression-statement :children (list child))
                     (copy node
                           :children (list (python "$1 = $2" name child))))))
                ast))))

(defun docstring? (ast)
  "Is AST a possible docstring?"
  (match ast
    ((python-expression-statement
      :children (list (type string-ast)))
     t)))

(defun improved-document-ast (fpcore buffer start end)
  "Convert FPCORE into a Python AST and insert it at the beginning of the
block nearest to START, assigning it to a unique temporary variable.

Insert the temporary variable in place of the AST between START and
END."
  ;; TODO Could this be generalized to multiple languages using
  ;; refactoring-mutations?
  (nest
   (when-let*
       ((sw (buffer-software buffer))
        (ast (genome sw))
        (range (make 'source-range
                     :begin (convert 'source-location start)
                     :end (convert 'source-location end)))
        (target-ast (first (asts-contained-in-source-range sw range)))))
   (mvlet* ((new-ast gensym
             (uniquify-ast (convert 'python-ast fpcore)))
            (assignment (make-assignment gensym new-ast))
            (path (ast-path sw target-ast))
            (id (make 'python-identifier :text gensym))
            ;; Replace the original AST with a reference to the new
            ;; variable.
            (sw (copy sw :genome (with (genome sw) path id)))
            (block (find-enclosing 'python-block sw (lookup sw path))))
     ;; Insert the assignment at the beginning of the nearest block.
     (unless block
       (error "Cannot insert formula without a containing block~
~@[. (File has syntax errors.)~]"
              (contains-error-ast-p ast)))
     (patch-whitespace
      (tree-copy
       (with ast (ast-path sw block)
             (copy block
                   :direct-children
                   (append
                    (take-while #'docstring? (direct-children block))
                    (list assignment)
                    (drop-while #'docstring? (direct-children block))))))))))

(defmethod applicable-commands
    ((h herbie-muse) &key start end text-document)
  (when-let (software (buffer-software (point-buffer start)))
    (let* ((ranges (ast-source-ranges software))
           (start (convert 'source-location start))
           (end (convert 'source-location end))
           (overall-range
            (make 'source-range
                  :begin start
                  :end end)))
      (iter (for (node . fpcore) in
                 (math-nodes-alist software (genome software)))
            (for range = (assure source-range
                           (assocdr node ranges)))
            (when (or (intersects overall-range range)
                      (contains range start))
              (collect (fpcore-command node fpcore text-document range)))))))

(defmethod apply-command ((h herbie-muse)
                          (cmd (eql :herbie.improve))
                          args)
  (destructuring-bind (op doc start end fpcore) args
    (declare (ignore op))
    (nest
     (let* ((doc (convert 'OptionalVersionedTextDocumentIdentifier doc))
            (uri (slot-value doc '|uri|))
            (buffer (uri-buffer uri))
            (start (convert 'Position start))
            (end (convert 'Position end))
            (fpcore (convert 'fpcore fpcore))
            (result (handler-case
                        (herbie-improve fpcore)
                      (error (e)
                        (show-message (fmt "Herbie failed: ~a" e)))))))
     (when result)
     (mvlet ((before after (fpcore-improvement result))))
     (if (> after before)
         (show-message "Herbie could not improve expression"))
     ;; It would be nicer to put this in the description of the
     ;; ChangeAnnotation but VS Code doesn't actually do anything with
     ;; it.
     (progn
       (show-message (fmt "Average error ~a -> ~a" before after)))
     (let* ((new-ast (improved-document-ast result buffer start end))
            (new-source (source-text new-ast))
            (old-source (buffer-to-string buffer))
            (edits (lsp-edit-diff-strings old-source new-source))))
     (make 'ApplyWorkspaceEditParams
           :label "Herbie improvement"
           :edit)
     (annotate-workspace-edit "Herbie improvement")
     (make 'WorkspaceEdit
           :documentChanges)
     (list)
     (make 'TextDocumentEdit
           :textDocument doc
           :edits edits))))
