;;; hypothesis-tests.lisp --- Muse for adding type-driven hypothesis tests.
(defpackage :argot-server/muses/hypothesis-tests
  (:use :gt/full
        :argot-server/readtable
        :argot-server/muses/base
        :argot-server/muses/passthrough
        :argot-server/muses/pygls)
  (:import-from :jsonrpc
                :request-params
                :copy-request)
  (:export :hypothesis-tests))
(in-package :argot-server/muses/hypothesis-tests)

(defclass hypothesis-tests (external-code-actor-muse tcp-muse pygls) ()
  (:default-initargs
   :name "Hypothesis Tests"
   :lang 'python
   :host "hypothesis"
   :port 3033))

(defmethod client-dispatch-request ((client passthrough-client)
                                    (muse hypothesis-tests)
                                    (method (eql :|workspace/applyEdit|))
                                    request)
  (call-next-method
   client muse method
   (copy-request
    request :params
    (annotate-workspace-edit
     "Hypothesis Test"
     (convert '|ApplyWorkspaceEditParams| (request-params request))
     ;; TODO This shouldn't be necessary.
     :always t))))
