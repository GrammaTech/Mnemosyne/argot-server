(defpackage :argot-server/muses/pygls
  (:documentation "Mixin for muses that use pygls.")
  (:use :gt/full
        :argot-server/readtable
        :argot-server/muses/base)
  (:import-from :jsonrpc
                :request
                :dispatch
                :request-method
                :copy-response
                :copy-request
                :request-params
                :response-result)
  (:export :pygls))
(in-package :argot-server/muses/pygls)
(in-readtable argot-readtable)

(defclass pygls (external-muse)
  ()
  (:documentation "Mixin for muses that use Pygls."))

(defmethod handle ((muse pygls)
                   (method (eql :|workspace/didChangeConfiguration|))
                   (params t))
  "Do not pass through workspace/didChangeConfiguration.
See https://github.com/openlawlibrary/pygls/issues/115."
  nil)

;;; TODO This should be automatically filtered out based on the
;;; initialization params.

(defmethod handle ((muse pygls)
                   (method (eql :|textDocument/documentSymbol|))
                   (params t))
  "Do not pass through textDocument/documentSymbol."
  nil)

(defmethod dispatch :context ((muse pygls) (request request))
  (if (equal "textDocument/codeAction" (request-method request))
      ;; Make sure the request is an actual CodeActionParams instance
      ;; so pygls will be able to parse the JSON conversion.
      (let* ((params (convert 'CodeActionParams (request-params request)))
             (request (copy-request request :params params))
             (response (call-next-method muse request)))
        (copy-response response
                       :result
                       ;; Remove extraneous keys from code actions
                       ;; that confuse VS Code.
                       (mapcar (lambda (result)
                                 (prog1 result
                                   (when (hash-table-p result)
                                     (dolist (key '("command" "diagnostics"))
                                       (when (null (gethash key result))
                                         (remhash key result))))))
                               (response-result response))))
      (call-next-method)))
