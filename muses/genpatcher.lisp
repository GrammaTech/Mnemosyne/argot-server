(defpackage :argot-server/muses/genpatcher
  (:use :gt/full
        :argot-server/readtable
        :argot-server/muses/base
        :argot-server/muses/code-actor
        :argot-server/lsp-diff
        :genpatcher/argot-gp
        :genpatcher/testing
        :genpatcher/mutations
        :jsonrpc
        :lsp-server
        :lsp-server/protocol
        :lsp-server/utilities/worker-thread
        :argot)
  (:import-from :argot-server/annotation-host
                :get-annotations
                :contributions-by-kind
                :has-contributions-by-kind-p)
  (:import-from :software-evolution-library/software-evolution-library
                :fitness)
  (:import-from :software-evolution-library/software/clang
                :clang)
  (:import-from :bordeaux-threads
                :thread)
  (:import-from :argot-server/utility
                :*reset-hook*)
  (:export
   :genpatcher-muse)
  (:documentation "Define a code actor that provides genpatcher synthesis tools."))

(in-package :argot-server/muses/genpatcher)
(in-readtable argot-readtable)

;: Mutation Settings


(define-constant +pop-size+ 16
  :test #'=
  :documentation "Population size to use for tests")

(def +mutations+
  (cumulative-distribution
   (normalize-probabilities
    '((tree-sitter-insert  . 1)
      (tree-sitter-swap    . 1)
      (tree-sitter-move    . 1)
      (tree-sitter-replace . 1)
      (tree-sitter-cut     . 1)
      (tree-sitter-nop     . 1)
      (tweak-constant    . 10)
      (swap-comparison-operator . 10)
      (swap-operator-sides . 10)
      (replace-zero-in-expression . 10)
      (fix-off-by-one    . 10)
      (swap-literal-with-bound-identifier . 10)
      (swap-identifier-with-bound-identifier . 10)
      (swap-arguments    . 10)))))


;; Job Management for Ongoing Patching Efforts.

(defvar *next-genpatcher-job-id* 0)

(define-reset-hook reset-genpatcher-job-id ()
  (setf *next-genpatcher-job-id* 0))

(defun next-job-id ()
  (setf *next-genpatcher-job-id* (+ 1  *next-genpatcher-job-id* ))
  *next-genpatcher-job-id*)

(defclass location ()
  ((start :accessor loc-start :initarg :start :type |Position|)
   (end :accessor loc-end :initarg :end :type |Position|)
   (text-document :accessor loc-document :initarg :text-document)))

(defmethod print-object ((self location) stream)
  (if *print-escape*
      (print-unreadable-object (self stream :type t)
        (format stream "~a" self))
      (format stream "~a:~a-~a"
              (uri self)
              (slot-value (loc-start self) '|line|)
              (slot-value (loc-end self) '|line|)))
  self)

(defmethod uri ((location location))
  (slot-value (slot-value location 'text-document) '|uri|))

(deftype genpatcher-job-status ()
  '(member :gp-started :gp-running :gp-terminated :gp-finished :gp-applied))

(defclass genpatcher-job ()
  ((location :accessor location :initarg :location)
   (population :accessor population :initarg :population)
   (test-suite :accessor test-suite :initarg :test-suite)
   (original-software :accessor original-software :initarg :original-software)
   (fitness :accessor fitness :initform 0)
   (work-done-token :accessor job-work-done-token :initarg :work-done-token :initform nil)
   (worker-thread :initarg :worker-thread :initform nil)
   (job-status :initarg :job-status :accessor job-status
               :initform :gp-started :type genpatcher-job-status)
   (final :accessor final :initarg :final :initform nil)
   (id :accessor job-id :initform (next-job-id))))

(defmacro with-bt-handler (msg expr)
  `(handler-bind
     ((serious-condition
        (lambda (c)
          (format *error-output* "===============================================~%")
          (format *error-output* "~A~%" ,msg)
          (format *error-output* "===============================================~%")
          (princ c *error-output*))))
     ,expr))

(defmethod job-alive-p ((job genpatcher-job))
  (member (job-status job) '(:gp-started :gp-running :gp-finished)))

(defmethod job-complete-p ((job genpatcher-job))
  (eq (job-status job) :gp-finished))

(defun update-gp-job (job &key (out-stream nil))
  (lambda (cur-pop cur-test-suite cur-fitness)
    (format out-stream "[GP] SETTING POPULATION FOR JOB ~A to ~A~%"
            (job-id job) cur-pop)
    (setf (population job) cur-pop)
    (format out-stream "[GP] SETTING TEST-SUITE FOR JOB ~A to ~A~%"
            (job-id job) cur-test-suite)
    (setf (test-suite job) cur-test-suite)
    (format out-stream "[GP] SETTING FITNESS FOR JOB ~A to ~A~%"
            (job-id job) cur-fitness)
    (setf (fitness job) cur-fitness)))

(defun start-gp-job (software test-suite location server &optional (work-done-token nil))
  (let* ((job
          (make-instance 'genpatcher-job
                         :location location
                         :population nil
                         :original-software software
                         :test-suite test-suite
                         :work-done-token work-done-token))
         (job-thread
          (with-lsp-worker-thread (:name (fmt "gp@~a" location))
            (let
                ((result
                  (with-bt-handler
                    "GENPATCH"
                    (genpatch
                     software test-suite
                     :max-population-size +pop-size+
                     :tree-sitter-mutations +mutations+
                     :update-fn (update-gp-job job :out-stream *error-output*)))))
              (setf (slot-value job 'final) result)
              ;; It appears during genpatch cleanup, fitness can be
              ;; "misset". This should be further investigated at a later
              ;; date.
              (setf (fitness job) (fitness result))
              (setf (job-status job) :gp-finished)
              (show-message
               (fmt "Repair for ~a complete" location)
               :server server)))))
    (setf (slot-value job 'worker-thread) job-thread)
    (setf (job-status job) :gp-running)
    job))

(defmethod location-contains-job ((loc location) (job genpatcher-job))
  (let ((job-loc (location job)))
    (and (equalp (loc-document loc) (loc-document job-loc))
         (flet ((pos<= (x y)
                  (source-<= (convert 'source-location x)
                             (convert 'source-location y))))
           (pos<= (loc-start loc) (loc-start job-loc))
           (pos<= (loc-end job-loc) (loc-end loc))))))

;; gp should be `genpatcher-muse` but it is not defined yet, and this
;; organization makes much more logical sense
(defmethod apply-result ((job genpatcher-job) gp)
  (nest
    (let ((loc (location job))
          (software (original-software job))
          (server (muse-server gp))))
    (when (job-complete-p job))
    (let ((result (slot-value job 'final))))
    (with-temporary-file-of
        (:pathname new-file)
      (source-text (patch-whitespace (genome result))))
    (with-temporary-file-of
        (:pathname old-file)
      (source-text (genome software)))
    (let* ((line-offset (slot-value (loc-start loc) '|line|))
           (char-offset (slot-value (loc-end loc) '|character|))
           (diff (lsp-edit-diff old-file new-file
                                :line-offset line-offset
                                :char-offset char-offset)))
      (bt:join-thread (slot-value job 'worker-thread))
      (setf (slot-value job 'worker-thread) nil)
      (setf (job-status job) :gp-applied)
      (remove-job gp job)
      (if diff
          (make 'TextDocumentEdit
                :textDocument (make 'OptionalVersionedTextDocumentIdentifier
                                    :uri (uri loc)
                                    :version (text-document-version (loc-document loc)))
                :|edits| diff)
          (progn
            (show-message
             (format nil "Repair for ~a:~a-~a:~a:~a complete with empty diff"
                     (uri loc)
                     (slot-value (loc-start loc) '|line|)
                     (slot-value (loc-end loc) '|line|)
                     (fitness job)
                     (test-case-count (test-suite job)))
             :server server)
            nil)))))

(defmethod check-status ((job genpatcher-job) server)
  (let ((loc (location job)))
    (if
      (= (fitness job) (test-case-count (test-suite job)))
      (progn
        (setf (job-status job) :gp-finished)
        (show-message (format nil "Repair for ~a:~a-~a complete"
                              (uri loc)
                              (slot-value (loc-start loc) '|line|)
                              (slot-value (loc-end loc) '|line|))
                      :server server))
      (show-message
       (format nil "~a/~a tests passing in repair for ~a:~a-~a"
               (fitness job)
               (test-case-count (test-suite job))
               (uri loc)
               (slot-value (loc-start loc) '|line|)
               (slot-value (loc-end loc) '|line|))
       :server server))
    nil))

(defmethod stop ((job genpatcher-job) server)
  (let ((loc (location job)))
    (let ((thread (slot-value job 'worker-thread)))
      (when (bt:thread-alive-p thread)
        (ignore-errors
         (bt:destroy-thread thread))))
    (setf (job-status job) :gp-terminated)
    (show-message
     (format nil "Discarded repair for ~a:~a-~a"
             (uri loc)
             (slot-value (loc-start loc) '|line|)
             (slot-value (loc-end loc) '|line|))
     :server server)
    nil))

(defmethod make-job-apply-action ((job genpatcher-job))
  (make 'Command
    :|title| (format nil "Apply results of synthesis job ~a" (job-id job))
    :|command| (string :argot.gp-apply-result)
    :|arguments| (list (job-id job))))

(defmethod make-job-status-action ((job genpatcher-job))
  (make 'Command
    :|title| (format nil "Check synthesis job ~a" (job-id job))
    :|command| (string :argot.gp-check-status)
    :|arguments| (list (job-id job))))

(defmethod make-job-stop-action ((job genpatcher-job))
  (make 'Command
    :|title| (format nil "Discard synthesis job ~a" (job-id job))
    :|command| (string :argot.gp-stop)
    :|arguments| (list (job-id job))))

;; Muse proper.

(defclass genpatcher-muse (code-actor)
  ((current-repairs :accessor current-repairs :initarg current-repairs
                    :initform nil))
  (:documentation "Integrate genpatcher as a code actor.")
  (:default-initargs
   :force-two-round-code-actions t
   :name "GenPatcher"))

(defmethod muse-available? and ((muse genpatcher-muse))
  (and (find-class 'javascript)
       (find-class 'python)))

(defmethod muse-commands append ((gp genpatcher-muse))
  (list :argot.gp-check-status :argot.gp-apply-result :argot.gp-start :argot.gp-stop))

(defmethod muse-stop progn ((gp genpatcher-muse) &key)
  (mapcar (lambda (job) (stop job (muse-server gp))) (current-repairs gp))
  (setf (slot-value gp 'current-repairs) nil))

(defmethod apply-job-result ((gp genpatcher-muse) (identifier integer))
  (let ((edits
          (remove-if (lambda (edit) (equalp nil edit))
            (mapcar
              (lambda (job) (apply-result job gp))
              (remove-if-not (lambda (job) (= (job-id job) identifier))
                             (slot-value gp 'current-repairs))))))
    (if edits
        (make 'ApplyWorkspaceEditParams
          :|label| "refactoring replace"
          :|edit|
          (annotate-workspace-edit
           "GenPatcher repair"
           (make 'WorkspaceEdit :documentChanges edits)))
        nil)))

(defmethod check-job-status ((gp genpatcher-muse) (identifier integer))
  (progn
    (mapcar (lambda (job) (check-status job (muse-server gp)))
            (remove-if-not (lambda (job) (= (job-id job) identifier))
                           (slot-value gp 'current-repairs)))
    nil))

(defmethod stop-job ((gp genpatcher-muse) (identifier integer))
  (mvlet* ((to-kill remaining
                    (partition (lambda (job) (= (job-id job) identifier))
                               (slot-value gp 'current-repairs))))
    (mapcar (lambda (job) (stop job (muse-server gp))) to-kill)
    (setf (slot-value gp 'current-repairs) remaining)
    nil))

(defmethod remove-job ((gp genpatcher-muse) (to-remove genpatcher-job))
  (progn
    (setf (slot-value gp 'current-repairs)
          (remove-if (lambda (job) (= (job-id job) (job-id to-remove)))
                     (slot-value gp 'current-repairs)))
    nil))

(defmethod applicable-commands ((gp genpatcher-muse) &key start end text-document)
  (nest
   (reduce #'append)
   (list (mapcar #'make-job-status-action (remove-if-not #'job-alive-p (current-repairs gp)))
         (mapcar #'make-job-stop-action (remove-if-not #'job-alive-p (current-repairs gp)))
         (mapcar #'make-job-apply-action (remove-if-not #'job-complete-p (current-repairs gp))))
   (when (lang-software (guess-language (point-buffer start))))
   (when (has-contributions-by-kind-p
          (muse-server gp) (slot-value text-document '|uri|) ArgotKind.Tests))
   (mvlet ((relevant-asts software (relevant-asts start end))))
   (when-let (fn (find-if (of-type 'function-ast) relevant-asts)))
   (unless (string^= "test_" (function-name fn)))
   (multiple-value-bind (start end)
       (ast-start+end software fn))
   (list)
   (make 'Command
         :|title| (fmt "Synthesize repair for ~a from tests" (function-name fn))
         :|command| (string :argot.gp-start)
         :|arguments|
         (list
          text-document
          (convert 'Position start)
          (convert 'Position end)
          (dict "text"
                (source-range-subseq
                 (source-text (genome software))
                 (make 'source-range :start start :end end))
                "imports"
                (extract-imports software))))))

(defmethod apply-command
   ((gp genpatcher-muse) (action (eql :argot.gp-start)) arguments)
    (nest
     (destructuring-bind (doc start end dict) arguments)
      (let* ((start (convert 'Position start))
             (end (convert 'Position end))
             (doc (convert 'OptionalVersionedTextDocumentIdentifier doc))))
      (let* ((uri (slot-value doc '|uri|))
             (buffer (get-buffer-from-uri uri :error nil))
             (start-point (position->point buffer start))
             (end-point (position->point buffer end))
             (software-class (lang-software (guess-language (point-buffer start-point))))
             (software (from-string (make software-class) (points-to-string start-point end-point)))
             (argot-tests
               (contributions-by-kind (muse-server gp) uri ArgotKind.Tests))))
      (when software)
      (when argot-tests)
      (let* ((imports (assure string (gethash "imports" dict)))
             (test-suite
              (argot-tests->genpatcher-test-suite
               argot-tests
               :imports imports))
             (new-job
               (with-bt-handler
                 "start-gp-job"
                 (start-gp-job
                   software test-suite
                   (make-instance
                    'location
                    :start start
                    :end end
                    :text-document doc)
                    (muse-server gp)))))
        (setf (slot-value gp 'current-repairs) (cons new-job (slot-value gp 'current-repairs)))
        (show-message
         (format nil "Repair for lines ~a-~a started"
                 (slot-value start '|line|)
                 (slot-value end '|line|))
         :server gp)
        nil)))

(defmethod apply-command
   ((gp genpatcher-muse) (action (eql :argot.gp-apply-result)) arguments)
   (with-bt-handler "APPLY JOB" (apply-job-result gp (car arguments))))

(defmethod apply-command
   ((gp genpatcher-muse) (action (eql :argot.gp-check-status)) arguments)
   (with-bt-handler "CHECK JOB" (check-job-status gp (car arguments))))

(defmethod apply-command
   ((gp genpatcher-muse) (action (eql :argot.gp-stop)) arguments)
  (with-bt-handler "STOP JOB" (stop-job gp (car arguments))))

(defgeneric extract-imports (software)
  (:documentation
   "Extract the import nodes from the genome of SOFTWARE.")
  (:method ((software t)) "")
  (:method ((software python))
    (let ((imports (filter (op (imports software _))
                           (children (genome software)))))
      ;; TODO Filter out imports that aren't used in the source or test.
      ;; TODO Signal an error if the import is not available in the
      ;; Genpatcher environment.
      (mapconcat #'source-text imports #.(string #\Newline)))))

(defun argot-test->genpatcher-test (test &key (imports ""))
  (let ((source-text (gethash "sourceText" test))
        (test-name (lastcar (gethash "path" test))))
    (if (string^= "@given(" source-text)
        (let ((source-lines (cdr (lines source-text))))
          (when-let
              ((example-entry
                (when (string^= "@example(" (car source-lines))
                  (car source-lines)))
               (test-body (cdr source-lines))
               (test-name (concatenate 'string test-name "_hypothesis")))
            (make-instance
             'fixture-unit-test-case
             :name test-name
             :target test-name
             :arguments nil
             :setup
             (list
              imports
               (string-join
                (append (list (format nil "def ~a():" test-name)
                              "    try:")
                        (mapcar (lambda (str) (format nil "        ~a" str))
                                test-body)
                        (list (format nil "        ~a~a"
                                      (lastcar (gethash "path" test))
                                      (drop 8 example-entry))
                              "        return True"
                              "    except:"
                              "        return False"
                              #.(coerce '(#\Newline) 'string)))
                #\Newline))
             :expected
             (make-instance 'unit-test-case-result :output t)
             :teardown nil)))
        (make-instance
         'fixture-unit-test-case
         :path (gethash "path" test)
         :target (lastcar (gethash "path" test))
         :arguments nil
         :setup (list imports (gethash "sourceText" test))
         :expected
         (make-instance 'unit-test-case-result :output t)
         :teardown nil))))

(defun argot-tests->genpatcher-test-suite (tests &key (imports ""))
  "Convert a series of ARGOT test definitions into a GenPatcher test suite."
  ;; TODO Support binary test cases.
  (make-instance
   'test-suite
   :test-cases
   (loop for tests-entry in tests
         append
         (loop for test in (gethash "tests"
                                    (convert 'hash-table
                                             (slot-value tests-entry '|data|)))
               collect (argot-test->genpatcher-test test :imports imports)))
   :verification-results nil
   :environment nil))
