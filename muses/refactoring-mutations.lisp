(defpackage :argot-server/muses/refactoring-mutations
  (:use :gt/full
        :functional-trees/attrs
        :argot-server/readtable
        :argot-server/muses/base
        :argot-server/muses/code-actor
        :argot-server/lsp-diff
        :lsp-server/protocol
        :refactoring-mutations/mutations
        :refactoring-mutations/c
        :refactoring-mutations/javascript
        :refactoring-mutations/python
        :refactoring-mutations/lisp
        :lsp-server.lem-base)
  (:shadow :add-hook :run-hooks :remove-hook)
  (:shadowing-import-from :serapeum :~> :lines)
  (:local-nicknames (:choice :argot-server/choices))
  (:import-from :software-evolution-library/software-evolution-library
                :mutation
                :mutate
                :apply-mutation
                :object
                :targets)
  (:import-from :cl-lsp/slime
                :search-buffer-package)
  (:import-from :lsp-server
                :text-document-version)
  (:export
   :refactoring-mutations)
  (:documentation "Define a code actor that provides refactoring mutations."))
(in-package :argot-server/muses/refactoring-mutations)
(in-readtable argot-readtable)

(defclass refactoring-mutations (code-actor)
  ()
  (:documentation "Integrate refactoring-mutations as a code actor."))

(defparameter *mutation-classes*
  (nest
   (mapcar #'find-class)
   (filter {proper-subtype-p _ 'refactoring-mutation})
   (package-exports :refactoring-mutations/mutations)))

(defmethod mutation-name ((mutation mutation))
  "The name that the user will be prompted with."
  (fmt "~{~:(~a~)~^ ~}~@[ @~a~]"
       (split-sequence #\- (string (class-name-of mutation)))
       (when-let (target (targets mutation))
         (~> target
             ensure-car
             (source-text)
             ;; TODO After the next Quicklisp dist (as of 2020-09-03),
             ;; add :count 1 here.
             (lines)
             first
             trim-whitespace
             (ellipsize 20)))))

(defmethod mutation-kind ((mutation mutation))
  "A reasonable value for the `kind' property of the code action.
Based on the name of the mutation's class."
  (let ((name (class-name-of mutation)))
    (cond
      ((string^= 'extract- name) :|refactor.extract|)
      ((string^= 'inline- name) :|refactor.inline|)
      ((string^= 'encapsulate- name) :|refactor.rewrite|)
      (t :|refactor|))))

(defmethod muse-code-action-kinds append ((rf refactoring-mutations))
  (list CodeActionKind.Refactor
        CodeActionKind.RefactorExtract
        CodeActionKind.RefactorInline
        CodeActionKind.RefactorRewrite))

(defmethod muse-commands append ((rf refactoring-mutations))
  (list :argot.refactoring-mutations))

(defmethod apply-command ((rf refactoring-mutations)
                          (cmd (eql :argot.refactoring-mutations))
                          arguments)
  (declare (ignore cmd))
  (nest
   (make 'ApplyWorkspaceEditParams
         :label "refactoring replace"
         :edit)
   (destructuring-bind (action new-names)
       arguments
     (assert (every (distinct) new-names)))
   (let ((edit (slot-value (convert 'CodeAction action) '|edit|))))
   (let* ((count 0)
          (substitutions
           (mapcar (lambda (name)
                     (cons name
                           (let ((result
                                  (choice:read-string
                                   :prompt (fmt "New name #~a/~a"
                                                (incf count)
                                                (length new-names)))))
                             (if (emptyp result) name
                                 result))))
                   new-names))))
   (ematch edit
     ((WorkspaceEdit
       :document-changes
       (list
        (and td-edit
             (TextDocumentEdit :edits edits))))
      (nest
       (copy edit :document-changes)
       (list)
       (copy td-edit :edits)
       (mapcar
        (lambda (edit)
          (ematch edit
            ((AnnotatedTextEdit :new-text new-text)
             (copy edit
                   :new-text
                   (reduce (lambda (new-text substitution)
                             (string-replace-all
                              (car substitution)
                              new-text
                              (cdr substitution)))
                           substitutions
                           :initial-value new-text)))))
        edits))))))

(defmethod applicable-commands ((rf refactoring-mutations) &key start end text-document)
  "Return a list of applicable commands (mutations).

Note that there may be multiple instances of the same mutation if it
can target more than one AST node between START and END."
  (declare (ignorable end))
  (nest
   (with-document-software ((start end)
                            :software software
                            :asts asts
                            :software-class software-class
                            :text text))
   (with-point ((start-pt start)))
   (let* ((*package*
           (if (typep software 'lisp)
               (search-buffer-package start-pt)
               *package*))))
   (filter-map (lambda (mutation)
                 (multiple-value-bind (edit new-names)
                     (mutation-workspace-edit+new-names
                      mutation text-document text)
                   (when edit
                     (make 'Command
                           :|title| (mutation-name mutation)
                           :|command| (string :argot.refactoring-mutations)
                           :|arguments| (list edit new-names))))))
   (applicable-mutations software asts)))

(defun applicable-mutations (software asts)
  "Find applicable refactorings in the Cartesian product of ASTs and
the set of known refactorings."
  (with-attr-table software
    (iter mutations
          (for mut-class in *mutation-classes*)
          (for m = (make mut-class :object software))
          (iter (for ast in asts)
                (when (ignore-errors
                       (mutation-target-p m software ast))
                  (in mutations
                      (collect (make mut-class
                                     :object software
                                     :targets ast))))))))

(defmethod mutation-applies? ((m mutation))
  "If mutation M can be applied to its object, return the result of applying it.

Does not side-effect M or the object it targets."
  (ignore-errors
   (apply-mutation (copy (object m)) m)))

(defmethod mutation-workspace-edit+new-names ((m mutation) text-document old-text)
  "Given an applicable mutation, compute the set of LSP edits
necessary to update the client with the result of applying that
mutation.

As a second value, return a list of new names \(gensyms) inserted into
the genome by the mutation."
  (nest
   (when-let* ((new (mutation-applies? m))
               (new-text (ignore-errors (genome-string new)))))
   (let ((new-names
          (nub (remove-if (op (string*= _ old-text))
                          (all-matches-as-strings "G\\d+" new-text))))
         (name (mutation-name m))))
   (values
    (nest
     (make-instance 'CodeAction
                    :|title| name
                    :|kind| (mutation-kind m)
                    :|edit|)
     (annotate-workspace-edit
      (mutation-name m))
     (make-instance 'WorkspaceEdit
                    :documentChanges)
     (list)
     (make-instance 'TextDocumentEdit
                    :textDocument (make 'OptionalVersionedTextDocumentIdentifier
                                        :uri (slot-value text-document '|uri|)
                                        :version (text-document-version text-document))
                    :|edits|)
     (lsp-edit-diff-strings old-text new-text))
    new-names)))
