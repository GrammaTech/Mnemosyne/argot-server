(defpackage :argot-server/muses/ssr-muse
  (:use :gt/full
        :trivia.ppcre
        :argot-server/ast-utils
        :argot-server/readtable
        :argot-server/muses/base
        :argot-server/muses/code-actor
        :argot-server/choices
        :argot-server/lsp-diff
        :software-evolution-library
        :lsp-server/protocol
        :ssr
        :lsp-server.lem-base)
  (:shadow :add-hook :run-hooks :remove-hook)
  (:shadowing-import-from :serapeum :~> :lines)
  (:import-from :cl-lsp/slime :search-buffer-package)
  (:import-from :lsp-server
                :text-document-version
                :connection-meta)
  (:export :ssr-muse)
  (:documentation "Define a code actor that provides ssr ast-substitution"))
(in-package :argot-server/muses/ssr-muse)
(in-readtable :curry-compose-reader-macros) ; note: we don't use argot-readtable

(defparameter *max-ast-lines* 4 "maximum lines in ast for pulldown menu")

(defparameter *default-choices* '("First suggestion" "Second suggestion" "None"))

(defclass ssr-muse (code-actor)
  ()
  (:default-initargs
   :force-two-round-code-actions t
   :name "SSR")
  (:documentation "Integrate ssr-muse as a code actor."))

(defmethod muse-available? and ((muse ssr-muse)) #+sbcl t)

(defmethod muse-code-action-kinds append ((s ssr-muse))
  (list "ssr-muse.methodsubst"))

(defplace multi-file? ()
  (connection-meta 'multi-file))

(defmethod muse-commands append ((s ssr-muse))
  (list :argot.ssr-toggle-scope
        :argot.ssr-rename
        :argot.ssr-subst))

(defconst +return-arrow+
  ;; Obey typical keyboard conventions.
  (assure character
    (cond ((uiop:os-macosx-p) #\↩)
          ((uiop:os-windows-p) #\↵)
          (t #\⏎))))

(defun encode-newlines (string)
  "Substitute a Unicode symbol for newlines in STRING."
  (assert (not (find +return-arrow+ string)))
  (substitute +return-arrow+ #\Newline string))

(defun decode-newlines (string)
  "Recover symbol-encoded newlines in STRING."
  (assert (not (find #\Newline string)))
  (substitute #\Newline +return-arrow+ string))

(defun list-like-files-in-workspace (uri)
  "Return a list of other files in URI's workspace having the same
extension."
  (let ((extension
         ;; TODO Windows?
         (ensure-prefix "."
                        (pathname-type (pathname
                                        (file-uri-path uri))))))
    (remove uri
            (filter {string$= extension}
                    (list-workspace-uris :uri uri))
            :test #'equal)))

(defun in-range? (software ast)
  (let ((range (node-source-range (genome software) ast)))
    (and (<= (- (line (end range)) (line (begin range)))
             *max-ast-lines*))))

(defmethod applicable-commands ((s ssr-muse) &key start end text-document)
  "Return a list of applicable commands."
  (declare (ignorable end))
  (with-document-software ((start end) :software software :asts asts)
    (let* ((cursor-ast (first asts))
           (asts (remove-if (of-type 'comment-ast) asts))
           (identifier
            (when (and cursor-ast (typep cursor-ast 'identifier-ast))
              (source-text cursor-ast)))
           (renames
            (and identifier
                 (list     ; SSR-RENAME action when on an identifier.
                  (make '|Command|
                        :title (format nil "Rename property ~A" identifier)
                        :command (string :argot.ssr-rename)
                        :arguments (pack-ast software cursor-ast)))))
           (substitutions
            (stable-sort
             (remove-duplicates
              (filter-map          ; SSR-SUBST actions for every AST.
               (lambda (ast)
                 (and (in-range? software ast)
                      (make '|Command|
                            :title (concatenate 'string
                                                "SSR: "
                                                (string-trim
                                                 (list #\Space)
                                                 (substitute #\Space #\Newline (source-text ast))))
                            :command (string :argot.ssr-subst)
                            :arguments (list text-document
                                             (pack-ast software ast)))))
               asts)
              :test #'equal
              :from-end t
              :key (op (slot-value _ '|title|)))
             #'length>
             :key (op (slot-value _ '|title|))))
           (toggle-commands
            (and (or renames substitutions)
                 (list
                  (make '|Command|
                        :title
                        (fmt "~
SSR: Target ~:[all ~@(~a~) files~;this file only~]"
                             (multi-file?)
                             (type-of software))
                        :command (string :argot.ssr-toggle-scope))))))
      (append
       renames
       substitutions
       toggle-commands))))

(defun rename-identifier (software identifier new-name)
  (genome-string
   (batch-apply-clauses
    software
    `(((identifier-ast :text ,identifier)
       (copy *ast* :text ,new-name))))))

(defun read-ssr-patterns (uri ast)
  "Read string clauses (from and to patterns) via LSP and return them
as multiple values."
  (let* ((from (present-choice-for
                uri *default-choices*
                :prompt "Modify matching asts"
                ;; Convert newlines into Unicode symbols (since VS
                ;; Code only exposes single-lines input boxes.)
                :default-text (encode-newlines (source-text ast))
                ;; :multi-line t
                :class 'open-choice))
         (to (present-choice-for
              uri *default-choices*
              :prompt "Substitute ast"
              :default-text from
              :class 'open-choice)))
    ;; At this point newlines are still encoded as Unicode symbols.
    ;; Convert them back to actual newlines.
    (setf from (decode-newlines from)
          to (decode-newlines to))
    ;; If the original AST's source text ended in a newline, make sure
    ;; that the patterns do too (otherwise they may parse
    ;; differently).
    (when (string$= #\Newline (source-text ast))
      (setf from (ensure-suffix from '(#\Newline))
            to (ensure-suffix to '(#\Newline))))
    (when (equal from to)
      (error "Patterns are the same, not substituting"))
    (values from to)))

(-> diff/doc (string string string) |TextDocumentEdit|)
(defun diff/doc (uri before after)
  "Diff BEFORE and AFTER and return a TextDocumentEdit for URI."
  (make '|TextDocumentEdit|
        :text-document
        (make '|OptionalVersionedTextDocumentIdentifier|
              :uri uri
              :version nil)
        :edits (lsp-edit-diff-strings before after)))

(-> map-diff-like-files
    ((-> (string software) software)
     string
     (or symbol software))
    list)
(defun map-diff-like-files (fn uri class)
  "Map FN over the other files in URI's workspace with the same
extension, interpreting them as instances of CLASS and collecting
diffs of the old file and the result of FN."
  (fbind (fn)
    (filter-map
     (lambda (uri)
       ;; TODO Check for client cap instead.
       (ignore-some-conditions (jsonrpc:jsonrpc-method-not-found)
         (let* ((old-string (get-file-contents uri))
                (sw (from-string class old-string))
                (new-string
                 (ignore-errors
                  (genome-string
                   (fn uri sw)))))
           (and new-string
                (not (equal new-string old-string))
                (diff/doc uri old-string new-string)))))
     (list-like-files-in-workspace uri))))

(defun wrap-workspace-edit (label edits)
  "Wrap EDITS as an instance of `ApplyWorkspaceEditParams' with LABEL."
  (nest
   (make '|ApplyWorkspaceEditParams| :label label :edit)
   (make '|WorkspaceEdit| :document-changes)
   edits))

(defmethod apply-command ((s ssr-muse)
                          (cmd (eql :argot.ssr-toggle-scope))
                          arguments)
  (declare (ignore arguments))
  (callf #'not (multi-file?))
  (values))

(defmethod apply-command ((s ssr-muse)
                          (cmd (eql :argot.ssr-rename))
                          arguments)
  (declare (ignorable cmd))
  (with-ast-pack (arguments :doc doc :uri uri :software software :ast ast)
    (let* ((orig (variable-name ast))
           (default-text "None")
           (choice (present-choice-for
                    uri
                    *default-choices*
                    :prompt "Choose a new name"
                    :default-text default-text
                    :class 'open-choice)))
      (unless (equal choice default-text)
        (let ((label (fmt "Rename ~a to ~a" orig choice))
              (from (variable-name ast)))
          (annotate-workspace-edit
           label
           (wrap-workspace-edit
            label
            (cons
             (diff/doc
              uri
              (genome-string software)
              (genome-string (rename-identifier
                              software
                              from
                              choice)))
             (and (multi-file?)
                  (map-diff-like-files
                   (op (rename-identifier _2 from choice))
                   uri (type-of software)))))))))))

(defmethod apply-command ((s ssr-muse)
                          (cmd (eql :argot.ssr-subst))
                          arguments)
  (declare (ignore cmd))
  (nest
   (with-ast-pack (arguments :doc doc :uri uri :software software :ast ast))
   (mvlet* ((from to (read-ssr-patterns uri ast))
            (pattern
             (string-clauses->pattern software from to ast))))
   (let ((label "SSR substitution")))
   (annotate-workspace-edit label)
   (wrap-workspace-edit label)
   (cons
    (diff/doc
     uri
     (genome-string software)
     (genome-string
      (batch-apply-clauses
       software (list pattern)
       :error t))))
   (and (multi-file?))
   (map-diff-like-files
    (lambda (uri sw)
      (declare (ignore uri))
      (batch-apply-clauses sw (list pattern) :error t))
    uri (type-of software))))
