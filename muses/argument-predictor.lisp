;;; argument-predictor.lisp --- Muse for callarg prediction.
(defpackage :argot-server/muses/argument-predictor
  (:use :gt/full
        :argot-server/readtable
        :argot-server/muses/base
        :argot-server/muses/passthrough
        :argot-server/muses/pygls)
  (:export :argument-predictor
           :+argument-predictor-command+))
(in-package :argot-server/muses/argument-predictor)

(defclass argument-predictor (tcp-muse pygls) ()
  (:default-initargs
   :host "code-predictor"
   :name "Argument Predictor"
   :lang 'python
   :port 6060))
