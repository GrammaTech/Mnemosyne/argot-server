(defpackage :argot-server/muses/jsls
  (:use :gt/full
        :argot-server/muses/base
        :argot-server/muses/incrementalizer
        :argot-server/muses/passthrough)
  (:export :jsls)
  (:documentation "Muse that provides baseline LSP support for JavaScript."))
(in-package :argot-server/muses/jsls)

(defclass javascript-typescript-stdio (stdio-muse)
  ()
  (:documentation
   "Base, unmediated instance of javascript-typescript-stdio.")
  (:default-initargs
   :program (list "javascript-typescript-stdio")
   :lang '(or javascript typescript)))

(defclass jsls (incrementalizer)
  ()
  (:default-initargs
   :next (make 'javascript-typescript-stdio))
  (:documentation
   "JavaScript language server with incremental update support."))
