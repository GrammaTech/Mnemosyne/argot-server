(defpackage :argot-server/muses/cli-muses
  (:use :gt/full
        :argot-server/muses/muse
        :software-evolution-library/utility/debug)
  (:export
    :muse-program
    :program
    :format-to
    :parse-from
    :drive
    :program-muse
    :process-muse
    :driver-muse
    :runner-muse
    :launcher-muse
    :muse-process))
(in-package :argot-server/muses/cli-muses)

(defgeneric format-to (muse output stream)
  (:documentation "Write something to the Muse's coprocess.")
  (:method-combination standard/context))

(defgeneric parse-from (muse input)
  (:documentation "Parse the output of the Muse's process.")
  (:method-combination standard/context)
  (:method ((muse t) (input t))
    (error "Don't know how to parse input for ~a" muse)))

(defgeneric drive (muse prompt)
  (:method-combination standard/context)
  (:documentation "Call MUSE with PROMPT and return the results."))

(defgeneric muse-environment-alist (muse)
  (:method-combination append)
  (:documentation "Return an alist of environment variables and values to use when running MUSE.")
  (:method append ((muse t)) nil)
  (:method append ((muse port-muse))
    (list (cons "PORT" (muse-port muse)))))

(defgeneric muse-load-chain (muse)
  (:documentation "Build a load chain to run MUSE's program.

That is, prefix PROGRAM with calls to other programs that alter the
environment, then `exec' their `argv'."))

(defclass program-muse (muse)
  ((program :initarg :program :reader muse-program
            :type list))
  (:documentation "A muse that runs a program."))

(defmethod print-object ((self program-muse) stream)
  (print-unreadable-object (self stream :type t)
    (ignore-some-conditions (unbound-slot)
      (format stream "~a" (muse-program self)))))

(defmethod muse-name ((muse program-muse))
  (first (muse-program muse)))

(defmethod muse-available? and ((muse program-muse))
  (resolve-executable (first (muse-program muse))))

(defmethod muse-load-chain ((muse program-muse))
  (muse-program muse))

(defclass process-muse (program-muse)
  ((process :initform nil
            :accessor muse-process)
   (max-restarts :type (integer 0 *)
                 :accessor muse-restarts
                 :initarg :restarts)
   (restart-count :initform 0
                  :type (integer 0 *)
                  :accessor muse-restart-count))
  (:documentation "A muse with an associated process.")
  (:default-initargs :restarts 10))

(defmethod muse-load-chain ((muse process-muse))
  (nest
   ((lambda (c) (wrap-environment c (muse-environment-alist muse))))
   (unbuffer)
   (untty)
   (call-next-method)))

(defmethod muse-started-p and ((muse process-muse))
  ;; The process slot is null unless a process has been started. (If
  ;; the process has exited, the muse is still "running", as `drive'
  ;; will attempt to restart the process.)
  (muse-process muse))

(defmethod muse-start ((muse process-muse))
  (unless (muse-started-p muse)
    (setf (muse-process muse)
          (multiple-value-call #'launch-program
            (muse-load-chain muse)
            :input :stream
            :output :stream
            ;; If the note level is high enough, output the muse's
            ;; stderr to the stderr of the Lisp process.
            (if (>= *note-level* 4)
                (values :error-output uiop:*stderr*)
                (values))))))

(defmethod muse-stop progn ((muse process-muse) &key)
  (with-accessors ((p muse-process)) muse
    (and p (terminate-process (shiftf p nil)))
    (setf (muse-restart-count muse) 0)))

(defmethod drive :context ((muse process-muse) prompt)
  (declare (ignore prompt))
  (synchronized (muse)
    (call-next-method)))

(defmethod drive ((muse process-muse) prompt)
  (let ((output (process-info-input (muse-process muse)))
        (input (process-info-output (muse-process muse))))
    (format-to muse prompt output)
    (parse-from muse input)))

(defclass driver-muse (muse)
  ()
  (:documentation "A muse that must be queried, outside of LSP, to get
  a result.

This could be a muse with an associated background process, or a muse
that runs a separate process for each query."))

(defmethod format-to ((muse driver-muse) (output t) (stream stream))
  (declare (ignore muse))
  (format stream "~a" output))

(defmethod format-to :after ((muse driver-muse) (output t) (stream stream))
  (terpri stream)
  (force-output stream))

(defclass runner-muse (program-muse driver-muse)
  ()
  (:documentation "A muse that runs its program on each call."))

(defmethod muse-load-chain ((muse runner-muse))
  (wrap-environment (call-next-method)
                    (muse-environment-alist muse)))

(defmethod drive ((muse runner-muse) prompt)
  (nest
   (with-output-to-string (s))
   (with-input-from-string (in prompt))
   (uiop:run-program (muse-load-chain muse)
                     :input in
                     :output s)))

(defclass launcher-muse (process-muse driver-muse)
  ()
  (:documentation "A muse that drives a long-running process."))

(defmethod drive :before ((muse launcher-muse) prompt)
  "Automatically restart MUSE's process if it has exited."
  (declare (ignore prompt))
  (unless (and-let* ((process (muse-process muse))
                     ((process-alive-p process))))
    (when (> (muse-restarts muse) 0)
      (decf (muse-restarts muse))
      (setf (muse-process muse) nil)
      (muse-start muse))))

(defun unbuffer (program)
  "Persuade PROGRAM not to buffer its output."
  ;; To answer the obvious question: why not use `unbuffer` (from
  ;; expect), if available? Because it merges standard output and
  ;; standard error; same for (ab)using script.
  (assert (listp program))
  (cond ((resolve-executable "stdbuf")
         (list* "stdbuf" "-o0" program))
        ((uiop:os-macosx-p)
         (list* "env" "NSUnbufferedIO=YES" program))
        (t program)))

(defun wrap-environment (program environment-alist)
  (if (null environment-alist) program
      (cons "env"
            (append (iter (for (key . value) in environment-alist)
                          (collect (fmt "~a=~a" key value)))
                    program))))

(defun find-nopty ()
  "Find the find-nopty script."
  (assert (uiop:os-unix-p))
  (namestring
   (asdf:system-relative-pathname :argot-server "bin/nopty")))

(defun untty (program)
  "If there is a controlling TTY, hide it from PROGRAM."
  (if (tty?)
      (list* (find-nopty) program)
      program))

(defun tty? ()
  "Return T if there is a controlling TTY."
  (handler-case
      (progn
        (open #p"/dev/tty" :direction :probe)
        t)
    (file-error () nil)))
