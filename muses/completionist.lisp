(defpackage :argot-server/muses/completionist
  (:use :gt/full
        :argot-server/merge-results
        :argot-server/readtable
        :argot-server/muses/base)
  (:export :complete
           :completionist
           :make-simple-completion)
  (:documentation "Define a base class for muses that provide completions."))
(in-package :argot-server/muses/completionist)
(in-readtable argot-readtable)

(defclass completionist (muse)
  ()
  (:documentation "A muse that provides completions."))

(defmethod contribute-server-capabilities list ((muse completionist))
  (make 'ServerCapabilities
        :text-document-sync TextDocumentSyncKind.Incremental
        :completion-provider
        (make-instance 'CompletionOptions)))

(defgeneric complete (completionist &key symbol start end)
  (:documentation "Specialize this function to provide code completion.
This function is called from within the relevant source buffer with
the point located where completion is desired.  This function should
take the following optional keyword parameters and should return
either a list of strings or a list of CompletionItems.  Optional
keyword parameters:

* symbol: Current symbol at point.  (Some editors will only accept
  completions that begin with this string.)

* start: The start position of the current symbol in the source
  buffer.

* end: The end position of the current symbol in the source buffer."))

(defun make-simple-completion (string)
  (make-instance '|CompletionItem|
                 :|label| string
                 :|insertTextFormat| 1 ; Plain text to insert, not a snippet.
                 :|insertText| string))

(define-class-method "textDocument/completion"
    completionist
    (params |TextDocumentPositionParams|)
  (with-text-document-position (point) params
    (with-point ((start point)
                 (end point))
      (skip-symbol-backward start)
      (skip-symbol-forward end)
      (let ((completions
             (complete *server*
                       :symbol (points-to-string start end)
                       :start start
                       :end end)))
        (convert 'hash-table
                 (make-instance '|CompletionList|
                                :|isIncomplete| nil
                                :|items| (mapcar (lambda (completion)
                                                   (etypecase completion
                                                     (string (make-simple-completion completion))
                                                     (|CompletionItem| completion)))
                                                 completions)))))))
