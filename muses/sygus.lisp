(defpackage :argot-server/muses/sygus
  (:use :gt/full
        :argot-server/utility
        :argot-server/readtable
        :argot-server/lsp-diff
        :argot-server/muses/base
        :argot-server/muses/code-actor
        :lsp-server
        :lsp-server/protocol
        :argot
        :argot-server/ast-utils)
  (:shadowing-import-from :uiop :run-program)
  (:shadowing-import-from :software-evolution-library/software-evolution-library
                          :software :genome-string)
  (:export :sygus))
(in-package :argot-server/muses/sygus)
(in-readtable :curry-compose-reader-macros)

(defclass sygus (code-actor) ()
  (:documentation "Sygus (specifically, CVC4) as a code-actor")
  (:default-initargs
   :force-two-round-code-actions t
   :lang 'c))

(defmethod muse-code-action-kinds append ((muse sygus))
  (list 'CodeActionKind.Synthesis))

(defmethod muse-available? and ((muse sygus))
  (resolve-executable "cvc4"))

(defmethod muse-commands append ((sygus sygus))
  (list :argot.sygus))

(defmethod applicable-commands ((muse sygus) &key start end text-document)
  ;; Find the first function declaration after START
  (when-let* ((lang-sym (guess-language (point-buffer start)))
              (software-class (lang-software lang-sym))
              (text (buffer-to-string (point-buffer start)))
              (sw (from-string (make-instance lang-sym) text)))
    ;; sw now contains the parsed file
    ;; Look for a fundcl after the start point
    (let ((asts (remove-if-not (of-type 'function-ast)
                               (ignore-some-conditions
                                   (reader-error)
                                 (asts-contained-in-source-range
                                  sw (points->source-range start end))))))
      (when-let ((ast (car asts)))
        (let ((name (function-name ast)))
          (list
           (make '|Command|
                 :|title|
                 "Synthesize function body" ; (format nil "Synthesize function body" at ~s using examples (source-range sw ast))
                 :|command| (string :argot.sygus)
                 :|arguments|
                 (list text-document
                       (convert 'hash-table
                                (fset:map ("software-class" (symbol-name software-class))
                                          ("text" text)
                                          ("name" name)))))))))))
(defmethod apply-command ((sygus sygus)
                          (cmd (eql :argot.sygus))
                          arguments)
  (nest
   (destructuring-bind (doc params) arguments)
   (let* ((software-class (intern (gethash "software-class" params) :sel/sw/ts))
          (old-text (gethash "text" params))
          (software (from-string (make-instance software-class) old-text))
          (name (gethash "name" params))))
   (make '|ApplyWorkspaceEditParams| :|label| "insert sygus code" :|edit|)
   (make-instance '|WorkspaceEdit| :|documentChanges|) (list)
   (make-instance '|TextDocumentEdit|
                  :|textDocument|
                  (make '|OptionalVersionedTextDocumentIdentifier|
                        :|uri| (gethash "uri" doc)
                        :|version| nil)
       :|edits|)
   (with-temporary-file-of (:pathname old-file) old-text)
   (with-temporary-file-of (:pathname new-file)
                           (genome-string (sygus-perform software name)))
   (lsp-edit-diff old-file new-file)))

(defgeneric get-preceding-comments (software ast path)
  (:documentation "Get the comment ASTs preceeding the node at PATH")
  (:method ((software tree-sitter) (ast tree-sitter-ast) (path list))
    (when path
      (let ((final-offset (lastcar path))
            (parent-path (butlast path)))
        (when (integerp final-offset)
          (nreverse
           (loop :for i :downfrom (1- final-offset) :downto 0
              :for ast := (@ software (append parent-path (list i)))
              :while (typep ast 'c-comment)
              :collect ast)))))))

(defgeneric extract-tests-from-comments (software comments)
  (:documentation "Convert the comment in COMMENTS into test cases")
  (:method ((software t) (comments list))
    (iter (for c in comments)
      (appending (extract-tests-from-comment software c)))))

(defgeneric extract-tests-from-comment (software comment)
  (:documentation "Convert a single comment object to a test")
  (:method ((software software) (comment c-comment))
    (extract-tests-from-comment software (source-text comment)))
  (:method ((software software) (comment string))
    (let* ((len (length comment))
           (reduced-string
             (cond
              ((< len 2) "")
              ((and (string= comment "/*" :end1 2)
                    (string= comment "*/" :start1 (- len 2)))
               (subseq comment 2 (- len 2)))
              ((string= comment "//" :end1 2)
               (subseq comment 2))
              (t (error "Not a valid comment: ~a~%" comment)))))
      ;; Read input, outputs
      (with-standard-io-syntax
        (with-input-from-string (s reduced-string)
          ;; do not further convert them here
          (iter (let ((test-inputs (read s nil :eof)))
                  (when (eql test-inputs :eof)
                    (finish))
                  (let ((result (read s nil :eof)))
                    (when (eql result :eof)
                      (finish))
                    (collecting (list test-inputs result))))))))))

(defgeneric decompose-fundcl (software fundcl-ast)
  (:documentation "Returns four things: the name of the function,
the names of the parameters, the types of the parameters, and the return
type of the function")
  (:method ((software c) (f c-function-definition))
    (handler-case
        (let* ((type (@ f :c-type))
               (params (@ f '(:c-declarator :c-parameters :children)))
               (param-types (mapcar #'c-type params)))
          (values
           (function-name f)
           (mapcar (lambda (p) (source-text (@ p ':c-declarator))) params)
           (mapcar #'source-text param-types)
           (source-text type)))
      (error () nil))))

;; Convert tests cases and inputs to SyGuS

(defgeneric sygus-from-tests (software fundcl tests &key logic)
  (:documentation "Generate a SyGuS input from a set of tests")
  (:method ((software c) (fundcl c-function-definition) (tests list) &key (logic 'LIA))
    (multiple-value-bind (fn-name param-names param-types return-type)
        (decompose-fundcl software fundcl)
        (nest
         (flet ((fail (&rest args)
                  (apply #'warn args)
                  (return-from sygus-from-tests nil))))
         (flet ((type-to-sygus (type)
                  (string-case type
                    ("int" '|Int|)
                    ("string" '|String|)
                    ("bool" '|Bool|)
                    (t (fail "Cannot convert tests to SyGuS: unknown type ~a" type)
                       (return-from sygus-from-tests nil))))))
         (if (not fn-name)
             (fail "Could not decompose funddcl: ~a"
                   (source-text fundcl)))
         (if (null tests)
             (fail "No tests found"))
         (let ((pkg (find-package :argot-server/muses/sygus))))
         (flet ((m (s) (intern (string s) pkg))))
         (let ((fn-name (m fn-name))
               (param-names (mapcar #'m param-names))
               (param-types (mapcar #'type-to-sygus param-types))
               (return-type (type-to-sygus return-type))))
         (flet ((names-of-type (type)
                  (iter (for n in param-names)
                    (for pt in param-types)
                    (when (eql pt type) (collecting n))))
                (translate-const (c)
                  (typecase c
                    ((integer 0) c)
                    ((integer * (0)) `(- ,(- c)))
                    (string c)
                    ((eql true) '|true|)
                    ((eql false) '|false|)
                    (t (fail "Cannot convert constant to sygus: ~a" c)))))
         `((|set-logic| ,logic)
           (|synth-fun| ,fn-name ,(mapcar #'list param-names param-types) ,return-type
                        ((I |Int|) (B |Bool|))
                        ((I |Int| (,@(names-of-type '|Int|)
                                     0 1
                                     (+ I I)
                                     (- I I)
                                     (|ite| B I I)))
                         (B |Bool| (,@(names-of-type '|Bool|)
                                    (|and| B B) (|or| B B) (|not| B)
                                    (= I I) (<= I I) (>= I I)))))
           ,@(iter (for n in param-names)
               (for tp in param-types)
               (collecting `(|declare-var| ,n ,tp)))

           ,@(iter (for (inputs result) in tests)
               (unless (= (length inputs) (length param-names))
                 (fail "Test has ~a arguments, needs ~a"
                       (length inputs) (length param-names)))
               (collecting
                 `(|constraint| (= (,fn-name ,@(mapcar #'translate-const inputs))
                                   ,(translate-const result)))))

           (|check-synth|)))))))

(defgeneric sygus-input (software fn-name)
  (:documentation "Invoke sygus on the test comments before function declaration
with name FN-NAME")
  (:method ((software software) (fn-name string))
    (nest
     (let ((ast (genome software))))
     (flet ((is-this-fundcl (a)
              (and (typep a 'function-ast)
                   (equal (function-name a) fn-name)))))
     (let ((fun-ast (find-if #'is-this-fundcl ast))))
     (when fun-ast)
     (let ((path (position fun-ast ast))))
     (let ((comments (get-preceding-comments software ast path))))
     (let ((tests (extract-tests-from-comments software comments))))
     (when tests)
     ;; Do not invoke for now, just compute the form
     (values (sygus-from-tests software fun-ast tests) path))))

(defun sygus-perform (software fn-name)
  "Insert a string with the sygus output as the body of function FN-NAME"
  (nest
   (multiple-value-bind (sygus path) (sygus-input software fn-name))
   (if (not path) software)
   (let ((s (print-sygus-to-string sygus))))
   (let ((new-body
           (nest
            (make-instance 'c-compound-statement
                           :children)
            (list)
            (make-instance 'c-expression-statement
                           :children)
            (list)
            (make-instance 'c-string-literal
                           :text (format nil "\"~a\"" s)
                           :children nil)))))
   (progn (setf (@ (genome software) (append path '(:c-body)))
                new-body)
          software)))

(defvar *sygus-readtable*
  (let ((r (copy-readtable nil)))
    (setf (readtable-case r) :preserve)
    r))

(defun print-sygus (sygus &key (stream t))
  "Print the sygus S-expr SYGUS to stream S in a form that's
intelligible to a sygus server"
  (let ((*readtable* *sygus-readtable*))
    (format stream "~a~%" sygus)))

(defun print-sygus-to-file (sygus pathname)
  (with-open-file (s pathname :direction :output
                              :if-exists :supersede
                              :if-does-not-exist :create)
    (dolist (x sygus) (print-sygus x :stream s))))

(defun print-sygus-to-string (sygus)
  (with-output-to-string (s)
    (dolist (x sygus) (print-sygus x :stream s))))

(defgeneric invoke-sygus (input &key program)
  (:documentation "Invoke SyGuS command line program PROGRAM.
Return the generated function.")
  (:method ((input list) &key (program "cvc4 --lang=sygus2"))
    (let ((input-str (print-sygus-to-string input)))
      (with-input-from-string (is input-str)
        (run-program program :input is :output :string)))))

(defun decode-sygus-output (str)
  "Extracts the synthesized function from a sygus output,
or NIL if it cannot find one"
  (nest
   (with-input-from-string (s str))
   (let ((*package* (find-package :argot-server/muses/sygus))
         (*readtable* *sygus-readtable*)))
   (let ((status (read s nil :eof))))
   (if (not (eql status '|unsat|)) nil)
   (let ((form (read s nil :eof))))
   (if (and (consp form) (eql (car form) '|define-fun|)) form nil)))
