(defpackage :argot-server/muses/regel
  (:use :gt/full
        :argot-server/muses/base
        :argot-server/muses/cli-muses
        :argot-server/muses/code-actor
        :argot-server/readtable
        :argot-server/choices
        :lsp-server
        :lsp-server/protocol)
  (:local-nicknames (:i :iterate))
  (:export :regel))
(in-package :argot-server/muses/regel)
(in-readtable argot-readtable)

(defvar *regel-script*
  #p"/usr/local/bin/regel-muse"
  "Absolute location of the regel script.
The default value is a location in the Argot Server Docker image.")

(defclass regel (runner-muse code-actor)
  ()
  (:default-initargs
   :name "REGEL"
   :force-two-round-code-actions t))

(defmethod muse-program ((muse regel))
  (list *regel-script*))

(defmethod muse-available? and ((muse regel))
  (if (dockerized?)
      (file-exists-p *regel-script*)
      (argot-image-exists? "regel")))

(defmethod muse-commands append ((regel regel))
  (list :argot.regel))

(defmethod apply-command ((regel regel)
                          (cmd (eql :argot.regel))
                          arguments)
  (declare (ignore cmd))
  (nest
   (destructuring-bind (doc start end text) arguments)
   (let ((start (convert 'Position start))
         (end   (convert 'Position end))
         (doc   (convert 'OptionalVersionedTextDocumentIdentifier doc))
         (text  (href text "text"))))
   (let* ((benchmark (parse-benchmark/loose text))
          (prompt (benchmark-prompt benchmark))
          (regexes (drive regel prompt))
          (choices (append1 regexes "None"))
          (uri (slot-value doc '|uri|))
          (final-choice
           (present-choice-for uri choices
                               :prompt "Choose a regex"))))
   (unless (equal final-choice "None"))
   (make 'ApplyWorkspaceEditParams
         :|label| "regex synthesis"
         :|edit|
         (make 'WorkspaceEdit
               :documentChanges
               (list
                (make 'TextDocumentEdit
                      :textDocument doc
                      :edits (list
                              (make 'TextEdit
                                    :range (make 'Range
                                                 :start start
                                                 :end end)
                                    :newText final-choice))))))))

(defmethod applicable-commands ((muse regel) &key start end text-document)
  (nest
   (with-text (text start end))
   (unless (blankp text))
   (when (scan "(?m)^[+-]" text))
   (list (make 'Command
               :title (format nil "Synthesize a regular expression")
               :command (string :argot.regel)
               :arguments (list text-document
                                (point-to-position start)
                                (point-to-position end)
                                (dict "text" (points-to-string start end)))))))

(defclass benchmark ()
  ((id :initarg :id :type string)
   (natlang :initarg :natlang :type string)
   (positive-examples :initarg :positive-examples :initarg :+ :type list)
   (negative-examples :initarg :negative-examples :initarg :- :type list)
   (ground-truth :initarg :ground-truth :initarg :gt :type (or string null)))
  (:default-initargs
   :id (string (gensym "regel"))
   :+ nil
   :- nil
   :gt "na"))

(defmethod initialize-instance :after ((self benchmark) &key)
  (with-slots (natlang positive-examples negative-examples) self
    (assert (not (find #\Newline natlang)))
    (assert (every #'stringp positive-examples))
    (assert (every #'stringp negative-examples))))

(defmethod print-benchmark ((benchmark benchmark) (stream null))
  (with-output-to-string (stream)
    (print-benchmark benchmark stream)))

(defmethod print-benchmark ((benchmark benchmark) (stream (eql t)))
  (print-benchmark benchmark *standard-output*))

(defmethod print-benchmark ((benchmark benchmark) (stream stream))
  (with-slots (id natlang negative-examples positive-examples ground-truth)
      benchmark
    (format stream "~
// natural language~%~a~2%~
// example~%~{~&\"~a\",+~}~{~&\"~a\",-~}~2%~
// gt~%~a"
            natlang
            positive-examples
            negative-examples
            ground-truth)))

(deftype benchmark-state ()
  '(member :start :natlang :examples :gt))

(defun parse-benchmark (text)
  "Parse a benchmark in Regel's internal format."
  (iter
   (i:with state = :start)
   (for raw-line in (lines text))
   (for line = (trim-whitespace raw-line))
   (cond ((blankp line))
         ((string^= line "// natural language")
          (setf state :natlang))
         ((string^= line "// example")
          (setf state :examples))
         ((string^= line "// gt")
          (setf state :gt))
         ((string^= line "//")
          (error "Invalid comment line: ~a" line))
         (t (ecase-of benchmark-state state
              (:start (error "Invalid line: ~a" line))
              (:natlang
               (collect line into natlang))
              (:examples
               (ematch line
                 ((ppcre "^\"(.*)?\",\\+" example)
                  (collect example into positive-examples))
                 ((ppcre "^\"(.*)?\",\\-" example)
                  (collect example into negative-examples))))
              (:gt (finish)))))
   (finally
    (return
      (make 'benchmark
            :natlang (string-join natlang " ")
            :+ positive-examples
            :- negative-examples)))))

(defun parse-benchmark/loose (text)
  (mvlet* ((lines (remove-if #'emptyp (mapcar #'trim-whitespace (lines text))))
           (positive-examples lines
            (partition {string^= "+"} lines))
           (negative-examples natlang
            (partition {string^= "-"} lines)))
    (make 'benchmark
          :natlang (string-join natlang " ")
          :positive-examples (mapcar (op (drop 1 _)) positive-examples)
          :negative-examples (mapcar (op (drop 1 _)) negative-examples))))

(defmethod benchmark-prompt ((self benchmark))
  (with-slots (id natlang positive-examples negative-examples) self
    (with-output-to-string (s)
      (format s "~a~%~a~%~{~a~%+~%~}~{~a~%-~%~}~%"
              id natlang
              positive-examples
              negative-examples))))

(defmethod drive ((muse regel) (benchmark benchmark))
  (drive muse (benchmark-prompt benchmark)))

(defmethod drive ((muse regel) (prompt string))
  (let* ((proc
          (uiop:launch-program
           (if (dockerized?)
               (muse-program muse)
               `("docker" "run" "--rm" "-i"
                          "--cap-drop" "ALL"
                          "--network=none"
                          ,(string+ argot-registry "regel")
                          "python3" "interactive.py"))
           :output :stream
           :input :stream
           :error-output nil))
         (input (process-info-input proc))
         (output (process-info-output proc)))
    (unwind-protect
         (progn
           ;; Send the benchmark.
           (write-string prompt input)
           (force-output input)
           ;; Read and return the generated regexes.
           (iter (i:with after-regexes = nil)
                 (for raw-line = (read-line output nil nil))
                 (while raw-line)
                 (for line = (trim-whitespace raw-line))
                 (when (and (emptyp line) after-regexes)
                   (finish))
                 (when (scan "^\\[\\d\\] " line)
                   (setf after-regexes t)
                   (collect line into regexes))
                 (finally
                  ;; TODO If none are acceptable we can report that
                  ;; back and Regel will ask for more examples.
                  (return
                    (iter (for regex in regexes)
                          (match regex
                            ((ppcre "^\\[\\d\\] (.*)" regex)
                             (collect regex))))))))
      (terminate-process proc))))
