(defpackage :argot-server/muses/trinity
  (:use #:gt/full
        #:argot-server/muses/base
        #:argot-server/muses/code-actor
        #:argot-server/muses/jsonrpc-muse)
  (:local-nicknames (:ar :fresnel/fresnel)
                    (:l :fresnel/lens)
                    (:ty :argot-server/lens/tyrell))
  (:import-from :jsonrpc/transport/http)
  (:export #:trinity))
(in-package :argot-server/muses/trinity)
(in-readtable :curry-compose-reader-macros)

(defclass trinity (code-actor port-muse jsonrpc-muse)
  ()
  (:documentation
   "Code actor for the Trinity Muse via translation through Argot Core.")
  (:default-initargs
   :name "Trinity"
   :lang '(or javascript python)
   :force-two-round-code-actions t
   :port 5000))

(defmethod make-jsonrpc-muse-client ((muse trinity))
  (jsonrpc:make-client
           :parser (lambda (string)
                     (let ((yason:*parse-json-arrays-as-vectors* t)
                           (yason:*parse-json-booleans-as-symbols* t))
                       (yason:parse string)))))

(defparameter pattern
  (create-scanner "^\\s*(\\S*)\\s*(-?\\d+),\\s*(-?\\d+)\\s*->\\s*(-?\\d+)$"))

(defmethod applicable-commands ((muse trinity) &key start end text-document)
  (let* ((lang-sym (guess-language (point-buffer start)))
         (comment (case lang-sym (javascript "//") (python "#"))))
    (with-text (text start end)
      (let ((cases
              (mapcar (lambda (line)
                        (register-groups-bind (start x y out) (pattern line)
                          (when (equal? comment start)
                            (destructuring-bind (x y out)
                                (mapcar #'parse-integer (list x y out))
                              (dict "input" (list x y)
                                    "output" out)))))
                      (lines text))))
        (when (and cases (notany #'null cases))
          (list (make-instance '|Command|
                  :|title|
                  (format nil
                          "Synthesize math expression from ~a test case~:p"
                          (length cases))
                  :|command| (string :argot.trinity)
                  :|arguments|
                  (list text-document
                        (point-to-position end)
                        (dict "language" (symbol-name lang-sym)
                              "method" "toy")
                        (dict "test" cases)))))))))

(defgeneric yason-to-fset (json)
  (:method ((json t)) json)
  (:method ((json (eql 'yason:true))) t)
  (:method ((json (eql 'yason:false))) :false)
  (:method ((json string)) json)        ; because strings are vectors
  (:method ((json vector)) (mapcar #'yason-to-fset (convert 'seq json)))
  (:method ((json hash-table))
    (convert 'fset:map
             (mapcar «cons #'car [#'yason-to-fset #'cdr]»
                     (convert 'list (convert 'fset:map json))))))

(defmethod muse-commands append ((muse trinity))
  (list :argot.trinity))

(defmethod apply-command ((muse trinity) (cmd (eql :argot.trinity)) arguments)
  (assert (muse-client muse) () "Not connected: ~a" muse)
  (make-instance '|ApplyWorkspaceEditParams|
    :|label| "trinity replace"
    :|edit|
    (make-instance '|WorkspaceEdit|
      :|documentChanges|
      (destructuring-bind (doc pos meta params) arguments
        (list (make-instance '|TextDocumentEdit|
                :|textDocument|
                ;; NB VS Code requires a version.
                (make '|OptionalVersionedTextDocumentIdentifier|
                      :uri (gethash "uri" doc)
                      :version nil)
                :|edits|
                (list (make-instance '|TextEdit|
                        :|range|
                        (let ((pos (convert '|Position| pos)))
                          (make-instance '|Range| :|start| pos :|end| pos))
                        :|newText|
                        (or (when-let* ((string
                                         (gethash "code"
                                                  (jsonrpc:call
                                                   (muse-client muse)
                                                   (gethash "method" meta)
                                                   params)))
                                        (dsl
                                         (l:get (ty:parser) string))
                                        (ast
                                         (l:create (ty:python-translator)
                                                   dsl)))
                              (source-text ast))
                            "")))))))))
