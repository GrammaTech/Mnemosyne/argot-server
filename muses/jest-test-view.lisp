(defpackage :argot-server/muses/jest-test-view
  (:use :gt/full
        :argot-server/readtable
        :argot-server/ast-utils
        :argot-server/muses/base
        :argot-server/muses/code-actor
        :argot-server/muses/test-view
        :argot-server/utility/jest
        :lsp-server)
  (:import-from :software-evolution-library
                :from-file)
  (:export :jest-test-view))
(in-package :argot-server/muses/jest-test-view)
(in-readtable argot-readtable)

(defconst +muse-name+ "Jest Test View")

(defclass jest-test-view (test-view)
  ()
  (:documentation "A view that extracts jest tests from files and makes
  them available as annotations.")
  (:default-initargs
   :force-two-round-code-actions t
   :name +muse-name+
   :framework (make 'jest :name +muse-name+)
   :lang '(or javascript typescript)))

(defmethod update-extracted-tests :around ((muse jest-test-view) uri &key)
  (labels ((get-possible-pathnames (uri)
             "Return a list of possible pathnames for package.json files."
             (iter
               (for directory in (maplist #'identity
                                          (reverse (pathname-directory uri))))
               (collect
                   (reduce (lambda (constructed-path path-part)
                             (if (stringp path-part)
                                 (string+ path-part constructed-path)
                                 constructed-path))
                           directory
                           :initial-value "package.json"))))
           (get-package-files (uri)
             "Return all potential package.json files for URI."
             (iter
               (for possible-file in (get-possible-pathnames uri))
               (ignore-errors
                (collect (from-string 'json (get-file-contents possible-file)))))))
    (if-let ((package-files (get-package-files uri)))
      (when (find-if #'has-jest-dependency? package-files)
        (call-next-method))
      (call-next-method))))
