(defpackage :argot-server/muses/spec-map
  (:use :gt/full
        :argot-server/readtable
        :argot-server/muses/base
        :argot-server/muses/code-actor
        :argot-server/choices
        :argot-server/project-root
        :argot-server/utility
        :argot-server/lsp-diff
        :spec-map/smm
        :lsp-server/protocol
        :lsp-server.lem-base
        :software-evolution-library/components/configuration
        :py-configparser)
  (:shadow :add-hook :run-hooks :remove-hook)
  (:shadowing-import-from :serapeum :~> :lines)
  (:local-nicknames (:choice :argot-server/choices))
  (:import-from :software-evolution-library/command-line
                :handle-comma-delimited-argument)
  (:import-from :software-evolution-library/software/project
                :evolve-files)
  (:import-from :software-evolution-library/software/c-project
                :c-project)
  (:import-from :software-evolution-library/software/cpp-project
                :cpp-project)
  (:import-from :argot-server/forward-client-request
                :make-forwarded-client-request
                :forward-client-request)
  (:import-from :lsp-server
                :text-document-version)
  (:import-from :spec-map/smm
                :pathname-relative
                :original-path
                :m-software
                :m-ast
                :lines-of-match
                :m-score
                :expr-filter
                :stmt-filter
                :decl-filter
                :typedecl-filter)
  (:import-from :functional-trees/attrs
                :with-attr-table
                :*attrs*)
  (:export
   :spec-map)
  (:documentation "Define a code actor that provides functionality for SPEC-MAP."))
(in-package :argot-server/muses/spec-map)
(in-readtable argot-readtable)

;;; TODO: 1. make all of the file logic work when not on the same system.
;;;          The current method for a project snapshot takes about 2 minutes for
;;;          magma. It may make sense to improve this when this happens.
;;;       2. begin work on table matching.
;;;       3. work on persistent matches.

(defclass spec-map (code-actor)
  ()
  (:documentation "Integrate spec-map as a code actor."))

(define-class-method "initialize" spec-map (params)
  (let* ((result (call-next-method))
         (capabilities (slot-value result '|capabilities|)))
    (setf (slot-value capabilities '|referencesProvider|) t
          (slot-value capabilities '|hoverProvider|) t)
    result))

(defmethod muse-code-action-kinds append ((spec-map spec-map))
  (list "CodeActionKind.SpecMapConfigureRootDir"
        "CodeActionKind.SpecMapConfigureMaxMatches"
        "CodeActionKind.SpecMapConfigureFilter"
        "CodeActionKind.SpecMapConfigureThreshold"
        "CodeActionKind.SpecMapMatch"
        "CodeActionKind.SpecMapStructuredMatch"
        "CodeActionKind.SpecMapGenerateQueryFile"
        "CodeActionKind.SpecMapQueryFileMatch"))

(defmethod muse-commands append ((spec-map spec-map))
  (list :argot.spec-map-configure-root-dir
        :argot.spec-map-configure-max-matches
        :argot.spec-map-configure-filter
        :argot.spec-map-configure-threshold
        :argot.spec-map-match-query
        :argot.spec-map-structured-match-query
        :argot.spec-map-generate-query-file
        :argot.spec-map-query-file-query))


;;; TODO: move the special variables into the class definition as slots.
;;;       This may no longer be desirable with the move to query files.

;;; TODO: this won't work with multiple files across different projects.
;;;       This likely doesn't matter for demonstration purposes.
(defparameter *project-root-directory* "/magma/"
  "The root directory of the project.")

(defparameter *persistent-project* nil
  "Persistent project for queries.")

(defparameter *persistent-attr-table* nil
  "Persistent attribute table for queries.")

(defparameter *max-matches* 10
  "Maximum number of matches for smm.")

(defparameter *filter* #'expr-filter
  "Filter for match.")

(defparameter *threshold* 0.0f0)

(defparameter *matches* nil
  "The matches to be returned as references.")

(defmethod applicable-commands ((spec-map spec-map)
                                &key start end text-document)
  "Return a list of applicable commands (mutations)."
  (declare (ignorable start end text-document))
  (remove
   nil
   (list (make '|Command|
               :title "spec-map: Set Project Root Directory"
               :command (string :argot.spec-map-configure-root-dir)
               :arguments (list text-document))
         (make '|Command|
               :title "spec-map: Configure Maximum Matches"
               :command (string :argot.spec-map-configure-max-matches)
               :arguments (list text-document))
         (make '|Command|
               :title "spec-map: Configure Match Filter"
               :command (string :argot.spec-map-configure-filter)
               :arguments (list text-document))
         (make '|Command|
               :title "spec-map: Configure Threshold"
               :command (string :argot.spec-map-configure-threshold)
               :arguments (list text-document))
         (make '|Command|
               :title "spec-map: Generate Query File"
               :command (string :argot.spec-map-generate-query-file)
               :arguments (list text-document))
         (when *persistent-project*
           (make '|Command|
                 :title "spec-map: Match Pattern"
                 :command (string :argot.spec-map-match-query)
                 :arguments (list text-document)))
         (when *persistent-project*
           (make '|Command|
                 :title "spec-map: Structured Match"
                 :command (string :argot.spec-map-structured-match-query)
                 :arguments (list text-document)))
         (when *persistent-project*
           (make '|Command|
                 :title "spec-map: Query File Match"
                 :command (string :argot.spec-map-query-file-query)
                 :arguments (list text-document))))))

(defmethod apply-command ((spec-map spec-map)
                          (cmd (eql :argot.spec-map-configure-root-dir))
                          arguments
                          &aux (text-document (car arguments)))
  (set-root-directory (gethash "uri" text-document))
  nil)

(defmethod apply-command ((spec-map spec-map)
                          (cmd (eql :argot.spec-map-configure-max-matches))
                          arguments
                          &aux (text-document (car arguments)))
  (set-max-matches (gethash "uri" text-document))
  nil)

(defmethod apply-command ((spec-map spec-map)
                          (cmd (eql :argot.spec-map-configure-filter))
                          arguments)
  (set-filter)
  nil)

(defmethod apply-command ((spec-map spec-map)
                          (cmd (eql :argot.spec-map-configure-threshold))
                          arguments)
  (set-threshold)
  nil)

(defmethod apply-command ((spec-map spec-map)
                          (cmd (eql :argot.spec-map-generate-query-file))
                          arguments)
  (generate-query-template-file)
  nil)

;; TODO: improve error message for unfound files
;; TODO: write documentation on how this muse is used.

(defmethod apply-command ((spec-map spec-map)
                          (cmd (eql :argot.spec-map-match-query))
                          arguments
                          &aux (uri (gethash "uri" (car arguments))))
  (let ((pattern (present-choice-for
                   uri (list "Spec Map Match Query")
                   :prompt "Provide Query for Spec Map Match (smm)"
                   ;; TODO: change default to "".
                   :default-text "support an attach type for emergency bearer services"
                   :class 'open-choice)))
    (perform-interactive-match uri (list pattern))))

(defmethod apply-command ((spec-map spec-map)
                          (cmd (eql :argot.spec-map-structured-match-query))
                          arguments
                          &aux (uri (gethash "uri" (car arguments))))
  (let* ((pattern
           (present-choice-for
            uri (list "Spec Map Structured Match Query")
            :prompt "Provide Structured Query for Spec Map Match (smm)"
            ;; TODO: change default to "".
            :default-text
            #.(string+
               "(:t \"does not support attach emergency bearer services\""
               " \"attach type EPS emergency attach\" :depth-limit 4)")
            :class 'open-choice))
         (structured-pattern
           (let ((*read-eval* nil)
                 (*package* (find-package :sel/sw/ts)))
             (read-from-string pattern))))
    (perform-interactive-match uri (list structured-pattern))))

(defmethod apply-command ((spec-map spec-map)
                          (cmd (eql :argot.spec-map-query-file-query))
                          arguments
                          &aux (uri (gethash "uri" (car arguments))))
  (perform-interactive-match-from-config uri))

(define-class-method "textDocument/references" spec-map (params |ReferenceParams|)
  (labels ((match-to-location (match)
             (let ((path (original-path (m-software match))))
               (make 'Location
                     :uri (string+ "file://" path)
                     :range (convert 'Range (ast-source-range (m-software match)
                                                              (m-ast match))))))
           (matches-to-locations(matches)
             (map 'list #'match-to-location matches)))
    (mapcar
     (op (convert 'hash-table _))
     (iter
       (for (nil matches) in-hashtable *matches*)
       (appending (matches-to-locations matches))))))

(define-class-method "textDocument/hover" spec-map
    (params HoverParams)
  "Display score if possible."
  ;; TODO: the file path logic will need to be converted to URIs
  ;;       at some point. It will likely require maintaining a root directory
  ;;       for the server and for the client.
  ;; TODO: it may make sense to make source-range an attr method.
  (labels ((cursor-in-match-region-p (location match)
             "Return 0 if LOCATION is in MATCH's region.
                     1 if LOCATION occurs before MATCH's region.
                    -1 if LOCATION occurs after MATCH's Region"
             (let ((match-region (ast-source-range (m-software match)
                                                   (m-ast match))))
               (econd
                 ((contains match-region location) 0)
                 ((sel/utility/range:source-< location (begin match-region))
                  1)
                 ((sel/utility/range:source-> location (end match-region))
                  -1))))
           (get-path-matches (original-path)
             "Get all matches that occured in ORIGINAL-PATH."
             ;; TODO: find better way to do this. Possibly add args to
             ;;       smm to allow for different keys?
             (iter
               (for (key value) in-hashtable *matches*)
               (when (equal original-path
                            (namestring
                             (pathname-relative *project-root-directory*
                                                (original-path key))))
                 (return value))))
           (in-match-region-p (uri location)
             "If LOCATION occurs in a match region, return the corresponding
              match region."
             (when-let* ((original-path
                          (first (nth-value 1 (scan-to-strings
                                               (format nil "file://~a(.*)"
                                                       *project-root-directory*)
                                               uri))))
                         (matches (get-path-matches original-path))
                         (match-index
                          (binary-search
                           location matches
                           :test {cursor-in-match-region-p location})))
               (aref matches match-index)))
           (match->hover (match)
             (make 'Hover
                   :contents (format nil "Score: ~a" (m-score match))
                   :range (convert 'Range
                                   (ast-source-range (m-software match)
                                                     (m-ast match))))))
    (ematch params
      ((HoverParams
        :textDocument (TextDocumentIdentifier :uri uri)
        :position position)
       (when-let ((match (in-match-region-p
                          uri
                          (convert 'source-location position))))

         (match->hover match))))))


;;; Utility

(defun set-root-directory (uri)
  "Set *project-root-directory* to be a string received from the client that
represents the root directory of the project."
  (labels ((language->project (language)
             (string-case (string-upcase language)
               ("c++" 'cpp-project)
               (otherwise 'c-project)))
           (canonicalize-root-directory (uri)
             "Add a trailing slash to the end of URI if one doesn't exist."
             ;; NOTE: there may be some ambiguity in regards to whether something
             ;;       is a directory without the trailing slash.
             (if (eql #\/ (fset:last uri))
                 uri
                 (string+ uri #\/))))
    (let* ((root-directory (present-choice-for
                            uri (list "Set Project Root Directory")
                            :prompt "Set Project Root Directory"
                            :default-text (file-uri-path (project-root-for uri))
                            :class 'open-choice))
           (language (present-choices
                      (list "c" "c++")
                      :prompt "Choose Language of Project"
                      :default-text "c")))
      ;; TODO: verify and send message if doesn't exist? The best way to do this
      ;;       may be to assume that the URI is somewhere in the root directory
      ;;       and validate that the root directory is a substring of it.
      (setf *project-root-directory* (canonicalize-root-directory root-directory)
            *persistent-project* (from-file (language->project language)
                                            root-directory)
            *persistent-attr-table* (with-attr-table *persistent-project*
                                      *attrs*)))))

(defun set-max-matches (uri)
  "Set *max-matches* to be a number received from the client as a string."
  (let ((max-matches-string (present-choice-for
                             uri (list "Set Max Matches")
                             :prompt "Set Max Matches"
                             :default-text (format nil "~d" *max-matches*)
                             :class 'open-choice)))
    (setf *max-matches* (parse-integer max-matches-string))))

(defun set-filter ()
  "Set *filter* to be a number received from the client as a string."
  (let ((filter-string (present-choices
                        (list "Expressions" "Statements"
                              "Declarations" "Type Declarations"
                              "No Filter")
                      :prompt "Choose a filter for matches"
                      :default-text "Expressions")))
    (setf *filter*
          (string-ecase filter-string
            ("Expressions" #'expr-filter)
            ("Statements" #'stmt-filter)
            ("Declarations" #'decl-filter)
            ("Type  Declarations" #'typedecl-filter)
            ("No Filter" (constantly t))))))

(defun set-threshold ()
  "Set *filter* to be a number received from the client as a string."
  (let ((threshold-string (present-choices
                           nil
                           :open t
                           :prompt "Set Threshold"
                           :default-text "0.0")))
    (setf *threshold* (parse-float threshold-string))))

(defun perform-match (uri patterns &key target-files)
  "Perform a match using URI and PATTERNS."
  (labels ((present-file-obj->matches (file-obj->matches)
             (maphash (lambda (key value)
                        (setf (gethash key file-obj->matches)
                              (apply #'vector value)))
                      file-obj->matches)
             (setf *matches* file-obj->matches)
             ;; NOTE: this is handled in textDocument/references
             (forward-client-request
              uri
              (make-forwarded-client-request
               :textDocument/references
               :uri uri)))
           (get-file-object (name)
             (or (aget name (evolve-files *persistent-project*)
                       :test #'equal)
                 (error (fmt "~a not found in the project directory ~a"
                             name *project-root-directory*))))
           (get-matches (patterns target-files)
             (with-attr-table *persistent-attr-table*
               (if target-files
                   (smm*
                    patterns
                    (mapcar
                     #'get-file-object
                     target-files)
                    :root-dir *project-root-directory*
                    :threshold *threshold*
                    :max-matches *max-matches*
                    :filter *filter*
                    :sort-matches t)
                   (smm*
                    patterns
                    nil
                    :obj *persistent-project*
                    :threshold *threshold*
                    :max-matches *max-matches*
                    :filter *filter*
                    :sort-matches t)))))
    (let ((file-obj->matches (get-matches patterns target-files)))
      (present-file-obj->matches file-obj->matches)
      nil)))

(defun perform-interactive-match (uri patterns)
  (let ((target-files
          (present-choice-for
           uri (list "Spec Map Match Files")
           :prompt "Comma Separated Target Files for Query (Optional)"
           ;; TODO: change default to "" or the currently opened file.
           :default-text
           #.(string+
              "lte/gateway/c/core/oai/tasks/nas/emm/Attach.c,"
              "lte/gateway/c/core/oai/tasks/nas/emm/Detach.c")
           :class 'open-choice)))
    (perform-match uri patterns
                   :target-files
                   (unless (emptyp target-files)
                     (handle-comma-delimited-argument target-files)))
    nil))

(define-condition root-directory-not-set (error)
  ()
  (:report (lambda (c s)
             (declare (ignorable c))
             (format s "Project Root Directory has not been set."))))

(defun generate-query-template-file (&aux (file-name "spec-map.ini"))
  "Generate an INI file for PROJECT that can be used to pre-populate searches."
  ;; TODO: make a global parameters section for defaults across the file?
  ;; TODO: add some output to indicate that the action has completed.
  (labels ((write-config-template (path &aux (config-template "
# Multiple Queries can be stored in the same file under different section names.
[Sample Query]
max-matches = 10
# Filter Types: Expressions, Statements, Declarations, Type Declarations, No
filter = Expressions
threshold = 0.0
# Optional, comma separated
# target-files = lte/gateway/c/core/oai/tasks/nas/emm/Attach.c,lte/gateway/c/core/oai/tasks/nas/emm/Detach.c
# Only one of these patterns should be provided.
# match-pattern = support an attach type for emergency bearer services
# structured-pattern = (:t \"does not support attach emergency bearer services\" \"attach type EPS emergency attach\" :depth-limit 4)
"))
             (with-output-to-file (file path)
               (format file "~a" config-template))))
    (let ((path (string+ *project-root-directory* file-name)))
      (cond
        ((not *project-root-directory*)
         (error 'root-directory-not-set))
        ((probe-file path)
         (error "File already exists"))
        (t
         (write-config-template path)
         (message "~a saved in ~a" file-name *project-root-directory*))))))

(defun match-with-input-from-config (uri config section-name)
  "Perform a search with input from FILE-NAME."
  (labels ((verify-pattern-input (match-pattern structured-pattern)
             "Verify that only one pattern input is provided."
             (mvlet ((valid? neither? (xor match-pattern structured-pattern)))
               (unless valid?
                 (cond
                   (neither?
                    (error
                     #.(string+ "One of match-pattern or"
                                " structured-pattern should be provided")))
                   (t
                    (error
                     #.(string+ "Either match-pattern or structured-pattern"
                                " should be provided to perform a match.")))))))
           (parse-max-matches (config section-name)
             (if-let ((max-matches (option config section-name "max-matches")))
               (parse-integer max-matches)
               *max-matches*))
           (parse-filter (config section-name)
             (if-let ((filter (option config section-name "filter")))
               (string-ecase filter
                 ("Expressions" #'expr-filter)
                 ("Statements" #'stmt-filter)
                 ("Declarations" #'decl-filter)
                 ("Type Declarations" #'typedecl-filter)
                 ("No" (constantly t)))
               *filter*))
           (parse-threshold (config section-name)
             (if-let ((max-matches (option config section-name "threshold")))
               (parse-float max-matches)
               *threshold*))
           (parse-target-files (config section-name)
             (when-let ((target-files
                         (option config section-name "target-files")))
               (handle-comma-delimited-argument target-files)))
           (parse-match-pattern (config section-name)
             (option config section-name "match-pattern"))
           (parse-structured-pattern (config section-name)
             (when-let ((structured-pattern
                         (option config section-name "structured-pattern")))
               (with-input-from-string (string-stream structured-pattern)
                 (let ((*read-eval* nil)
                       (*package* (find-package :sel/sw/ts)))
                   (read string-stream))))))
    (let ((*max-matches* (parse-max-matches config section-name))
          (*filter* (parse-filter config section-name))
          (*threshold* (parse-threshold config section-name))
          (target-files (parse-target-files config section-name))
          (match-pattern (parse-match-pattern config section-name))
          (structured-pattern (parse-structured-pattern config section-name)))
      (verify-pattern-input match-pattern structured-pattern)
      (perform-match uri (list (or structured-pattern match-pattern))
                     :target-files target-files))))

(defun perform-interactive-match-from-config (uri)
  "Perform an interactive match from the relevant config file."
  (labels ((get-config ()
             (let ((configuration-path
                     (make-pathname :directory *project-root-directory*
                                    :name "spec-map" :type "ini"))
                   (config (make-config)))
               (if (probe-file configuration-path)
                   (read-files config (list configuration-path))
                   config)))
           (choose-query (config)
             "Choose a predefined search to perform from CONFIG."
             ;; NOTE: each section is considered a different search.
             (let ((sections (sections config)))
               (unless sections
                 (error "No available queries defined in spec-map.ini"))
               (present-choices
                sections
                :prompt "Choose an Available Query"))))
    (unless *project-root-directory*
      (error 'root-directory-not-set))
    (let* ((config (get-config))
           (section-name (choose-query config)))
      (match-with-input-from-config uri config section-name))))
