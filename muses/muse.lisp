(defpackage :argot-server/muses/muse
  (:use :gt/full
        :lsp-server
        :lsp-server/protocol
        :argot
        :argot-server/readtable)
  (:shadow :name)
  (:import-from :argot-server/utility
                :random-port
                :muse-server
                :service-host)
  (:import-from :lsp-server/utilities/lsp-utilities
                :show-message)
  (:import-from :jsonrpc :dispatch)
  (:import-from :jsonrpc/connection
                :*connection*)
  (:import-from :argot-server/merge-results
                :merge-server-capabilities
                :merge-results)
  (:documentation "Base class and generic functions for Muses.")
  (:export
    :muse-start
    :muse-started-p
    :muse-stop
    :muse-lang
    :muse-supports?
    :muse-available?
    :muse-enabled?
    :muse-disabled?
    :muse
    :muse-name
    :muse-server
    :muse-port
    :port-muse
    :muse-hung?
    :muse-speaks-argot?
    :reinitialize-muse
    :muse-timed-out
    :with-muse-blocked
    :muse-host
    :muse-client
    :on-change
    :on-annotate
    :muse-worker-name
    :muse-commands
    :muse-accepts-command-p
    :contribute-server-capabilities))
(in-package :argot-server/muses/muse)
(in-readtable argot-readtable)

(defgeneric muse-start (muse)
  (:method-combination standard/context)
  (:method :context (muse)
    (unless (muse-started-p muse)
      (synchronized (muse)
        (unless (muse-started-p muse)
          (call-next-method))
        (assert (muse-started-p muse)))))
  (:method ((muse t)) nil)
  (:documentation "Start a muse.

Note that this is called in a critical section, so you do not have to
worry about concurrency.

By default this does nothing."))

(defgeneric muse-started-p (muse)
  (:method-combination and)
  (:method and ((muse t)) t)
  (:documentation "Boolean to test if a muse is running.

Since not all muses will have state, the default method always returns
true."))

(defgeneric muse-stop (muse &key)
  ;; Default is to do nothing.
  (:method-combination progn)
  (:method :around (muse &key)
    ;; Note that there is no assertion, since some muses are stateless
    ;; and in that sense always running, in which case this is a no-op.
    (when (muse-started-p muse)
      (synchronized (muse)
        (when (muse-started-p muse)
          (call-next-method)))))
  (:method progn ((muse t) &key) nil)
  (:documentation "Stop a muse. Cf. `muse-start'."))

(defgeneric muse-lang (muse)
  (:method-combination standard/context)
  (:method ((muse t)) t)
  (:documentation "Language supported by MUSE.

Unless a muse specializes this function, it is assumed to support all
languages."))

(defgeneric muse-supports? (muse document)
  (:method-combination standard/context)
  (:documentation "Does MUSE support this document?
By default this is based on the document's declared language.")
  (:method ((muse t) (document null))
    t))

(defclass muse (lsp-server)
  ((server :initarg :server
           :documentation "The Argot server this muse belongs to."
           :type lsp-server
           :accessor muse-server)
   (lang :initarg :lang :initarg :language :reader muse-lang)
   (name :initarg :name :initarg :muse-name :type string)
   (hung :type boolean :initform nil :accessor muse-hung?)
   (speaks-argot :type boolean
                 :initarg :speaks-argot
                 :accessor muse-speaks-argot?))
  (:documentation "The base class for muses.")
  (:default-initargs
   :speaks-argot nil
   :lang t
   :server *server*))

(defmethod print-object ((self muse) stream)
  (print-unreadable-object (self stream :type t)
    (without-recursion ()
      (format stream "~a #~a~@[ (running)~]"
              (muse-name self)
              (server-id self)
              (muse-started-p self)))))

(defgeneric muse-name (muse)
  (:method ((muse muse))
    (or (slot-value-safe muse 'name)
        (string-capitalize
         (substitute #\Space #\-
                     (string (class-name-of muse)))))))

(defgeneric muse-host (muse)
  (:method ((muse muse))
    (service-host (muse-name muse))))

(defgeneric muse-client (muse)
  (:documentation "The JSONRPC client for a muse."))

(defgeneric muse-available? (muse)
  (:method-combination and)
  (:documentation "Can MUSE be run? Are its requirements met?
Defaults to true.")
  (:method and ((muse t)) t)
  (:method and ((class symbol))
    (muse-available? (make class))))

(defun muse-enabled? (muse params)
  (not (muse-disabled? muse params)))

(defgeneric muse-disabled? (muse init-params)
  (:method-combination or)
  (:method :around ((muse t) (params null))
    nil)
  (:method or ((muse t) params) nil)
  (:method or ((class symbol) params)
    (muse-disabled? (make class) params))
  (:documentation "Is MUSE disabled by INIT-PARAMS?

Unlike `muse-available?', this is run after initialization."))

(defgeneric muse-commands (muse)
  (:method-combination append)
  (:method append ((muse muse)) nil)
  (:documentation "A list of command names supported by the muse."))

(defgeneric muse-accepts-command-p (muse command)
  (:documentation "Does MUSE accept this command?")
  (:method ((muse t) (command t))
    t)
  (:method ((muse muse) (command string))
    (member command (muse-commands muse) :test #'string-equal))
  (:method ((muse muse) (params hash-table))
    (muse-accepts-command-p muse (gethash "command" params)))
  (:method ((muse muse) (params Command))
    (muse-accepts-command-p muse (slot-value-safe params '|command|))))

(defclass port-muse ()
  ((port :initarg :port :reader muse-port))
  (:default-initargs
   :port (random-port))
  (:documentation "Mixin for a muse that listens on a particular port.
By default sets the port slot to a random port."))

(defgeneric reinitialize-muse (muse &key initialize-params)
  (:documentation "Prime MUSE for further queries by passing INITIALIZE-PARAMS.

This is called reinitialization since it is intended to be used when a
muse hangs and has to be restarted; it must be reinitialized before it
can actually be used.")
  (:method-combination standard/context))

(defgeneric muse-timed-out (muse callback &key initialize-params)
  (:documentation "Notify MUSE that it is has been marked as hung and
  provide it with a CALLBACK it can invoke to notify Argot Server it
  is ready to receive requests again.")
  (:method (muse callback &key initialize-params)
    (declare (ignore muse callback initialize-params))))

(defgeneric on-change (muse uri &key &allow-other-keys)
  (:method-combination progn)
  (:documentation "Let MUSE handle a change to URI.

Note that if handling the change results in further requests it should
be run in another thread.")
  (:method progn ((muse muse) uri &key)))

(defmethod handle :after
    ((muse muse)
     (method (eql :textDocument/didOpen))
     (params DidOpenTextDocumentParams))
  (ematch params
    ((DidOpenTextDocumentParams
      :textDocument (and document
                         (TextDocumentItem
                          :uri uri)))
     (on-change muse uri :document document))))

(defmethod handle :after
    ((muse muse)
     (method (eql :textDocument/didChange))
     (params DidChangeTextDocumentParams))
  (ematch params
    ((DidChangeTextDocumentParams
      :textDocument (and document
                         (VersionedTextDocumentIdentifier
                          :version version
                          :uri uri)))
     (on-change muse uri :document document :version version))))

(defgeneric contribute-server-capabilities (muse)
  (:documentation "Contribute to server capabilities with a ServerCapabilities instance,
or a hash table, or nil.")
  (:method-combination list)
  (:method list ((muse muse))
    nil))

(defmethod handle
    ((muse muse)
     (method (eql :|initialize|))
     (params InitializeParams))
  (let ((results (remove nil (contribute-server-capabilities muse))))
    (assert (every (of-type '(or hash-table ServerCapabilities)) results))
    (make 'InitializeResult
          :capabilities
          (merge-server-capabilities (contribute-server-capabilities muse)))))

;;; TODO Eventually, if annotations become persistent, then we want to
;;; send them out to muses when the server restarts.

(defgeneric on-annotate (muse uri annotations &key &allow-other-keys)
  (:method-combination progn)
  (:documentation "Let MUSE know URI has updated annotations.")
  (:method progn ((muse muse) uri annotations &key)))

(define-class-method #.+argot/didAnnotate+ t
    (params DidAnnotateParams))

(defmethod handle :after
    ((muse muse)
     (method (eql #.(make-keyword +argot/didAnnotate+)))
     (params DidAnnotateParams))
  (ematch params
    ((DidAnnotateParams
      :annotations annotations)
     (flet ((annotation-uri (annotation)
              (ematch annotation
                ((Annotation
                  :text-document (TextDocumentIdentifier :uri uri))
                 uri))))
       (dolist (annotations (assort annotations
                                    :key #'annotation-uri
                                    :test #'equal))
         (on-annotate muse
                      (annotation-uri (first annotations))
                      annotations))))))


;;; Muse workers
(defgeneric muse-worker-name (muse)
  (:method ((muse muse))
    (fmt "~a worker thread" (muse-name muse))))
