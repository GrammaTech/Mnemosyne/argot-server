(defpackage :argot-server/muses/jsonrpc-muse
  (:use :gt/full
        :argot-server/muses/base
        :argot-server/muses/http-muse)
  (:import-from :jsonrpc
                :client-disconnect)
  (:export :jsonrpc-muse :make-jsonrpc-muse-client))
(in-package :argot-server/muses/jsonrpc-muse)

(defclass jsonrpc-muse (http-muse)
  ()
  (:documentation
   "Mixin for muses that generic JSON-RPC (not LSP) over HTTP."))

(defgeneric make-jsonrpc-muse-client (muse)
  (:documentation "Make a JSON-RPC client for MUSE.
By default calls `jsonrpc:make-client'.")
  (:method ((muse jsonrpc-muse))
    (jsonrpc:make-client)))

(defmethod muse-start ((muse jsonrpc-muse))
  (with-accessors ((client muse-client)) muse
    (setf client (jsonrpc:make-client))
    (client-connect/retries client :url (fmt "http://~a:~a"
                                             (muse-host muse)
                                             (muse-port muse))
                                   :mode :http)
    client))

(defmethod muse-stop progn ((muse jsonrpc-muse) &key)
  (with-accessors ((client muse-client)) muse
    (client-disconnect (shiftf client nil))))
