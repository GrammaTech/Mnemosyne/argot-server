(uiop:define-package :argot-server/muses/base
    (:use-reexport :argot-server/passthrough
                   :argot-server/muses/passthrough
                   :argot-server/utility
                   :argot-server/muses/muse
                   :argot-server/muses/cli-muses
                   :lsp-server.lem-base
                   :lsp-server/protocol
                   :lsp-server/protocol-util
                   :lsp-server/lsp-server
                   :lsp-server/utilities/lsp-utilities
                   :software-evolution-library/software/c
                   :software-evolution-library/software/cpp
                   :software-evolution-library/software/javascript
                   :software-evolution-library/software/typescript
                   :software-evolution-library/software/lisp
                   :software-evolution-library/software/python
                   :software-evolution-library/software/tree-sitter
                   :software-evolution-library/software/parseable
                   :software-evolution-library/utility/range
                   :software-evolution-library/utility/debug)
  (:import-from :software-evolution-library
                :genome
                :genome-string
                :from-string
                :from-file
                :software)
  (:export :genome :genome-string :from-string :from-file :software)
  (:import-from :jsonrpc/connection :*connection*)
  (:export :*connection*)
  (:shadowing-import-from :gt/full
                          :add-hook
                          :remove-hook
                          :run-hooks)
  (:documentation "A package containing all the symbols needed to implement
                   a muse, like LSP interfaces and the names of languages."))
