(defpackage :argot-server/muses/type-refinement
  (:use :gt/full
        :argot-server/readtable
        :argot-server/muses/base
        :argot-server/muses/jsonrpc-muse
        :argot-server/muses/code-actor
        :argot-server/ast-utils
        :argot-server/annotation-host
        :lsp-server/protocol
        :lsp-server/utilities/worker-thread
        :argot)
  (:import-from :argot-server/muses/typewriter
                :predictions->annotations
                :annotations->predictions)
  (:export :type-refinement-muse
           :refined-string-predictions))
(in-package :argot-server/muses/type-refinement)
(in-readtable argot-readtable)

(defclass type-refinement-muse (port-muse jsonrpc-muse)
  ()
  (:documentation "Muse for refining types.")
  (:default-initargs
   :name "Type Refinement"
   :port 4500
   :speaks-argot t))

(defun refined-string-predictions (muse string predictions)
  (declare (hash-table predictions)
           (string string))
  (assure string
    (gethash "prediction list"
             (jsonrpc:call (muse-client muse)
                           "run_type_refinement"
                           (dict "source" string
                                 "probs"
                                 (with-output-to-string (s)
                                   (yason:encode predictions s)))
                           :timeout (interval :hours 1)))))

(defun refined-predictions (muse string predictions)
  (assure hash-table
    (yason:parse
     (refined-string-predictions muse string predictions))))

(defmethod on-annotate progn ((muse type-refinement-muse) uri annotations &key)
  (with-lsp-worker-thread (:name (muse-worker-name muse))
    (when-let (predictions (annotations->predictions annotations))
      (message "Refining predictions...")
      (did-annotate muse
                    (predictions->annotations
                     uri
                     (refined-predictions
                      muse
                      (buffer-to-string (uri-buffer uri))
                      predictions))))))
