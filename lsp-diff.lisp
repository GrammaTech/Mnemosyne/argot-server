(defpackage :argot-server/lsp-diff
  (:use :gt/full
        :argot-server/readtable
        :lsp-server/protocol)
  (:export :lsp-edit-diff
           :lsp-edit-diff-strings
           :command-line-diff)
  (:import-from :cmd :$cmd)
  (:shadowing-import-from :serapeum
                          :lines)
  (:documentation "Compute file diffs as LSP."))
(in-package :argot-server/lsp-diff)
(in-readtable argot-readtable)

(deftype linum () '(integer 0 *))

(deftype edit ()
  '(tuple linum (integer 0 *) string))

(defunion change-command
  (add-command (line linum) (text string))
  (replace-command (from linum) (to linum) (text string))
  (delete-command (from linum) (to linum)))

(defpattern int (var)
  (with-unique-names (it)
    `(guard1 (,it :type string) (stringp ,it)
             (values (parse-integer ,it :junk-allowed t))
             (and ,var (type integer)))))

(defun parse-ed-script (script)
  (flet ((extract-text (script)
           (let* ((end (member (fmt ".") script
                               :test #'equal
                               :key #'chomp))
                  (text-lines (ldiff script end))
                  (text (string-join text-lines "")))
             (values text (rest end)))))
    (nlet parse ((script (lines script :keep-eols t))
                 (edits nil))
      (if (endp script) (nreverse edits)
          (ematch (chomp (first script))
            ;; NB To delete a line in LSP we need to delete up to the
            ;; beginning of the *next* line.
            ((ppcre "^(\\d+),(\\d+)d" (int from) (int to))
             (parse (rest script)
                    (cons (list from
                                (1+ (- to from))
                                "")
                          edits)))
            ((ppcre "^(\\d+)d$" (int from))
             (parse (rest script)
                    (cons (list from 1 "")
                          edits)))
            ;; NB `a` means to add *after* the line.
            ((ppcre "^(\\d+)a$" (int line))
             (multiple-value-bind (text script)
                 (extract-text (rest script))
               (parse script
                      (cons (list (1+ line) 0 text)
                            edits))))
            ;; NB To change we need to replace *up to* the beginning
            ;; of the next line.
            ((ppcre "^(\\d+)c$" (int line))
             (multiple-value-bind (text script)
                 (extract-text (rest script))
               (parse script
                      (cons (list line 1 text)
                            edits))))
            ;; Same for multi-line changes.
            ((ppcre "^(\\d+),(\\d+)c$" (int from) (int to))
             (multiple-value-bind (text script)
                 (extract-text (rest script))
               (parse script
                      (cons (list from (1+ (- to from)) text)
                            edits)))))))))

(defun edit->lsp-edit (edit &key (line-offset 0) (char-offset 0))
  (ematch (assure edit edit)
    ((list line-number line-count text)
     ;; NB LSP line numbers are 0-based.
     (let* ((line-number (+ (1- line-number) line-offset))
            (last-line-number (+ line-number line-count))
            (character (if (= line-number line-offset) char-offset 0))
            (last-line-character
             (if (= last-line-number line-offset) char-offset 0)))
       (make '|TextEdit|
             :|newText| text
             :range
             (make '|Range|
                   :start
                   (make '|Position|
                         :line line-number
                         :character character)
                   :end
                   (make '|Position|
                         :line last-line-number
                         :character last-line-character)))))))

(defun edits->lsp-edits (edits &key (line-offset 0) (char-offset 0))
  (mapcar (lambda (edit)
            (edit->lsp-edit edit
                            :line-offset line-offset
                            :char-offset char-offset))
          edits))

(defun command-line-diff (old-file new-file)
  "Generate an Ed script by calling `diff` on the given files."
  ($cmd "diff -e" old-file new-file :ignore-error-status t))

(defun lsp-edit-diff (old-file new-file &key (line-offset 0) (char-offset 0))
  "Compute a list of LSP TextEdit entries representing the diff between the two
files."
  ;; LSP clients (conventionally?) apply edits in reverse order, but
  ;; Ed scripts are already "reversed" (later edits come first to
  ;; avoid affecting line numbers).
  (edits->lsp-edits
   (parse-ed-script (command-line-diff old-file new-file))
   :line-offset line-offset
   :char-offset char-offset))

(defun lsp-edit-diff-strings (old-string new-string
                              &key (line-offset 0) (char-offset 0))
  "Compute a list of LSP TextEdit entries representing the diff between the two
strings."
  (nest
   (with-temporary-file-of (:pathname old-file) old-string)
   (with-temporary-file-of (:pathname new-file) new-string)
   (lsp-edit-diff old-file new-file
                  :line-offset line-offset
                  :char-offset char-offset)))
