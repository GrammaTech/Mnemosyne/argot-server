Argot Server
============

The hub of the Mnemosyne system for automated development assistance.

## Usage

Launch Argot Server with the default muses by executing `make up`.

## Abstract

The Argot Server is the technical hub of the [Mnemosyne system][] for automated software development assistance. This server mediates communication between a software developer's IDE and automated "Muses" which provide development assistance. An extended [Language Server Protocol][], called Argot, is used as the format of communication. Extensions to LSP provide for the communication of annotations which partition the intents (tests, types, prose) that may decorate program subtrees. The architecture is documented in greater detail in [Mnemosyne architecture][].

![Argot Server graphic.](etc/.argot-server.svg)

## Getting Started

For comprehensive instructions on getting Argot Server running and integrated with your editor, see the [usage instructions in the Mnemosyne documentation](https://grammatech.gitlab.io/Mnemosyne/docs/usage/).

## Copyright and Acknowledgments

Copyright (C) 2020 GrammaTech, Inc.

This code is licensed under the MIT license. See the LICENSE file in
the project root for license terms.

This material is based upon work supported by the US Air Force,
AFRL/RIKE and DARPA under Contract No. FA8750-20-C-0208.  Any
opinions, findings and conclusions or recommendations expressed in
this material are those of the author(s) and do not necessarily
reflect the views of the US Air Force, AFRL/RIKE or DARPA.

[Mnemosyne system]: https://grammatech.gitlab.io/Mnemosyne/docs/
[Language Server Protocol]: https://en.wikipedia.org/wiki/Language_Server_Protocol
[Mnemosyne architecture]: https://grammatech.gitlab.io/Mnemosyne/docs/architecture
