// from https://github.com/node-modules/utility/blob/master/lib/crypto.js
'use strict';

var crypto = require('crypto');

exports.hash = function hash(method, s, format) {
  var sum = crypto.createHash(method);
  var isBuffer = Buffer.isBuffer(s);
  if (!isBuffer && typeof s === 'object') {
    s = JSON.stringify(sortObject(s));
  }
  sum.update(s, isBuffer ? 'binary' : 'utf8');
  return sum.digest(format || 'hex');
};

exports.md5 = function md5(s, format) { return exports.hash('md5', s, format); };

exports.sha1 = function sha1(s, format) { return exports.hash('sha1', s, format); };

exports.sha256 = function sha256(s, format) { return exports.hash('sha256', s, format); };

exports.hmac = function hmac(algorithm, key, data, encoding) {
  encoding = encoding || 'base64';
  var hmac = crypto.createHmac(algorithm, key);
  hmac.update(data, Buffer.isBuffer(data) ? 'binary' : 'utf8');
  return hmac.digest(encoding);
};

exports.base64encode = function base64encode(s, urlsafe) {
  if (!Buffer.isBuffer(s)) {
    s = typeof Buffer.from === 'function' ? Buffer.from(s) : new Buffer(s);
  }
  var encode = s.toString('base64');
  if (urlsafe) {
    encode = encode.replace(/\+/g, '-').replace(/\//g, '_');
  }
  return encode;
};

exports.base64decode = function base64decode(encodeStr, urlsafe, encoding) {
  if (urlsafe) {
    encodeStr = encodeStr.replace(/\-/g, '+').replace(/_/g, '/');
  }
  var buf = typeof Buffer.from === 'function' ? Buffer.from(encodeStr, 'base64') : Buffer.from(encodeStr, 'base64');
  if (encoding === 'buffer') {
    return buf;
  }
  return buf.toString(encoding || 'utf8');
};

function sortObject(o) {
  if (!o || Array.isArray(o) || typeof o !== 'object') {
    return o;
  }
  var keys = Object.keys(o);
  keys.sort();
  var values = [];
  for (var i = 0; i < keys.length; i++) {
    var k = keys[i];
    values.push([k, sortObject(o[k])]);
  }
  return values;
}

console.log(exports.md5(Buffer.from('this is a test'), 'base64'));
console.log(exports.sha1(Buffer.from('this is a test'), 'base64'));
console.log(exports.sha256(Buffer.from('this is a test'), 'base64'));
