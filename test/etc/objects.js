lastName = "Smith";

var person = new Object();
person.firstName = "John";
person.lastName = "Doe";
person.age = 50;
person.eyeColor = "blue";
person.fullName = function() { return person.firstName + " " + person.lastName };

console.log(lastName);
console.log(person.firstName);
console.log(person.lastName);
console.log(person.fullName());
