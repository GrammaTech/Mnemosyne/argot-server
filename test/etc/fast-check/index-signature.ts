function foo(a: { [key: string]: number }) {
    if (Object.keys(a).length > 3) { throw Error(); }
}
