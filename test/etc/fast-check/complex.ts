function foo([a, b]: [object, number[]], c: number[][]) {
    if (Object.keys(a).length > 1 && b.length > 1 && c.length > 1) { throw Error(); }
}

function bar(a: object | null, b: [number, string][] | undefined) {
    if (a && b && b.length > 1) { throw Error(); }
}
