(defpackage :argot-server/test/utility
  (:documentation "Utilities for testing (not tests for utilities!).")
  (:use :gt/full
        :lsp-server/protocol
        :lsp-server/protocol-util
        :lsp-server/lsp-server
        :lsp-server/logger
        :lsp-server/utilities/lsp-utilities
        :lsp-server.lem-base
        :argot-server/readtable
        :argot-server/server
        :argot-server/utility
        :stefil+
        :argot-server/test/transcript)
  (:local-nicknames (:lp.q :lparallel.queue)
                    (:lp :lparallel)
                    (:ts :software-evolution-library/software/tree-sitter))
  (:import-from :cmd)
  (:import-from :argot-server/test/eglot :parse-eglot-transcript)
  (:import-from :argot-server/passthrough
                :server-muses
                :muse-timeout
                :set-passthrough)
  (:import-from :argot-server/muses/incrementalizer :incrementalize)
  (:import-from :jsonrpc)
  (:import-from :yason)
  ;; (:import-from :resolve/ast-diff :ast-diff :print-diff)
  (:import-from :argot-server/muses/muse
                :muse-available?
                :muse-start
                :muse-stop)
  (:import-from :argot-server/muses/passthrough
                :muse-started-p)
  (:import-from :argot-server/annotation-host
                :has-contributions-by-source-p)
  (:shadow :add-hook :remove-hook :run-hooks)
  (:export
    :log-dir
    :json-subset?
    :parse-server-transcript
    :json-diff
    :hash-table-alist*
    :encode-to-string
    :dispatching-client
    :server-incremental?
    :rerun-baseline
    :*debug-rerun*
    :with-timeout-backtrace
    :define-timed-test
    :dockerized?
    :with-throwaway-server
    :with-temporary-muse))
(in-package :argot-server/test/utility)
(in-readtable :curry-compose-reader-macros)

(def log-dir
  (asdf:system-relative-pathname :argot-server "test/logs/"))

(defgeneric json-subset? (sub super)
  (:documentation "Is SUB a subset of SUPER, where SUB and SUPER are JSON structures using Yason's representation?")
  (:method (sub super) (equal sub super))
  (:method ((sub list) (super list))
    (subsetp sub super :test #'json-subset?))
  (:method ((sub hash-table) (super hash-table))
    ;; Remove some keys we don't care about.
    (setf sub (copy-hash-table sub)
          super (copy-hash-table super))
    (dolist (key '("isIncomplete" "documentation" "textDocumentSync"
                   ;; These are randomly generated.
                   "annotationId" "changeAnnotations"))
      (remhash key sub)
      (remhash key super))
    (and (<= (hash-table-count sub)
             (hash-table-count super))
         (iter (for (key value) in-hashtable sub)
               (always (json-subset? value (href super key))))))
  (:method (sub (super list))
    (some {json-subset? sub} super))
  ;; Special case: no signatures.
  (:method ((sub hash-table) (super null))
    (match sub
      ((dict "signatures" nil) t)))
  ;; Special case: no completions is a subset of completions.
  (:method ((sub list) (super hash-table))
    (match super
      ((dict "items" items)
       (json-subset? sub items)))))

(defun parse-server-transcript (source)
  "Parse the Argot server transcript format."
  (let* ((lines
          (nest
           (remove-if (disjoin #'emptyp {string^= "#"}))
           (lines)
           (read-file-into-string source :external-format :utf-8)))
         (runs
          (runs lines
                :test (lambda (x y)
                        (and (string^= "* " x)
                             (not (string^= "* " y))))))
         (runs
          (mapcar [{mapcar {drop-prefix "params:" }}
                   {remove-if {string^= "name: "}}]
                  runs))
         (json
          (mapcar [#'yason:parse {string-join _ ""} #'rest]
                  runs))
         (tags
          (mapcar (lambda (run)
                    (assure transcript-tag
                      (ecase-using (flip #'string$=) (first run)
                        ;; These are all we log internally right now.
                        ("client" +client-request-tag+)
                        ("server" +server-reply-tag+))))
                  runs)))
    (assure transcript
      (mapcar #'cons tags json))))

(defun parse-vscode-transcript (source)
  (nest
   (assure transcript)
   (flet ((type-prop-tag (type-prop)
            (assure transcript-tag
              ;; Remember this is from the perspective of the client.
              (string-ecase type-prop
                ("send-notification" +client-notification-tag+)
                ("receive-notification" +server-notification-tag+)
                ("send-request" +client-request-tag+)
                ("receive-request" +server-request-tag+)
                ("send-response" +client-reply-tag+)
                ("receive-response" +server-reply-tag+))))))
   (let* ((string (read-file-into-string source :external-format :utf-8))
          (lines (lines string))))
   (iter (for line in lines)
         (for json = (drop (1+ (position #\] line)) line))
         (ematch (yason:parse json)
           ((dict "type" type-prop
                  "message" message
                  ;; Is this ever false?
                  "isLSPMessage" (eql t)
                  "timestamp" _)
            (collect (cons (type-prop-tag type-prop)
                           message)))))))

(defun json-diff (json1 json2)
  ;; Disabled until Resolve is ported to structured-text.
  #+(or)
  (let* ((sw1 (sel:from-string (make 'ts:json) json1))
         (sw2 (sel:from-string (make 'ts:json) json2))
         (diff (ast-diff (sel:genome sw1) (sel:genome sw2))))
    (with-output-to-string (s)
      (print-diff diff :no-color t :stream s))))

(defun hash-table-alist* (x)
  (typecase x
    (list (mapcar #'hash-table-alist* x))
    (hash-table
     (iter (for (k v) in-hashtable x)
           (collect (cons k (hash-table-alist* v)))))
    (t x)))

(defun encode-to-string (x)
  (with-output-to-string (s)
    (yason:encode x s)))

(defclass dispatching-client (jsonrpc:client)
  ((handler :initarg :handler :type function)))

(defmethod jsonrpc:dispatch ((self dispatching-client) msg)
  (with-slots (handler) self
    (funcall handler msg)))

(defun server-incremental? (params)
  (declare (optimize debug))
  (match params
    ((dict "capabilities"
           (dict "textDocumentSync"
                 (or 2 (dict "change" 2))))
     t)))

(defun call/throwaway-server (fn &key
                                   (port (random-port))
                                   (client (make 'jsonrpc:client))
                                   enable root
                                   (initialize-as (and root :eglot))
                                   (output (make-string-output-stream))
                                   (timeout 60)
                                   (allow-local t))
  (let* ((handle
          (launch-server :port port
                         :enable enable
                         :log-file output
                         :reset t
                         :allow-local allow-local))
         (*server* (server-handle-server handle)))
    (setf (muse-timeout *server*) timeout)
    (handler-bind
        ((serious-condition
          (lambda (e) (declare (ignore e))
            (when (typep output 'string-stream)
              (let ((string (get-output-stream-string output)))
                (format t
                        "~&Last 100 lines of log output:~%~{    ~a~%~}"
                        (last (lines string) 100)))))))
      (unwind-protect
           (progn
             (client-connect/retries client :port port :mode :tcp)
             (let ((initialize-params
                    (ecase initialize-as
                      (:eglot
                       (nest
                        (gethash "params")
                        (cdar)
                        (parse-eglot-transcript)
                        (path-join log-dir #p"eglot-pyls.log")))
                      (:vscode
                       (nest
                        (gethash "params")
                        (cdar)
                        (parse-vscode-transcript)
                        (path-join log-dir #p"vscode-refactoring.json")))
                      ((nil)))))
               (when initialize-params
                 (when root
                   (setf (gethash "rootPath" initialize-params) root
                         (gethash "rootUri" initialize-params)
                         (string+ "file://" root)))
                 (jsonrpc:call client "initialize"
                               (convert 'hash-table initialize-params)))
               (funcall fn client)))
        (progn
          (server-handle-stop handle)
          (jsonrpc:client-disconnect client))))))

(defmacro with-throwaway-server ((client &rest args &key &allow-other-keys)
                                 &body body)
  `(call/throwaway-server
    (lambda (,client)
      ,@body)
    ,@args))

(defvar *debug-rerun* nil
  "If non-nil, print the messages as they are sent.")

(defun rerun-baseline (file &key
                                (port (random-port))
                                enable
                                (format :eglot)
                                (cores (count-cpus))
                              (timeout 60)
                              (output (make-broadcast-stream))
                              await-annotations-from
                              (allow-local t))
  "Rerun a transcript as a \"baseline\".
The rerun is considered a success if the transcript is a *subset* of
the actual client/server traffic."
  (declare (optimize debug)
           (type (or string null) await-annotations-from))
  (nest
   (if (notevery #'muse-available? enable)
       (progn
         (format *error-output* "Skipping testing ~a: muse~p not available"
                 enable
                 (length enable))))
   ;; Some LSP servers (e.g. javascript-typescript-langserver) demand
   ;; that the file exist.
   (let* ((transcript
           (ecase format
             (:eglot (parse-eglot-transcript file))
             (:vscode (parse-vscode-transcript file))))
          (pending '())
          (notifications '())
          (received-notifications '())
          (received-commands '())
          (replies (keep +client-reply-tag+ transcript
                         :test 'equal
                         :key #'car))
          (expected-notifications (lp.q:make-queue :fixed-capacity 10))
          (waiting (lp.q:make-queue :fixed-capacity 10))
          (client
           (make 'dispatching-client
                 :handler
                 (lambda (msg)
                   (if (jsonrpc:request-id msg)
                       (progn
                         (when (> cores 1)
                           (lp.q:push-queue msg waiting))
                         (push (dict "id"     (jsonrpc:request-id msg)
                                     "params" (jsonrpc:request-params msg)
                                     "method" (jsonrpc:request-method msg))
                               received-commands)
                         (jsonrpc:make-response
                          :id (jsonrpc:request-id msg)
                          :result (gethash "result" (cdr (pop replies)))))
                       (progn
                         (when (> cores 1)
                           (lp.q:push-queue msg expected-notifications))
                         (push (jsonrpc:request-params msg)
                               received-notifications)
                         nil)))))))
   (with-throwaway-server (client :client client :enable enable :port port
                                  :timeout timeout :output output
                                  :allow-local allow-local)
     (is (length= enable (server-muses *server*)))
     (iter (until (every #'muse-started-p (server-muses *server*)))
           (sleep 0.01))
     (dolist (msg transcript)
       (assert (typep (car msg) 'transcript-tag))
       (when *debug-rerun*
         (print (cons (car msg)
                      (hash-table-alist* (cdr msg)))
                *error-output*)
         (force-output *error-output*))
       (ematch msg
         ;; Skip client replies (they are handled by the client).
         ((cons '(:client :reply) _))
         ((cons '(:server :request) _)
          (when (> cores 1)
            (lp.q:pop-queue waiting)))
         ((cons '(:client :notification)
                (dict "method" method "params" params))
          (jsonrpc:notify client method params))
         ((cons '(:client :request)
                (dict "id" id
                      "params" params
                      "method" method))
          (when (and (equal method "textDocument/codeAction")
                     await-annotations-from)
            ;; Wait for annotations from a particular source.
            (let ((uri
                   (assure string
                     (href params "textDocument" "uri"))))
              (iter (until (has-contributions-by-source-p
                            *server* uri await-annotations-from))
                    ;; Print some feedback while waiting.
                    (if (first-iteration-p)
                        (format *error-output*
                                "~&Awaiting contributions from ~a"
                                await-annotations-from)
                        (progn (format *error-output* ".")
                               (force-output *error-output*)))
                    (finally (terpri *error-output*))
                    (sleep 0.1))))
          (let ((reply (jsonrpc:call client method params
                                     :timeout timeout)))
            (push (cons id reply) pending)))
         ((cons '(:server :notification) params)
          (when (> cores 1)
            ;; Wait until the server actually sends a
            ;; notification. If no notification is sent this will
            ;; hang.
            (lp.q:pop-queue expected-notifications)
            (push params notifications)))
         ;; Skip initializing; the rules for merging
         ;; capabilities are more complex than a simple union.
         ((cons '(:server :reply) (dict "id" 1))
          (alexandria:removef pending 1 :key #'car))
         ((cons '(:server :reply)
                (and params
                     (dict "id" id
                           "result" old
                           "method" method)))
          ;; Some servers require the document to actually exist.
          (when (equal method "textDocument/didOpen")
            (when-let* ((uri (href params "textDocument" "uri"))
                        (file (file-uri-path uri)))
              (open file
                    :direction :probe
                    :if-does-not-exist :create)))
          (let* ((new-reply (find id pending :key #'car))
                 (new (cdr new-reply))
                 ;; The request we are replying to.
                 (request
                   (find-if (lambda (msg)
                              (match msg
                                ((cons '(:client :request)
                                       (dict "id" rid))
                                 (equal rid id))))
                            transcript)))
            (is new-reply "No request for reply ~a" id)
            (if (equal id 1)
                (is (true (server-incremental? new)))
                (let ((old-alist (hash-table-alist* old))
                      (new-alist (hash-table-alist* new)))
                  (restart-case
                      (is (json-subset? old new)
                          "Old: ~s~%New: ~s~%JSON diff: ~a~%Request: ~a"
                          old-alist
                          new-alist
                          (json-diff (encode-to-string old)
                                     (encode-to-string new))
                          (cons (car request)
                                (hash-table-alist (cdr request))))
                    (diff-strings ()
                      :report "Print a diff of the results as strings"
                      (flet ((strip-edit-ids (s)
                               (regex-replace-all "edit\\d+" s "edit_")))
                        (let ((old-string (strip-edit-ids (fmt "~s" old-alist)))
                              (new-string (strip-edit-ids (fmt "~s" new-alist))))
                          (cmd:cmd "diff -y -W 200"
                                   (cmd:psub "fmt -w 100" :<<< old-string)
                                   (cmd:psub "fmt -w 100" :<<< new-string)
                                   :check nil))))))))
          (alexandria:removef pending id :key #'car))))
     (when (> cores 1)
       (is (emptyp pending))))
   ;; Notifications are not deterministic enough to reliably
   ;; test this way.
   ;; (is (json-subset? received-notifications notifications))
   ))

(defun call/timeout-backtrace (fn &key (timeout (* 60 5)))
  #-sbcl (funcall fn)
  #+sbcl
  (handler-bind ((timeout
                   (lambda (c)
                     (uiop:print-backtrace
                      :condition c
                      :stream *error-output*))))
    (with-deadline (timeout)
      (funcall fn))))

(defmacro with-timeout-backtrace ((&key (timeout (* 60 5)))
                                  &body body)
  (with-thunk (body)
    `(call/timeout-backtrace ,body :timeout ,timeout)))

(defmacro define-timed-test (name lambda-list &body body)
  (destructuring-bind (name &key (timeout (* 60 5))
                              (long-running t))
      (ensure-list name)
    `(stefil+:deftest (,name :long-running ,long-running) ,lambda-list
       (format *error-output* "~&Running test ~a~%" ',name)
       (force-output *error-output*)
       (with-timeout-backtrace (:timeout ,timeout)
         ,@body))))

(defun call/temporary-muse (muse fn)
  (declare (function fn))
  (when (muse-available? muse)
    (unwind-protect
         (progn
           (muse-start muse)
           (let ((*server* muse))
             (funcall fn muse)))
      (muse-stop muse))))

(defmacro with-temporary-muse ((var muse) &body body)
  "Bind VAR to MUSE, start it, and run BODY, ensuring the temporary
muse is shut down."
  (with-thunk (body var)
    `(call/temporary-muse ,muse ,body)))
