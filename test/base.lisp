(uiop:define-package :argot-server/test/base
    (:use-reexport :argot-server/server
                   :argot-server/utility
                   :argot-server/muses/muse
                   :stefil+
                   :software-evolution-library/utility/range
                   :argot-server/test/utility)
  (:import-from :argot-server/test/root :test)
  (:import-from :asdf :system-relative-pathname)
  (:import-from :software-evolution-library
                :from-file
                :from-string
                :genome)
  (:import-from :software-evolution-library/software/parseable
                :source-text)
  (:export :test
           :system-relative-pathname
           :from-file
           :from-string
           :genome
           :source-text)
  (:documentation "Base package for test suites."))
