(defpackage :argot-server/test/muses/type-view
  (:use :gt/full
        :argot-server/test/base
        :argot-server/muses/type-view
        :argot-server/muses/typescript-type-view
        :argot-server/muses/jsdoc-type-view
        :argot-server/muses/python-type-view
        :software-evolution-library/software/tree-sitter
        :software-evolution-library/software/typescript
        :lsp-server/protocol
        :argot)
  (:shadowing-import-from :serapeum :~>)
  (:documentation "Tests for the Test View muse."))
(in-package :argot-server/test/muses/type-view)

(defsuite type-view-tests "Tests for the Type View muse.")

(def +function-param-type.ts+
  (system-relative-pathname :argot-server "test/etc/function-param-type.ts"))

(defun test-extracted-types (sw &key length)
  (declare (optimize debug))
  (let ((alist
          (dump-type-view-string-annotations (make 'typescript-type-view) sw)))
    (when length
      (is (length= alist length)))
    (iter (for (k . v) in alist)
          (is (string*= ": " k))
          (let* ((data
                  (~> v
                      (slot-value '|contributions|)
                      (only-elt)
                      (slot-value '|data|)))
                 (type (etypecase data
                         (|TypeCandidate| (slot-value data '|typeName|))
                         (|FunctionReturnType| (slot-value data '|returns|)))))
            (is (stringp type))
            (is (not (string^= ": " type)))
            (is (string*= type k))))))

(deftest test-extracted-param-types ()
  (test-extracted-types
   (from-file 'typescript +function-param-type.ts+)
   :length 3))

(deftest test-extracted-union-type ()
  (test-extracted-types
   (from-string 'typescript
                "function printId(id: number | string) {
  console.log(id.toUpperCase());
}")
   :length 1))

(deftest test-simple-return-type ()
  (test-extracted-types
   (from-string 'typescript
                "function plus(x: number, y: number): number {
  return x+y;
}")
   :length 3))

(deftest test-extract-types-with-overloads ()
  (test-extracted-types
   (from-string 'typescript
                "function myCoolFunction(f: (x: number) => void, nums: number[]): void;
function myCoolFunction(f: (x: number) => void, ...nums: number[]): void;
function myCoolFunction() {
}")
   :length 8))

(def +dummy-pos+
  (make '|Position| :line 0 :character 0))

(defun contrib-type (contrib)
  (slot-value (only-elt (contrib-text-edits
                         (make 'type-view)
                         (slot-value contrib '|data|)
                         :end +dummy-pos+))
              '|newText|))

(deftest test-contrib-type ()
  "Test extracting the type from a contribution with a definite type."
  (is (string$= "something"
                (contrib-type
                 (make '|Contribution|
                       :source "Type View"
                       :kind |ArgotKind.Types|
                       :data (convert '|TypeCandidate|
                                      (dict "typeName" "something"
                                            "probability" 1)))))))

(deftest test-contrib-candidate-type ()
  "Test extracting the type from a contribution with a list of candidates."
  (is (string$= "something"
                (contrib-type
                 (make '|Contribution|
                       :source "Type View"
                       :kind |ArgotKind.Types|
                       :data (convert '|TypeCandidates|
                                      (dict "candidates"
                                            (list (dict "typeName"
                                                        "something-else"
                                                        "probability" 0.1)
                                                  (dict "typeName" "something"
                                                        "probability" 1)))))))))
