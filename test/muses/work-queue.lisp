(defpackage :argot-server/test/muses/work-queue
  (:use :gt/full
        :argot-server/test/base
        :argot-server/muses/work-queue)
  (:import-from :argot :|Annotation|)
  (:import-from :argot-server/muses/work-queue
                :string-stamp
                :hash-string)
  (:shadowing-import-from :argot-server/muses/work-queue :never))
(in-package :argot-server/test/muses/work-queue)

(defsuite work-queue-tests "Test for the work-queue-muse."
  (muse-available? (make 'work-queue-muse)))

;;; TODO These really should be property tests.

(deftest test-can-hash ()
  (is (octet-vector-p (hash-string ""))))

(deftest test-hash-matches ()
  (is (equalp (hash-string "")
              (hash-string ""))))

(deftest test-stamp-matches ()
  (is (equalp (string-stamp "") (string-stamp ""))))

(deftest test-never-hash ()
  (is (not (equalp (string-stamp "") never))))
