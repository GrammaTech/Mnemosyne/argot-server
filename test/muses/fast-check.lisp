(defpackage :argot-server/test/muses/fast-check
  (:use :gt/full
         :stefil+
         :argot/argot
         :argot-server/muses/fast-check
         :software-evolution-library
         :software-evolution-library/software/typescript
         :software-evolution-library/software/tree-sitter
         :software-evolution-library/software/parseable)
  (:import-from :argot-server/test/root :test)
  (:import-from :argot-server/muses/fast-check
                :+typescript-compiler+
                :+javascript-interpreter+
                :parse-typescript-type-annotation-ast
                :arguments
                :arbitraries
                :functions-to-annotate
                :create-fast-check-tests
                :create-example-cases))

(in-package :argot-server/test/muses/fast-check)
(in-readtable :curry-compose-reader-macros)


;;; Test suite definition.
(defsuite fast-check-muse-tests "Tests for the fast-check muse"
  (and (which +typescript-compiler+)
       (which +javascript-interpreter+)))


;;; Common utilities and variables.
(define-constant +fast-check-dir+
  (asdf:system-relative-pathname :argot-server "test/etc/fast-check/")
  :test #'equalp
  :documentation "Directory containing fast-check unit test files.")

(defmacro with-fixed-gensym-counter (&body body)
  "Execute BODY with the gensym-counter initially bound to 0."
  `(let ((*gensym-counter* 0))
     ,@body))


;;; Tests for parsing type annotation strings.
(defun parse-typescript-type-annotation-ast-test (text)
  "Ensure a typescript-type-annotation AST may be created from TEXT,
representing a potential annotation from the type-view muse."
  (let ((ast (parse-typescript-type-annotation-ast text)))
    (is (typep ast 'typescript-type-annotation))
    (is (equal (source-text ast) (format nil ": ~a" text)))))

(deftest parse-typescript-type-annotation-ast-number ()
  (parse-typescript-type-annotation-ast-test "number"))

(deftest parse-typescript-type-annotation-ast-string ()
  (parse-typescript-type-annotation-ast-test "string"))

(deftest parse-typescript-type-annotation-ast-array ()
  (parse-typescript-type-annotation-ast-test "number[]"))

(deftest parse-typescript-type-annotation-ast-tuple ()
  (parse-typescript-type-annotation-ast-test "[number,string]"))

(deftest parse-typescript-type-annotation-ast-generic ()
  (parse-typescript-type-annotation-ast-test "Array<number>"))


;;; Tests for creating example cases of failing inputs to a function.
(defun test-example-cases (filename expected-results)
  "Ensure the process of creating and running fast-check tests from the
functions in FILENAME yields the given example failing inputs found
in EXPECTED-RESULTS."
  (labels ((simplify-arbitrary (spec)
             "Simplify a given fast-check arbitrary SPEC by removing arguments
              controlling the random distributions."
             (cond ((stringp spec) spec)
                   ((equal (car spec) "constant") spec)
                   (t (nest (cons (car spec))
                            (mapcar #'simplify-arbitrary)
                            (remove-if «or #'stringp #'hash-table-p»)
                            (cdr spec)))))
           (simplify-dates (x)
             "Simplify date failing test case examples."
             (typecase x
               (list (mapcar #'simplify-dates x))
               (string (if (string^= "new Date(" x)
                           "new Date()"
                           x))
               (otherwise x)))
           (do-test (sw-type)
             "Perform the test by creating and running fast-check tests on
             software objects of SW-TYPE."
             (let* ((sw (from-file (make-instance sw-type)
                                   (path-join +fast-check-dir+ filename)))
                    (funcs (functions-to-annotate sw))
                    (fast-check-tests
                     (with-fixed-gensym-counter
                         (create-fast-check-tests sw funcs)))
                    (example-cases (create-example-cases sw fast-check-tests)))
               (is (= (length funcs)
                      (length fast-check-tests)
                      (length example-cases)))

               (iter (for fast-check-test in fast-check-tests)
                     (for example-case in example-cases)
                     (for expected-result in expected-results)
                     (is (equalp (test-target fast-check-test)
                                 (aget :test-target expected-result)))
                     (is (equalp (arguments fast-check-test)
                                 (aget :arguments expected-result)))
                     (is (equalp (mapcar #'simplify-arbitrary
                                         (arbitraries fast-check-test))
                                 (aget :arbitraries expected-result)))
                     (is (equalp
                          (simplify-dates (test-target example-case))
                          (simplify-dates
                           (aget :test-target expected-result))))
                     (is (equalp (simplify-dates (arguments example-case))
                                 (simplify-dates
                                  (aget :failure-case expected-result))))))))
    (do-test 'typescript-ts)
    (do-test 'typescript-tsx)))

(deftest (test-example-cases-boolean :long-running) ()
  (nest (test-example-cases "boolean.ts")
        '(((:test-target . "foo")
           (:arguments . ("a"))
           (:arbitraries . (("boolean")))
           (:failure-case . ("true"))))))

(deftest (test-example-cases-number :long-running) ()
  (nest (test-example-cases "number.ts")
        '(((:test-target . "foo")
           (:arguments . ("a"))
           (:arbitraries . (("double")))
           (:failure-case . ("10.000000000000002"))))))

(deftest (test-example-cases-bigint :long-running) ()
  (nest (test-example-cases "bigint.ts")
        '(((:test-target . "foo")
           (:arguments . ("a"))
           (:arbitraries . (("bigint")))
           (:failure-case . ("11n"))))))

(deftest (test-example-cases-string :long-running) ()
  (nest (test-example-cases "string.ts")
        '(((:test-target . "foo")
           (:arguments . ("a"))
           (:arbitraries . (("string")))
           (:failure-case . ("\"           \""))))))

(deftest (test-example-cases-constant-null :long-running) ()
  (nest (test-example-cases "constant-null.ts")
        '(((:test-target . "foo")
           (:arguments . ("a"))
           (:arbitraries . (("constant" "null")))
           (:failure-case . ("null"))))))

(deftest (test-example-cases-constant-undefined :long-running) ()
  (nest (test-example-cases "constant-undefined.ts")
        '(((:test-target . "foo")
           (:arguments . ("a"))
           (:arbitraries . (("constant" "undefined")))
           (:failure-case . ("undefined"))))))

(deftest (test-example-cases-date :long-running) ()
  (nest (test-example-cases "date.ts")
        '(((:test-target . "foo")
           (:arguments . ("a"))
           (:arbitraries . (("date")))
           (:failure-case . ("new Date(\"+025667-01-15T07:00:00.000Z\")"))))))

#+(or)
(deftest (test-example-cases-maybe :long-running) ()
  (nest (test-example-cases "maybe.ts")
        '(((:test-target . "foo")
           (:arguments . ("a"))
           (:arbitraries . (("oneof" ("double")
                                     ("constant" "null")
                                     ("constant" "undefined"))))
           (:failure-case . ("null"))))))

(deftest (test-example-cases-object-predefined-type :long-running) ()
  (nest (test-example-cases "object-predefined-type.ts")
        '(((:test-target . "foo")
           (:arguments . ("a"))
           (:arbitraries . (("object")))
           (:failure-case . ("{\"\":0,\" \":false,\"!\":false,\"\\\"\":[]}"))))))

(deftest (test-example-cases-object-any :long-running) ()
  (nest (test-example-cases "object-any.ts")
        '(((:test-target . "foo")
           (:arguments . ("a"))
           (:arbitraries . (("anything")))
           (:failure-case . ("null"))))))

(deftest (test-example-cases-unknown :long-running) ()
  (nest (test-example-cases "unknown.ts")
        '(((:test-target . "foo")
           (:arguments . ("a"))
           (:arbitraries . (("anything")))
           (:failure-case . ("[]"))))))

(deftest (test-example-cases-any :long-running) ()
  (nest (test-example-cases "any.ts")
        '(((:test-target . "foo")
           (:arguments . ("a"))
           (:arbitraries . (("anything")))
           (:failure-case . ("[]"))))))

(deftest (test-example-cases-array :long-running) ()
  (nest (test-example-cases "array.ts")
        '(((:test-target . "foo")
           (:arguments . ("a"))
           (:arbitraries . (("array" ("double"))))
           (:failure-case . ("[0,0,0,0,0,0,0,0,0,0,0]"))))))

(deftest (test-example-cases-tuple :long-running) ()
  (nest (test-example-cases "tuple.ts")
        '(((:test-target . "foo")
           (:arguments . ("a"))
           (:arbitraries . (("tuple" ("double") ("bigint"))))
           (:failure-case . ("[10.000000000000002,0n]"))))))

(deftest (test-example-cases-tuple-destructure :long-running) ()
  (nest (test-example-cases "tuple-destructure.ts")
        '(((:test-target . "foo")
           (:arguments . ("G0"))
           (:arbitraries . (("tuple" ("double") ("bigint"))))
           (:failure-case . ("[10.000000000000002,0n]"))))))

(deftest (test-example-cases-union :long-running) ()
  (nest (test-example-cases "union.ts")
        '(((:test-target . "foo")
           (:arguments . ("a"))
           (:arbitraries . (("oneof" ("array" ("double")) ("string"))))
           (:failure-case . ("\"           \""))))))

(deftest (test-example-cases-intersection :long-running) ()
  (nest (test-example-cases "intersection.ts")
        '(((:test-target . "foo")
           (:arguments . ("a"))
           (:arbitraries . (("oneof" ("array" ("double")) ("string"))))
           (:failure-case . ("\"           \""))))))

(deftest (test-example-cases-object-empty :long-running) ()
  (nest (test-example-cases "object-empty.ts")
        '(((:test-target . "foo")
           (:arguments . ("a"))
           (:arbitraries . (("object")))
           (:failure-case . ("{}"))))))

(deftest (test-example-cases-index-signature :long-running) ()
  (nest (test-example-cases "index-signature.ts")
        '(((:test-target . "foo")
           (:arguments . ("a"))
           (:arbitraries . (("dictionary" ("string") ("double"))))
           (:failure-case . ("{\"\":0,\" \":0,\"!\":0,\"\\\"\":0}"))))))

(deftest (test-example-cases-object-properties :long-running) ()
  (nest (test-example-cases "object-properties.ts")
        '(((:test-target . "foo")
           (:arguments . ("a"))
           (:arbitraries . (("record")))
           (:failure-case . ("{\"b\":10.000000000000002}"))))))

(deftest (test-example-cases-generic-array :long-running) ()
  (nest (test-example-cases "generic-array.ts")
        '(((:test-target . "foo")
           (:arguments . ("a"))
           (:arbitraries . (("array" ("double"))))
           (:failure-case . ("[0,0,0,0,0,0,0,0,0,0,0]"))))))

(deftest (test-example-cases-generic-readonly-array :long-running) ()
  (nest (test-example-cases "generic-readonly-array.ts")
        '(((:test-target . "foo")
           (:arguments . ("a"))
           (:arbitraries . (("array" ("double"))))
           (:failure-case . ("[0,0,0,0,0,0,0,0,0,0,0]"))))))

(deftest (test-example-cases-generic-nonnullable :long-running) ()
  (nest (test-example-cases "generic-nonnullable.ts")
        '(((:test-target . "foo")
           (:arguments . ("a"))
           (:arbitraries . (("double")))
           (:failure-case . ("10.000000000000002"))))))

(deftest (test-example-cases-generic-record1 :long-running) ()
  (nest (test-example-cases "generic-record1.ts")
        '(((:test-target . "foo")
           (:arguments . ("a"))
           (:arbitraries . (("dictionary" ("string") ("double"))))
           (:failure-case . ("{\"\":0,\" \":0,\"!\":0,\"\\\"\":0}"))))))

(deftest (test-example-cases-generic-record2 :long-running) ()
  (nest (test-example-cases "generic-record2.ts")
        '(((:test-target . "foo")
           (:arguments . ("a"))
           (:arbitraries . (("record")))
           (:failure-case . ("{\"b\":10.000000000000002}"))))))

(deftest (test-example-cases-generic-map1 :long-running) ()
  (nest (test-example-cases "generic-map1.ts")
        '(((:test-target . "foo")
           (:arguments . ("a"))
           (:arbitraries . (("dictionary" ("string") ("double"))))
           (:failure-case . ("{\"\":0,\" \":0,\"!\":0,\"\\\"\":0}"))))))

#+(or)
(deftest (test-example-cases-generic-map2 :long-running) ()
  (nest (test-example-cases "generic-map2.ts")
        '(((:test-target . "foo")
           (:arguments . ("a"))
           (:arbitraries . (("record")))
           (:failure-case . ("{\"b\":10.000000000000002}"))))))

(deftest (test-example-cases-complex :long-running) ()
  (nest (test-example-cases "complex.ts")
        '(((:test-target . "foo")
           (:arguments . ("G0" "c"))
           (:arbitraries . (("tuple" ("object") ("array" ("double")))
                            ("array" ("array" ("double")))))
           (:failure-case . ("[{\"\":{},\" \":[]},[0,0]]" "[[],[]]")))
          ((:test-target . "bar")
           (:arguments . ("a" "b"))
           (:arbitraries . (("oneof" ("object") ("constant" "null"))
                            ("oneof" ("array" ("tuple" ("double") ("string")))
                                     ("constant" "undefined"))))
           (:failure-case . ("{}" "[[0,\"\"],[0,\"\"]]"))))))
