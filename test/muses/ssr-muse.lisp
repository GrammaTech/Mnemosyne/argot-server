(defpackage :argot-server/test/muses/ssr-muse
  (:use :gt/full
        :argot-server/test/base)
  (:import-from :argot-server/muses/ssr-muse))
(in-package :argot-server/test/muses/ssr-muse)

(defsuite ssr-muse-tests "Test SSR Muse")
