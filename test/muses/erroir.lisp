(defpackage :argot-server/test/muses/erroir
  (:use
    :gt/full
    :argot-server/test/base
    :argot-server/muses/base
    :argot-server/muses/erroir))
(in-package :argot-server/test/muses/erroir)

(defsuite test-erroir "Tests for the Erroir muse")

(deftest test-insert-colon-after-sig ()
  (is (search "int:"
              (fix-source 'python "def foo (x) -> int
    pass")))
  (let ((source
         (fix-source 'python "# A function
def foo (x) -> int
    pass" :start (length (fmt "# A function~%")))))
    (is (search "int:" source))
    (is (search "# A function" source)))
  (is (search "int:"
              (fix-source 'python "def foo (x,
                                        y,
                                        z) -> int
    pass"))))

(deftest test-insert-colon-after-def ()
  (is (search "):"
              (fix-source 'python "def foo (x)
    pass")))
  (is (search "):"
              (fix-source 'python "def foo (x,
                                        y,
                                        z)
    pass")))
  (is (search "):"
              (fix-source 'python "def foo (x,
                                        y,
                                        z=foo())
    pass"))))

(deftest test-insert-colon-without-expression ()
  (is (search "else:"
              (fix-source 'python "if fn(x):
    True
else
    False")))
  (is (search "try"
              (fix-source 'python "try
    something()
except:
    print(\"An error\")"))))

(deftest test-insert-colon-with-expression ()
  (is (search "):"
              (fix-source 'python "if fn(x)
    True")))
  (is (search "):"
              (fix-source 'python "if fn(x,
                                     y)
    True")))
  (is (search "y:"
              (fix-source 'python "if x > y
    True")))
  ;; An *unparenthesized* expression extending across multiple lines!
  (is (search "y:"
              (fix-source 'python "if x >
       y
    True"))))
