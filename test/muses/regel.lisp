(defpackage :argot-server/test/muses/regel
  (:use :gt/full
        :argot-server/test/base)
  (:import-from :argot-server/muses/regel
                :print-benchmark :parse-benchmark :regel :benchmark)
  (:import-from :argot-server/muses/cli-muses
                :drive))
(in-package :argot-server/test/muses/regel)

(defsuite regel-tests "Test Regel integration"
  (argot-image-exists? "regel"))

(deftest test-benchmark-format ()
  (let* ((file (system-relative-pathname
                :argot-server
                "test/etc/digits"))
         (file-string (read-file-into-string file)))
    (is (equal file-string
               (print-benchmark (parse-benchmark file-string) nil)))))

(define-timed-test test-regex-synthesis ()
  (is (equal '("<num>")
             (drive (make 'regel)
                    (make 'benchmark
                          :natlang "I want digits"
                          :positive-examples '("1")
                          :negative-examples '("a"))))))
