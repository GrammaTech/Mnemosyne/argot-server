(defpackage :argot-server/test/muses/cli-muse
  (:use :gt/full
        :argot-server/test/base
        :argot-server/muses/cli-muses
        :stefil+))
(in-package :argot-server/test/muses/cli-muse)
(in-readtable :curry-compose-reader-macros)

(defsuite cli-muse-tests "Base muse for CLI muses")

#+(or) (defclass shell-muse (cli-muse)
         ((prompt :initarg :prompt))
         (:default-initargs
          :prompt (error "No prompt!")))

#+(or) (defmethod parse-input ((muse shell-muse) (input stream))
         (with-slots (prompt) muse
           (let ((err
                  (process-info-error-output
                   (muse-process muse))))
             ;; Wait for a prompt.
             (loop while (listen err)
                   for line = (read-line err)
                   until (equal prompt (trim-whitespace line)))
             (with-output-to-string (s)
               (loop for line = (and (listen input) (read-line input))
                     do (write-string line s))))))

(deftest test-runner-muse ()
  (let ((muse (make 'runner-muse :program '("tee"))))
    (is (equal "hello" (drive muse "hello")))))

(defclass linewise-muse (launcher-muse)
  ())

(defmethod format-to ((muse linewise-muse) prompt dest)
  (format dest "~a" prompt))

(defmethod parse-from ((muse linewise-muse) source)
  (read-line source))

(deftest test-launcher-muse ()
  (let ((muse (make 'linewise-muse :program '("bash" "-i"))))
    (muse-start muse)
    (unwind-protect
         (progn
           ;; Shells ignore SIGTERM by default.
           (drive muse "trap 'exit 0' TERM; echo")
           (is (equal "hello" (drive muse "echo hello")))
           ;; Must output something lest we hang.
           (drive muse "export X=1; echo")
           (is (equal "1" (drive muse "echo $X"))))
      (muse-stop muse))))
