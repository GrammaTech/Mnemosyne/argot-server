(defpackage :argot-server/test/muses/sygus
  (:use
   :gt/full
   :argot-server/test/base
   :argot-server/muses/sygus
   :argot-server/ast-utils
   :software-evolution-library/software/parseable
   :software-evolution-library/software/tree-sitter
   )
  (:import-from
   :argot-server/muses/sygus
   :sygus-input
   :invoke-sygus
   :decode-sygus-output
   )
  (:export :test))

(in-package :argot-server/test/muses/sygus)

(defun read-c (str)
  "Read STR as a C program, returning the software object"
  (from-string (make-instance 'c) str))

(defun into-sygus-package (form)
  (typecase form
    (symbol (intern (symbol-name form) :argot-server/muses/sygus))
    (cons (mapcar #'into-sygus-package form))
    (t form)))

(defsuite sygus-tests "Tests for the SyGuS muse"
  (muse-available? (make 'sygus)))

;; Confirm that comments are there
;; This is not actually sygus specific
(deftest comments.1 ()
  (let* ((p (read-c "/* The first comment */   /* second comment */ int x;"))
         (c1 (@ (genome p) 0))
         (c2 (@ (genome p) 1)))
    (is (eql (type-of c1) 'c-comment))
    (is (equal (source-text c1) "/* The first comment */"))
    (is (eql (type-of c2) 'c-comment))
    (is (equal (source-text c2) "/* second comment */"))))

(deftest comments.2 ()
  (let* ((p (read-c "// The first comment\
// second comment\
 int x;"))
         (c1 (@ (genome p) 0))
         (c2 (@ (genome p) 1)))
    (is (eql (type-of c1) 'c-comment))
    (is (equal (source-text c1) "// The first comment"))
    (is (eql (type-of c2) 'c-comment))
    (is (equal (source-text c2) "// second comment"))))

(defparameter *sygus.1*
  "// (1 2) 3\
int f(int x, int y) { }")

(defparameter *sygus-input.1*
  '((|set-logic| LIA)
    (|synth-fun| |f| ((|x| |Int|) (|y| |Int|)) |Int|
     ((I |Int|) (B |Bool|))
     ((I |Int| (|x| |y| 0 1 (+ I I) (- I I) (|ite| B I I)))
      (B |Bool| ((|and| B B) (|or| B B) (|not| B) (= I I) (<= I I) (>= I I)))))
    (|declare-var| |x| |Int|)
    (|declare-var| |y| |Int|)
    (|constraint| (= (|f| 1 2) 3))
    (|check-synth|)))

(deftest sygus-input.1 ()
  (equal (sygus-input (read-c *sygus.1*) "f")
         (into-sygus-package *sygus-input.1*)))

(defparameter *sygus.2*
  "// (1 2) 1
// (2 1) -1
// (1 1) 0
// (2 7) 1
// (7 0) -1
// (5 5) 0
// (101 5) -1
// (5 101) 1
int f(int x, int y) { }")

(defparameter *sygus-input.2*
  '((|set-logic| LIA)
    (|synth-fun| |f| ((|x| |Int|) (|y| |Int|)) |Int|
     ((I |Int|) (B |Bool|))
     ((I |Int| (|x| |y| 0 1 (+ I I) (- I I) (|ite| B I I)))
      (B |Bool| ((|and| B B) (|or| B B) (|not| B) (= I I) (<= I I) (>= I I)))))
    (|declare-var| |x| |Int|)
    (|declare-var| |y| |Int|)
    (|constraint| (= (|f| 1 2) 1))
    (|constraint| (= (|f| 2 1) (- 1)))
    (|constraint| (= (|f| 1 1) 0))
    (|constraint| (= (|f| 2 7) 1))
    (|constraint| (= (|f| 7 0) (- 1)))
    (|constraint| (= (|f| 5 5) 0))
    (|constraint| (= (|f| 101 5) (- 1)))
    (|constraint| (= (|f| 5 101) 1))
    (|check-synth|)))

(deftest sygus-input.2 ()
  (equal (sygus-input (read-c *sygus.2*) "f")
         (into-sygus-package *sygus-input.2*)))

(defparameter *sygus-output.1*
  '(|define-fun| |f| ((|x| |Int|) (|y| |Int|)) |Int| (+ |x| |y|)))

(deftest invoke-sygus.1 ()
  (equal (decode-sygus-output (invoke-sygus (sygus-input (read-c *sygus.1*) "f")))
         (into-sygus-package *sygus-output.1*)))

(defparameter *sygus-output.2*
  '(|define-fun| |f| ((|x| |Int|) (|y| |Int|)) |Int|
    (|ite| (<= |y| |x|) (|ite| (= |x| |y|) 0 (- 0 1)) 1)))

(deftest invoke-sygus.2 ()
  (equal (decode-sygus-output (invoke-sygus (sygus-input (read-c *sygus.2*) "f")))
         (into-sygus-package *sygus-output.2*)))
