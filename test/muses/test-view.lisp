(defpackage :argot-server/test/muses/test-view
  (:use :gt/full
        :argot
        :argot-server/test/base
        :argot-server/muses/test-view
        :argot-server/ast-utils
        :software-evolution-library/software/tree-sitter)
  (:import-from :argot-server/muses/test-view
                :extract-test-nodes
                :extract-test-texts
                :call-plist)
  (:documentation "Tests for the Test View muse."))
(in-package :argot-server/test/muses/test-view)

(defsuite test-view-tests "Tests for the Test View muse.")

(def has_tests.py
  (system-relative-pathname :argot-server "test/etc/has_tests.py"))

(def has_tests.js
  (system-relative-pathname :argot-server "test/etc/has_tests.js"))

(deftest test-extract-python-test-nodes ()
  (let* ((string (read-file-into-string has_tests.py :external-format :utf-8))
         (nodes (extract-test-nodes 'python string)))
    (is (length= nodes 1))
    (is (equal "test_fn_1" (function-name (only-elt nodes))))))

(deftest test-extract-js-test-nodes ()
  (let* ((string (read-file-into-string has_tests.js :external-format :utf-8))
         (nodes (extract-test-nodes 'javascript string)))
    (is (length= nodes 1))
    (is (equal "test_fn_1" (function-name (only-elt nodes))))))

(deftest test-extract-python-test-texts ()
  (let* ((string (read-file-into-string has_tests.py :external-format :utf-8))
         (texts (extract-test-texts 'python string)))
    (is (> (length (only-elt texts)) 1))
    (is (string*= (only-elt texts) string))
    (is (string*= "test_fn_1" string))))

(deftest test-extract-js-test-texts ()
  (let* ((string (read-file-into-string has_tests.js :external-format :utf-8))
         (texts (extract-test-texts 'javascript string)))
    (is (> (length (only-elt texts)) 1))
    (is (string*= (only-elt texts) string))
    (is (string*= "test_fn_1" string))))

(deftest test-extract-example-subscript ()
  (let* ((sw (from-string 'python (fmt "~
def test_egcd_0():
    try:
        return egcd(17, 0)[0] == 17
    except:
        return False")))
         (ast (find-if (of-type 'function-ast) sw))
         (examples (extract-examples sw ast)))
    (is (= 1 (length examples)))
    (let ((example (first examples)))
      (is (equal "egcd" (name example)))
      (is (equal '(0) (subscripts example)))
      (is (equal '(17 0) (arguments example))))))

(deftest test-call-plist-unknown ()
  (is (null (call-plist (python "s.name")))))
