(defpackage :argot-server/test/eglot
  (:use :gt/full
        :argot-server/readtable
        :argot-server/test/transcript)
  (:local-nicknames
   (:el :argot-server/test/elisp)
   (:interpol :cl-interpol))
  (:documentation "Parse Eglot transcripts (which means parsing a subset of Emacs Lisp).")
  (:export
   :read-elisp-from-string))
(in-package :argot-server/test/eglot)
(in-readtable :curry-compose-reader-macros)

(defun read-square-vector (stream n)
  (declare (ignore n))
  (coerce (read-delimited-list #\] stream t) 'vector))

(defun read-hash-table (stream char arg)
  (declare (ignore char arg))
  (let* ((*package* #.(find-package :el))
         (spec (rest (read stream nil nil t)))
         (data (getf spec 'el:|data|)))
    (plist-hash-table data)))

(defun read-string-with-escapes (stream &optional (char #\"))
  (unread-char char stream)
  (let ((interpol::*regex-delimiters* nil)
        (interpol:*inner-delimiters* nil)
        (interpol:*outer-delimiters* '(#\")))
    (interpol:interpol-reader stream nil nil)))

(defreadtable elisp
  (:merge :standard)
  (:macro-char #\[ 'read-square-vector)
  (:syntax-from :standard #\) #\])
  (:macro-char #\" 'read-string-with-escapes)
  (:dispatch-macro-char #\# #\s 'read-hash-table)
  (:case :preserve))

(defun read-elisp-from-string (string &rest args)
  (let ((*readtable* (find-readtable 'elisp))
        (*package* #.(find-package :el)))
    (apply #'read-from-string string args)))

(defun parse-eglot-transcript (source)
  (lret* ((lines
           (nest
            (remove-if (disjoin #'emptyp {string^= "-----"}))
            (lines)
            (read-file-into-string source :external-format :utf-8)))
          (runs
           (runs lines
                 :test (lambda (x y)
                         (and (string^= "[" x)
                              (not (string^= "[" y))))))
          (plists
           (mapcar (lambda (run)
                     (and (rest run)
                          (read-elisp-from-string
                           (fmt "~{~a~^~%~}" (rest run)))))
                   runs))
          (tags
           (mapcar (lambda (run)
                     (ecase-using (flip #'string^=) (first run)
                       ("[client-request" +client-request-tag+)
                       ("[client-reply" +client-reply-tag+)
                       ("[client-notification" +client-notification-tag+)
                       ("[server-reply" +server-reply-tag+)
                       ("[server-request" +server-request-tag+)
                       ("[server-notification" +server-notification-tag+)
                       ("[internal" :internal)
                       ("[stderr" :stderr)))
                   runs))
          (tagged
           (mapcar #'cons tags plists))
          (external
           (remove '(:internal :stderr) tagged
                   :key #'car
                   :test (flip #'member)))
          (final
           (assure transcript
             (mapcar (lambda (msg)
                       (cons (car msg)
                             (eglot->yason (cdr msg))))
                     external))))))

(deftype json ()
  '(or list vector hash-table number string
    (member el:|nil| el:|t| :|json-false|)))

(defun eglot->yason (json)
  "Translate Eglot's JSON to Yason's JSON representation."
  (etypecase-of json json
    ((member :|json-false| el:|nil| nil) nil)
    ((eql el:|t|) t)
    ((or number string) json)
    (vector
     (map 'list #'eglot->yason json))
    (hash-table
     (let ((table (copy-hash-table json :test 'equal)))
       (do-hash-table (k v table table)
         (setf (href table (string k))
               (eglot->yason v)))))
    (list
     (let ((table (dict)))
       (doplist (k v json table)
         (setf (href table (string k))
               (eglot->yason v)))))))
