(defpackage :argot-server/test/ast-utils
  (:use :gt/full
        :argot-server/test/base
        :argot-server/ast-utils
        :software-evolution-library/software/tree-sitter))
(in-package :argot-server/test/ast-utils)

(defsuite ast-utils-tests "Tests for AST utilities.")

(deftest test-valid-ast ()
  (let* ((invalid-files
          (directory
           (make-pathname
            :type :wild
            :defaults
            (system-relative-pathname :argot-server "test/etc/bad-ast"))))
         (valid-files
          (directory
           (make-pathname
            :type :wild
            :defaults
            (system-relative-pathname :argot-server "test/etc/good-ast")))))
    (dolist (file valid-files)
      (is (valid-ast? (guess-language file)
                      (read-file-into-string file))))
    ;; TODO Find a JS AST tree-sitter can't parse.
    ;; (dolist (file invalid-files)
    ;;   (is (not (valid-ast? (guess-language file)
    ;;                        (read-file-into-string file)))))
    ))

(deftest test-function-name ()
  (let ((dir (asdf:system-relative-pathname :argot-server "test/etc/")))
    (flet ((first-function-node-name (lang file)
             (function-name
              (find-if (lambda (node)
                         (typep node 'function-ast))
                       (genome
                        (from-file (make lang)
                                   (path-join dir file)))))))
      (is (equal "square"
                 (first-function-node-name 'c "square.c")))
      (is (equal "maxArray"
                 (first-function-node-name 'cpp "max_array.cpp")))
      (is (equal "maxArray"
                 (first-function-node-name 'cpp "max_array_opt.cpp")))
      (is (equal "testFunction"
                 (first-function-node-name 'cpp "sum_array.cpp")))
      (is (equal "testFunction"
                 (first-function-node-name 'cpp "sum_array_opt.cpp"))))))

(def functions.py
  (system-relative-pathname :argot-server "test/etc/functions.py"))

(deftest test-function-node-in-range ()
  (let* ((sw (from-file (make 'python) functions.py))
         (ast (genome sw))
         (start (make 'source-location
                      :line 1
                      :column 1))
         (text (source-text ast))
         (end (make 'source-location
                    :line (1+ (count #\Newline text))
                    :column (- (length text)
                               (or (position #\Newline text :from-end t)
                                   0)))))
    (is (equal
         "function1"
         (function-name
          (function-node-in-range sw start end))))))
