(defpackage :argot-server/test/lsp-diff
  (:use :gt/full
        :argot-server/test/base
        :argot-server/lsp-diff
        :lsp-server/protocol
        :lsp-server/utilities/lsp-utilities)
  (:shadowing-import-from :serapeum :lines)
  (:local-nicknames (:lem :lsp-server.lem-base)
                    (:lsp :lsp-server)
                    (:lsp-protocol :lsp-server/protocol)))
(in-package :argot-server/test/lsp-diff)
(in-readtable :curry-compose-reader-macros)

(defsuite lsp-diff-tests "Test suite for LSP diffing")

(def etc (system-relative-pathname "argot-server" "test/etc/"))
(def test.js (path-join etc "test.js"))
(def test2.js (path-join etc "test2.js"))

(deftest test-no-diff ()
  "Test that the same file diffed with itself contains no differences."
  (is (emptyp (command-line-diff test.js test.js))))

(deftest test-no-lsp-diff ()
  "Test that the same file diffed with itself contains no differences."
  (is (equal (lsp-edit-diff test.js test.js) '())))

(defun drop-lines (n string)
  "Drop N lines from the beginning of STRING."
  (string-join
   (drop n (lines string :keep-eols t :count n))
   ""))

(defun make-buffer-from-string (string)
  "Make an LSP buffer with initial contents of STRING."
  (lret ((buffer (lem:make-buffer (string (gensym)))))
    (lem:insert-string (lem:buffer-point buffer) string)))

(defun call/buffer-of (fn string)
  "Helper for `with-buffer-of'."
  (let ((buf (make-buffer-from-string string)))
    (unwind-protect (funcall fn buf)
      (lem:delete-buffer buf))))

(defmacro with-buffer-of ((buf) string &body body)
  "Bind BUF to a buffer with initial contents of STRING.
The buffer is automatically deleted at the end of the form."
  `(call/buffer-of (lambda (,buf) ,@body) ,string))

(defmacro with-buffer-of-file ((buf) file &body body)
  "Bind BUF to a buffer with initial contents from FILE.
The buffer is automatically deleted at the end of the form."
  `(call/buffer-of (lambda (,buf) ,@body) (read-file-into-string ,file)))

(deftest test-with-buffer-of ()
  (with-buffer-of (buf) "hello world"
    (is (equal "hello world" (buffer-to-string buf)))))

(defun text-edits->document-changes (edits)
  "Convert LSP TextEdit instances into LSP
TextDocumentContentChangeEvent instances."
  ;; Edits are conventially given backward.
  (mapcar #'text-edit->text-document-content-change-event
          (reverse edits)))

(defun text-edit->text-document-content-change-event (edit)
  (ematch edit
    ((lsp-protocol:|TextEdit| :|newText| text :|range| range)
     (make 'lsp-protocol:|TextDocumentContentChangeEvent|
           :text text
           :range range))))

(defun apply-diff! (buffer diff)
  (nest
   (lsp:apply-content-changes-to-buffer buffer)
   (mapcar #'text-edit->text-document-content-change-event diff)))

(deftest test-diff-applies ()
  "Test that two files differ as expected."
  (let ((diff (lsp-edit-diff test.js test2.js)))
    (with-buffer-of-file (b1) test.js
      (apply-diff! b1 diff)
      (is (equal (buffer-to-string b1)
                 (read-file-into-string test2.js)))))
  (let ((diff (lsp-edit-diff test2.js test.js)))
    (with-buffer-of-file (b2) test2.js
      (apply-diff! b2 diff)
      (is (equal (buffer-to-string b2)
                 (read-file-into-string test.js))))))

(deftest test-lsp-diff-line-offset ()
  (declare (optimize debug))
  (let ((short-diff (lsp-edit-diff-strings "var y = 2" "var y = 4" :line-offset 1)))
    (with-buffer-of (buffer)
      (fmt "var x = 1~%var y = 2~%var z = 3")
      (apply-diff! buffer short-diff)
      (is (equal (buffer-to-string buffer)
                 (fmt "var x = 1~%var y = 4~%var z = 3"))))))

(deftest test-lsp-diff-line-and-character-offset ()
  (declare (optimize debug))
  (let ((short-diff (lsp-edit-diff-strings "y = 2" "y = 4"
                                           :line-offset 1
                                           :char-offset 4)))
    (with-buffer-of (buffer)
      (fmt "var x = 1~%var y = 2~%var z = 3")
      (apply-diff! buffer short-diff)
      (is (equal (buffer-to-string buffer)
                 (fmt "var x = 1~%var y = 4~%var z = 3"))))))

(deftest test-lsp-change-end-line-offset ()
  (let* ((string1 "def distance(lat1: int,
             lon1: int,
             lat2: int,
             lon2: int):")
         (string2 "def distance(lat1: int, lon1: int, lat2: int, lon2: int):")
         (diff (only-elt (lsp-edit-diff-strings string1 string2)))
         (range (slot-value diff '|range|))
         (end (slot-value range '|end|)))
    (is (= (slot-value end '|line|) 4))
    (is (= (slot-value end '|character|) 0))))
