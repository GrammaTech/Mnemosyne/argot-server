(defpackage :argot-server/test/root
  (:use :gt/full
        :stefil+)
  (:export :test))
(in-package :argot-server/test/root)

(defroot test)
