(defpackage :argot-server/test/lens/tyrell
  (:use :gt/full
        :argot-server/test/base
        :software-evolution-library/software/tree-sitter
        :argot-server/lens/tyrell)
  (:local-nicknames (:fr :fresnel/fresnel)
                    (:l :fresnel/lens))
  (:import-from :argot-server/lens/tyrell))
(in-package :argot-server/test/lens/tyrell)
(in-readtable :curry-compose-reader-macros)
(defsuite test-tyrell-lens "Tyrell parsing, printing and translation.")

(def sexps
  '(
    ;; NB This one differs from the one in the docs, which has strings
    ;; as the arguments to IntConst instead of numbers.
    "(mult
          (plus
              (@param 0)
              (const (IntConst 1))
          )
          (minus
              (@param 1)
              (const (IntConst 2))
          )
      )"
    "(@param 0)"
    "(const (IntConst 1))"
    ;; Disabled because it doesn't fit the same rules as the others.
    ;; Maybe a mistake in the docs?
    ;; "(plus (@param 1) (IntConst 2))"
    "(@param 0)"
    "(const (IntConst 1))"
    "(plus (@param 1) (const (IntConst 2)))"
    "(plus (@param 1) (const (IntConst 2)))"
    "(plus (@param 1) (const (IntConst 2)))"
    "(plus (@param 0) (const (IntConst 1)))"
    "(@param 1)"
    "(plus (@param 0) (@param 1))"
    "(mult (@param 1) (minus (@param 0) (@param 1)))"
    "(minus (mult (@param 0) (@param 1)) (mult (@param 1) (@param 1)))")
  "Example S-expressions from the Tyrell documentation.")

(deftest sexp-smoke-test ()
  (let ((trees (mapcar {l:get (parser)} sexps)))
    (is (notany #'null trees))
    (is (notany #'stringp trees))
    (let ((asts (mapcar {l:create (python-translator)} trees)))
      (is (notany #'null asts))
      (is (notevery (of-type 'python-identifier) asts))
      (iter (for tree in trees)
            (for ast in asts)
            (is (equal tree (l:get (python-translator) ast)))))))
