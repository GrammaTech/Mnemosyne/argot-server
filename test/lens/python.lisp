(defpackage :argot-server/test/lens/python
  (:use :gt/full
        :argot-server/test/base
        :software-evolution-library/software/tree-sitter
        :software-evolution-library/software/python
        :argot-server/lens/python)
  (:import-from :software-evolution-library/software/parseable
                :source-text)
  (:local-nicknames (:fr :fresnel/fresnel)
                    (:l :fresnel/lens)))
(in-package :argot-server/test/lens/python)
(defsuite test-python-lens "Test basic Python lens.")

(defun pyast (type string)
  (or (find-if (of-type type)
               (convert 'python-ast string))
      (error "No ~a in ~a" type string)))

(defun equiv? (x y)
  (or (equal? x y)
      (and (symbolp x)
           (symbolp y)
           (null (symbol-package x))
           (null (symbol-package y))
           (string= x y))
      (tree-equal x y :test #'equiv?)))

(defun round-trip-file (file)
  (declare (optimize debug))
  (let* ((file (asdf:system-relative-pathname :argot-server file))
         (ast (sel:genome (sel:from-file (make 'python) file))))
    (round-trip-ast ast)))

(defun round-trip-ast (ast)
  (declare (optimize debug))
  (let* ((sexp (l:get (translator) ast))
         (ast2 (l:put (translator) sexp ast)))
    (is (equal (source-text ast)
               (source-text ast2)))))

(defun round-trip-string (string)
  (round-trip-ast (convert 'python-ast string :deepest t)))

(deftest test-string ()
  (let ((l (translator)))
    (flet ((test-delim (string delim1 &optional (delim2 delim1))
             (let ((ast (pyast 'python-string
                               (string+ delim1 string delim2))))
               (is (l:check-laws l ast string)))))
      (test-delim "hello" "'")
      (test-delim "hello" "u'" "'")
      (test-delim "hello" "r'" "'")
      (test-delim "hello" "\"")
      (test-delim "hello" "u\"" "\"")
      (test-delim "hello" "r\"" "\"")
      (test-delim "hello" "'''" "'''")
      (test-delim "hello" "r'''")
      (test-delim "hello" "u'''")
      (test-delim "hello" "\"\"\"")
      (test-delim "hello" "u\"\"\"" "\"\"\"")
      (test-delim "hello" "r\"\"\"" "\"\"\""))))

(deftest test-bool ()
  (let ((ast (pyast 'python-true "True")))
    (is (l:check-laws (translator) ast t)))
  (let ((ast (pyast 'python-false "False")))
    (is (l:check-laws (translator) ast :false)))
  (is (equal "True" (source-text (l:create (translator) t))))
  (is (equal "False" (source-text (l:create (translator) :false)))))

(deftest test-num ()
  (let ((l (translator))
        (int (pyast 'python-integer "2"))
        (float (pyast 'python-float "2.0")))
    (is (l:check-laws l int 2))
    (is (l:check-laws l float 2.0d0))
    (is (equal "2" (source-text (l:create l 2))))
    (is (typep (l:get l float) 'double-float))
    (is (null (l:create (translator) 2/3)))))

(deftest test-name ()
  (let ((ast (pyast 'python-identifier "foo")))
    (is (l:check-laws (translator) ast :foo)))
  (is (equal "foo" (source-text (l:create (translator) :|foo|))))
  (is (typep (l:create (translator) :|foo|) 'python-identifier))
  (is (not (typep (l:create (translator) '#:foo) 'python-identifier)))
  (is (not (typep (l:create (translator) 'foo) 'python-identifier))))

(deftest test-unop ()
  (let ((l (translator)))
    (is (typep (l:create l '(- 20)) 'python-unary-operator))
    (is (not (typep (l:create l '(:x 20)) 'python-unary-operator)))))

(deftest test-not ()
  (let ((l (translator)))
    (is (typep (l:create l '(not t)) 'python-not-operator))
    (is (not (typep (l:create l '(:x :false)) 'python-not-operator)))
    (round-trip-string "not x")))

(deftest test-binop ()
  (let ((l (translator)))
    (is (typep (l:create l '(+ 2 2)) 'python-binary-operator))
    (is (not (typep (l:create l '(:x 2 2)) 'python-binary-operator)))
    ;; Precedence
    (round-trip-string "2 * (3 + 3)")
    (round-trip-string "(-x)**2")
    (round-trip-string "(a + b) * c / d")))

(deftest test-boolean ()
  (let ((l (translator)))
    (is (typep (l:create l '(and t :false)) 'python-boolean-operator))
    (is (not (typep (l:create l '(:x 2 2)) 'python-boolean-operator)))
    (round-trip-string "x and y")
    (round-trip-string "x or y")
    ;; Precedence.
    (round-trip-string "x and (y or z)")))

(deftest test-comparison ()
  (let ((l (translator)))
    (is (typep (l:create l '(< 1 2)) 'python-comparison-operator))
    (is (not (typep (l:create l '(:x 1 2)) 'python-comparison-operator)))))

(deftest test-lambda ()
  (is (typep (l:create (translator) '(lambda (:x) 1)) 'python-lambda))
  (round-trip-string "lambda x: 1")
  (is (l:check-laws (translator)
                    (pyast 'python-lambda "lambda x: 1")
                    '(lambda (:x) 1)))
  (round-trip-string "lambda x, y: 2"))

(deftest test-call ()
  (round-trip-string "x()")
  (round-trip-string "x(1)")
  (round-trip-string "x(1, 2)")
  (round-trip-string "x(1, 2, 3)"))

(deftest test-attribute ()
  (is (typep (l:create (translator) '(:dot :x :y)) 'python-attribute))
  (round-trip-string "x.y"))

(deftest test-assignment ()
  (is (typep (l:create (translator) '(setf :x 1)) 'python-assignment))
  (round-trip-string "x = 1"))

(deftest round-trip-def ()
  (round-trip-file "test/etc/def-pass.py")
  (round-trip-file "test/etc/def-return.py"))

(deftest round-trip-if ()
  (round-trip-file "test/etc/if.py"))

(deftest round-trip-if-block ()
  (round-trip-file "test/etc/if-block.py"))

(deftest round-trip-if-else ()
  (round-trip-file "test/etc/if-else.py"))

(deftest round-trip-elif ()
  (round-trip-file "test/etc/if-elif.py"))
