(defpackage :argot-server/test/lens/fpcore
  (:documentation "Tests for the FPCore lens.")
  (:use :gt/full
        :argot-server/test/base
        :software-evolution-library/software/tree-sitter
        :argot-server/utility/fpcore
        :argot-server/lens/ast)
  (:local-nicknames (:l :fresnel/lens)
                    (:fpcore :argot-server/lens/fpcore)
                    (:fp :argot-server/lens/fpcore-builtins)
                    (:params :argot-server/lens/fpcore-symbols)
                    (:py :argot-server/lens/python))
  (:import-from :software-evolution-library/software/parseable
                :ast
                :source-text)
  (:import-from :argot-server/lens/fpcore :fpcore-lens))
(in-package :argot-server/test/lens/fpcore)
(in-readtable :curry-compose-reader-macros)
(defsuite test-fpcore-lens "Fpcore parsing, printing and translation.")

(defparameter *test-strings*
  '("sqrt(x+1)-sqrt(x)"
    "0.5 * sqrt(2.0 * (r + x.re))"
    "0.5 * math.sqrt(2.0 * (r + x.re))"
    "0.5*sqrt(2.0*(r-x.re))"
    "0.5*math.sqrt(2.0*(r-x.re))"
    "-0.5*sqrt(2.0*(r-x.re))"
    "-0.5*math.sqrt(2.0*(r-x.re))"
    "0.5 * math.sqrt(2.0 * (x.re * x.re + x.im * x.im + x.re))"
    ;; Operator precedence.
    "2*(3+3)"
    "(-x)**2"
    ;; Comparators.
    "x.im>=0"
    "x.im<0"))

(deftest py-translator-sanity-check ()
  (let ((l (l:compose (py:translator) (py:parser))))
    (dolist (string *test-strings*)
      (l:check-laws l string (l:get l string)))))

(deftest test-fpcore-builtin ()
  (is (fpcore::fpcore-builtin? '+))
  (is (fpcore::fpcore-builtin? '!=))
  (is (not (fpcore::fpcore-builtin? '/=))))

(deftest fpcore-boolean ()
  (let ((l (l:compose (fpcore:fpcore-lens) (py:translator))))
    (is (equal 'fp:false
               (l:get l
                      (find-if (of-type 'python-false)
                               (convert 'python-ast "False")))))
    (is (equal 'fp:true
               (l:get l
                      (find-if (of-type 'python-true)
                               (convert 'python-ast "True")))))
    (is (typep (l:create l 'fp:false) 'python-false))
    (is (typep (l:create l 'fp:true) 'python-true))
    (is (equal "True" (source-text (l:create l 'fp:true))))
    (is (equal "False" (source-text (l:create l 'fp:false))))))

(deftest fpcore-smoke-tests ()
  (let ((l (fpcore-lens)))
    (is (l:check-laws l 2 2))
    (is (l:check-laws l t 'fp:true))
    (is (l:check-laws l :false 'fp:false))
    (is (l:check-laws l ':|sqrt| 'fp:|sqrt|))
    (is (l:check-laws l '(:dot :|x| :|y|) 'params::|x.y|))
    (is (eql 'fp:|sqrt| (l:get l '(:dot :|math| :|sqrt|))))
    (iter (for cmp in '(< > >= <= fp:== fp:!=))
          (is (eql cmp (l:get l (make-keyword (string cmp))))))
    (is (l:check-laws l '(< :x :y) '(< params::x params::y)))
    (is (l:check-laws l '(- :x :y) '(- params::x params::y)))
    (is (l:check-laws l '(- :x) '(- params::x)))
    (is (l:check-laws l '(:|tan| :x :y) '(fp:|tan| params::x params::y)))
    (is (l:check-laws l
                      '(cond (:x (block nil (progn 1)))
                        (:y (block nil (progn 2)))
                        (t (block nil (progn 3))))
                      '(fp:|if| params::x 1 (fp:|if| params::y 2 3))))))

(defun extract-math (ast)
  (nest
   (mapcar {lookup ast})
   (mapcar #'car)
   (translate-partially ast)
   (l:compose (fpcore-lens) (py:translator))))

(defun round-trip (string &optional (n 1))
  (let ((nodes
         (extract-math (convert 'python-ast string))))
    (is (not (emptyp nodes)))
    (is (= n (length nodes)))
    (dolist (ast nodes)
      (is (equal
           ;; TODO Parameterize the math. prefix.
           (string-replace-all "math."
                               (remove-if #'whitespacep (source-text ast))
                               "")
           (remove-if #'whitespacep
                      (let ((l (l:compose (fpcore-lens) (py:translator))))
                        (source-text
                         (l:create l (l:get l ast))))))))))

(deftest fpcore-round-trip-tests ()
  (dolist (string *test-strings*)
    (round-trip string)))

(deftest fpcore-constant ()
  (is (eql 'fp:infinity
           (l:get (l:compose (fpcore-lens) (py:translator))
                  (first
                   (extract-math
                    (convert 'python-ast "math.inf")))))))

(deftest fpcore-pow ()
  "Test translating exponents."
  (let ((l (l:compose (fpcore-lens) (py:translator)))
        (ast (convert 'python-ast "x**2" :deepest t)))
    (is (l:check-laws l ast '(fp:|pow| params::|x| 2)))))
