(defpackage :argot-server/test/utility/tap-test
  (:use :gt/full
        :argot-server/test/base)
  (:documentation "Test suite for TAP parser.")
  (:local-nicknames (:tap :argot-server/utility/tap)))
(in-package :argot-server/test/utility/tap-test)

(defsuite tap-tests "Test TAP support.")

(deftest test-general-format ()
  (let ((output (fmt "~
1..4
ok 1 - Input file opened
not ok 2 - First line of the input valid
ok 3 - Read the rest of the file
not ok 4 - Summarized correctly # TODO Not written yet")))
    (finishes (tap:parse output))
    (finishes (tap:tap-test-results output))))

(deftest test-first-example ()
  (let ((output (fmt "~
1..6
not ok
ok
not ok
ok
ok")))
    (finishes (tap:parse output))
    (finishes (tap:tap-test-results output))))

(deftest test-version ()
  (let ((output "TAP version 13"))
    (is (= 13 (tap:version-number (tap:parse1 output))))))

(deftest test-plan ()
  (let ((output "1..10"))
    (is (= 10 (tap:plan-end (tap:parse1 output))))))

(deftest test-number-only ()
  (let* ((output (fmt "~
1..6
not ok
ok
not ok
ok
ok"))
         (lines (tap:parse output))
         (test-lines (filter (of-type 'tap:test-line) lines)))
    (is (= 5 (length test-lines)))
    (is (= 3 (count-if #'tap:test-line-ok test-lines)))
    (is (= 2 (count-if-not #'tap:test-line-ok test-lines)))
    (is (equal '(1 2 3 4 5)
               (mapcar #'tap:test-line-test-number test-lines)))))

(deftest test-description ()
  (let ((output "ok 42 this is the description of the test"))
    (is (equal "this is the description of the test"
               (tap:test-line-description
                (tap:parse1 output))))))

(deftest test-todo-directive ()
  (let ((output "not ok 13 # TODO bend space and time"))
    (is (string^= "TODO" (tap:test-line-directive
                          (tap:parse1 output))))))

(deftest test-skipping-tests ()
  (let* ((output "ok 23 # skip Insufficient flogiston pressure.")
         (line (tap:parse1 output)))
    (is (typep line 'tap:test-line))
    (ematch line
      ((tap:test-line ok number description directive)
       (is (eql ok t))
       (is (= number 23))
       (is (stringp description))
       (is (emptyp description))
       (is (string-prefix-p "skip" directive))))))

(deftest test-skip-plan ()
  (let ((output "1..0 # Skipped: WWW::Mechanize not installed"))
    (is (equal (tap:plan-directive (tap:parse1 output))
               "Skipped: WWW::Mechanize not installed"))))

(deftest test-bailout ()
  (let ((output "Bail out!"))
    (is (typep (tap:parse1 output) 'tap:bailout)))
  (let* ((output "Bail out! MySQL is not running.")
         (bailout (tap:parse1 output)))
    (is (typep bailout 'tap:bailout))
    (is (equal "MySQL is not running." (tap:bailout-reason bailout)))))

(deftest test-diagnostics ()
  (let* ((output (fmt "~
...
ok 18 - Closed database connection
# End of database section.
# This starts the network part of the test.
# Daemon started on port 2112
ok 19 - Opened socket
...
ok 47 - Closed socket
# End of network tests"))
         (lines (tap:parse output)))
    (is (= 2 (count-if (of-type 'tap:unknown-line) lines)))
    (is (= 4 (count-if (of-type 'tap:diagnostic) lines)))))

(deftest common-example ()
  (let* ((output (fmt "~
1..6
#
# Create a new Board and Tile, then place
# the Tile onto the board.
#
ok 1 - The object isa Board
ok 2 - Board size is zero
ok 3 - The object isa Tile
ok 4 - Get possible places to put the Tile
ok 5 - Placing the tile produces no error
ok 6 - Board size is 1"))
         (lines (tap:parse output)))
    (is (= 6 (count-if (of-type 'tap:test-line) lines)))
    (let ((test-lines (filter (of-type 'tap:test-line) lines)))
      (is (every #'tap:test-line-ok test-lines)))))
