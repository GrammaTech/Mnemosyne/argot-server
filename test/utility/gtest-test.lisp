(defpackage :argot-server/test/utility/gtest-test
  (:use :gt/full
        :argot-server/test/base
        :argot
        :software-evolution-library/software/tree-sitter)
  (:documentation "Test suite for gtest.")
  (:local-nicknames (:gtest :argot-server/utility/gtest)
                    (:test :argot-server/utility/testing)
                    (:l :fresnel/lens)))
(in-package :argot-server/test/utility/gtest-test)
(in-readtable :curry-compose-reader-macros)

(defsuite gtest-tests "Test gtest support.")

(defun ->cpp (ast-type string)
  (find-if (of-type ast-type)
           (convert 'cpp-ast string)))

(deftest test-example-lens ()
  (is (equal (l:get (gtest:example-lens)
                    (->cpp 'cpp-call-expression
                           "ASSERT_EQ(1, corge_desc->field_count());"))
             '(= 1 ((:dot :|field_count| :|corge_desc|))))))

(deftest test-binary-expression ()
  (let ((c (->cpp 'cpp-binary-expression
                  "pool_.BuildFile(file_proto) == nullptr;"))
        (a '("==" ((:dot :|BuildFile| :|pool_|) :|file_proto|)
             :nullptr)))
    (is (equal a (l:get (gtest:example-lens) c)))
    (is (typep (l:create (gtest:example-lens) a)
               'cpp-binary-expression))))

(def inlining-test-test #.(fmt "~
TEST(GeneratedMapFieldTest, DuplicatedValueWireFormat) {
  unittest::TestMap message;
  // Two value fields in wire format
  std::string data = \"\x0A\x06\x08\x01\x10\x01\x10\x02\";

  EXPECT_TRUE(message.ParseFromString(data));
  EXPECT_EQ(1, message.map_int32_int32().size());
  EXPECT_EQ(2, message.map_int32_int32().at(1));
}"))

(defun extract-example (condit)
  (test:test-condition-example (gtest:example-lens) condit))

(deftest test-example-inlining ()
  (with-temporary-file-of (:pathname p :type "cpp")
    inlining-test-test
    (let* ((test
            (first
             (test:tests-for (make 'gtest:gtest)
                             (sel:from-file (make 'cpp) p))))
           (assertions
            (test-case-conditions test)))
      (is (equal (extract-example (first assertions))
                 '(true
                   ((:dot :|ParseFromString| :|message|)
                    "x0Ax06x08x01x10x01x10x02")))))))
