(defpackage :argot-server/test/utility/xunit-test
  (:use :gt/full
        :argot
        :argot-server/test/base)
  (:documentation "Test suite for xUnit XML parser.")
  (:local-nicknames (:xunit :argot-server/utility/xunit)
                    (:test :argot-server/utility/testing)))
(in-package :argot-server/test/utility/xunit-test)
(in-readtable :curry-compose-reader-macros)

(defsuite xunit-tests "Test xUnit support.")

(def +xunit-test-dir+
  (asdf:system-relative-pathname :argot-server "test/etc/xunit/"))

(deftest test-can-parse-all ()
  "Sanity check: can we at least parse everything without errors?"
  (let ((xml-files (directory (path-join +xunit-test-dir+ #p"*.xml"))))
    (do-each (file (reshuffle xml-files))
      (finishes (xunit:parse file)))))

(deftest test-protobufs-passed ()
  "Test with actual output from the protobufs test suite."
  (let ((results (xunit:parse (path-join +xunit-test-dir+ "protobufs.xml"))))
    (is (every (of-type 'passed) results))
    (is (every [(of-length 2) #'test:test-path] results))
    (is (= 2182 (length results)))))
