(defpackage :argot-server/test/unit
  (:use :gt/full
        :argot-server/test/base
        :argot-server/muses/base
        :lsp-server/protocol
        :argot-server/project-root)
  (:import-from :yason)
  (:import-from :argot-server/progress-reporting
                :supports-progress?)
  (:import-from :argot-server/passthrough
                :merge-results
                :map-parallel)
  (:import-from :argot-server/muses/code-actor
                :code-actor
                :applicable-commands)
  (:local-nicknames (:lp :lparallel)
                    (:fpcore :argot-server/utility/fpcore))
  (:import-from :argot-server :all-muses)
  (:import-from :jsonrpc/connection
                :connection)
  (:shadow :true :false))
(in-package :argot-server/test/unit)

(defconst true 'yason:true)
(defconst false 'yason:false)

(defsuite argot-unit-tests "Unit tests that don't relate to a specific muse.")

(deftest test-all-muses ()
  (flet ((same-muses? (list1 list2)
           (set-equal list1 list2
                      :test #'equal
                      :key #'muse-name)))
    (let ((all (all-muses)))
      (is (same-muses? all (all-muses :disable nil)))
      (is (same-muses? all (all-muses :enable all)))
      (is (same-muses? all (all-muses :enable (mapcar #'type-of all))))
      (is (same-muses? all (all-muses :enable (coerce (reshuffle all) 'list))))
      (is (null (all-muses :disable all))))))

(deftest test-guess-language ()
  (is (eql 'python (guess-language "foo.py")))
  (is (eql 'python (guess-language #p"foo.py")))
  (is (eql 'javascript (guess-language "foo.js")))
  (is (eql 'javascript (guess-language #p"foo.js")))
  (is (eql 'typescript-ts (guess-language "foo.ts")))
  (is (eql 'typescript-ts (guess-language #p"foo.ts")))
  (is (eql 'typescript-tsx (guess-language "foo.tsx")))
  (is (eql 'typescript-tsx (guess-language #p"foo.tsx")))
  (is (eql 'lisp (guess-language "foo.lisp")))
  (is (eql 'lisp (guess-language #p"foo.lisp")))
  (is (eql 'lisp (guess-language "foo.cl")))
  (is (eql 'lisp (guess-language #p"foo.cl")))
  (is (eql 'lisp (guess-language "foo.asd")))
  (is (eql 'lisp (guess-language #p"foo.asd"))))

(deftest test-convert-positions-and-source-locations ()
  (let ((source-location (make 'source-location :line 1 :column 1))
        (position (make '|Position| :line 0 :character 0)))
    (is (equal? source-location (convert 'source-location position)))
    (is (equal? position (convert '|Position| source-location)))))

(deftest test-convert-source-range-and-range ()
  (let ((source-range
         (make 'source-range
               :begin (make 'source-location :line 1 :column 1)
               :end (make 'source-location :line 10 :column 10)))
        (lsp-range
         (make '|Range|
               :start (make '|Position|
                            :line 0
                            :character 0)
               :end (make '|Position|
                          :line 9
                          :character 9))))
    (is (equal? source-range (convert 'source-range lsp-range)))
    (is (equal? lsp-range (convert '|Range| source-range)))))

(deftest test-supports-progress ()
  (is (supports-progress?
       (make '|InitializeParams|
             :capabilities
             (make '|ClientCapabilities|
                   :window (make '|WindowClientCapabilities|
                                 :|workDoneProgress| t))
             ;; Required arguments.
             :|processId| 1
             :|rootPath| ""
             :|rootUri| "file://"))))

(deftest test-merge-sync-results ()
  (is (= 0 (gethash "textDocumentSync"
                    (merge-results
                     (dict "textDocumentSync" nil)
                     (dict "textDocumentSync" nil)))))
  (is (= 0 (gethash "textDocumentSync"
                    (merge-results
                     (dict "textDocumentSync" 1)
                     (dict "textDocumentSync" nil)))))
  (is (= 1 (gethash "textDocumentSync"
                    (merge-results
                     (dict "textDocumentSync" 1)
                     (dict "textDocumentSync" 2)))))
  (is (= 0 (gethash "textDocumentSync"
                    (merge-results
                     (dict "textDocumentSync" 0)
                     (dict "textDocumentSync" 2)))))
  (is (= 2 (href
            (merge-results
             (dict "textDocumentSync" 2)
             (dict "textDocumentSync"
                   (dict "openClose" t
                         "change" 2)))
            "textDocumentSync" "change")))
  (is (= 0 (href
            (merge-results
             (dict "textDocumentSync" 2)
             (dict "textDocumentSync"
                   (dict "openClose" t)))
            "textDocumentSync" "change"))))

(deftest test-merge-false ()
  (is (null (merge-results nil 'yason:false)))
  (is (null (merge-results 'yason:false nil)))
  (is (vectorp (merge-results #() 'yason:false)))
  (is (vectorp (merge-results 'yason:false #()))))

(deftest test-merge-completion-lists ()
  ;; Disjoint, complete.
  (let ((x (dict "label" "x"))
        (y (dict "label" "y"))
        (z (dict "label" "z")))
    (is (equal? (dict "items" (list x y z)
                      "isIncomplete" false)
                (merge-results
                 (dict "items" (list x)
                       "isIncomplete" false)
                 (dict "items" (list y z)
                       "isIncomplete" false))))
    ;; Not disjoint, complete.
    (is (equal? (dict "items" (list x y z)
                      "isIncomplete" false)
                (merge-results
                 (dict "items" (list x y)
                       "isIncomplete" false)
                 (dict "items" (list y z x)
                       "isIncomplete" false))))
    ;; Not complete.
    (is (equal? (dict "items" (list x y z)
                      "isIncomplete" true)
                (merge-results
                 (dict "items" (list x)
                       "isIncomplete" true)
                 (dict "items" (list y z)
                       "isIncomplete" false))))
    (is (equal? (dict "items" (list x y z)
                      "isIncomplete" true)
                (merge-results
                 (dict "items" (list x)
                       "isIncomplete" false)
                 (dict "items" (list y z)
                       "isIncomplete" true))))
    (is (equal? (dict "items" (list x y z)
                      "isIncomplete" true)
                (merge-results
                 (dict "items" (list x)
                       "isIncomplete" true)
                 (dict "items" (list y z)
                       "isIncomplete" true))))
    ;; Merging a complete table with a list.
    (is (equal? (dict "items" (list x y z)
                      "isIncomplete" false)
                (merge-results
                 (dict "isIncomplete" false
                       "items" (list x))
                 (list y z))))
    ;; Merging an incomplete table with a list.
    (is (equal? (dict "items" (list x y z)
                      "isIncomplete" true)
                (merge-results
                 (dict "isIncomplete" true
                       "items" (list x))
                 (list y z))))))

(deftest test-merge-hovers ()
  (is (equal? (dict "contents" '("hover1" "hover2"))
              (merge-results
               (dict "contents" '("hover1"))
               (dict "contents" '("hover2")))))
  (is (equal? (dict "contents" '("hover1" "hover2" "hover3"))
              (merge-results
               (dict "contents" '("hover1" "hover2"))
               (dict "contents" '("hover2" "hover3"))))))

(deftest (test-map-parallel :long-running t) ()
  (let ((lp:*kernel* (lp:make-kernel 3)))
    (unwind-protect
         (is (equal? (filter #'oddp (serapeum:range 20))
                     (remove nil
                             (map-parallel (lambda (x)
                                             (if (oddp x) x (sleep 30)))
                                           (serapeum:range 20)
                                           :timeout 5))))
      (lp:end-kernel))))

(defclass broken-muse (code-actor)
  ())

(defmethod muse-commands :around ((muse broken-muse))
  nil)

(defmethod applicable-commands ((muse broken-muse) &key start end text-document)
  (declare (ignore start end text-document))
  (list
   (make '|Command|
         :title "Command"
         :arguments (list)
         :command "unknown-command")))

(deftest test-should-register-commands ()
  (signals warning
    (applicable-commands (make 'broken-muse))))

(defclass on-change-muse (muse)
  ((count :initform 0)))

(defmethod on-change progn ((muse on-change-muse) uri &key)
  (declare (ignore uri))
  (with-slots (count) muse
    (incf count)))

(deftest test-on-change-handler ()
  "Test that the on-change generic function is called after a change."
  (let ((muse (make 'on-change-muse)))
    (with-slots (count) muse
      (is (= 0 count))
      (jsonrpc:dispatch muse
                        (jsonrpc:make-request
                         :method "textDocument/didOpen"
                         :params
                         (make '|DidOpenTextDocumentParams|
                               :text-document
                               (make '|TextDocumentItem|
                                     :uri "file://whatever"
                                     :language-id "COBOL"
                                     :version 0
                                     :text "ADD 2 TO SUM"))))
      (is (= 1 count))
      (jsonrpc:dispatch muse
                        (jsonrpc:make-request
                         :method "textDocument/didChange"
                         :params
                         (make '|DidChangeTextDocumentParams|
                               :text-document
                               (make '|VersionedTextDocumentIdentifier|
                                     :uri "file://whatever"
                                     :version 1)
                               :content-changes nil)))
      (is (= 2 count)))))

(deftest test-extract-workspace-folders ()
  "Test that we can extract the workspace folders from an
InitializeParams instance."
  (is (equal '("file://tmp/foo")
             (extract-workspace-folders
              (make '|InitializeParams|
                    :process-id 0
                    :root-uri nil
                    :capabilities (make '|ClientCapabilities|)
                    :workspace-folders
                    (list (make '|WorkspaceFolder|
                                :uri "file://tmp/foo"
                                :name "foo")))))))

(deftest test-root-uri-overrides-root-path ()
  "Test that rootUri overrides rootPath when both are provided."
  (is (equal '("file://tmp/foo")
             (extract-workspace-folders
              (make '|InitializeParams|
                    :process-id 0
                    :capabilities (make '|ClientCapabilities|)
                    :root-uri "file://tmp/foo"
                    :root-path "/tmp/bar")))))

(deftest test-workspace-folders-includes-root-uri ()
  "Test that rootUri is included along with WorkspaceFolders."
  (is (equal '("file://tmp/foo" "file://tmp/bar")
             (extract-workspace-folders
              (make '|InitializeParams|
                    :process-id 0
                    :capabilities (make '|ClientCapabilities|)
                    :workspace-folders
                    (list (make '|WorkspaceFolder|
                                :uri "file://tmp/foo"
                                :name "foo"))
                    :root-uri "file://tmp/bar")))))

(deftest test-extract-root-for ()
  "Test that we can get the correct root for a file, even when there
is more than one candidate (nested roots; is this possible?)."
  (is (equal "file://tmp/foo/sub1/"
             (extract-project-root-for
              "file://tmp/foo/sub1/myfile"
              (make '|InitializeParams|
                    :process-id 0
                    :capabilities (make '|ClientCapabilities|)
                    :workspace-folders
                    ;; Note that we want not the first match but the
                    ;; longest match.
                    (list (make '|WorkspaceFolder|
                                :uri "file://tmp/foo"
                                :name "foo")
                          (make '|WorkspaceFolder|
                                :uri "file://tmp/foo/sub1/"
                                :name "foo_1"))
                    :root-uri "file://tmp/bar")))))

(deftest test-extract-workspace-folder ()
  "Test that we don't return a matching root when no root matches."
  (is (null (extract-project-root-for
             "file://tmp/foo/sub1/myfile"
             (make '|InitializeParams|
                   :process-id 0
                   :capabilities (make '|ClientCapabilities|)
                   :root-uri "file://tmp/bar")))))

(deftest extract-workspace-folders-deduplicates ()
  "Test that project roots are deduplicated."
  (is (equal '("file://tmp/foo")
             (extract-workspace-folders
              (make '|InitializeParams|
                    :process-id 0
                    :capabilities (make '|ClientCapabilities|)
                    :workspace-folders
                    (list (make '|WorkspaceFolder|
                                :uri "file://tmp/foo"
                                :name "foo"))
                    :root-uri "file://tmp/foo")))))

(deftest test-project-root-for ()
  "Test that project-root-for falls back to the containing directory."
  ;; A precondition of the test, not a test itself.
  (assert (not (boundp '*connection*)))
  (is (equal "file:///home/nemo"
             (project-root-for "file:///home/nemo/file.js")))
  (is (equal "file:///home/nemo"
             (project-root-for "file:///home/nemo/file")))
  (is (equal "file:///"
             (project-root-for "file:///file.js"))))

(deftest test-write-project-snapshot ()
  "Test that we can write out a project snapshot."
  (let ((snapshot
         (dict "file:///home/user/rando/project/file1.txt"
               '(:string "my file"
                 :uri "file:///home/user/rando/project/file1.txt"
                 :root "file:///home/user/rando/project")
               "file:///home/user/rando/project/subdir/file2.txt"
               '(:string "my other file"
                 :uri "file:///home/user/rando/project/subdir/file2.txt"
                 :root "file:///home/user/rando/project"))))
    (with-temporary-directory (:pathname dir)
      (write-project-snapshot snapshot (pathname dir))
      (is (directory-exists-p dir))
      (is (file-exists-p (path-join dir "file1.txt")))
      (is (file-exists-p (path-join dir "subdir/file2.txt")))
      (is (equal "my file"
                 (read-file-into-string
                  (path-join dir "file1.txt"))))
      (is (equal "my other file"
                 (read-file-into-string
                  (path-join dir "subdir/file2.txt"))))
      ;; Check that writing into a non-empty directory causes an
      ;; error.
      (signals error
        (write-project-snapshot snapshot (pathname dir))))))

(deftest test-dont-write-project-snapshot/dots ()
  "Test that we reject pathnames where .. would take us outside the
destination directory."
  (let ((snapshot
         (dict "file:///home/user/rando/project/../file1.txt"
               '(:string "my file"
                 :uri "file:///home/user/rando/project/../file1.txt"
                 :root "file:///home/user/rando/project")
               "file:///home/user/rando/project/subdir/../file2.txt"
               '(:string "my other file"
                 :uri "file:///home/user/rando/project/subdir/../file2.txt"
                 :root "file:///home/user/rando/project"))))
    (with-temporary-directory (:pathname dir)
      (signals error
        (write-project-snapshot snapshot (pathname dir))))))

(deftest test-dont-write-project-snapshot/multiple-roots ()
  "Test that we reject snapshots with multiple roots."
  (let ((snapshot
         (dict "file:///home/user/rando/project1/file1.txt"
               '(:string "my file"
                 :uri "file:///home/user/rando/project/file1.txt"
                 :root "file:///home/user/rando/project1")
               "file:///home/user/rando/project2/subdir/file2.txt"
               '(:string "my other file"
                 :uri "file:///home/user/rando/project/subdir/file2.txt"
                 :root "file:///home/user/rando/project2"))))
    (with-temporary-directory (:pathname dir)
      (signals error
        (write-project-snapshot snapshot (pathname dir))))))

(defun test-file-action-annotation (orig)
  "Test that annotating ORIG preserves all its properties."
  (let* ((annotated
          (annotate-workspace-edit
           "Annotation"
           (make '|WorkspaceEdit|
                 :document-changes
                 (list orig))
           :always t)))
    (ematch annotated
      ((|WorkspaceEdit|
        :change-annotations hash-table
        :document-changes (list change))
       (let* ((alist (hash-table-alist hash-table))
              (annotation (cdar alist))
              (annotation-id (caar alist))
              (annotation-label (slot-value annotation '|label|)))
         ;; The annotation is present.
         (is (typep annotation '|ChangeAnnotation|))
         ;; The ID and label are present.
         (is (stringp annotation-id))
         (is (stringp annotation-label))
         ;; The class is preserved.
         (is (typep change (type-of orig)))
         ;; The JSON rendering is preserved.
         (is (equal? (convert 'hash-table orig)
                     (delete-from-hash-table
                      (convert 'hash-table change)
                      "annotationId")))
         ;; The change's ID equals the workspace edit's ID.
         (is (equal (slot-value change '|annotationId|)
                    annotation-id)))))))

(deftest test-annotate-create-file ()
  (test-file-action-annotation
   (make '|CreateFile| :uri "file://newfile")))

(deftest test-annotate-create-file/options ()
  (test-file-action-annotation
   (make '|CreateFile| :uri "file://newfile"
                       :options (make '|CreateFileOptions|
                                      :overwrite t))))
(deftest test-annotate-delete-file ()
  (test-file-action-annotation
   (make '|DeleteFile| :uri "file://newfile")))

(deftest test-annotate-rename-file ()
  (test-file-action-annotation
   (make '|RenameFile| :old-uri "file://oldfile"
                       :new-uri "file://newfile")))

(deftest test-annotate-rename-file/options ()
  (test-file-action-annotation
   (make '|RenameFile| :old-uri "file://oldfile"
                       :new-uri "file://newfile"
                       :options (make '|RenameFileOptions|
                                      :overwrite t))))
