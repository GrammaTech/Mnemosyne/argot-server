(defpackage :argot-server/test/transcript
  (:use :gt/full)
  (:export :transcript
           :transcript-tag
           :+client-request-tag+
           :+client-reply-tag+
           :+client-notification-tag+
           :+server-request-tag+
           :+server-reply-tag+
           :+server-notification-tag+
           :transcript-entry)
  (:documentation "Type definitions for transcripts."))
(in-package :argot-server/test/transcript)

(deftype transcript-tag ()
  '(tuple
    (member :server :client)
    (member :request :reply :notification)))

(defconst +client-request-tag+ '(:client :request)
  "Tag for client request.")
(defconst +client-reply-tag+ '(:client :reply)
  "Tag for client reply.")
(defconst +client-notification-tag+ '(:client :notification)
  "Tag for client notification.")
(defconst +server-request-tag+ '(:server :request)
  "Tag for server request.")
(defconst +server-reply-tag+ '(:server :reply)
  "Tag for server reply.")
(defconst +server-notification-tag+ '(:server :notification)
  "Tag for server notification.")

(deftype transcript-entry ()
  '(cons transcript-tag hash-table))

(deftype transcript ()
  '(or null (cons transcript-entry list)))
