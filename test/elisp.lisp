(defpackage :argot-server/test/elisp
  (:use)
  (:export :|nil| :|t| :|data|)
  (:documentation "Package for reading in Elisp forms"))
