(defpackage :argot-server/test/bad-muses
  (:use :gt/full
        :argot-server/readtable
        :argot-server/muses/base
        :argot-server/muses/completionist
        :argot-server/muses/code-actor
        :argot-server/muses/passthrough
        :argot-server/muses/background-thread-muse)
  (:export :bad-completion
           :restartable-bad-muse)
  (:documentation "Muses that deliberately misbehave, for testing purposes."))
(in-package :argot-server/test/bad-muses)
(in-readtable argot-readtable)

(defclass bad-completion (completionist)
  ((bad :type boolean :initarg :bad))
  (:documentation "A muse that hangs the first time it is asked for a completion.")
  (:default-initargs
   :bad t))

(defmethod complete ((c bad-completion) &key symbol start end)
  (declare (ignore end))
  (with-slots (bad) c
    (if bad
        (sleep most-positive-fixnum)
        (mapcar {concatenate 'string symbol}
                (list (buffer-to-string (point-buffer start)))))))

(defmethod muse-timed-out :before
    ((c bad-completion) callback &key &allow-other-keys)
  (with-slots (bad) c
    (setf bad nil))
  (funcall callback))

(defclass restartable-bad-muse (tcp-muse
                                background-thread-muse
                                bad-completion)
  ()
  (:default-initargs :name "bad"))

(define-class-method "initialize" restartable-bad-muse (params InitializeParams)
  (make 'InitializeResult :capabilities (make 'ServerCapabilities)))

(defmethod muse-available? :around ((muse restartable-bad-muse))
  t)

(defmethod muse-lang ((muse restartable-bad-muse))
  t)

(defmethod muse-foreground ((muse restartable-bad-muse))
  (with-slots (bad) muse
    (jsonrpc:server-listen
     (make 'bad-completion :bad bad)
     :port (muse-port muse)
     :mode :tcp)))
