(defpackage :argot-server/test/integration
  (:use :gt/full
        :lsp-server/protocol
        :argot-server/config
        :argot-server/readtable
        :argot-server/test/base
        :argot-server/test/bad-muses
        :software-evolution-library/software/tree-sitter
        :software-evolution-library/software/python
        :software-evolution-library/software/cpp
        :software-evolution-library/software/project
        ;; Muses.
        :argot-server/muses/argument-predictor
        :argot-server/muses/autocomplete
        :argot-server/muses/cl-lsp
        :argot-server/muses/fast-check
        :argot-server/muses/galois-autocomplete
        :argot-server/muses/godbolt-muse
        :argot-server/muses/herbie
        :argot-server/muses/hypothesis-tests
        :argot-server/muses/jest-test-view
        :argot-server/muses/jsls
        #+lambdanet :argot-server/muses/lambdanet
        :argot-server/muses/muse
        :argot-server/muses/project-ast-muse
        :argot-server/muses/pyls
        :argot-server/muses/refactoring-mutations
        #+regel :argot-server/muses/regel
        :argot-server/muses/snippet-mining
        :argot-server/muses/ssr-muse
        :argot-server/muses/trinity
        :argot-server/muses/typewriter)
  (:local-nicknames (:tw :argot-server/muses/typewriter)
                    (:tr :argot-server/muses/type-refinement)
                    (:godbolt :argot-server/utility/godbolt))
  (:import-from :argot-server/muses/incrementalizer :incrementalize)
  (:import-from :argot-server/server :all-muses)
  (:import-from :lsp-server
                :get-buffer-from-uri)
  (:import-from :argot-server/utility
                :*allow-local*))
(in-package :argot-server/test/integration)
(in-readtable argot-readtable)

(defsuite argot-integration-tests
    "Integration tests for the Argot server.")

(deftest print-info-hack ()
  "This pseudo-test exists solely to print useful information."
  (let ((cpus (count-cpus)))
    (is (> cpus 0))
    (format t "~%CPU count: ~a~%" cpus))
  (format t "Network: ~a~%" *network*)
  (mvlet* ((muses (all-muses))
           (on off (partition #'muse-available? muses)))
    (format t "Available muses: ~{~a~^, ~}~%" (mapcar #'muse-name on))
    (format t "Unavailable muses: ~{~a~^, ~}~%" (mapcar #'muse-name off)))
  (format t "PATH: ~a~%" (getenv "PATH")))

(define-timed-test test-pyls-completion ()
  "Test that we can pass through to the Python language server."
  (rerun-baseline (path-join log-dir #p"eglot-pyls.log")
                  :enable '(pyls)))

(define-timed-test test-js ()
  "Test that we can use javascript-tyepscript-server over stdio."
  (rerun-baseline (path-join log-dir #p"eglot-jts.log")
                  :enable '(jsls)))

#+sbcl
(define-timed-test test-trinity-js ()
  (rerun-baseline (path-join log-dir #p"eglot-trinity-js.log")
                  :enable '(trinity)))

(defun test-string-predictions (string)
  (with-temporary-muse (muse (make 'tw:typewriter-muse))
    (format *error-output* "~&Running TypeWriter... ")
    (force-output *error-output*)
    (let ((predictions (tw:string-predictions muse string)))
      (format *error-output* "done~%")
      (force-output *error-output*)
      (is (stringp predictions))
      (finishes
       (yason:parse predictions)))))

(define-timed-test test-type-writer-muse ()
  (when (muse-available? 'typewriter-muse)
    (test-string-predictions "import sys")
    (let* ((string
            (read-file-into-string
             (system-relative-pathname
              :argot-server "test/etc/source_test_code.py"))))
      (test-string-predictions string))))

(def +typewriter-predictions-example+
  (dict "get_colors"
        (dict "return"
              (list (dict "SearchStrategy[str]" 3
                          "SearchStrategy[Ex]" 2
                          "\"APIKeyModel\"" 1
                          "Optional[float]" 0)
                    '(13 35)))
        "find_match"
        (dict "return"
              (list (dict
                     "SearchStrategy[str]" 3
                     "SearchStrategy[Ex]" 2
                     "\"APIKeyModel\"" 1
                     "Optional[float]" 0)
                    '(1 15))
              "color"
              (list (dict
                     "Sprite" 3
                     "SplitterFnType" 2
                     "\"AtomicInput\"" 1
                     "UserProfile" 0)
                    '(1 15)))))

(defun test-refinement (string raw-predictions)
  (with-temporary-muse (muse (make 'tr:type-refinement-muse))
    (format *error-output* "~&Running TypeRefinement... ")
    (force-output *error-output*)
    (let ((predictions
           (tr:refined-string-predictions muse string raw-predictions)))
      (format *error-output* "done~%")
      (force-output *error-output*)
      (is (stringp predictions))
      (finishes
       (yason:parse predictions)))))

(define-timed-test test-type-refinement-muse ()
  (when (muse-available? 'tr:type-refinement-muse)
    (let* ((string
            (read-file-into-string
             (system-relative-pathname
              :argot-server "test/etc/source_test_code.py")))
           (predictions +typewriter-predictions-example+))
      (test-refinement string predictions))))

(define-timed-test test-refactoring-mutation-sees-whole-file ()
  (rerun-baseline
   (path-join log-dir #p"eglot-replace-inline-code.log")
   :enable '(refactoring-mutations)))

(define-timed-test test-argument-predictor ()
  (rerun-baseline
   (path-join log-dir #p"eglot-argument-prediction-at-point.log")
   :enable '(argument-predictor)))

(define-timed-test test-galois-autocomplete ()
  (when (> (count-cpus) 1)
    (rerun-baseline
     (path-join log-dir #p"eglot-galois-autocomplete-at-point.log")
     :enable '(galois-autocomplete))))

(define-timed-test test-autocomplete ()
  (when (> (count-cpus) 1)
    (rerun-baseline
     (path-join log-dir #p"eglot-argument-prediction-at-point.log")
     :enable '(autocomplete))
    (rerun-baseline
     (path-join log-dir #p"eglot-galois-autocomplete-at-point.log")
     :enable '(autocomplete))
    (rerun-baseline
     (path-join log-dir #p"eglot-autocomplete.log")
     :enable '(autocomplete))))

(define-timed-test test-hypothesis-tests ()
  (rerun-baseline
   (path-join log-dir #p"eglot-hypothesis-tests.log")
   :enable (list (make 'hypothesis-tests))))

(define-timed-test test-snippet-mining ()
  (rerun-baseline
   (path-join log-dir #p"vscode-snippet-mining.json")
   :enable '(snippet-mining)
   :format :vscode))

(define-timed-test test-vscode-refactoring ()
  (rerun-baseline
   (path-join log-dir #p"vscode-refactoring.json")
   :enable '(refactoring-mutations)
   :format :vscode))

#+(or)
(define-timed-test test-fast-check-and-jest-in-vscode ()
  (rerun-baseline
   (path-join log-dir #p"vscode-jest-test.json")
   :enable '(jest-test-view fast-check)
   :await-annotations-from "Fast Check"
   :format :vscode))

#+(or )       ; FIXME: How to write transparent modifiable muse tests.
(define-timed-test test-vscode-ssr-muse ()
  (rerun-baseline
   (path-join log-dir #p"vscode-ssr-muse.json")
   :enable '(ssr-muse)
   :format :vscode))

(deftest test-internal-muse-commands ()
  (let ((muse (make 'refactoring-mutations)))
    (is (equal '(:argot.refactoring-mutations) (muse-commands muse)))
    (is (muse-accepts-command-p muse :argot.refactoring-mutations))
    (let ((good-command (make 'Command
                              :title "My command"
                              :command "ARGOT.REFACTORING-MUTATIONS"))
          (bad-command (make 'Command
                             :title "My command"
                             :command "BAD")))
      (is (muse-accepts-command-p muse good-command))
      (is (not (muse-accepts-command-p muse bad-command)))
      (is (muse-accepts-command-p muse (convert 'hash-table good-command)))
      (is (not (muse-accepts-command-p muse (convert 'hash-table bad-command))))
      (is (muse-accepts-command-p muse (dict))))))

(define-timed-test test-external-muse-commands ()
  "Test that we can get the declared commands from an external muse."
  (let ((muse (make 'hypothesis-tests)))
    (when (muse-available? muse)
      (with-throwaway-server (client :enable (list muse)
                                     :initialize-as :eglot)
        (declare (ignore client))
        (is (subsetp '("generate_hypothesis_test")
                     (muse-commands muse)
                     :test #'string-equal))
        (muse-accepts-command-p muse "generate_hypothesis_test")
        (not (muse-accepts-command-p muse "some_other_command"))
        (let ((good-command (make 'Command
                                  :title "Good command"
                                  :command "generate_hypothesis_test"))
              (bad-command (make 'Command
                                 :title "Bad command"
                                 :command "bad_command")))
          (is (muse-accepts-command-p muse good-command))
          (is (not (muse-accepts-command-p muse bad-command)))
          (is (muse-accepts-command-p muse (convert 'hash-table good-command)))
          (is (not (muse-accepts-command-p muse
                                           (convert 'hash-table bad-command))))
          (is (muse-accepts-command-p muse (dict))))))))

(defun test-hung-muse (client muse)
  (let* ((uri "file://tmp/whatever.py")
         (text "prefix-")
         (tdi (make 'TextDocumentIdentifier
                    :uri uri))
         (pos (make 'Position
                    :line 0
                    :character (length text))))
    (nest
     (jsonrpc:notify client "textDocument/didOpen")
     (convert 'hash-table)
     (make 'DidOpenTextDocumentParams :textDocument)
     (make 'TextDocumentItem
           :uri uri
           :languageId "python"
           :version 0
           :text text))
    (flet ((get-completion ()
             (nest
              (jsonrpc:call client "textDocument/completion")
              (convert 'hash-table)
              (make 'CompletionParams
                    :textDocument tdi
                    :position pos))))
      (format t "Waiting for timeout...~%")
      (force-output)
      (is (null (get-completion)))
      (loop while (muse-hung? muse))
      (let ((response (get-completion)))
        (is (hash-table-p response))
        (is (equal (string+ text text)
                   (nest
                    (gethash "insertText")
                    (first)
                    (gethash "items")
                    response)))))))

;; (define-timed-test test-reset-bad-completion ()
;;   (let ((muse (make 'bad-completion)))
;;     (with-throwaway-server (client :enable (list muse)
;;                                    :initialize-as :eglot
;;                                    :timeout 10)
;;       (test-hung-muse client muse))))

;; (define-timed-test test-reinitialize-bad-completion ()
;;   (let ((muse (make 'restartable-bad-muse)))
;;     (with-throwaway-server (client :enable (list muse)
;;                                    :initialize-as :eglot
;;                                    :timeout 10)
;;       (test-hung-muse client muse))))

(define-timed-test test-herbie-simple ()
  "Test that Herbie can synthesize an expression."
  (rerun-baseline
   (path-join log-dir #p"vscode-herbie-simple.json")
   :enable (list (make 'herbie-muse
                       :seed 42 :temp "herbie_temp_42"))
   :format :vscode))

(define-timed-test test-godbolt-api ()
  (unless (muse-available? 'godbolt-muse)
    (return-from test-godbolt-api))
  (with-temporary-muse (g (make 'godbolt-muse))
    (let ((godbolt:*godbolt* (muse-client g)))
      (let ((lang (muse-lang g)))
        (is (subtypep 'cpp lang))
        (is (subtypep 'python lang)))
      (let ((compiler
             (find "clangdefault" (godbolt:compilers)
                   :key #'godbolt:id
                   :test #'equal)))
        (is compiler)
        (let* ((output (godbolt:compile compiler (fmt "~
int trivial() {
  return 1;
}")))
               (output-string
                (mapconcat #'godbolt:asm-text (godbolt:output-asm output)
                           (string #\Newline))))
          (every (op (string~= _ output-string))
                 '("push" "mov" "pop" "ret")))))))

(defclass project-ast-muse/muse (project-ast-muse muse)
  ())

(define-timed-test test-project-ast-muse ()
  "Test a project-ast-muse adding new files to its project AST."
  (let ((m (make 'project-ast-muse/muse))
        (old-allow-local argot-server/utility::*allow-local*))
    (setf argot-server/utility::*allow-local* t)
    (unwind-protect
         (nest
          (with-temporary-directory (:pathname d)
            (write-string-into-file "print(\"hello\")"
                                    (path-join d "hello.py")))
          (with-throwaway-server (client :enable (list m)
                                         :root d))
          (let* ((root-uri (string+ "file://" d))
                 (new-file-uri (string+ root-uri "file.py"))
                 old-ast))
          (progn
            ;; Send a new file.
            (nest
             (jsonrpc:notify client "textDocument/didOpen")
             (make 'DidOpenTextDocumentParams :textDocument)
             (make 'TextDocumentItem
                   :uri new-file-uri
                   :languageId "python"
                   :version 0
                   :text "#!/usr/bin/env python3"))
            ;; Wait until the file exists.
            (iter (sleep 1)
                  (repeat 5)
                  (until (setf old-ast (project-ast root-uri))))
            (let ((project-ast (is (ensure-project-ast root-uri))))
              (is (evolve-files-ref project-ast "file.py"))
              (is (lookup project-ast "file.py"))
              (is (equal (source-text (lookup project-ast "file.py"))
                         "#!/usr/bin/env python3")))
            (nest
             (jsonrpc:notify client "workspace/didDeleteFiles")
             (make 'DeleteFilesParams :files)
             (list)
             (make 'FileDelete :uri new-file-uri))
            ;; Wait for the file to be deleted.
            (iter (sleep 1)
                  (repeat 5)
                  (while (eql old-ast (project-ast root-uri))))
            (let ((project-ast (is (ensure-project-ast root-uri))))
              (is (null (evolve-files-ref project-ast "file.py")))
              (is (null (lookup project-ast "file.py")))
              (is (evolve-files-ref project-ast "hello.py")))))
      (setf *allow-local* old-allow-local))))
