(defpackage :argot-server/test/annotations
  (:use :gt/full
        :lsp-server/protocol
        :argot-server/readtable
        :argot
        :argot-server/annotation-host
        :argot-server/test/base)
  (:import-from :argot-server/annotation-host
                :best-type-at)
  (:import-from :lsp-server/test-utils
                :with-temporary-buffer
                :with-temporary-server))
(in-package :argot-server/test/annotations)
(in-readtable argot-readtable)

(defsuite annotations-tests "Test annotation support.")

(defun make-annotation (uri line1 col1 line2 col2 contributions)
  (make 'Annotation
        :textDocument (make 'TextDocumentIdentifier :uri uri)
        :range (make 'Range
                     :start (make 'Position
                                  :line line1
                                  :character col1)
                     :end (make 'Position
                                :line line2
                                :character col2))
        :contributions contributions))

(defun make-type-contribution (type)
  (make 'Contribution
        :source "No Muse"
        :kind ArgotKind.Types
        :data (make 'TypeCandidate
                    :typeName type
                    :probability 1)))

(deftest test-annotation ()
  (let* ((text "x y z")
         (uri "file://some-file")
         (annotations
          (list
           (make-annotation uri 0 0 0 1
                            (list (make-type-contribution "x-type")))
           (make-annotation uri 0 2 0 3
                            (list (make-type-contribution "y-type")))
           (make-annotation uri 0 4 0 5
                            (list (make-type-contribution "z-type"))))))
    (with-temporary-buffer (b
                            :text text
                            :uri uri
                            :server-class 'annotation-host)
      (declare (ignore b))
      (is (not (has-type-contributions? *server* uri)))
      (did-annotate *server* annotations)
      (is (has-type-contributions? *server* uri))
      (let ((new-annotations (get-annotations *server* uri)))
        (is (set-equal annotations new-annotations
                       :test #'equal?)))
      (is (has-type-contributions? *server* uri))
      (is (equal "x-type"
                 (best-type-at uri
                               (make 'Position :line 0 :character 1)))))))
