(defpackage :argot-server/annotation-host
  (:use :gt/full
        :lsp-server
        :lsp-server/protocol
        :lsp-server/utilities/lsp-utilities
        :argot
        :argot-server/utility
        :argot-server/merge-results
        :argot-server/readtable
        :software-evolution-library/utility/range)
  (:import-from :software-evolution-library/software/parseable
                :source-text)
  (:import-from :software-evolution-library
                :genome)
  (:import-from :bordeaux-threads :make-recursive-lock)
  (:import-from :lsp-server.lem-base
                :buffer-start-point
                :buffer-end-point
                :point-buffer
                :point
                :point=
                :point>=
                :point>
                :point<=
                :point<
                :delete-point)
  (:import-from :jsonrpc
                :make-request
                :dispatch)
  (:import-from :argot-server/muses/muse
                :contribute-server-capabilities)
  (:export :annotation-host
           :get-annotations
           :did-annotate
           :has-type-contributions?
           :contributions-by-kind
           :contributions-by-source
           :has-contributions-by-kind-p
           :has-contributions-by-source-p
           :dump-string-annotations
           :contribution-source
           :contribution-kind
           :contribution-key
           :contributions))
(in-package :argot-server/annotation-host)
(in-readtable argot-readtable)

;;; TODO Annotations should be hosted in a more reasonable data structure.

(defstruct annotation
  "Internal representation of an annotation for a given range in a buffer."
  (start (required-argument 'start) :type point :read-only t)
  (end (required-argument 'end) :type point :read-only t)
  (contributions nil :type list))

(defclass annotation-host (lsp-server)
  ((annotations :initform (make-hash-table :test 'equal)
                :accessor annotations-table))
  (:documentation "Mixin for a server that stores annotations."))

(defmethod contribute-server-capabilities list ((muse annotation-host))
  (make 'ServerCapabilities
        :textDocumentSync |TextDocumentSyncKind.Incremental|
        :hoverProvider t))

(define-class-method "textDocument/didClose" annotation-host
    (params DidCloseTextDocumentParams)
  ())

(define-class-method "shutdown" annotation-host (params)
  ())

(defun ensure-uri-buffer (uri)
  "If URI does not already have a buffer, fake a didOpen event."
  (or (get-buffer-from-uri uri)
      (progn (force-did-open-file uri)
             (get-buffer-from-uri uri :error t))))

;;; This won't be necessary once we support annotating unopened files.
(defun force-did-open-file (uri)
  "Fake a didOpen event for URI."
  ;; TODO This will break if we ever want to support running Argot
  ;; Server remotely.
  (nest
   (let* ((path (file-uri-path uri))
          ;; TODO Encoding?
          (text (read-file-into-string path))))
   (dispatch *server*)
   (make-request :method "textDocument/didOpen"
                 :params)
   (convert 'hash-table)
   (make 'DidOpenTextDocumentParams
         :textDocument (make 'TextDocumentItem
                             :uri uri
                             :version 0
                             :languageId ""
                             :text text))))

(defmethod handle :before
    ((muse annotation-host)
     (method (eql #.(make-keyword +argot/didAnnotate+)))
     (params DidAnnotateParams))
  (nest
   (with-slots ((table annotations)) *server*)
   (ematch params)
   ((DidAnnotateParams :annotations annotations))
   (iter (for annotation in annotations))
   (mvlet* ((uri start end contributions
             (dissect-lsp-annotation annotation))
            (buffer (ensure-uri-buffer uri))
            (start (if start
                       (position->point buffer start)
                       (buffer-start-point buffer)))
            (end (if end
                     (position->point buffer end)
                     (buffer-end-point buffer)))))
   (synchronized (*server*))
   (let* ((annotation (ensure-annotation start end))
          (all-contributions
           (append (annotation-contributions annotation)
                   contributions))
          (all-contributions
           ;; Keep the last (newest) instance only.
           (remove-duplicates all-contributions
                              :test #'contribution=))))
   (setf (annotation-contributions annotation)
         all-contributions)))

(defun contribution= (c1 c2)
  "Are two contributions equal?

Two contributions are equal if they are contributed by the same muse,
and if they either (1) have no IDs or (2) have the same ID."
  (and (equal (slot-value c1 '|source|)
              (slot-value c2 '|source|))
       (if (slot-boundp c1 '|key|)
           (if (slot-boundp c2 '|key|)
               (equal (slot-value c1 '|key|)
                      (slot-value c2 '|key|))
               nil)
           (not (slot-boundp c2 '|key|)))))

(defun annotation-dead? (annot)
  "Is ANNOT dead?
Dead annotations are those where start=end."
  (let ((start (annotation-start annot))
        (end (annotation-end annot)))
    (point= start end)))

(defun gc-annotations (annotations)
  "Remove any dead annotations (per `annotation-dead?`)."
  (iter (for annot in annotations)
        (if (annotation-dead? annot)
            (progn
              (delete-point (annotation-start annot))
              (delete-point (annotation-end annot)))
            (collect annot))))

(define-class-method #.+argot/getAnnotations+ annotation-host
    (params GetAnnotationsParams)
  "Handle the argot/getAnnotations request."
  (let* ((text-document (slot-value params 'textDocument))
         (uri (slot-value text-document '|uri|))
         (buffer (get-buffer-from-uri uri))
         (range
          (and (slot-boundp params '|range|)
               (slot-value params '|range|)))
         (start-point
          (if range
              (position->point buffer (slot-value range '|start|))
              (buffer-start-point buffer)))
         (end-point
          (if range
              (position->point buffer (slot-value range '|end|))
              (buffer-end-point buffer)))
         (annotations (annotations-between-points start-point end-point))
         (lsp-annotations
          (mapcar {annotation->lsp-annotation text-document}
                  (remove-if #'annotation-dead?
                             annotations))))
    (mapcar {convert 'hash-table}
            lsp-annotations)))

(defmethod handle :after ((host annotation-host)
                          (method (eql :|textDocument/didChange|))
                          (params DidChangeTextDocumentParams))
  "Garbage-collect buffer annotations after a change."
  (ematch params
    ((DidChangeTextDocumentParams
      :textDocument (VersionedTextDocumentIdentifier
                     :uri uri))
     (synchronized (host)
       (when-let (buffer (get-buffer-from-uri uri))
         (setf (buffer-annotations buffer)
               (gc-annotations (buffer-annotations buffer))))))))

(-> dissect-lsp-annotation (Annotation)
    (values string
            (or Position null)
            (or Position null)
            list
            &optional))
(defun dissect-lsp-annotation (annotation)
  "Extract the uri, start, end, and list of contributions from the
external representation of an annotation."
  (if (slot-boundp annotation '|range|)
      (ematch annotation
        ((Annotation :range (Range :start start :end end)
                     :textDocument (TextDocumentIdentifier :uri uri)
                     :contributions contributions)
         (values uri start end contributions)))
      (ematch annotation
        ((Annotation :contributions contributions
                     :textDocument (TextDocumentIdentifier :uri uri))
         (values uri nil nil contributions)))))

(-> annotation->lsp-annotation (TextDocumentIdentifier annotation)
    Annotation)
(defun annotation->lsp-annotation (text-document annotation)
  "Translate from the internal to the external representation of an
annotation."
  (let* ((start (annotation-start annotation))
         (end (annotation-end annotation))
         (contribs (annotation-contributions annotation))
         (range (make 'Range
                      :start (point-to-position start)
                      :end (point-to-position end))))
    (values
     (make 'Annotation
           :textDocument text-document
           :range range
           :contributions contribs))))

(-> ensure-annotation (point point) annotation)
(defun ensure-annotation (start end)
  "Get the annotation that spans START and END, or create one if it does not already exist."
  (assert (eql (point-buffer start) (point-buffer end)))
  (nest
   (assure annotation)
   (synchronized (*server*))
   (let* ((buffer (point-buffer start))))
   (or (iter (for a in (buffer-annotations buffer))
             (when (and (point= start (annotation-start a))
                        (point= end (annotation-end a)))
               (return a)))
       (let ((a (make-annotation :start start :end end)))
         (push a (buffer-annotations buffer))
         a))))

(defun buffer-annotations (buffer)
  "Return the annotations associated with BUFFER."
  (with-slots (annotations) *server*
    (let ((uri (buffer-uri buffer)))
      (gethash uri annotations))))

(defun (setf buffer-annotations) (value buffer)
  "Replace the annotations for BUFFER with VALUE."
  (declare (list value))
  (with-slots (annotations) *server*
    (let ((uri (buffer-uri buffer)))
      (setf (gethash uri annotations) value))))

(-> annotations-between-points (point point) list)
(defun annotations-between-points (start end)
  "Return annotations falling between START and END."
  (assert (eql (point-buffer start) (point-buffer end)))
  (iter (for annot in (buffer-annotations (point-buffer start)))
        (for annot-start = (annotation-start annot))
        (for annot-end = (annotation-end annot))
        (when (and (point<= start annot-start)
                   (point<= annot-end end))
          (collect annot))))

(-> annotations-between-positions (string Position Position) list)
(defun annotations-between-positions (uri start end)
  "Return annotations falling between two LSP positions."
  (let ((buffer (get-buffer-from-uri uri)))
    (annotations-between-points
     (position->point buffer start :temporary)
     (position->point buffer end :temporary))))

(-> annotations-at-point (point) list)
(defun annotations-at-point (point)
  "Return annotations enclosing POINT."
  (iter (for annot in (buffer-annotations (point-buffer point)))
        (for start = (annotation-start annot))
        (for end = (annotation-end annot))
        (when (and (point<= start point)
                   (point<= point end))
          (collect annot))))

(-> annotations-at-position (string Position) list)
(defun annotations-at-position (uri position)
  "Return annotation enclosing POSITION in URI."
  (when-let ((buffer (get-buffer-from-uri uri)))
    (annotations-at-point
     (position->point buffer position :temporary))))

(defun best-type-at (uri position)
  "Get the best guess for a type at POSITION in URI.
This can be either an explictly stored type name, or the best
candidate from a list of candidates."
  (let* ((annots (annotations-at-position uri position))
         (contribs (mappend #'annotation-contributions annots)))
    (iter (for contrib in contribs)
          (match contrib
            ((Contribution
              :kind (eql ArgotKind.Types)
              :data (TypeCandidate :type-name (and name (type string))))
             (return name))
            ((Contribution
              :kind (eql ArgotKind.Types)
              :data (|FunctionReturnType|
                     :function-name (and fname (type string))
                     :returns (and returns (type string))))
             (return (fmt "~a() -> ~a" fname returns)))
            ;; NB This should come last.
            ((Contribution
              :kind (eql ArgotKind.Types)
              :data (|TypeCandidates| :candidates candidates))
             (appending candidates into candidate-list)))
          (finally
           (when candidate-list
             (return
               (iter (for candidate in candidate-list)
                     (for name = (slot-value candidate '|typeName|))
                     (for probability = (slot-value candidate '|probability|))
                     (finding name maximizing probability))))))))

(defun handle-hover (params)
  "Handle and respond to a hover request."
  (ematch params
    ((HoverParams
      :textDocument (TextDocumentIdentifier :uri uri)
      :position pos)
     (when-let (best (best-type-at uri pos))
       (make 'Hover :contents (list best))))))

(define-class-method "textDocument/hover" annotation-host
    (params HoverParams)
  ;; TODO Conversion here shouldn't be necessary.
  (when-let (help (handle-hover params))
    (convert 'hash-table help)))

(defun get-annotations (host uri &rest args &key &allow-other-keys)
  "Return stored annotations for URI, as a list of LSP Annotations."
  (nest
   (mapcar {convert 'Annotation})
   (jsonrpc:response-result)
   (dispatch host)
   (make-request :method +argot/getAnnotations+
                 :id "912091"
                 :params)
   (convert 'hash-table)
   (apply #'make 'GetAnnotationsParams
          :textDocument (make 'TextDocumentIdentifier
                              :uri uri)
          args)))

(defun did-annotate (host annotations)
  "Send the Argot Server a didAnnotate notification with ANNOTATIONS."
  (nest
   (dispatch (muse-server host))
   (make-request :method +argot/didAnnotate+
                 :params)
   (convert 'hash-table)
   (make 'DidAnnotateParams
         :annotations annotations)))

(defun has-type-contributions? (server uri)
  "Are there any type contributions for URI?"
  (iter outer
        (for a in (get-annotations server uri))
        (iter (for c in (slot-value a '|contributions|))
              (in outer
                  (thereis (eql (slot-value c '|kind|)
                                ArgotKind.Types))))))

(defun contributions-by-kind (server uri kind)
  "Find all test contributions for the current URI."
  (loop for annotation in (get-annotations server uri)
        append (loop for contribution
                       in (slot-value annotation '|contributions|)
                     when (eql (slot-value contribution '|kind|) kind)
                       collect contribution)))

(defun has-contributions-by-kind-p (server uri kind)
  "Find all test contributions for the current URI."
  (iter (for annotation in (get-annotations server uri))
        (thereis
         (iter (for contribution in (slot-value annotation '|contributions|))
               (thereis (eql (slot-value contribution '|kind|) kind))))))

(defun contributions-by-source (server uri source)
  "Get all contributions for the current URI from SOURCE."
  (iter outer
        (for annotation in (get-annotations server uri))
        (iter (for contribution in (slot-value annotation '|contributions|))
              (when (equal (slot-value contribution '|source|)
                           source)
                (in outer (collect contribution))))))

(defun has-contributions-by-source-p (server uri source)
  "Return T if there are contributions for the current URI from SOURCE."
  (iter outer
        (for annotation in (get-annotations server uri))
        (iter (for contribution in (slot-value annotation '|contributions|))
              (in outer
                  (thereis (equal (slot-value contribution '|source|)
                                  source))))))

(defmethod contributions ((a Annotation))
  (slot-value a '|contributions|))

(defmethod contribution-source ((c Contribution))
  (slot-value c '|source|))

(defmethod contribution-kind ((c Contribution))
  (slot-value c '|kind|))

(defmethod contribution-key ((c Contribution))
  (slot-value c '|key|))

(defun dump-string-annotations (software annotations)
  "For debugging and testing, return an alist where the values are
ANNOTATIONS and the keys are the substrings of SOFTWARE that the
annotations apply to."
  (let ((text (source-text (genome software))))
    (iter (for a in annotations)
          (for r = (slot-value a '|range|))
          (for sr = (convert 'source-range r))
          (collect (cons (source-range-subseq text sr)
                         a)))))

(defun dump-string-annotations-for (uri)
  (dump-string-annotations
   (uri-buffer-software uri)
   (get-annotations *server* uri)))
