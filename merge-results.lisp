(defpackage :argot-server/merge-results
  (:use :gt/full
        :lsp-server/lsp-server
        :lsp-server/protocol
        :lsp-server/logger
        :lsp-server/utilities/lsp-utilities
        :argot-server/readtable
        :argot-server/utility
        :jsonrpc)
  (:export :merge-results
           :merge-server-capabilities))
(in-package :argot-server/merge-results)
(in-readtable argot-readtable)

(defun falsy? (x)
  (or (null x) (eql x 'yason:false)))

(defun truthy? (x)
  (not (falsy? x)))

(defparameter *forbidden-keys*
  '("documentFormattingProvider"
    "documentRangeFormattingProvider"
    "documentOnTypeFormattingProvider")
  "Keys to remove from responses.")

(defgeneric merge-results (result1 result2)
  (:documentation "Merge two sets of results.

If the results are incompatible (e.g. RESULT1 is an object and RESULT2
is a list) then always return RESULT1.

Objects are merged by merging their keys.

Lists are merged by merging their elements and deduplicating. Note
that results may be removed if they are shadowed by other results.")
  (:method-combination standard/context)
  (:method :context (result1 result2)
    (let ((result (call-next-method)))
      (if (hash-table-p result)
          (dolist (key *forbidden-keys* result)
            (remhash key result))
          result)))
  (:method ((result1 response) (result2 response))
    (nest
     (let ((id1 (response-id result1))
           (id2 (response-id result2))))
     (make-response :id (or id1 id2)
                    :result)
     (merge-results
      (response-result result1)
      (response-result result2))))
  (:method ((result1 response) (result2 t))
    (let* ((id (response-id result1))
           (error (response-error result1))
           (result (merge-results (response-result result1) result2)))
      ;; A response cannot have both a result and an error.
      (if result
          (make-response :id id :result result)
          (make-response :id id :error error))))
  ;; Special handling for yason:false in the second argument. Saves a
  ;; lot of methods.
  (:method :around ((result1 t) (result2 (eql 'yason:false)))
    result1)
  (:method :around ((result1 (eql 'yason:false)) result2) result2)
  (:method ((result1 (eql 'yason:true)) (result2 (eql t)))
    'yason:true)
  (:method ((result1 (eql t)) (result2 (eql 'yason:true)))
    'yason:true)
  ;; Handle merging protocol objects.
  (:method ((result1 protocol) (result2 protocol))
    "Merge two protocol objects. Return a protocol object when
possible."
    (let ((result
           (merge-results (convert 'hash-table result1)
                          (convert 'hash-table result2))))
      (if (type= (type-of result1) (type-of result2))
          (convert (type-of result1) result)
          result)))
  (:method ((result1 protocol) result2)
    (merge-results (convert 'hash-table result1) result2))
  (:method (result1 (result2 protocol))
    (merge-results result1 (convert 'hash-table result2)))
  (:method ((result1 number) (result2 number))
    result1)
  (:method ((result1 t) (result2 t))
    (if (equal? result1 result2) result1
        (call-next-method)))
  ;; NB We want the keys in table1 to overwrite table2 (should they ,clash).
  (:method ((table1 hash-table) (table2 hash-table))
    ;; TODO This should be a utility. Cf. Clojure merge-with.
    (let ((table (copy-hash-table table2)))
      (do-hash-table (k v1 table1 table)
        (multiple-value-bind (v2 v2?)
            (href table k)
          (if (not v2?)
              (setf (href table k) v1)
              (setf (href table k)
                    (string-case k
                      ("textDocumentSync"
                       (merge-sync-options v1 v2))
                      ;; If any of the contributing lists is
                      ;; incomplete the result is too.
                      ("isIncomplete"
                       (if (or (truthy? v1)
                               (truthy? v2))
                           'yason:true
                           'yason:false))
                      (t (merge-results v1 v2)))))))))
  (:method ((result1 hash-table) (result2 list))
    (match result1
      ((dict "items" (list* item1 items))
       (dict* (copy-hash-table result1)
              "items" (merge-results (cons item1 items)
                                     result2)))
      ((dict "contents" (and contents (type (not null))))
       (dict* (copy-hash-table result1)
              "contents" (append contents result2)))
      (otherwise result2)))
  ;; Merging hash tables and booleans is something that has to be
  ;; supported for ServerCapabilities, where many of the slots can be
  ;; interfaces or booleans and we need to be able to merge them
  ;; accordingly. However since we already handle null we only need to
  ;; handle `t` and `yason:true` here.
  (:method ((result1 (eql t)) (result2 hash-table))
    result2)
  (:method ((result1 (eql 'yason:true)) (result2 hash-table))
    result2)
  (:method ((result1 hash-table) (result2 (eql t)))
    result1)
  (:method ((result1 hash-table) (result2 (eql 'yason:true)))
    result1)
  (:method ((result1 list) (result2 hash-table))
    (merge-results result2 result1))
  (:method ((result1 hash-table) (result2 null))
    result1)
  (:method ((result1 list) (result2 list))
    (let* ((list (append result1 result2))
           (set '()))
      (dolist (item list)
        (unless (some {result-shadows? _ item} set)
          (setf set (delete-if {result-shadows? item} set))
          (push item set)))
      (sort set (ordering list))))
  (:method ((result1 t) (result2 null))
    result1)
  (:method ((result1 null) (result2 t))
    result2)
  (:method ((result1 sequence) result2)
    (if (emptyp result1) result2 (call-next-method)))
  (:method (result1 (result2 sequence))
    (if (emptyp result2) result1 (call-next-method))))

(defun merge-sync-options (v1 v2)
  "Handling merging sync options.
Sync options are merged pessimistically, so that if one server
supports incremental updates, but the other server only supports full
updates, the combined server only supports full updates.

Note that per LSP if no sync option is provided it defaults to 0, not
to 1, in which case nothing will be synced."
  (dispatch-case ((v1 (or number null hash-table))
                  (v2 (or number null hash-table)))
    ((number number) (min v1 v2))
    ((number null) (min v1 0))
    ((hash-table null) (merge-sync-options v1 0))
    ((null number) (min 0 v2))
    ((null hash-table) (merge-sync-options 0 v2))
    ((number hash-table)
     (merge-sync-options
      (dict* (copy-hash-table v2)
             "change" v1)
      v2))
    ((hash-table number)
     (merge-sync-options v2 v1))
    ((hash-table hash-table)
     (dict* (merge-results v1 v2)
            "change"
            (min (gethash "change" v1 0)
                 (gethash "change" v2 0))))
    ((null null) 0)))

(defgeneric result-shadows? (x y)
  (:documentation "Test whether X shadows Y (for deduplication).

As a special case, hash table X shadows hash table Y if the data in Y
is a subset of the data in X. This is to allow \"richer\" results from
one server to shadow less-interesting results from another.")
  (:method (x y) (equal x y))
  (:method ((x list) (y list))
    (and (length= x y)
         (every #'result-shadows? x y)))
  (:method ((x hash-table) (y hash-table))
    (and (eql (hash-table-test x) (hash-table-test y))
         (>= (hash-table-count x) (hash-table-count y))
         (iterate (for (k v1) in-hashtable y)
                  (always
                   (multiple-value-bind (v2 v2?) (href x k)
                     (always (and v2? (result-shadows? v1 v2)))))))))

(-> merge-server-capabilities
    ((soft-list-of (or null hash-table ServerCapabilities)))
    InitializeResult)
(defun merge-server-capabilities (caps)
  (make 'InitializeResult
        :capabilities
        (convert 'ServerCapabilities
                 (reduce #'merge-results caps))))
