FROM ubuntu:20.04 AS snippet-mining-source

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    python-is-python3 python3-pip git git-lfs \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /
RUN git clone --recurse-submodules \
    https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/snippet-mining.git \
    && rm -rf snippet-mining/.git
WORKDIR /snippet-mining
RUN pip3 install --no-cache-dir setuptools
RUN python3 setup.py bdist_wheel --universal

FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive
COPY --from=snippet-mining-source /snippet-mining/ /snippet-mining/

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    python-is-python3 python3-pip python3-venv git \
    && rm -rf /var/lib/apt/lists/*

RUN pip install /snippet-mining/dist/snippet_mining-0.0.0-py2.py3-none-any.whl
RUN rm -rf /snippet-mining

COPY bin/fork-server /usr/local/bin/
ENV PORT=7070
EXPOSE $PORT
CMD fork-server $PORT snippet-mining-server --stdio 2>&1
