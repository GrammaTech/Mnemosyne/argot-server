FROM ubuntu:20.04 as typerefinement-source

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get -y update && \
    apt-get install -y --no-install-recommends \
    git curl python3-pip \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /
RUN git clone --recurse-submodules --depth=1 \
    https://gitlab.com/GrammaTech/Mnemosyne/muses/type-refinement.git \
    && rm -rf type-refinement/.git

FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive
# NB We need git here to install kitchensink.
RUN apt-get update \
    && apt-get install -y --no-install-recommends python3-pip \
    git ca-certificates \
    && rm -rf /var/lib/apt/lists/*
COPY --from=typerefinement-source /type-refinement /typerefinement
WORKDIR /typerefinement
RUN pip3 --no-cache-dir install -r requirements.txt

ENV PORT=4500
EXPOSE $PORT
CMD python3 run_type_refinement.py
