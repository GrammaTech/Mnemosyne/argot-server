(defpackage :argot-server/choices
  (:use :gt/full
        :argot-server/passthrough
        :argot-server/muses/muse
        :lsp-server
        :lsp-server/protocol
        :lsp-server/utilities/lsp-utilities
        :argot
        :argot-server/readtable
        :argot-server/utility
        :jsonrpc)
  (:import-from :jsonrpc/connection
                :*connection*
                :connection)
  (:import-from :jsonrpc/errors
                :jsonrpc-callback-error)
  (:import-from :jsonrpc/class
                :server-client-connections)
  (:local-nicknames (:lp :lparallel))
  (:export
    :with-lsp-restarts
    :present-choice-for
    :present-choice
    :choice
    :open-choice
    :read-string
    :present-choices
    :yes-or-no?
    :present-choice-from-alist))
(in-package :argot-server/choices)
(in-readtable argot-readtable)

(defclass choice ()
  ((choices :initarg :choices :type list)
   (type :initarg :type :type MessageType)
   (prompt :initarg :prompt :type string)
   ;; Required in case the response is interrupted.
   (default-text :initarg :default-text :type string :initform ""))
  (:default-initargs
   :type MessageType.Info
   :prompt "Choose: "))

(defmethod initialize-instance :after ((self choice) &key)
  (with-slots (choices default-text) self
    (unless (emptyp default-text)
      (pushnew default-text choices :test #'equal))))

(defclass open-choice (choice)
  ((multi-line :initarg :multi-line :type boolean))
  (:documentation "Like `choice', but also allow the user to enter something directly.")
  (:default-initargs
   :multi-line nil))

(defun sort-choices (choices default-text)
  (declare ((soft-list-of string) choices)
           (string default-text))
  (if (emptyp default-text) choices
      (cons default-text
            (remove default-text choices :test #'equal))))

(defgeneric make-choice-params (choice &key actions)
  (:method ((choice choice) &key actions)
    (with-slots (type prompt) choice
      (make 'ShowMessageRequestParams
            :type type
            :message prompt
            :actions actions)))
  (:method ((choice open-choice) &key actions)
    (with-slots (type prompt default-text multi-line) choice
      (make 'ArgotShowPromptParams
            :type type
            :message prompt
            :defaultValue (or default-text "")
            :multiLine multi-line
            :actions actions))))

(defgeneric choice-endpoint (choice)
  (:method ((choice choice))
    "window/showMessageRequest")
  (:method ((choice open-choice))
    +argot/window/showPrompt+))

(defgeneric present-choice (server connection choice)
  (:documentation "Present CHOICES to the user, by way of SERVER and
  CONNECTION, one of SERVER's client connections.")
  (:method ((muse muse) conn choice)
    (present-choice (muse-server muse) conn choice))
  (:method ((server server) (conn null) choice)
    (present-choice server
                    (current-connection)
                    choice))
  (:method :before ((server server)
                    (c connection)
                    (choice open-choice))
    (check-argot-support +argot/window/showPrompt+))
  (:method ((server server)
            (c connection)
            (choice choice))
    (let ((*connection* c)
          (params
           (nest
            (with-slots (choices type prompt default-text) choice)
            (convert 'hash-table)
            (make-choice-params choice :actions)
            (iter (for choice in (sort-choices choices default-text))
                  (for string = (if (stringp choice)
                                    choice
                                    (princ-to-string choice)))
                  (collect (make 'MessageActionItem
                                 :title string)))))
          (promise (lp:promise)))
      ;; TODO This is redundant with jsonrpc:call.
      (call-async-to server c
                     (choice-endpoint choice)
                     params
                     (lambda (result)
                       (lp:fulfill promise (cons :result result)))
                     (lambda (message code)
                       (let ((e (make-condition
                                 'jsonrpc-callback-error
                                 :message message
                                 :code code)))
                         (lp:fulfill promise (cons :error e)))))
      (ematch (lp:force promise)
        ((cons :error e) (error e))
        ((cons :result (dict "title" (and title (type string))))
         title)
        ;; Interrupted before user could type anything.
        ((or (cons :result (type hash-table))
             (list :result))
         (slot-value choice 'default-text))))))

(defun present-choice-for (uri choices &rest args
                           &key (class 'choice)
                             (connection
                              (or (ignore-errors
                                   (current-connection))
                                  (document-connection uri)))
                           &allow-other-keys)
  "Present a choice to the user, using the appropriate connection for URI."
  (present-choice (muse-server *server*)
                  connection
                  (apply #'make class
                         :choices choices
                         (remove-from-plist args :class :connection))))

(defun read-string (&rest args &key uri &allow-other-keys)
  "Read a string from the editor."
  (present-choice (muse-server *server*)
                  (if uri (document-connection uri)
                      (current-connection))
                  (apply #'make 'open-choice
                         :choices nil
                         :allow-other-keys t
                         args)))

(defun present-choices (choices &rest args &key open uri &allow-other-keys)
  "Present choices to the editor.
If the choice is intended to be open (a text box with completions),
specify `:open t'."
  (present-choice (muse-server *server*)
                  (if uri (document-connection uri)
                      (current-connection))
                  (apply #'make (if open 'open-choice 'choice)
                         :choices choices
                         :allow-other-keys t
                         args)))

(defun yes-or-no? (prompt &key (yes "Yes") (no "No") (default no))
  "Ask a yes-or-no question and return T for an affirmative answer.
Defaults to negative if the user did not answer."
  (let ((choice
         (present-choices (list no yes)
                          :prompt prompt
                          :default-text default)))
    (selector choice equal
              (yes t)
              (no nil)
              (t (error "Not a yes or no answer: ~a" choice)))))

(defun present-choice-from-alist (choice-alist &rest args
                                  &key (test #'equal)
                                    default-text
                                  &allow-other-keys)
  (when default-text
    (assert (assoc default-text choice-alist :test test)))
  (when-let ((choice
              (apply #'present-choices (mapcar #'car choice-alist)
                     (remove-from-plist args :test))))
    (assocdr choice choice-alist :test test)))

(defun call/lsp-restarts (uri fn)
  "Helper function for `with-lsp-restarts'."
  (if (null uri) (funcall fn)
      (handler-bind
          ((serious-condition
            (lambda (c)
              (and-let* ((restarts (compute-restarts))
                         (strings
                          (iter (for restart in restarts)
                                (for i from 1)
                                (collect (fmt "~a ~a" i restart))))
                         (choice
                          (present-choice-for
                           uri strings
                           :type MessageType.Error
                           :prompt (fmt "Error: ~a" c)))
                         (restart
                          (iter (for restart in restarts)
                                (for string in strings)
                                (finding restart such-that
                                         (equal string choice)))))
                ;; Using `invoke-restart-interactively' is necessary
                ;; to trigger the request to the editor.
                (invoke-restart-interactively restart)))))
        (funcall fn))))

(defmacro with-lsp-restarts ((uri) &body body)
  "Call BODY in such a way that, if an error occurs, restarts are
presented for selection via LSP."
  (with-thunk (body)
    `(call/lsp-restarts ,uri ,body)))

(defclass lsp-restart-mixin ()
  ()
  (:documentation "Mixing for selecting restarts via LSP."))

(defmethod handle :around ((s lsp-restart-mixin) method (params hash-table))
  (let ((uri
         (or (ignore-errors (href params "uri"))
             (ignore-errors (href params "textDocument" "uri")))))
    (with-lsp-restarts (uri)
      (call-next-method))))
