(defpackage :argot-server/config
  (:documentation "Define configuration variables")
  (:use :gt/full)
  (:export
    :*allow-local*
    :network
    :*network*))
(in-package :argot-server/config)

(deftype network ()
  '(member :bridge :host))

(declaim (type network *network*))

(defvar *network*
  (assure network
    (let ((env-network (getenv "ARGOT_SERVER_NETWORK")))
      (if (emptyp env-network) :bridge
          (make-keyword (string-upcase env-network))))))

(defvar *allow-local* t
  "Whether to allow projects to be loaded from the file system.")
