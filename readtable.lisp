(defpackage :argot-server/readtable
  (:documentation "A case-inverting readtable with support for curry-compose-reader-macros.")
  (:use :gt/full :argot/readtable)
  ;; Just re-export argot/readtable:argot-readtable.
  (:export :argot-readtable))
(in-package :argot-server/readtable)
