(defpackage :argot-server/progress-reporting
  (:use :gt/full
        :argot-server/muses/muse
        :lsp-server/protocol
        :jsonrpc
        :argot-server/readtable
        :lsp-server)
  (:import-from :argot-server :*server*)
  (:export :begin-work :report-work :end-work
           :with-progress-reporting))
(in-package :argot-server/progress-reporting)
(in-readtable argot-readtable)

(defun client-supports-progress? ()
  "Does the client have the workDoneProgress capability?"
  (match (convert 'InitializeParams (initialize-params*))
    ((InitializeParams :capabilities caps)
     (and (slot-boundp caps '|window|)
          (let ((window-caps (slot-value caps '|window|)))
            (and (slot-boundp window-caps 'workDoneProgress)
                 (slot-value window-caps 'workDoneProgress)))))))

(defun supports-progress? (params)
  (match (convert 'InitializeParams params)
    ((InitializeParams :capabilities caps)
     (and (slot-boundp caps '|window|)
          (let ((window-caps (slot-value caps '|window|)))
            (and (slot-boundp window-caps 'workDoneProgress)
                 (slot-value window-caps 'workDoneProgress)))))))

(defgeneric progress-token (params)
  (:documentation "Extract a progress token from PARAMS.")
  (:method ((params string)) params)
  (:method ((params null)) params)
  (:method ((params protocol)) nil)
  (:method ((params WorkDoneProgressParams))
    (and (slot-boundp params 'workDoneToken)
         (slot-value params 'workDoneToken)))
  (:method ((params hash-table))
    (gethash "workDoneToken" params)))

(defgeneric progress (server token payload)
  (:documentation "Send PAYLOAD to SERVER for TOKEN.")
  (:method ((muse muse) token payload)
    (progress (muse-server muse) token payload))
  (:method ((server t) token payload)
    (jsonrpc:notify
     server "$/progress"
     (dict "token" (progress-token token)
           "value" (convert 'hash-table payload)))))

(defun begin-work (protocol &rest args
                   &key (server *server*)
                   &allow-other-keys)
  "Send a WorkDoneProgressBegin notification."
  (setf args (remove-from-plist args :server))
  (when (client-supports-progress?)
    (progress server protocol (apply #'make 'WorkDoneProgressBegin args))))

(defun report-work (protocol &rest args
                    &key (server *server*)
                    &allow-other-keys)
  "Send a WorkDoneProgressReport notification."
  (setf args (remove-from-plist args :server))
  (when (client-supports-progress?)
    (progress server protocol (apply #'make 'WorkDoneProgressReport args))))

(defun end-work (protocol &rest args
                 &key (server *server*)
                 &allow-other-keys)
  "Send a WorkDoneProgressEnd notification."
  (setf args (remove-from-plist args :server))
  (when (client-supports-progress?)
    (progress server protocol (apply #'make 'WorkDoneProgressEnd args))))

(defun call/work-done-progress (fn server protocol
                                &rest init-args
                                &key
                                &allow-other-keys)
  "Helper function for `with-progress-reporting'."
  (if-let (token (progress-token protocol))
          (unwind-protect
               (progn
                 (apply #'begin-work token
                        :server server
                        init-args)
                 (multiple-value-prog1
                     (funcall fn
                              (lambda (&rest report-args)
                                (apply #'report-work
                                       protocol
                                       :server server
                                       report-args)))
                   (apply #'end-work token
                          :allow-other-keys t
                          :server server
                          init-args))))
          (funcall fn (constantly nil))))

(defmacro with-progress-reporting ((fn protocol &rest init-args
                                    &key (server '*server*)
                                    &allow-other-keys)
                                   &body body)
  "Call BODY, binding FN to a function that calls `report-work' with the appropriate progress token. FN takes the same arguments as `report-work', which are the same arguments specified for WorkDoneProgressReport by the LSP protocol: `:cancellable`, `:message`, and `:percentage`.

The progress token is extracted from PROTOCOL, a protocol object. If
it does not have a progress token, then progress is not reported (FN
is bound no a no-op).

Automatically calls `begin-work' on entry and `end-work' on exit, passing INIT-ARGS to both."
  (with-unique-names (body-thunk)
    `(flet ((,body-thunk (,fn)
              (fbind ((,fn ,fn))
                ,@body)))
       (declare (dynamic-extent #',body-thunk))
       (call/work-done-progress #',body-thunk ,server ,protocol
                                ,@init-args))))
